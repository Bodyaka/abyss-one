In short, this is my pet project: rendering engine for learning purposes (a very raw engine I have to admit), which is largely influenced by OGRE API. 
It is using: SFML, SFGUI, ASSIMP, OpenGL (3+), GLEW.

Long version:
Like almost all students, I wanted to make some games. And I wanted to know how many things are implemented. So, I decided to write my own engine (douh!)
But now, after several years, I think, it was a bad idea. Sure, I understand many things, learned some CG effects and rendering engine API, but was it worth it? Wouldn't be better to work on some startup or just learn other programming languages, like Rust or Scala?

Anyway, writing own engine is a hell of a job. I rewrote it almost completely one or 2 times (discarded many shaders and effects), forgot to implemented some basic stuff (like correct resizing of window) and spent too much time on thinking how should architecture looks or which GUI to choose.

Many times I thought that I gave up and started doing smth else, but WDYT? I started to writing some new functionality again. 

Anyway here is some short list what I have implemented:

* simple tree-like scene graph; 
* resources managers (textures, shaders, etc.);
* integration of ASSIMP loader; 
* Phong shading; 
* bump mapping; 
* deferred rendering; 
* SSAO; 
* simple GUI via SFGUI.


But so many things I wanted to add... Many shader effects like cube mapping, Gaussian blur, motion blur, bloom, len's flare, OIT, etc. (many of these effects were implemented in previous versions of engine, I just was too lazy to add them to new version); different optimization techniques, like frustum culling, portals, BSP or octree-based scene graph;  learn compute and tessellation shaders. 

So many interesting stuff to do, so, I think this project will be only hobby project. And I don't think that will be support it in future. But, who knows. :)