1) Abyss must be rewrited that it contains not Container of pointers to objects, but container of objects itself.
2) Abyss::Mesh - it must be inherited from Abyss::Deus<MeshPtr>, not Abyss::Deus<Mesh*>, or smth like that.
3) Need rewrite interface of Abyss::MeshLoader in such manner, so, it will return array of meshes, not single mesh.
4) Abyss::Mesh must have input not GLenum, but some enum, that indicates primitive type.
5) AssimpLoader must sort meshes by their primitive types.
6) Add Uniform buffer objects to shaders.
7) Move content of AbyssGL into AbyssRendering.h
8) Add possibility to load all types of textures with support of several textures per type.
9) Add emissive component to Material (and in AssimpLoader too).
10) Make assimp loader's method not static (so file path will be as member).
11) Make s_invalidBindingPoint accessible in public section.
