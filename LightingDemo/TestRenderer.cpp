#include "TestRenderer.h"

#include "AbyssRenderer.h"
#include "AbyssSceneObject.h"
#include "AbyssShaderProgram.h"

using namespace Abyss;

static const String sWorldViewProjectionalMatrixName = "WorldViewProjectionalMatrix";

const String TestRenderer::SHADER_PROGRAM_NAME = "Test1";

//-------------------------------------------------------------------------------------------------
TestRenderer::TestRenderer()
	: TSuper(SHADER_PROGRAM_NAME)
{

}
//-------------------------------------------------------------------------------------------------
TestRenderer::~TestRenderer()
{

}
//-------------------------------------------------------------------------------------------------
void TestRenderer::beforeRender(const Abyss::RenderableObject * const object)
{
	SceneObject * sceneObject = object->getBoundSceneObject();
	setWorldViewProjectionalMatrix(sceneObject->getWorldViewProjectionalMatrix());
}
//-------------------------------------------------------------------------------------------------
void TestRenderer::afterRender(const Abyss::RenderableObject * const object)
{

}
//-------------------------------------------------------------------------------------------------
void TestRenderer::setWorldViewProjectionalMatrix(const Abyss::Matrix4D & matrix)
{
	getShaderProgram()->setUniform(sWorldViewProjectionalMatrixName, matrix);
}
//-------------------------------------------------------------------------------------------------