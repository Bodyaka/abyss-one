#pragma once

#pragma once

class LightingPanel;
class LightTypesScene;

namespace Abyss
{
	class Material;
	class Renderer;
}

class LightTypesPanelHandler
{
public:
	LightTypesPanelHandler(LightingPanel* panel, LightTypesScene* scene);
	~LightTypesPanelHandler();

	void updatePanel();

private:

	Abyss::Material* getCurrentObjectMaterial() const;

	LightTypesPanelHandler(const LightTypesPanelHandler& other) = delete;
	LightTypesPanelHandler& operator=(const LightTypesPanelHandler& other) = delete;

	LightTypesScene* m_scene;
	LightingPanel* m_guiPanel;

	size_t m_button1Signal,
		   m_button2Signal,
		   m_shinessSliderChangeSignal,
		   m_diffuseIntensitySliderChangeSignal,
		   m_specularIntensitySliderChangeSignal,
		   m_ssaoEnableCheckBoxSignal;
};