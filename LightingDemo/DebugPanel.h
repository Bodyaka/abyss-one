#pragma once

#include <SFGUI/SFGUI.hpp>
#include "SfguiWidgetDocker.h"

namespace Abyss
{
	class SceneObject;
	class SFGUIManager;
	class FPSCounter;
}

class DebugPanel
	: public sfg::Window
	, public Abyss::SfguiWidgetDocker
{
public:

	typedef std::shared_ptr<DebugPanel> Ptr;

public:

	static Ptr Create(Abyss::SFGUIManager* manager, std::uint8_t style = sfg::Window::Style::TOPLEVEL);

	DebugPanel(Abyss::SFGUIManager* manager, std::uint8_t style);
	~DebugPanel();

	sfg::ToggleButton::Ptr getFPSPanelVisibilityButton() const;

	void updateFPS_Panel(Abyss::FPSCounter* counter);

public: // Overridden method

	const std::string& GetName() const override;

private:

	void createControlButtons();
	void createFPS_StatsPanel();
	void FinishCreation();

private:

	Abyss::SFGUIManager* m_manager;
	sfg::ToggleButton::Ptr m_fpsPanelButton;
	sfg::Box::Ptr m_widgesContainer;
	sfg::Frame::Ptr m_fpsFrame;

	sfg::Label::Ptr m_lastFPSLabel,
					m_averageFPSLabel,
					m_averageFPS2Label,
					m_averageFrameTimeLabel;

	size_t m_fpsVisibilitySignal;
};

// INLINE METHODS:
inline sfg::ToggleButton::Ptr DebugPanel::getFPSPanelVisibilityButton() const
{
	return m_fpsPanelButton;
}
