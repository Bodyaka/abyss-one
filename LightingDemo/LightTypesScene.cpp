#include "LightTypesScene.h"

#include "AbyssTexture.h"
#include "AbyssMeshUtilities.h"
#include "AbyssCamera.h"
#include "AbyssMaterial.h"
#include "AbyssRenderableObject.h"
#include "AbyssLight.h"
#include "AbyssSceneManager.h"
#include "AbyssMeshObjLoader.h"
#include "AbyssMeshRenderable.h"
#include "AbyssTextureManager.h"

#include "AbyssDefaultCameraController.h"
#include "AbyssTrackBallRotationController.h"
#include "AbyssCameraZoomController.h"

#include "SFGUIManager.h"
#include "LightTypesPanelHandler.h"
#include "LightingPanel.h"

#include "AbyssSimpleRenderer.h"
#include "AbyssDeferredRenderer.h"

#include "AbyssScopedTimer.h"

#include <boost/format.hpp>

using namespace Abyss;

//-------------------------------------------------------------------------------------------------
LightTypesScene::LightTypesScene(Application * app)
	: m_application(app)
	, m_currentItem(ItemType::None)
	, m_currentLight(nullptr)
{

}
//-------------------------------------------------------------------------------------------------
LightTypesScene::~LightTypesScene()
{

}
//-------------------------------------------------------------------------------------------------
void LightTypesScene::start(ItemType type)
{
	m_panel = LightingPanel::Create(Abyss::SFGUIManager::getInstance());
	m_debugPanel = DebugPanel::Create(Abyss::SFGUIManager::getInstance());
	
	auto rootWindow = Abyss::SFGUIManager::getInstance()->getRootWindow();
	rootWindow->Put(m_debugPanel, sf::Vector2f(0.f, 0.f));
	rootWindow->Put(m_panel, sf::Vector2f(0.f, 0.f));

	m_debugPanel->dock(rootWindow.get(), SfguiWidgetDocker::CornerType::TopLeft);
	m_panel->dock(rootWindow.get(), SfguiWidgetDocker::CornerType::TopRight);

	auto fpsCounter = Abyss::ApplicationContext::getSharedApplicationContext()->getFPSCounter();
	fpsCounter->addListener(this);
	
	m_panelHandler.reset(new LightTypesPanelHandler(m_panel.get(), this));

	SceneManager* sceneManager = m_application->getSceneManager();
	auto light = sceneManager->createLight();
	light->setType(LightType::DirectionalLight);
	light->setAttenutation(1.f, 0.8f, 0.5f);
	light->setDirection(Vector3D(0.f, 0.f, -1.f));

	m_currentLight = light;

	setCurrentItem(type);

	DeferredRenderer::getInstance();
}
//-------------------------------------------------------------------------------------------------
Abyss::SceneObjectPtr LightTypesScene::createCat()
{
	Abyss::ScopedTimer<> t("Cat's scene object creation time:");

	Abyss::MeshPtr catMesh = createCatMesh();
	Abyss::RenderableObjectPtr catRenderable = createCatRenderableObject(catMesh);
	Abyss::SceneObjectPtr catScene = createCatSceneObject(catRenderable);
	return catScene;
}
//-------------------------------------------------------------------------------------------------
Abyss::MeshPtr LightTypesScene::createCatMesh()
{
	return Abyss::MeshLoader::getDefaultLoader()->load("D:\\Development\\Abyss\\Debug\\meshes\\cat\\cat.obj");
}
//-------------------------------------------------------------------------------------------------
Abyss::RenderableObjectPtr LightTypesScene::createCatRenderableObject(Abyss::MeshPtr mesh)
{
	return MeshRenderable::create("PhongShadingCatRenderableObject", mesh);
}
//-------------------------------------------------------------------------------------------------
Abyss::SceneObjectPtr LightTypesScene::createCatSceneObject(Abyss::RenderableObjectPtr renderable)
{
	SceneManager * sceneManager = m_application->getSceneManager();
	SceneObjectPtr sceneObject = sceneManager->createSceneObject("PhongShadingCatSceneObject");
	sceneObject->addRenderableObject(renderable);
	//sceneObject->move(Vector3D(0, -0.5, 7));

	return sceneObject;
}
//-------------------------------------------------------------------------------------------------
Abyss::SceneObjectPtr LightTypesScene::createBook()
{
	// Book model are from http://www.turbosquid.com/FullPreview/Index.cfm/ID/252756 site.
	auto mesh = Abyss::MeshLoader::getDefaultLoader()->load("D:\\Development\\Abyss\\Debug\\meshes\\book\\fei_book.3ds");
	auto renderable = MeshRenderable::create("PhongShadingBookRenderableObject", mesh);
	auto material = renderable->getMaterial();
	material->setShiness(100);
	auto sceneObject = m_application->getSceneManager()->createSceneObject("PhongShadingBookSceneObject");
	sceneObject->addRenderableObject(renderable);
	sceneObject->move(Vector3D(0, 0, -100));
	sceneObject->pitch(90);
	return sceneObject;
}
//-------------------------------------------------------------------------------------------------
Abyss::SceneObjectPtr LightTypesScene::createTestRect()
{
	Abyss::ScopedTimer<> t("TestRect's creation time:");

	auto mesh = Abyss::MeshUtilities::createRectMesh(5.f);

	MaterialPtr material = Material::create(Vector3D(0.2, 0.2, 0.2),
		Vector4D(0.7, 0.7, 0.7, 1),
		Vector4D(1, 1, 1, 1),
		500);

	//String path = "textures/brick_1/diffuse.png";
	std::string path = "D:/Development/Abyss/Debug/textures/brick/diffuse.jpg";
	TexturePtr texture = TextureManager::getInstance()->createTexture(path);
	texture->setFiltering(Abyss::Texture::LinearFiltering);
	material->setTexture(Abyss::DiffuseTextureType, texture);


	std::string normalMapPath = "D:/Development/Abyss/Debug/textures/brick/normal.jpg";
	TexturePtr normapMapTexture = TextureManager::getInstance()->createTexture(normalMapPath);

	material->setTexture(Abyss::NormalMapTextureType, normapMapTexture);
	normapMapTexture->setFiltering(Abyss::Texture::LinearFiltering);
	mesh->setMaterial(material);

	auto renderable = MeshRenderable::create("TestRectRenderableObject", mesh);
	auto sceneObject = m_application->getSceneManager()->createSceneObject("TestRectSceneObject");
	sceneObject->addRenderableObject(renderable);

	return sceneObject;

}
//-------------------------------------------------------------------------------------------------
Abyss::SceneObjectPtr LightTypesScene::createHellKnight()
{
	Abyss::ScopedTimer<> t("HellKnight's creation time:");

	static int index = 0;
	auto mesh = Abyss::MeshLoader::getDefaultLoader()->load("D:\\Development\\Abyss\\Debug\\meshes\\hell_knight\\hellknight.md5mesh");
	//auto mesh = Abyss::MeshLoader::getDefaultLoader()->load("D:\\Development\\Abyss\\Debug\\meshes\\conference\\conference.obj");
	//auto mesh = Abyss::MeshLoader::getDefaultLoader()->load("D:\\Development\\Abyss\\Debug\\meshes\\crytek_sponza\\sponza.obj");
	//auto mesh = Abyss::MeshLoader::getDefaultLoader()->load("D:\\Development\\Abyss\\Debug\\meshes\\dabrovic_sponza\\sponza.obj");
	//auto mesh = Abyss::MeshLoader::getDefaultLoader()->load("D:\\Development\\Abyss\\Debug\\meshes\\nanosuit\\nanosuit.obj");

	auto pos = mesh->getAABB().center();

	AABB hellKnightAABB = mesh->getAABB();
	hellKnightAABB.extend(Vector3D(hellKnightAABB.width() / 2, 0.f, hellKnightAABB.depth() / 2));
	Vector3D leftTopFar = hellKnightAABB.leftTopFar();
	Vector3D rightBottomFar = hellKnightAABB.rightTopNear();

	Vector3D ssaoRectOffset = Vector3D(0.f, hellKnightAABB.height() / 5, 0.f);
	auto ssaoRect = Mesh::createRect("HellKnighter_SSAO_plane", 
									  hellKnightAABB.leftTopNear()  - ssaoRectOffset, 
									  hellKnightAABB.leftTopFar()   - ssaoRectOffset, 
									  hellKnightAABB.rightTopFar()  - ssaoRectOffset, 
									  hellKnightAABB.rightTopNear() - ssaoRectOffset);

	auto ssaoRectRenderable = MeshRenderable::create("HellKnighterSSAORect", ssaoRect);
	//ssaoRect->getMaterial()->setAmbientColor(Vector4D(0.f, 0.5f, 0.f, 1.)
	
	ABYSS_LOG((boost::format("LightTypesScene - hell knight pos is: %1%; %2%; %3%") % (pos.x) % (pos.y) % (pos.z)).str());

	auto renderable = MeshRenderable::create("HellKnighter_" + std::to_string(index), mesh);
	renderable->setBoundingBoxEnabled(true);
	auto material = renderable->getMaterial();
	material->setShiness(100);
	renderable->setLightingEnabled(true);
	auto sceneObject = m_application->getSceneManager()->createSceneObject("HellKnighterScenObject_" + std::to_string(index));
	sceneObject->addRenderableObject(renderable);
	sceneObject->addRenderableObject(ssaoRectRenderable);
	sceneObject->setPosition(Vector3D(-100, 0, 100));
	sceneObject->setScale(0.2f);

	//sceneObject->setScale(Vector3D(0.1f, 0.1f, 0.1f));

	++index;

	return sceneObject;
}
//-------------------------------------------------------------------------------------------------
void LightTypesScene::setCurrentItem(ItemType type)
{
	if (getCurrentItem() == type)
		return;

	m_currentItem = type;

	m_application->getSceneManager()->removeSceneObject(m_currentSceneObject);

	Abyss::SceneObjectPtr newSceneObject;

	switch (type)
	{
	case ItemType::Cat:
		newSceneObject = createCat();
		break;
	case ItemType::HellKnight:
		newSceneObject = createHellKnight();
		break;
	case ItemType::TestRect:
		newSceneObject = createTestRect();
		break;
	}

	m_currentSceneObject = newSceneObject;

	auto previousHandlers = m_application->getEventHandlers();
	for (auto handler : previousHandlers)
		m_application->deleteEventHandler(handler);

	if (newSceneObject)
	{
		TrackBallRotationController* rotationController = new TrackBallRotationController(newSceneObject->getName());
		m_application->addEventHandler(rotationController);

		auto cameraZoomController = new CameraZoomController();
		m_application->addEventHandler(cameraZoomController);
	}
	m_panelHandler->updatePanel();

	updateLightPosition();
	setupPointLightPathModifier();
}
//-------------------------------------------------------------------------------------------------
void LightTypesScene::updateLightPosition()
{
	if (!m_currentLight)
		return;

	SceneManager* sceneManager = m_application->getSceneManager();
	auto pos = sceneManager->getCamera()->getEyePosition();
	m_currentLight->setPosition(pos);
}
//-------------------------------------------------------------------------------------------------
void LightTypesScene::setupPointLightPathModifier()
{
	static const std::string s_sphereObjectName = "TreefoilSphereObject";
	SceneManager * sceneManager = m_application->getSceneManager();
	auto currentItem = getCurrentSceneObject();
	if (!currentItem)
	{
		m_pointLightPathModifier.reset();
		sceneManager->removeSceneObject(s_sphereObjectName);
		return;
	}

	if (!m_pointLightPathModifier)
	{
		m_pointLightPathModifier.reset(new Abyss::TreefoilPathModifier());

		// Creating sphere object:
		{
			auto mesh = Abyss::MeshLoader::getDefaultLoader()->load("D:\\Development\\Abyss\\Debug\\meshes\\sphere.obj");
			auto material = Material::create(Vector3D(0.f, 0.f, 0.3f), Vector4D(0, 0.f, 1.f, 1), Vector4D(0, 0, 1, 1), 25);
			auto renderableObject = MeshRenderable::create("TrefoilSphereInstance", mesh, material);
			renderableObject->setLightingEnabled(false);

			SceneObjectPtr sceneObject = sceneManager->createSceneObject(s_sphereObjectName);
			sceneObject->addRenderableObject(renderableObject);
		}
	}

	auto sphereObject = sceneManager->getSceneObject(s_sphereObjectName);
	assert(sphereObject);

	m_pointLightPathModifier->setSceneObjectName(sphereObject->getName());

	auto currentItemBbbox = currentItem->getWorldAABB();
	auto largetstSide = std::max(std::max(currentItemBbbox.width(), currentItemBbbox.height()), currentItemBbbox.depth());
	auto objectPos = currentItem->getPosition();

	auto sphereBBox = sphereObject->getWorldAABB();
	auto sphereLargestSide = std::max(std::max(sphereBBox.width(), sphereBBox.height()), sphereBBox.depth());

	auto sphereRadiusMultiplier = largetstSide / sphereLargestSide;
	sphereRadiusMultiplier /= 10.f;
	sphereObject->setScale(Vector3D(sphereRadiusMultiplier, sphereRadiusMultiplier, sphereRadiusMultiplier));

	m_pointLightPathModifier->setTreefoilRadius(largetstSide);
	m_pointLightPathModifier->setTreefoilCenterPosition(objectPos);
	m_pointLightPathModifier->setSceneObjectName(s_sphereObjectName);

	m_pointLightPathModifier->start();
}

SceneManager* LightTypesScene::getSceneManager() const
{
	return  m_application->getSceneManager();
}

void LightTypesScene::onFPSChanged(Abyss::FPSCounter* counter)
{
	m_debugPanel->updateFPS_Panel(counter);
}
