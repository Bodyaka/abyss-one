
#include "AbyssSceneObject.h"
#include "AbyssMeshRenderable.h"
#include "AbyssMesh.h"
#include "AbyssMaterial.h"
#include "FPSCounter.h"

// DebugPanel must be included after Abyss headers in order to avoid error C1189: gl.h included before glew.h
#include "DebugPanel.h"
#include "SFGUIManager.h"

DebugPanel::DebugPanel(Abyss::SFGUIManager* manager, std::uint8_t style)
	: sfg::Window(style)
	, Abyss::SfguiWidgetDocker(this)
	, m_manager(manager)
	, m_fpsVisibilitySignal(0)
{
	
}

DebugPanel::~DebugPanel()
{
}
 

DebugPanel::Ptr DebugPanel::Create(Abyss::SFGUIManager* manager, std::uint8_t style /*= sfg::Window::Style::TOPLEVEL*/)
{
	DebugPanel::Ptr debugPanel(new DebugPanel(manager, style));

	debugPanel->RequestResize();
	debugPanel->SetTitle("Debug Panel");
	debugPanel->setDraggingEnabled(false);
	debugPanel->FinishCreation();

	return debugPanel;
}


void DebugPanel::createFPS_StatsPanel()
{
	m_fpsFrame = sfg::Frame::Create("FPS stats");
	auto fpsFrameLayout = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

	// Last Frame FPS:
	{
		auto lastFPSLayout = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 2.0f);
		
		auto lastFPSNameLabel = sfg::Label::Create("Last FPS:");
		m_lastFPSLabel = sfg::Label::Create("0");
		lastFPSLayout->Pack(lastFPSNameLabel);
		lastFPSLayout->Pack(m_lastFPSLabel);

		fpsFrameLayout->Pack(lastFPSLayout);
	}

	// Average Frame FPS:
	{
		auto averageFPSLayout = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 2.0f);

		auto averageFPSNameLabel = sfg::Label::Create("Average FPS:");
		m_averageFPSLabel = sfg::Label::Create("0");
		averageFPSLayout->Pack(averageFPSNameLabel);
		averageFPSLayout->Pack(m_averageFPSLabel);

		fpsFrameLayout->Pack(averageFPSLayout);
	}

	// Average Frame FPS (second method):
	{
		auto averageFPS2Layout = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 2.0f);

		auto averageFPS2NameLabel = sfg::Label::Create("Average FPS2:");
		m_averageFPS2Label = sfg::Label::Create("0");
		averageFPS2Layout->Pack(averageFPS2NameLabel);
		averageFPS2Layout->Pack(m_averageFPS2Label);

		fpsFrameLayout->Pack(averageFPS2Layout);
	}

	// Average FrameTime:
	{
		auto averageFrameTimeLayout = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 2.0f);

		auto averageFrameTimeLabel = sfg::Label::Create("Average Frame Time:");
		m_averageFrameTimeLabel = sfg::Label::Create("0");
		averageFrameTimeLayout->Pack(averageFrameTimeLabel);
		averageFrameTimeLayout->Pack(m_averageFrameTimeLabel);

		fpsFrameLayout->Pack(averageFrameTimeLayout);
	}



	m_fpsFrame->Add(fpsFrameLayout);
	m_widgesContainer->Pack(m_fpsFrame, false);
}

const std::string& DebugPanel::GetName() const
{
	static const std::string s_panelName = "DebugPanel";
	return s_panelName;
}

void DebugPanel::FinishCreation()
{
	m_widgesContainer = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

	createControlButtons();
	createFPS_StatsPanel();

	m_fpsPanelButton->SetActive(true);

	Add(m_widgesContainer);
}

void DebugPanel::createControlButtons()
{
	// FPS panel button show/hides panel:
	m_fpsPanelButton = sfg::ToggleButton::Create("Show FPS stats");
	m_widgesContainer->Pack(m_fpsPanelButton, false);

	m_fpsVisibilitySignal = getFPSPanelVisibilityButton()->GetSignal(sfg::ToggleButton::OnToggle).Connect(
		[this]() -> void
	{
		bool isVisible = m_fpsPanelButton->IsActive();
		m_fpsFrame->Show(isVisible);
		this->RequestResize();
	});
}

void DebugPanel::updateFPS_Panel(Abyss::FPSCounter* counter)
{
	m_lastFPSLabel->SetText(std::to_string(counter->getLastFrameRate()));
	m_averageFPSLabel->SetText(std::to_string(counter->getAverageFrameRate()));
	m_averageFPS2Label->SetText(std::to_string(counter->getAverageFrameRate2()));
	m_averageFrameTimeLabel->SetText(std::to_string(counter->getAverageFrameTimeInMilliseconds()));
}
