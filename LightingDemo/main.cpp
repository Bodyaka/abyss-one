#include <iostream>
#include "MyApplication.h"
#include "AbyssDefaultCameraController.h"
#include "AbyssSceneManager.h"
#include "AbyssMessageBox.h"
#include "SFML/Graphics.hpp"

#include <map>

int main(int argc, char * argv[])
{
	srand(time(nullptr));

	using namespace Abyss;

	try
	{
		Application::CreateOptions createOptions = { argc, argv, "Application", 0, 0, 1366, 768 };
		MyApplication * app = new MyApplication(createOptions);
		app->start();
	}
	catch (const std::exception & exc)
	{
		MessageBox::showMessageBox("Error", exc.what());
	}


	return 0;
}