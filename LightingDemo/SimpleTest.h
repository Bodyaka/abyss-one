#pragma once

#include "Abyss.h"
#include "AbyssRendering.h"

class SimpleTest
{
public:
	void start();
private:
	void testShaderLoading();
	Abyss::MeshPtr testMeshLoading(const Abyss::String & meshName);
	void testSimpleSceneObject(const Abyss::MeshPtr & mesh);
};