#include "MyApplication.h"

#include "SimpleTest.h"

#include "AbyssAssimpLoader.h"
#include "LightTypesScene.h"


MyApplication::MyApplication(const Abyss::Application::CreateOptions & options)
	: Abyss::Application(options)
	, m_lightTypesScene(nullptr)
{
	m_lightTypesScene.reset(new LightTypesScene(this));
}

MyApplication::~MyApplication()
{

}

void MyApplication::setup()
{
	Abyss::AssimpLoader::makeAsDefaultLoader();

	Application::setup();
	SimpleTest().start();
	m_lightTypesScene->start();
}