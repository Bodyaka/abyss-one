#pragma once

#include <map>
#include <vector>
#include "AbyssGlm.h"
#include "AbyssRendering.h"

class RayTracedLineStrip
{
public:
	typedef boost::shared_ptr<RayTracedLineStrip> Pointer;
	typedef std::vector<Abyss::Vector3D> LineType;

	RayTracedLineStrip(const LineType & line);
	~RayTracedLineStrip();

	void bind();
	void release();

	int getVerticesCount() const;

private:
	void setup();
	void setupVertexAttribute(int attribute, int elementsCountPerVertex);

	LineType m_line;

	Abyss::VertexArrayObjectPtr m_vao;
	std::map<int, Abyss::VertexBufferObjectPtr> m_buffers;
};