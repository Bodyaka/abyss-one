#pragma once

#include "RayTracedLineStrip.h"
#include "AbyssRenderableObject.h"

// TODO: class is incomplete
class RayTracedTubesRenderer
	: public Abyss::RenderableObject
{
public:

	typedef boost::shared_ptr<RayTracedTubesRenderer> Pointer;

public:
	RayTracedTubesRenderer();
	~RayTracedTubesRenderer();

	void addLineStrip(RayTracedLineStrip::Pointer lineStrip);
	void addLineStrip(const RayTracedLineStrip::LineType & data);
	
	//void render(const Abyss::RenderPass & pass) override;

private:

	std::vector<RayTracedLineStrip::Pointer> m_lines;


};