#include "SimpleTest.h"

#include "AbyssApplication.h"
#include "AbyssSceneManager.h"
#include "AbyssMeshObjLoader.h"
#include "AbyssShaderProgram.h"
#include "AbyssShaderProgramManager.h"
#include "AbyssSceneObject.h"
#include "AbyssMeshRenderable.h"

#include "TestRenderer.h"
#include "AbyssMaterial.h"

using namespace Abyss;

//--------------------------------------------------------------------------------------------------
void SimpleTest::start()
{
	testShaderLoading();
	MeshPtr mesh = testMeshLoading("D:\\Development\\Abyss\\Debug\\meshes\\cubik.obj");
	testSimpleSceneObject(mesh);
}
//--------------------------------------------------------------------------------------------------
void SimpleTest::testShaderLoading()
{
	// Note, that shaders should contains Test1 shaderProgram:
	if (!ShaderProgramManager::instance()->hasShaderProgram(TestRenderer::SHADER_PROGRAM_NAME))
		throw std::exception("Bad luck at shader loading!");
}
//--------------------------------------------------------------------------------------------------
MeshPtr SimpleTest::testMeshLoading(const Abyss::String & meshName)
{
	static const int CUBE_VERTICES_COUNT = 24;

	MeshPtr mesh = MeshLoader::getDefaultLoader()->load(meshName);
	if (mesh == nullptr)
		throw std::exception("Bad Luck at testing mesh loading!");
	
	return mesh;
}
//--------------------------------------------------------------------------------------------------
void SimpleTest::testSimpleSceneObject(const Abyss::MeshPtr & mesh)
{
	// Create scene object:
	SceneManager * sceneManager = SceneManager::instance();
	SceneObjectPtr sceneObject = sceneManager->createSceneObject("TestSceneObject_1");

	// Setup renderable object and scene object:
	MaterialPtr material = Material::create(Vector3D(1.f), Vector4D(1.f), Vector4D(1.f));
	MeshRenderablePtr renderableObject = MeshRenderable::create("SimpleTest_TestRenderableObject_1", mesh, material);
	sceneObject->addRenderableObject(renderableObject);
	sceneObject->rotateAlongSelfAxis(45.f, Vector3D(0.f, 1.f, 0.f));
	sceneObject->rotateAlongSelfAxis(30.f, Vector3D(1.f, 0.f, 0.f));
	sceneObject->move(Vector3D(0.f, 0.f, -10.f));
}
//--------------------------------------------------------------------------------------------------

