#pragma once

#include "AbyssRenderer.h"

class TestRenderer
	: public Abyss::Renderer
{
	typedef Abyss::Renderer TSuper;
public:
	static const Abyss::String SHADER_PROGRAM_NAME;

	TestRenderer();
	~TestRenderer();

	void setWorldViewProjectionalMatrix(const Abyss::Matrix4D & matrix);

	bool isSupports(const Abyss::RenderableObject * const object) const override { return true; }

protected:
	virtual void beforeRender(const Abyss::RenderableObject * const object);
	virtual void afterRender(const Abyss::RenderableObject * const object);
};