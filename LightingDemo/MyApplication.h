#pragma once

#include "AbyssApplication.h"

class LightingScene;
class LightTypesScene;

class MyApplication
	: public Abyss::Application
{
public:
	MyApplication(const Abyss::Application::CreateOptions & options);
	~MyApplication();

	virtual void setup() override;

private:
	std::unique_ptr<LightTypesScene> m_lightTypesScene;
};