#include "RayTracedLineStrip.h"

#include "RayTracedTubesGlobal.h"
#include "AbyssVertexBufferObject.h"
#include "AbyssVertexArrayObject.h"

using namespace Abyss;

RayTracedLineStrip::RayTracedLineStrip(const LineType & line)
: m_line(line)
, m_vao(VertexArrayObject::createVAO())
{
	setup();
}

RayTracedLineStrip::~RayTracedLineStrip()
{

}

void RayTracedLineStrip::setup()
{
	m_vao->bind();

	int numOfVertices = getVerticesCount();
	const int sizeOfVector3d = sizeof(Abyss::Vector3D);

	VertexBufferObjectPtr posBuffer = VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER, sizeOfVector3d * numOfVertices, (void *)m_line.data(), GL_STATIC_DRAW);
	m_buffers[ray_traced_tubes::PositionAttribute] = posBuffer;

	auto normals = LineType();
	for (int i = 0; i < getVerticesCount(); ++i)
	{
		auto position = m_line[i];
		auto normal = glm::normalize(position);
		normals.push_back(normal);
	}

	VertexBufferObjectPtr normalBuffer = VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER, sizeOfVector3d * numOfVertices, (void *)normals.data(), GL_STATIC_DRAW);
	m_buffers[ray_traced_tubes::NormalAttribute] = normalBuffer;


	for (int i = 0; i <= ray_traced_tubes::NormalAttribute; ++i)
		setupVertexAttribute(i, 3);

	m_vao->release();
}

void RayTracedLineStrip::setupVertexAttribute(int attribute, int elementsCountPerVertex)
{
	if (m_buffers.find(attribute) == m_buffers.end())
		return;

	m_vao->bind();

	auto bufferObject = m_buffers.at(attribute);

	bufferObject->bind();
	glEnableVertexAttribArray(attribute);
	glVertexAttribPointer(attribute, elementsCountPerVertex, GL_FLOAT, GL_FALSE, 0, (GLvoid*)nullptr);

	m_vao->release();
}

int RayTracedLineStrip::getVerticesCount() const
{
	return m_line.size();
}

void RayTracedLineStrip::bind()
{
	m_vao->bind();
}

void RayTracedLineStrip::release()
{
	m_vao->release();
}