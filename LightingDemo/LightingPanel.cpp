#include "AbyssSceneObject.h"
#include "AbyssMeshRenderable.h"
#include "AbyssMesh.h"
#include "AbyssMaterial.h"

#include "LightingPanel.h"
#include "SFGUIManager.h"

LightingPanel::LightingPanel(Abyss::SFGUIManager* manager, std::uint8_t style)
	: sfg::Window(style)
	, Abyss::SfguiWidgetDocker(this)
	, m_manager(manager)
	, m_button1(nullptr)
	, m_button2(nullptr)
	, m_shinessSlider(nullptr)
	, m_shinessLevelLabel(nullptr)
	, m_shinessLabelSignal(-1)
	, m_diffuseIntensityLabelSignal(-1)
	, m_specularIntensityLabelSignal(-1)
{
	

}

LightingPanel::~LightingPanel()
{
	getShinessLevelAdjustment()->GetSignal(sfg::Adjustment::OnChange).Disconnect(m_shinessLabelSignal);
	getDiffuseIntensityAdjustment()->GetSignal(sfg::Adjustment::OnChange).Disconnect(m_diffuseIntensityLabelSignal);
	getSpecularIntensityAdjustment()->GetSignal(sfg::Adjustment::OnChange).Disconnect(m_specularIntensityLabelSignal);
}

LightingPanel::Ptr LightingPanel::Create(Abyss::SFGUIManager* manager, std::uint8_t style /*= sfg::Window::Style::TOPLEVEL*/)
{
	LightingPanel::Ptr panelPtr(new LightingPanel(manager, style));

	panelPtr->RequestResize();
	panelPtr->setDraggingEnabled(false);
	panelPtr->SetTitle("Lighting Panel");
	panelPtr->FinishCreation();

	return panelPtr;
}


void LightingPanel::setObjectPanelTitle(const std::string& title)
{
	m_objectPropertiesFrame->SetLabel(title);
}

void LightingPanel::onShinessSliderChanged()
{
	auto shinessValue = m_shinessSliderAdjustment->GetValue();
	m_shinessLevelLabel->SetText(std::to_string(int(shinessValue)));
}

void LightingPanel::onDiffuseIntensitySliderChanged()
{
	auto diffuseIntensity = getDiffuseIntensityAdjustment()->GetValue();
	m_diffuseIntensityLabel->SetText(std::to_string(diffuseIntensity));
}

void LightingPanel::onSpecularIntensitySliderChanged()
{
	auto specularIntensity = getSpecularIntensityAdjustment()->GetValue();
	m_specularIntensityLabel->SetText(std::to_string(specularIntensity));
}

void LightingPanel::createShinessControls(sfg::Box* layoutBox)
{
	assert(layoutBox);

	auto shinessNameLabel = sfg::Label::Create("Shiness:");

	m_shinessSlider = sfg::Scale::Create(sfg::Range::Orientation::HORIZONTAL);
	m_shinessSlider->SetRequisition(sf::Vector2f(100.f, 25.f));
	m_shinessSliderAdjustment = m_shinessSlider->GetAdjustment();
	m_shinessSliderAdjustment->SetLower(0.f);
	m_shinessSliderAdjustment->SetUpper(200.f);
	m_shinessSliderAdjustment->SetMinorStep(1.f);
	m_shinessSliderAdjustment->SetMajorStep(30.f);

	m_shinessLevelLabel = sfg::Label::Create("0");

	auto shinessWidgetsLayout = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 3.0f);
	shinessWidgetsLayout->Pack(m_shinessSlider, true);
	shinessWidgetsLayout->Pack(m_shinessLevelLabel, false);

	layoutBox->Pack(shinessNameLabel, false);
	layoutBox->Pack(shinessWidgetsLayout, false);
}

void LightingPanel::createDiffuseIntensityControls(sfg::Box* layoutBox)
{
	assert(layoutBox);

	auto diffuseIntensityNameLabel = sfg::Label::Create("Diffuse intensity:");
	auto diffuseIntensitySlider = sfg::Scale::Create(sfg::Range::Orientation::HORIZONTAL);
	diffuseIntensitySlider->SetRequisition(sf::Vector2f(100.f, 25.f));
	m_diffuseIntensitySliderAdjustment = diffuseIntensitySlider->GetAdjustment();
	m_diffuseIntensitySliderAdjustment->SetLower(0.f);
	m_diffuseIntensitySliderAdjustment->SetUpper(5.f);
	m_diffuseIntensitySliderAdjustment->SetMinorStep(0.1f);
	m_diffuseIntensitySliderAdjustment->SetMajorStep(0.5f);

	m_diffuseIntensityLabel = sfg::Label::Create("0");

	auto diffuseIntensityWidgetsLayout = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 3.0f);
	diffuseIntensityWidgetsLayout->Pack(diffuseIntensitySlider, true);
	diffuseIntensityWidgetsLayout->Pack(m_diffuseIntensityLabel, false);

	layoutBox->Pack(diffuseIntensityNameLabel);
	layoutBox->Pack(diffuseIntensityWidgetsLayout);
}

void LightingPanel::createSpecularIntensityControls(sfg::Box* layoutBox)
{
	assert(layoutBox);

	auto specularIntensityNameLabel = sfg::Label::Create("Specular intensity:");
	auto specularIntensitySlider = sfg::Scale::Create(sfg::Range::Orientation::HORIZONTAL);
	specularIntensitySlider->SetRequisition(sf::Vector2f(100.f, 25.f));
	m_specularIntensitySliderAdjustment = specularIntensitySlider->GetAdjustment();
	m_specularIntensitySliderAdjustment->SetLower(0.f);
	m_specularIntensitySliderAdjustment->SetUpper(5.f);
	m_specularIntensitySliderAdjustment->SetMinorStep(0.1f);
	m_specularIntensitySliderAdjustment->SetMajorStep(0.5f);

	m_specularIntensityLabel = sfg::Label::Create("0");

	auto specularIntensityWidgetsLayout = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 3.0f);
	specularIntensityWidgetsLayout->Pack(specularIntensitySlider, true);
	specularIntensityWidgetsLayout->Pack(m_specularIntensityLabel, false);

	layoutBox->Pack(specularIntensityNameLabel);
	layoutBox->Pack(specularIntensityWidgetsLayout);
}

void LightingPanel::createSSAOControls(sfg::Box* layoutBox)
{
	assert(layoutBox);

	m_enableSSAOCheckButton = sfg::CheckButton::Create("Enable SSAO");

	layoutBox->Pack(m_enableSSAOCheckButton);
}

void LightingPanel::FinishCreation()
{
	m_button1 = sfg::Button::Create("Load rectangle");
	m_button2 = sfg::Button::Create("Load Hell Knight");

	m_box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
	m_box->Pack(m_button1, false);
	m_box->Pack(m_button2, false);

	m_objectPropertiesFrame = sfg::Frame::Create("Object's name");
	auto propertiesWidgetsLayout = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

	// Create object properties controls:
	// Shiness control widgets:
	createShinessControls(propertiesWidgetsLayout.get());
	// Diffuse intensity control widgets:
	createDiffuseIntensityControls(propertiesWidgetsLayout.get());
	// Specular intensity control widgets:
	createSpecularIntensityControls(propertiesWidgetsLayout.get());

	m_objectPropertiesFrame->Add(propertiesWidgetsLayout);
	m_box->Pack(m_objectPropertiesFrame, false);


	// Create SSAO properties controls:
	m_ssaoPropertiesFrame = sfg::Frame::Create("SSAO");
	auto ssaoPropertiesWidgetsLayout = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
	createSSAOControls(ssaoPropertiesWidgetsLayout.get());
	m_ssaoPropertiesFrame->Add(ssaoPropertiesWidgetsLayout);
	m_box->Pack(m_ssaoPropertiesFrame, false);

	Add(m_box);

	m_shinessLabelSignal = getShinessLevelAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(std::bind(&LightingPanel::onShinessSliderChanged, this));
	m_diffuseIntensityLabelSignal = getDiffuseIntensityAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(std::bind(&LightingPanel::onDiffuseIntensitySliderChanged, this));
	m_specularIntensityLabelSignal = getSpecularIntensityAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(std::bind(&LightingPanel::onSpecularIntensitySliderChanged, this));
}
