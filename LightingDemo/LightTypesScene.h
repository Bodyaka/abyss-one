#pragma once

#include "Abyss.h"
#include "AbyssRendering.h"
#include "AbyssApplication.h"
#include "AbyssSceneObject.h"
#include "AbyssTrefoilPathModifier.h"
#include "DebugPanel.h"
#include "LightingPanel.h"
#include "FPSCounter.h"

class LightTypesPanelHandler;

namespace Abyss
{
	class SceneManager;
}

class LightTypesScene
	: public Abyss::FPSCounterListener
{
public:

	enum class ItemType
	{
		None = 0,
		TestRect,
		HellKnight,
		Cat
	};

	LightTypesScene(Abyss::Application * app);
	~LightTypesScene();

	void start(ItemType type = ItemType::HellKnight);

	void setCurrentItem(ItemType type);
	ItemType getCurrentItem() const;

	Abyss::SceneObject* getCurrentSceneObject() const;
	Abyss::LightWeakPtr getCurrentLightObject() const;

	Abyss::SceneManager* getSceneManager() const;

private:
	void updateLightPosition();

	void setupPointLightPathModifier();

	Abyss::SceneObjectPtr createTestRect();
	Abyss::SceneObjectPtr createHellKnight();

	Abyss::SceneObjectPtr createCat();
	Abyss::MeshPtr createCatMesh();
	Abyss::RenderableObjectPtr createCatRenderableObject(Abyss::MeshPtr mesh);
	Abyss::SceneObjectPtr createCatSceneObject(Abyss::RenderableObjectPtr renderable);
	Abyss::SceneObjectPtr createBook();

	virtual void onFPSChanged(Abyss::FPSCounter* counter) override;
private:

	Abyss::Application * m_application;

	std::unique_ptr<LightTypesPanelHandler> m_panelHandler;
	LightingPanel::Ptr m_panel;
	DebugPanel::Ptr m_debugPanel;
	Abyss::SceneObjectPtr m_currentSceneObject;

	ItemType m_currentItem;

	Abyss::LightWeakPtr m_currentLight;

	std::unique_ptr<Abyss::TreefoilPathModifier> m_pointLightPathModifier;
};

// INLINE METHODS
//-------------------------------------------------------------------------------------------------
inline LightTypesScene::ItemType LightTypesScene::getCurrentItem() const
{
	return m_currentItem;
}
//-------------------------------------------------------------------------------------------------
inline Abyss::SceneObject* LightTypesScene::getCurrentSceneObject() const
{
	return m_currentSceneObject.get();
}
//-------------------------------------------------------------------------------------------------

