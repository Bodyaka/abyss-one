#pragma once

namespace ray_traced_tubes
{
	const int PositionAttribute = 0;
	const int NormalAttribute = 1;
}
