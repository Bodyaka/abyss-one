#include "RayTracedTubesRenderer.h"

#include "RayTracedTubesGlobal.h"
#include "AbyssVertexBufferObject.h"

using namespace Abyss;

RayTracedTubesRenderer::RayTracedTubesRenderer()
	: RenderableObject("RayTracedTubesRendererr")
{

}

RayTracedTubesRenderer::~RayTracedTubesRenderer()
{

}

void RayTracedTubesRenderer::addLineStrip(RayTracedLineStrip::Pointer lineStrip)
{
	assert(lineStrip.get() != nullptr);

	m_lines.push_back(lineStrip);
}

void RayTracedTubesRenderer::addLineStrip(const RayTracedLineStrip::LineType & data)
{
	auto ptr = RayTracedLineStrip::Pointer(new RayTracedLineStrip(data));
	addLineStrip(ptr);
}

/*
void RayTracedTubesRenderer::render(const Abyss::RenderPass & pass)
{
	for (auto lineStrip : m_lines)
	{
		lineStrip->bind();

		glDrawArrays(GL_LINE_STRIP_ADJACENCY, 0, lineStrip->getVerticesCount());

		lineStrip->release();
	}
}
*/