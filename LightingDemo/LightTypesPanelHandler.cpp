#include "LightTypesPanelHandler.h"

#include "LightTypesPanelHandler.h"
#include "LightTypesScene.h"
#include "LightingPanel.h"
#include "AbyssSceneObject.h"
#include "AbyssMeshRenderable.h"
#include "AbyssMaterial.h"

#include "AbyssRenderPass.h"
#include "AbyssSceneManager.h"
#include "AbyssRenderPassManager.h"

LightTypesPanelHandler::LightTypesPanelHandler(LightingPanel* panel, LightTypesScene* scene)
: m_scene(scene)
, m_guiPanel(panel)
, m_button1Signal(0)
, m_button2Signal(0)
, m_diffuseIntensitySliderChangeSignal(0)
, m_specularIntensitySliderChangeSignal(0)
, m_ssaoEnableCheckBoxSignal(0)

{
	m_button1Signal = panel->getButton1()->GetSignal(sfg::Widget::OnLeftClick).Connect(
		[this]() -> void
	{
		this->m_scene->setCurrentItem(LightTypesScene::ItemType::TestRect);
	});

	m_button2Signal = panel->getButton2()->GetSignal(sfg::Widget::OnLeftClick).Connect(
		[this]() -> void
	{
		this->m_scene->setCurrentItem(LightTypesScene::ItemType::HellKnight);
	});

	m_shinessSliderChangeSignal = panel->getShinessLevelAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(
		[this]() -> void
	{
		auto shinessValue = m_guiPanel->getShinessLevelAdjustment()->GetValue();
		getCurrentObjectMaterial()->setShiness(shinessValue);
	});

	m_diffuseIntensitySliderChangeSignal = panel->getDiffuseIntensityAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(
		[this]() -> void
	{
		auto diffuseIntensity = m_guiPanel->getDiffuseIntensityAdjustment()->GetValue();
		getCurrentObjectMaterial()->setDiffuseIntensity(diffuseIntensity);
	});

	m_specularIntensitySliderChangeSignal = panel->getSpecularIntensityAdjustment()->GetSignal(sfg::Adjustment::OnChange).Connect(
		[this]() -> void
	{
		auto specularIntensity = m_guiPanel->getSpecularIntensityAdjustment()->GetValue();
		getCurrentObjectMaterial()->setSpecularIntensity(specularIntensity);
	});


	// SSAO:
	m_ssaoEnableCheckBoxSignal = panel->getSSAO_EnableCheckButton()->GetSignal(sfg::CheckButton::OnToggle).Connect(
		[this]() -> void
	{
		auto sceneManager = this->m_scene->getSceneManager();

		auto isEnabled = this->m_guiPanel->getSSAO_EnableCheckButton()->IsActive();
		sceneManager->setSSAOEnabled(isEnabled);
	});

}

LightTypesPanelHandler::~LightTypesPanelHandler()
{
	m_guiPanel->getButton1()->GetSignal(sfg::Widget::OnLeftClick).Disconnect(m_button1Signal);
	m_guiPanel->getButton2()->GetSignal(sfg::Widget::OnLeftClick).Disconnect(m_button2Signal);
	m_guiPanel->getShinessLevelAdjustment()->GetSignal(sfg::Adjustment::OnChange).Disconnect(m_shinessSliderChangeSignal);
	m_guiPanel->getDiffuseIntensityAdjustment()->GetSignal(sfg::Adjustment::OnChange).Disconnect(m_diffuseIntensitySliderChangeSignal);
	m_guiPanel->getSpecularIntensityAdjustment()->GetSignal(sfg::Adjustment::OnChange).Disconnect(m_specularIntensitySliderChangeSignal);

	m_guiPanel->getSSAO_EnableCheckButton()->GetSignal(sfg::CheckButton::OnToggle).Disconnect(m_ssaoEnableCheckBoxSignal);
}

Abyss::Material* LightTypesPanelHandler::getCurrentObjectMaterial() const
{
	Abyss::SceneObject* object = m_scene->getCurrentSceneObject();
	auto renderables = object->getRenderableObjects();
	// TODO: uncomment this check later
	/*
	if (renderables.size() != 1)
	{
		assert(false && "Renderables should have been only one!");
		return nullptr;
	}
	*/

	if (auto meshRenderable = dynamic_cast<Abyss::MeshRenderable*>(renderables.front().get()))
		return meshRenderable->getMaterial().get();

	assert(false && "Renderable should be of type MeshRenderable!");
	return nullptr;
}

void LightTypesPanelHandler::updatePanel()
{
	Abyss::SceneObject* object = m_scene->getCurrentSceneObject();
	m_guiPanel->setObjectPanelTitle(object->getName());
	auto material = getCurrentObjectMaterial();
	m_guiPanel->getShinessLevelAdjustment()->SetValue(material->getShiness());
	m_guiPanel->getDiffuseIntensityAdjustment()->SetValue(material->getDiffuseIntensity());
	m_guiPanel->getSpecularIntensityAdjustment()->SetValue(material->getSpecularIntensity());

	auto sceneManager = m_scene->getSceneManager();
	m_guiPanel->getSSAO_EnableCheckButton()->SetActive(sceneManager->isSSAOEnabled());
}

