#pragma once

#include <SFGUI/SFGUI.hpp>
#include "SfguiWidgetDocker.h"

namespace Abyss
{
	class SceneObject;
	class SFGUIManager;
}

class LightingPanel
	: public sfg::Window
	, public Abyss::SfguiWidgetDocker
{
public:
	typedef std::shared_ptr<LightingPanel> Ptr;
public:
	static Ptr Create(Abyss::SFGUIManager* manager, std::uint8_t style = sfg::Window::Style::TOPLEVEL);

	LightingPanel(Abyss::SFGUIManager* manager, std::uint8_t style);
	~LightingPanel();

	sfg::Button::Ptr getButton1() const;
	sfg::Button::Ptr getButton2() const;

	sfg::Adjustment::Ptr getShinessLevelAdjustment() const;
	sfg::Adjustment::Ptr getDiffuseIntensityAdjustment() const;
	sfg::Adjustment::Ptr getSpecularIntensityAdjustment() const;

	sfg::CheckButton::Ptr getSSAO_EnableCheckButton() const;

	void setObjectPanelTitle(const std::string& title);


private:

	void createShinessControls(sfg::Box* layoutBox);
	void createDiffuseIntensityControls(sfg::Box* layoutBox);
	void createSpecularIntensityControls(sfg::Box* layoutBox);
	void createSSAOControls(sfg::Box* layoutBox);

	void onShinessSliderChanged();
	void onDiffuseIntensitySliderChanged();
	void onSpecularIntensitySliderChanged();

	void FinishCreation();


	Abyss::SFGUIManager* m_manager;

	sfg::Button::Ptr m_button1,
					 m_button2;

	sfg::Frame::Ptr m_objectPropertiesFrame,
					m_ssaoPropertiesFrame;
	sfg::Scale::Ptr m_shinessSlider;
	
	sfg::Adjustment::Ptr m_shinessSliderAdjustment;
	sfg::Adjustment::Ptr m_diffuseIntensitySliderAdjustment;
	sfg::Adjustment::Ptr m_specularIntensitySliderAdjustment;

	sfg::Label::Ptr m_shinessLevelLabel,
					m_diffuseIntensityLabel,
					m_specularIntensityLabel;

	sfg::CheckButton::Ptr m_enableSSAOCheckButton;

	sfg::Window::Ptr m_window;
	sfg::Box::Ptr m_box;

	size_t m_shinessLabelSignal,
		   m_diffuseIntensityLabelSignal,
		   m_specularIntensityLabelSignal;

};

inline sfg::Button::Ptr LightingPanel::getButton1() const
{	return m_button1;	}

inline sfg::Button::Ptr LightingPanel::getButton2() const
{	return m_button2;	}

inline sfg::Adjustment::Ptr LightingPanel::getShinessLevelAdjustment() const
{	return m_shinessSliderAdjustment; }

inline sfg::Adjustment::Ptr LightingPanel::getDiffuseIntensityAdjustment() const
{	return m_diffuseIntensitySliderAdjustment;	}

inline sfg::Adjustment::Ptr LightingPanel::getSpecularIntensityAdjustment() const
{	return m_specularIntensitySliderAdjustment;	}

inline sfg::CheckButton::Ptr LightingPanel::getSSAO_EnableCheckButton() const
{	return m_enableSSAOCheckButton;	}
