#version 150

//in
in VertexData
{
	vec3 vertexPosition_worldSpace;
	vec3 vertexNormal_worldSpace;
	vec3 vertexTangent_worldSpace;
	vec3 vertexBiTangent_worldSpace;
	vec2 vertexTextureCoord;
};

// out
out vec4 finalColor;

#abyss include("Helper/matrices.inl")
#abyss include("Helper/forwardLighting.inl")

uniform vec3 eyePosition_WorldSpace;

void main(void)
{
	if (enableLighting)
	{
		vec3 normal = normalize(vertexNormal_worldSpace);
		
		if (enableNormalMapping)
		{
			vec3 tangent = normalize(vertexTangent_worldSpace);
			
			normal = calculateBumpedNormal(normal, tangent, vertexTextureCoord);
		}

		vec3 eyeDir = normalize(eyePosition_WorldSpace - vertexPosition_worldSpace);
		finalColor = getTotalPhongShadedColor(normal, vertexPosition_worldSpace, vertexTextureCoord, eyeDir);
		
	}
	else
		finalColor = getSimpleColor(vertexTextureCoord);
}
