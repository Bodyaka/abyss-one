#version 150

//in
// Based on http://rastergrid.com/blog/2010/09/efficient-gaussian-blur-with-linear-sampling/

in vec2 TextureCoord;

// out
out vec4 BlurResult;

// Uniforms
uniform sampler2D uInputTexture;
uniform int uIsHorizontalBlur; // If 1 then horizontal, 0 - non-horizontal;

// Constants
const int cKernelSize = 5;
const float cOffsets[cKernelSize] = float[](0.f, 1.f, 2.f, 3.f, 4.f);
// These weights are based on the discrete sampling of continuous normal distribution:
const float cWeights[cKernelSize] = float[](0.2270270270f, 0.1945945946f, 0.1216216216f, 0.0540540541f, 0.0162162162f );

void main(void)
{
	vec2 texelSize = 1.0f / vec2(textureSize(uInputTexture, 0));
	vec2 directionMultiplier = vec2(uIsHorizontalBlur == 1, uIsHorizontalBlur == 0);

	BlurResult = texture(uInputTexture, TextureCoord) * cWeights[0];
	for (int i = 1; i<cKernelSize; ++i) 
	{
		float offset = cOffsets[i];
		float weight = cWeights[i];
		vec2 texelOffset = vec2(offset, offset) * texelSize * directionMultiplier;
		
		BlurResult += texture(uInputTexture, TextureCoord + texelOffset) * weight;
		BlurResult += texture(uInputTexture, TextureCoord - texelOffset) * weight;		
	}
}
