#version 150

//in
in vec3 outVertexPosition_viewSpace;
in vec3 outVertexNormal_viewSpace;
in vec3 outVertexTangent_viewSpace;
in vec3 outVertexBiTangent_viewSpace;
in vec2 outVertexTexel;

// out
out vec4 finalColor;

#abyss include("Helper/matrices.inl")
#abyss include("Helper/forwardLighting.inl")


void main(void)
{
	if (enableLighting)
	{
		vec3 normal = normalize(outVertexNormal_viewSpace);
		vec3 tangent = normalize(outVertexTangent_viewSpace);

		// TODO: which to choose?
		vec3 bitangent = cross(tangent, normal);
		//vec3 bitangent = normalize(outVertexBiTangent_viewSpace);
	
		vec3 lightDir;
		vec3 eyeDir = -normalize(outVertexPosition_viewSpace);
		mat3x3 TBN;
		if (enableNormalMapping)
		{
	
			TBN = transpose(mat3(tangent,
									  bitangent,
									  normal));

			lightDir = TBN * lightDir;
			eyeDir = TBN * eyeDir;
		
			normal = normalize(texture(material.normalTexture, outVertexTexel).rgb * 2.0 - 1.0);		
		}

		// Actual computation:
		finalColor = DefaultColor;

		int currentLightsNumber = min(activeLightsCount, maxLightsCount);
		for (int lightIndex = 0; lightIndex < currentLightsNumber; ++lightIndex)
		{
			vec4 lightPosition_viewSpace = ViewMatrix * lights[lightIndex].position;
			vec3 lightDir = calculateLightDirection(lightPosition_viewSpace, outVertexPosition_viewSpace);
			
			if (enableNormalMapping)
				lightDir = TBN * lightDir;
				
			finalColor += calculateShadedColor(lightDir, eyeDir, normal, outVertexTexel);

			// Enable attenuation:
			float attenuationFactor = calculateAttenuation(lightIndex, lightPosition_viewSpace, outVertexPosition_viewSpace);
			finalColor *= attenuationFactor * lights[lightIndex].color;

		}
	}
	else
		finalColor = getSimpleColor(outVertexTexel);
}
