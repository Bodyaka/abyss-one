#version 150

//in
in VertexData
{
	vec3 vertexPosition_worldSpace;
	vec3 vertexNormal_worldSpace;
	vec3 vertexTangent_worldSpace;
	vec3 vertexBiTangent_worldSpace;
	vec2 vertexTextureCoord;
};

// out
out vec4 gBufferPositionDepth;
out vec4 gBufferNormals;
out vec4 gBufferColor;

// Uniforms
uniform float uNearPlaneDist;
uniform float uFarPlaneDist;

// Includes
#abyss include("Helper/matrices.inl")
#abyss include("Helper/material.inl")

float linearizeDepth(float depth)
{
/*
	float A = ProjectionMatrix[2].z;
    float B = ProjectionMatrix[3].z;
    float zNear = - B / (1.0 - A);
    float zFar  =   B / (1.0 + A);
*/

	float zNear = uNearPlaneDist;
	float zFar = uFarPlaneDist;
	
	// The depth is linear in view space. However, in screen space it is non-linear and similar to 1/z.
	// Why dividing by z? This will add us much better precision, when z-values are closer to camera and the depth precision will be neglegated far away.
	// And this is logical - why we should support same precision over all depth range? Depth values on far distances are not important.
	// 
	// About this case, see these articles:
	// 1. http://dev.theomader.com/linear-depth/
	// 2. https://fgiesen.wordpress.com/2011/07/08/a-trip-through-the-graphics-pipeline-2011-part-7/
	//
	// Also, about interpolation. The vertex in clip space is converted to screen space (NDC) and applied perspective correct interpolation.
	// In few words, this feature is used in order to achieve correct interpolation of vertex attributes (color, texture coordinates) for perspective projection.
	// Also, interpolation in screen space is faster than converting vertex back to clip space, perform interpolation and convert it back to NDC space.
	// For more info about perspective correct interpolation, see these articles:
	// 3. http://www.scratchapixel.com/lessons/3d-basic-rendering/rasterization-practical-implementation/perspective-correct-interpolation-vertex-attributes
	// 4. https://www.comp.nus.edu.sg/~lowkl/publications/lowk_persp_interp_techrep.pdf
	//
	// About visualizing depth value.
	// We visualize depth value in view space. This process leads to linear z-value. Thus, this process is also called depth linerealization.
	// In order to properly show depth value, it is needed to convert z-value from NDC space to view space and then use inverted projection-matrix multiplication.
	// For more info, see here:
	// 5. http://learnopengl.com/#!Advanced-OpenGL/Depth-Testing
	// 6. http://learnopengl.com/#!Advanced-Lighting/SSAO

	depth = depth * 2.0f - 1.0f;

	// In order to visualize the depth in understandable for viewer manner, it is needed to divide original formula by zFar:
	// (2.0 * zNear ) / (zFar + zNear - depth * (zFar - zNear));
	return (2.0 * zNear * zFar) / (zFar + zNear - depth * (zFar - zNear));
}


void main(void)
{
	// TODO: should we store depth too?
	// Normals calculation:
	vec3 normal = normalize(vertexNormal_worldSpace);
	if (enableNormalMapping)
	{
		vec3 tangent = normalize(vertexTangent_worldSpace);
		normal = calculateBumpedNormal(normal, tangent, vertexTextureCoord);
	}

	gBufferNormals = vec4(normal, material.shiness);

	// Position calculation:
	gBufferPositionDepth.xyz = vertexPosition_worldSpace;
	gBufferPositionDepth.w = linearizeDepth(gl_FragCoord.z);
	// Albedo calculation:
	// TODO: this computation doesn't include some properties of material, like:
	// - ambient;
	// - attenuation;
	// - specularIntensity;
	vec4 diffuseTexColor = texture(material.diffuseTexture, vertexTextureCoord);

	// TODO: should we compute ambient color here?
	//vec4 ambientColor = vec4(material.ambient, 1.f) * diffuseTexColor * material.diffuseIntensity;
	
	// We assume that material doesn't contain any translucency
	gBufferColor.rgb = vec4(material.diffuse * diffuseTexColor * material.diffuseIntensity).rgb;
	gBufferColor.a = material.interactivityFlag;
}
