#version 150

//in
in vec2 TextureCoord;

// out
out vec4 finalColor;

// Uniforms
uniform sampler2D gPositionsTexture;
uniform sampler2D gNormalsTexture;
uniform sampler2D gAlbedoTexture;
uniform sampler2D ssaoTexture;
uniform vec3 eyePosition_WorldSpace;
uniform bool enableSSAO;

// Const:
const vec3 s_specularIntensity = vec3(1.f);

#abyss include("Helper/matrices.inl")
#abyss include("Helper/commonLighting.inl")

//#define ABYSS_DISPLAY_SSAO_ONLY


vec4 getTotalDeferredPhongShadedColor(in vec3 vertexNormal_worldSpace,
									  in vec3 vertexPosition_worldSpace,
									  in vec4 albedo,
									  in vec3 eyeDir_worldSpace)
{
	vec4 finalColor = DefaultColor;

	float ambientOcclusionRatio = texture(ssaoTexture, TextureCoord).r;

#ifndef ABYSS_DISPLAY_SSAO_ONLY

	int currentLightsNumber = min(activeLightsCount, maxLightsCount);
	for (int lightIndex = 0; lightIndex < currentLightsNumber; ++lightIndex)
	{
		float diffuseComponent = 0.f;
		float specularComponent = 0.f;

		calculateShadingComponents(lightIndex, 
								   vertexNormal_worldSpace, 
								   vertexPosition_worldSpace, 
								   eyeDir_worldSpace, 
								   albedo.w,
								   diffuseComponent, 
								   specularComponent);
	
		vec3 diffuseTexColor = vec3(albedo.xyz);

		vec3 diffuseColor = diffuseTexColor * diffuseComponent;
		if (enableSSAO)
			diffuseColor *= ambientOcclusionRatio;
				
		vec3 specularColor = s_specularIntensity * specularComponent;

		// Enable attenuation:
		float attenuationFactor = calculateAttenuation(lightIndex, vertexPosition_worldSpace);
		diffuseColor *= attenuationFactor;
		specularColor *= attenuationFactor;

		vec4 lightShadedColor = vec4(diffuseColor + specularColor, 1.f) * lights[lightIndex].color;

		finalColor += lightShadedColor;
	}
#else
	finalColor = vec4(ambientOcclusionRatio, ambientOcclusionRatio, ambientOcclusionRatio, 1.f);
#endif
	return finalColor;
}


void main(void)
{
	vec4 vertexNormal_raw   = texture(gNormalsTexture, TextureCoord);

	vec3 vertexPosition_worldSpace = texture(gPositionsTexture, TextureCoord).rgb;
	vec3 vertexNormal_worldSpace   = vertexNormal_raw.rgb;
	vec4 vertexAlbedo			   = texture(gAlbedoTexture, TextureCoord);

	vertexAlbedo.w = vertexNormal_raw.w;
	
	if (enableLighting)
	{
		vec3 normal = normalize(vertexNormal_worldSpace);
		
		vec3 eyeDir = normalize(eyePosition_WorldSpace - vertexPosition_worldSpace);
		finalColor = getTotalDeferredPhongShadedColor(normal, vertexPosition_worldSpace, vertexAlbedo, eyeDir);
	}
	else
		finalColor = DefaultColor;
}
