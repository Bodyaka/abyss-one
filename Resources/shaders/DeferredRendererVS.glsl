#version 150

// Input data:
in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexel;
in vec3 VertexTangent;
in vec3 VertexBiTangent;

// Output data
out VertexData
{
	vec3 vertexPosition_worldSpace;
	vec3 vertexNormal_worldSpace;
	vec3 vertexTangent_worldSpace;
	vec3 vertexBiTangent_worldSpace;
	vec2 vertexTextureCoord;
};

#abyss include("Helper/matrices.inl")

// Main shader method. Entry point:
void main(void)
{
	// Write data to fragment shader:
	// When use SSAO use WorldViewMatrix. Else - use WorldMatrix!
	vertexPosition_worldSpace = (WorldMatrix * vec4(VertexPosition, 1.f)).xyz;
	vertexTextureCoord = VertexTexel;
	
	mat3x3 worldMatrix = mat3x3(WorldMatrix);
	vertexNormal_worldSpace = normalize(worldMatrix * VertexNormal);
	vertexTangent_worldSpace = normalize(worldMatrix * VertexTangent);
	vertexBiTangent_worldSpace = normalize(worldMatrix * VertexBiTangent);
			
	gl_Position = WorldViewProjectionMatrix * vec4(VertexPosition, 1.0);
}
