#version 150

// Input data:
in vec3 VertexPosition;
in vec3 VertexNormal;

uniform mat4x4 WorldViewProjectionalMatrix; 

// Main shader method. Entry point:
void main(void)
{
	gl_Position = WorldViewProjectionalMatrix * vec4(VertexPosition, 1.0);
}
