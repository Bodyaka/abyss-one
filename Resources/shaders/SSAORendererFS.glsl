#version 150

//in
in vec2 TextureCoord;

// out
out float AmbientOcclusionRatio;

// Const
const int MAX_NUMBER_OF_SAMPLES = 256;

// Uniforms
uniform sampler2D gPositionsTexture;
uniform sampler2D gNormalsTexture;
uniform sampler2D gAlbedoTexture;

uniform int	  uKernelSize;	 // 64 by default.
uniform float uSampleRadius; // 1.0f by default.
uniform vec2  uNoiseScale;
uniform vec3  uSamples[MAX_NUMBER_OF_SAMPLES];
uniform sampler2D uNoiseTexture;

#abyss include("Helper/matrices.inl")

void main(void)
{
	vec4 gBufferNormal = texture(gNormalsTexture, TextureCoord);
	
	vec4 fragmentPositionDepth = texture(gPositionsTexture, TextureCoord);
	
	// In order to improve performance of SSAO renderer, we have to discard those fragments, which are outside of the actual scene.
	// In this case - we know, that RenderBuffer's textures are all to zero, if nothing was rendered into them.
	if (fragmentPositionDepth == vec4(0, 0, 0, 0))
		discard;

	vec3 normal = (ViewMatrix * vec4(gBufferNormal.xyz, 0.f)).xyz;
	vec3 fragmentPosition = (ViewMatrix * vec4(fragmentPositionDepth.xyz, 1.f)).xyz;


	// The purpose of uNoiseScale is to map TextureCoord on all image (ScreenWidth x ScreenHeight) instead of mapping into range (4x4).
	// Basically, when perform multiplying by this noiseScale, we just simulate picking from texture with size of (ScreenWidth x ScreenHeight).
	// This is achievable cause of GL_REPEAT flag in uNoiseTexture (see front-end of the shader).
	vec3 randomVec = texture(uNoiseTexture, TextureCoord * uNoiseScale).xyz;  

	// Calculations are performed in the TBN space (WHY??)
	// Here is performed so-called Gram�Schmidt process, which is, basically, orthogonalization of some artibary basis (set of vectors in Euclerian part R3) (WHAT?)
	// For more info about Gram-Schmidt process, see this video: https://www.youtube.com/watch?v=Zk_ua7zBELg
	vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
	vec3 bitangent = cross(normal, tangent);
	mat3 TBN = mat3(tangent, bitangent, normal);  

	int kernelSize = min(uKernelSize, MAX_NUMBER_OF_SAMPLES);
	
	AmbientOcclusionRatio = 0.f;
	for (int i = 0; i<kernelSize; ++i)
	{
		// Conversion from TBN to view-space, cause position is defined in view space.
		vec3 samplePos_viewSpace = 	TBN * uSamples[i];
		
		// Now we are picking real sample in the hemishepre related to view-space fragmentPosition.
		// Scattering of view sample is controlled by uSampleRadius uniform.
		samplePos_viewSpace = fragmentPosition + samplePos_viewSpace * uSampleRadius;

		vec4 samplePos_screenSpace = ProjectionMatrix * vec4(samplePos_viewSpace, 1.f); // clip-space
		samplePos_screenSpace /= samplePos_screenSpace.w;								// homogeneous clip-space
		samplePos_screenSpace.xyz = samplePos_screenSpace.xyz * 0.5f + 0.5f;			// NDC-space
		
		// Now, it is needed to perform what acutal AO should do: compare depth of reference fragment (which is stored in G-Buffer) with actual sample depth.
		float referenceDepth_viewSpace = -texture(gPositionsTexture, samplePos_screenSpace.xy).w; // WHY NEGATE???

		// Perform range check in order to discard long-distance fragments.
		// Basically, it just check whether fragment is outside uniform sample radius
		float rangeCheck = smoothstep(0.0, 1.0, uSampleRadius / abs(referenceDepth_viewSpace - samplePos_viewSpace.z));
		
		// Note, that the depth are stored in view space, so, we have to compare viewSpaceSample.z instead using screenSpaceSample.z
		float aoWithoutRangeCheck = (referenceDepth_viewSpace >= samplePos_viewSpace.z) ? 1.f : 0.f;
		
		#ifdef ABYSS_SSAO_RANGE_CHECK
			AmbientOcclusionRatio += aoWithoutRangeCheck * rangeCheck; 
		#else
			AmbientOcclusionRatio += aoWithoutRangeCheck;
		#endif
	}

	AmbientOcclusionRatio = 1.f - AmbientOcclusionRatio / kernelSize;
}
