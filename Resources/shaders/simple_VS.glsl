#version 150

// Input data:
in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexel;
in vec3 VertexTangent;
in vec3 VertexBiTangent;

// Output data
out vec2 outVertexTexel;

#abyss include("Helper/matrices.inl")

// Main shader method. Entry point:
void main(void)
{
	// Write data to fragment shader:
	outVertexTexel = VertexTexel;
			
	gl_Position = WorldViewProjectionMatrix * vec4(VertexPosition, 1.0);
}
