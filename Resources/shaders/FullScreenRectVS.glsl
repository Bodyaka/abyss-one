#version 150

// Input data:
in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexel;
in vec3 VertexTangent;
in vec3 VertexBiTangent;

// Output data
out vec2 TextureCoord;

// Main shader method. Entry point:
void main(void)
{
	// Write data to fragment shader:
	TextureCoord = VertexTexel;
	
	gl_Position = vec4(VertexPosition, 1.0);
}