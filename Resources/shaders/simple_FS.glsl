#version 150

//in
in vec2 outVertexTexel;

// out
out vec4 finalColor;

#abyss include("Helper/matrices.inl")
#abyss include("Helper/forwardLighting.inl")

void main(void)
{
	finalColor = vec4(1.f, 0.f, 0.f, 1.f);
	//finalColor = material.defaultColor;
}
