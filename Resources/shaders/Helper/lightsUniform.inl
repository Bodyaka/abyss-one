const int maxLightsCount = 5;

// light positions:
uniform vec4 lightPositions[maxLightsCount];
// light intensity:
uniform vec4 lightIntensities[maxLightsCount];

// current enabled lights count:
uniform int enabledLightsCount;

// Eye location:
uniform vec3 eyeLocation;
