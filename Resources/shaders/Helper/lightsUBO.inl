const int maxLightsCount = 5;

// Light struct.
struct Light
{
	vec4 position;	// Position of lgiht (if w = 1.f) or direction (if w = 0.f)
	vec4 intensity; // Intensity of light (color of light).
};

// Global shading info for all shaders:
uniform ShadingInfo
{
	// Current enabled lights count:
	int enabledLightsCount;
	
	//	All lights:
	Light lights[maxLightsCount];
	
	// Location of eye in world coordinates:
	vec3 eyeLocation;
};
