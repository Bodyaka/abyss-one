struct Material
{
	vec3 ambient;		// ambient reflection (material)
	int interactivityFlag;
	
	vec4 diffuse;		// diffuse reflection
	vec4 specular;		// specular reflection
	float shiness;		// shiness (from 1. to 200.)
	float diffuseIntensity; // for test purposes: strength of the diffuse lighting.
	float specularIntensity; // for test purposes: strength of the specular lighting.

	sampler2D diffuseTexture;
	sampler2D normalTexture;

	vec4 defaultColor;

};

uniform Material material;
uniform bool enableNormalMapping;

// Helper methods:
vec3 calculateBumpedNormal(in vec3 normal, in vec3 tangent, in vec2 textureCoord)
{
	// TODO: which to choose?
	vec3 bitangent = cross(tangent, normal);
	//vec3 bitangent = normalize(vertexBiTangent_worldSpace);

	mat3x3 TBN = mat3x3(tangent, bitangent, normal);

	vec3 normalColor = texture(material.normalTexture, textureCoord).rgb;
	vec3 bumpedNormal = normalColor * 2.0 - vec3(1.0);
	normal = TBN * normalize(bumpedNormal);

	return normalize(normal);
}