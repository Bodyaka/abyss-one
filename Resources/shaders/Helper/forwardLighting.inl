#abyss include("commonLighting.inl")
#abyss include("material.inl")

vec4 calculateShadedColor(in int lightIndex,
	in vec3 vertexNormal_worldSpace,
	in vec3 vertexPosition_worldSpace,
	in vec2 textureCoord,
	in vec3 eyeDir_worldSpace)
{
	float diffuseComponent = 0.f;
	float specularComponent = 0.f;

	calculateShadingComponents(lightIndex,
		vertexNormal_worldSpace,
		vertexPosition_worldSpace,
		eyeDir_worldSpace,
		material.shiness,
		diffuseComponent,
		specularComponent);

	vec4 diffuseTexColor = texture(material.diffuseTexture, textureCoord);

	vec4 ambientColor = vec4(material.ambient, 1.f) * diffuseTexColor * material.diffuseIntensity;
	vec4 diffuseColor = material.diffuse * diffuseComponent * diffuseTexColor * material.diffuseIntensity;
	vec4 specularColor = material.specularIntensity * material.specular * specularComponent;

	// Enable attenuation:
	float attenuationFactor = calculateAttenuation(lightIndex, vertexPosition_worldSpace);
	diffuseColor *= attenuationFactor;
	specularColor *= attenuationFactor;

	//return vec4(lights[lightIndex].direction, 1.f);
	return (ambientColor + diffuseColor + specularColor) * lights[lightIndex].color;
}

vec4 getSimpleColor(vec2 textureCoord)
{
	vec4 diffuseTexColor = texture(material.diffuseTexture, textureCoord);
	return vec4(material.ambient, 1.f) + diffuseTexColor;
}

vec4 getTotalPhongShadedColor(in vec3 vertexNormal_worldSpace,
	in vec3 vertexPosition_worldSpace,
	in vec2 textureCoord,
	in vec3 eyeDir_worldSpace)
{
	vec4 finalColor = DefaultColor;

	int currentLightsNumber = min(activeLightsCount, maxLightsCount);
	for (int lightIndex = 0; lightIndex < currentLightsNumber; ++lightIndex)
	{
		finalColor += calculateShadedColor(lightIndex, vertexNormal_worldSpace, vertexPosition_worldSpace, textureCoord, eyeDir_worldSpace);
	}

	return finalColor;
}
