struct Light
{
	int type;

	vec3 position;		// light position
	vec3 direction;
	vec4 color;			// light color

	// For point light:
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;

	// For spot light:
	float innerCutOffCosine;
	float outerCutOffCosine;
};

const int POINT_LIGHT_TYPE = 0;
const int DIRECTIONAL_LIGHT_TYPE = 1;
const int SPOTLIGHT_LIGHT_TYPE = 2;

//	const
const int maxLightsCount = 10;

// Uniforms
uniform int activeLightsCount;
uniform Light lights[maxLightsCount];

uniform bool enableLighting;

uniform vec4 DefaultColor;


float calculateAttenuation(in int lightIndex, in vec3 vertexPosition_worldSpace)
{
	if (lights[lightIndex].type == DIRECTIONAL_LIGHT_TYPE)
		return 1.f;

	vec3 lightPosition_worldSpace = lights[lightIndex].position;
	if (lights[lightIndex].type == POINT_LIGHT_TYPE)
	{
		float distToVertex = length(lightPosition_worldSpace - vertexPosition_worldSpace);

		float inverseAttenutation = lights[lightIndex].constantAttenuation +
			lights[lightIndex].linearAttenuation * distToVertex +
			lights[lightIndex].quadraticAttenuation * distToVertex * distToVertex;

		return 1.f / inverseAttenutation;
	}

	// TODO: should we add attenutation formula from PointLight?
	if (lights[lightIndex].type == SPOTLIGHT_LIGHT_TYPE)
	{
		vec3 lightDir_worldSpace = normalize(lightPosition_worldSpace - vertexPosition_worldSpace);
		vec3 spotLightDir_worldSpace = -normalize(lights[lightIndex].direction);

		// TODO: maybe bunch of IFs will be better?
		float cosineTheta = dot(lightDir_worldSpace, spotLightDir_worldSpace);
		float cosineInner = lights[lightIndex].innerCutOffCosine;
		float cosineOuter = lights[lightIndex].outerCutOffCosine;

		float zone = cosineInner - cosineOuter;
		float outerDiff = cosineTheta - cosineOuter;

		return clamp(outerDiff / zone, 0.f, 1.f);
	}

	return 1.f;
}

void calculateShadingComponents(in int lightIndex,
								in vec3 vertexNormal_worldSpace,
								in vec3 vertexPosition_worldSpace,
								in vec3 eyeDir_worldSpace,
								in float materialShiness,
								out float diffuseComponent,
								out float specularComponent)
{
	vec3 lightPosition_worldSpace = lights[lightIndex].position;
	vec3 lightDir_worldSpace;

	if (lights[lightIndex].type == DIRECTIONAL_LIGHT_TYPE)
		lightDir_worldSpace = -normalize(lights[lightIndex].direction); // The direction is headed from light, so, we have to negate it.
	else
		lightDir_worldSpace = normalize(lightPosition_worldSpace - vertexPosition_worldSpace);

	diffuseComponent = dot(lightDir_worldSpace, vertexNormal_worldSpace);
	diffuseComponent = max(diffuseComponent, 0.f);

	specularComponent = 0.f;

	if (diffuseComponent > 0.f)
	{
		vec3 reflectedLightDir_worldSpace = reflect(-lightDir_worldSpace, vertexNormal_worldSpace);
		float specularFactor = dot(reflectedLightDir_worldSpace, eyeDir_worldSpace);

		if (specularFactor > 0)
		{
			specularComponent = pow(specularFactor, materialShiness);
			specularComponent = max(specularComponent, 0.f);
		}
	}
}
