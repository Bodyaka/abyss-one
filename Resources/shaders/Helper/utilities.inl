bool isSamplerValid(in sampler2D sampler)
{
	return textureSize(sampler).x > 0;
}