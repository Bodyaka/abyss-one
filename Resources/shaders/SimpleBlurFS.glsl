#version 150

//in
in vec2 TextureCoord;

// out
out float BlurResult;

// Uniforms
uniform sampler2D uInputTexture;
uniform int uKernelSize;

void main(void)
{
	vec2 texelSize = 1.0f / vec2(textureSize(uInputTexture, 0));

	BlurResult = 0.f;

	for (int x = -uKernelSize; x < uKernelSize; ++x)
	{
		for (int y = -uKernelSize; y < uKernelSize; ++y)
		{
			vec2 texelOffset = vec2(float(x), float(y)) * texelSize;
			BlurResult += texture(uInputTexture, TextureCoord + texelOffset).r;
		}
	}
	
	int uKernelDiameter = uKernelSize * 2;
	BlurResult /= (uKernelDiameter * uKernelDiameter); // Performs weighting of blur result.
	//BlurResult = 0.5f;
	
}
