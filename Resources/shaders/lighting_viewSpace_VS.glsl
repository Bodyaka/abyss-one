#version 150

// Input data:
in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexel;
in vec3 VertexTangent;
in vec3 VertexBiTangent;

// Output data
out vec3 outVertexPosition_viewSpace;
out vec3 outVertexNormal_viewSpace;
out vec3 outVertexTangent_viewSpace;
out vec3 outVertexBiTangent_viewSpace;
out vec2 outVertexTexel;

#abyss include("Helper/matrices.inl")

// Main shader method. Entry point:
void main(void)
{
	// Write data to fragment shader:
	outVertexPosition_viewSpace = (WorldViewMatrix * vec4(VertexPosition, 1.0)).xyz;
	outVertexTexel = VertexTexel;
	
	outVertexNormal_viewSpace = normalize(NormalMatrix * VertexNormal);
	outVertexTangent_viewSpace = normalize(NormalMatrix * VertexTangent);
	outVertexBiTangent_viewSpace = normalize(NormalMatrix * VertexBiTangent);
			
	gl_Position = WorldViewProjectionMatrix * vec4(VertexPosition, 1.0);
}
