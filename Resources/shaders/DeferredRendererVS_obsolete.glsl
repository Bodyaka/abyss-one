#version 150

// Input data:
in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexel;
in vec3 VertexTangent;
in vec3 VertexBiTangent;

// Output data
out vec3 vertexPosition_worldSpace;
out vec3 vertexPosition_
out vec3 vertexNormal_worldSpace;
out vec2 vertexTexel;

#abyss include("Helper/matrices.inl")

// Main shader method. Entry point:
void main(void)
{
	// Write data to fragment shader:
	vertexPosition_worldSpace = (WorldMatrix * vec4(VertexPosition, 1.0)).xyz;
	
	mat3x3 worldMatrix = mat3x3(WorldMatrix);
	vertexNormal_worldSpace = normalize(worldMatrix * VertexNormal);

	vertexTexel = VertexTexel;
	
	// Write data to fragment shader:
	outVertexPosition_worldSpace = (WorldMatrix * vec4(VertexPosition, 1.0)).xyz;
	outVertexTexel = VertexTexel;
	
	mat3x3 worldMatrix = mat3x3(WorldMatrix);
	vertexNormal_worldSpace = normalize(worldMatrix * VertexNormal);
	vertexTangent_worldSpace = normalize(worldMatrix * VertexTangent);
	vertexBiTangent_worldSpace = normalize(worldMatrix * VertexBiTangent);
			
	gl_Position = WorldViewProjectionMatrix * vec4(VertexPosition, 1.0);
}
