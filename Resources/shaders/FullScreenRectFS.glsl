#version 150

//in
in vec2 TextureCoord;

// out
out vec4 finalColor;

uniform sampler2D uInputTexture;

void main(void)
{
	finalColor = texture(uInputTexture, TextureCoord);
}
