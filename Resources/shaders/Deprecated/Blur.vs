#version 150

// in data
in vec3 vertex;
in vec3 normal;
in vec2 texel;

// out data
out vec3 Position;
out vec3 Normal;
out vec2 TexCoord;
out mat4x4 modelView;

// uniform
uniform mat4x4 WVPMatrix; 
uniform mat4x4 NormalMatrix; 
uniform mat4x4 ModelMatrix;

// map values, such as vertex and normals into coord that is needed
void mapValues(out vec3 v, out vec3 n)
{
	v = mat3(ModelMatrix) * vertex;
	n = normalize(mat3(NormalMatrix) * normal);
}


// Main
void main(void)
{
	// sent values from vs to fs shader:
	mapValues(Position, Normal);
	
	TexCoord = texel;

	gl_Position = WVPMatrix * vec4(vertex, 1.0);
}
