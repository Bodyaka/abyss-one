#version 150

// in
in vec2 TexCoord;

// out
out vec4 FinalColor;

// const
const int MaxNumOfGammas = 7;

// uniform
uniform sampler2D Texture;
uniform float     Gammas[MaxNumOfGammas];
uniform int       numOfGammas;


// main function
void main(void)
{
	FinalColor = vec4(1.f);

	vec4 color = texture(Texture, TexCoord);
	int count = min(numOfGammas, MaxNumOfGammas);
	for (int i = 0; i<count; ++i)
		FinalColor = FinalColor * vec4(pow(color.rgb, vec3(1.0f / Gammas[i])), color.a);
}
