#version 150

// in
in vec3 vertex;
in vec2 texel;
in vec3 normal;
// out
out vec2 TexCoord;
out vec4 VertexColor;

// uniforms
uniform sampler2D DisplacementTexture;
uniform float Height;
uniform mat4x4 WVPMatrix; 

float luma(vec3 color)
{
	return 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
}

void main(void)
{
	vec3 n = normalize(normal);
	vec4 texColor = texture(DisplacementTexture, texel);
	float h = (luma(texColor.rgb)) * Height;
	vec3 v = vertex + n * h;

	TexCoord = texel;

	gl_Position = WVPMatrix * vec4(v, 1.0f);
	VertexColor = texture(DisplacementTexture, texel);

}