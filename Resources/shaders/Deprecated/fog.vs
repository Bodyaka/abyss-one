#version 150

// WARINING: subroutines was presented first only in 4.0 glsl

//														   in data:
in vec3 vertex;
in vec3 normal;
in vec2 texel;

//													       out data
out vec3 Position;
out vec3 Normal;
out mat4x4 modelView;

uniform mat4x4 WVPMatrix; 
uniform mat4x4 NormalMatrix; 
uniform mat4x4 ModelViewMatrix;

//														  functions:
// map values, such as vertex and normals into coord that is needed
void mapValues(out vec3 v, out vec3 n)
{
	v = mat3(ModelViewMatrix) * vertex;
	n = normalize(mat3(NormalMatrix) * normal);
}

//														main
void main(void)
{

	// sent values from vs to fs shader:
	mapValues(Position, Normal);
	modelView = ModelViewMatrix;
	
	gl_Position = WVPMatrix * vec4(vertex, 1.0);
}
