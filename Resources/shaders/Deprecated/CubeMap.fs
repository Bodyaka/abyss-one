#version 150
//															struct

struct MaterialInfo
{
	vec3 ambient;		// ambient reflection (material)
	vec3 diffuse;		// diffuse reflection
	vec3 specular;		// specular reflection
	float shiness;		// shiness (from 1. to 200.)
	float reflectFactor;
};

//in
in vec3      ReflectDir;

// out
out vec4 finalColor;

//														   uniforms
uniform int isSkyBox;
uniform MaterialInfo material;
uniform samplerCube cubeMap;

//														Functions:
//-------------------------------------------------------------------------------------------
void main(void)
{
	// maybe, it is not needed to normalize
	vec3 _dir = normalize(ReflectDir);
	
	if(isSkyBox >= 1)
	{
		_dir = vec3(_dir.x, -_dir.y, _dir.z);
		vec4 cubeMapColor = texture(cubeMap, _dir);
		finalColor = cubeMapColor;
	}
	else
	{
		_dir = vec3(-_dir.x, _dir.y, -_dir.z);
		vec4 cubeMapColor = texture(cubeMap, _dir);
		finalColor = cubeMapColor;//mix(vec4(material.diffuse, 1.0f), cubeMapColor, material.reflectFactor);
	}
}