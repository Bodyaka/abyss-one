#version 150

in vec2 TexCoord;
in vec4 VertexColor;

out vec4 FinalColor;

uniform sampler2D Texture;

void main(void)
{
	FinalColor = texture(Texture, TexCoord);
	FinalColor = VertexColor;
}