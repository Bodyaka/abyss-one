#version 150

out vec4 finalColor;
in  vec4 vertexColor;

void main(void)
{
	finalColor = vertexColor;
}