#version 150

// in
in vec3 Position;
in vec3 Normal;
in vec2 TexCoord;

// out
out vec4 FinalColor;

// uniform
uniform sampler2D Texture;
uniform sampler2D RawTexture;

// relative to blur:
uniform int width;
uniform int height;

uniform int mode;

const int maxSmoothCount = 10;
// number of elements in array
uniform int smoothCount;

uniform float weights[maxSmoothCount];
uniform float pixOffsets[maxSmoothCount];

// relative for bloom:
uniform float bloomThreshold;

uniform float Gamma;

// compute grayscale luma part.
float luma(vec3 color)
{
	return 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
}

// computing luma
void firstPass()
{
	vec4 val = texture(RawTexture, TexCoord);
	FinalColor = val * clamp(luma(val.rgb) - bloomThreshold, 0.0, 1.0) * (1.0 / (1.0 - bloomThreshold));
}

// horizontal blur:
void secondPass()
{
	float dy = 1. / height;
	
	vec4 sum = texture(Texture, TexCoord) * weights[0];
	
	int count = min(maxSmoothCount, smoothCount);
	for (int i = 1; i<count; ++i)
	{
		sum += texture(Texture, TexCoord + vec2(0.f, pixOffsets[i])*dy) * weights[i];
		sum += texture(Texture, TexCoord - vec2(0.f, pixOffsets[i])*dy) * weights[i];
	}
	
	FinalColor = sum;
}

// vertical blur:
void thirdPass()
{
	float dx = 1. / width;
	
	vec4 rawColor = texture(RawTexture, TexCoord);
	vec3 correctedColor = pow(rawColor.rgb, vec3(1.f/Gamma));
	vec4 src = vec4(correctedColor, rawColor.a);

	vec4 dest = texture(Texture, TexCoord) * weights[0];
	
	int count = min(maxSmoothCount, smoothCount);
	for (int i = 1; i<count; ++i)
	{
		dest += texture(Texture, TexCoord + vec2(pixOffsets[i], 0.f)*dx) * weights[i];
		dest += texture(Texture, TexCoord - vec2(pixOffsets[i], 0.f)*dx) * weights[i];
	}
	
	/*
	vec4 rawColor = val + sum;
	vec3 correctedColor = pow(rawColor.rgb, vec3(1.f/Gamma));

	FinalColor = vec4(correctedColor, rawColor.a);
	*/

	FinalColor = src + dest - (src * dest);
}


// main function
void main(void)
{
	//FinalColor = vec4(1.0f, 0.f, 0.f, 0.f);

	if (mode == 0)
		firstPass();
	if (mode == 1)
		secondPass();
	if (mode == 2)
	{
		thirdPass();
	}
}
