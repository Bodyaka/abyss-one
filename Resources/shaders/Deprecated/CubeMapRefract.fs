#version 150
//															struct

//in
in vec3 ReflectDir;
in vec3 RefractDir;
in float FresnelFactor;

// out
out vec4 FinalColor;

//														   uniforms
uniform vec3 eye;
uniform int isSkyBox;
uniform samplerCube cubeMap;

//														Functions:
//-------------------------------------------------------------------------------------------
void main(void)
{
	// maybe, it is not needed to normalize
	vec3 reflect_dir = -normalize(ReflectDir);
		
	if(isSkyBox >= 1)
	{
		vec4 cubeMapColor = texture(cubeMap, reflect_dir);
		FinalColor = cubeMapColor;
	}
	else
	{
		vec3 refract_dir = -normalize(RefractDir);
		vec4 cubeReflectColor = texture(cubeMap, reflect_dir);
		vec4 cubeRefractColor = texture(cubeMap, refract_dir);

		FinalColor = mix(cubeRefractColor, cubeReflectColor, FresnelFactor);
	}
}