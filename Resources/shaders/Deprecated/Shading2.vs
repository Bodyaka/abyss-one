#version 150

// WARINING: subroutines was presented first only in 4.0 glsl

//														   in data:
in vec3 vertex;
in vec3 normal;
in vec2 texel;

//													       out data
out vec3 Position;
out vec3 Normal;
out vec2 TexCoord;

uniform mat4x4 WVPMatrix; 
uniform mat4x4 NormalMatrix; 
uniform mat4x4 ModelMatrix;

//														  functions:
// map values, such as vertex and normals into coord that is needed
void mapValues(out vec3 v, out vec3 n)
{
	v = mat3(ModelMatrix) * vertex;
	n = normalize(mat3(NormalMatrix) * normal);
}

//														main
void main(void)
{
	// Preparing varying values:
	TexCoord = texel;
	vec4 rawPosition = ModelMatrix * vec4(vertex, 1.0f);
	Position = (rawPosition / rawPosition.w).xyz;
	Normal = normalize(mat3(NormalMatrix) * normal);
	
	gl_Position = WVPMatrix * vec4(vertex, 1.0);
}
