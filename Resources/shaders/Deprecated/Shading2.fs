#version 150

//in
in vec3 Position;
in vec3 Normal;
in vec2 TexCoord;

#mine include("Helper\\lights.inl")

// Material struct:
struct Material
{
	vec4 ambient;		// ambient reflection (material)
	vec4 diffuse;		// diffuse reflection
	vec4 specular;		// specular reflection
	float shiness;		// shiness (from 1. to 200.)
};
uniform Material material;

// out
out vec4 finalColor;

//-------------------------------------------------------------------------------------------
// Ads type in wolrd coordinates:
vec4 ads1(in int index, in vec3 vertex, in vec3 normal)
{
	
	vec4 lightPosition;
	vec4 lightIntensity;

	// AMD hack:
	if (index == 0)
	{
		lightPosition = lights[0].position;
		lightIntensity = lights[0].intensity;
	} 
	else if (index == 1)
	{
		lightPosition = lights[1].position;
		lightIntensity = lights[1].intensity;

	}
	else if (index == 2)
	{
		lightPosition = lights[2].position;
		lightIntensity = lights[2].intensity;

	}
	else if (index == 3)
	{
		lightPosition = lights[3].position;
		lightIntensity = lights[3].intensity;

	}
	else if (index == 4)
	{
		lightPosition = lights[4].position;
		lightIntensity = lights[4].intensity;

	}

return vec4(normal, 1.f);
	// Computation of diffuse/specular light:
	vec3 l;
	
	if (lightPosition.w == 0.0f)
		l = normalize(vec3(lightPosition));
	else
		l = normalize(vec3(lightPosition) - vertex);

	vec3 v = normalize(eyeLocation - vertex);

	vec3 r = reflect(-l, normal);
	
	vec4 ambient = material.ambient;
	float diff = max(dot(normal, l), 0.0);
	vec4 diffuse = material.diffuse * diff;
	vec4 specular = vec4(0.0);
	if (diff > 0.0)
		specular = material.specular * pow(max(dot(r, v), 0.0), material.shiness);
	
	return lightIntensity * (ambient + diffuse + specular);
}

void main(void)
{
	vec3 n = normalize(Normal);
	vec3 v = Position;

	int maxLights = min(enabledLightsCount, maxLightsCount);
	vec4 accumulator = vec4(1.f);
	for (int i = 0; i<maxLights; ++i)
		accumulator *= ads1(i, v, n);
	
	finalColor = accumulator;
}
