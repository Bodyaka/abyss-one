#version 150

// in
in vec3 vertex;

// uniforms
uniform mat4x4 WVPMatrix; 

void main(void)
{
	gl_Position = WVPMatrix * vec4(vertex, 1.0);

}