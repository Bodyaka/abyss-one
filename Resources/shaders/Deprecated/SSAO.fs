#version 150

in  vec2 TexCoord;
out vec4 finalColor;

uniform sampler2D PositionsDepthTexture;
uniform sampler2D NormalsTexture;
uniform sampler2D ColorsTexture;
uniform sampler2D ImageTexture; // dummy.

// Provide radius of sampling.
uniform float samplingRadius;
uniform mat4x4 CurrentViewProjectionalMatrixInverse;



vec2 fromNormalizedToTextureCoordinates(in vec2 coord)
{
	return vec2((coord.x + 1) / 2., 1 - (coord.y + 1) / 2.);
}

void main(void)
{
	vec4 texColor = texture(ColorsTexture, TexCoord);
	vec4 normalData = texture(NormalsTexture, TexCoord);
	vec4 vertexData = texture(PositionsDepthTexture, TexCoord);

	// computing current world position.
	vec4 H = vec4(TexCoord.x * 2 - 1., (1 - TexCoord.y) * 2 - 1, vertexData.w, 1.f);
	vec4 currentWorldPos = CurrentViewProjectionalMatrixInverse * H;
	currentWorldPos /= currentWorldPos.w;

	// computing previous position in clip coordinates
	vec4 previousClipPos = PreviousViewProjectionalMatrix * currentWorldPos;
	previousClipPos /= previousClipPos.w;

	vec4 currentClipPos = H;

	// Calculating "velocity" texture coordinates:
	vec2 currentClipPosTexCoord  = fromNormalizedToTextureCoordinates(currentClipPos.xy);
	vec2 previousClipPosTexCoord = fromNormalizedToTextureCoordinates(previousClipPos.xy);
	
	// Calculating velocity:
	vec2 velocity = (currentClipPosTexCoord.xy - previousClipPosTexCoord.xy) / 10;
	
	// Sampling:
	vec2 currentTexCoord = TexCoord;
	vec4 currentColor = texColor;
	currentTexCoord += velocity;
	
	int numberOfSamples = 15;

	// actual sampling of motion blur
	int index = 1;
	for (index = 1; index<numberOfSamples; ++index, currentTexCoord += velocity)
	{
		if (currentTexCoord.x < 0 || currentTexCoord.x > 1 || 
			currentTexCoord.y < 0 || currentTexCoord.y > 1)
				break;

		currentColor += texture(ColorsTexture, currentTexCoord);
	}
	
	currentColor /= index;
	
	// passing final image result.
	finalColor = currentColor;
	
}