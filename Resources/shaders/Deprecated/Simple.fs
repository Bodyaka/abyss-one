#version 150

out vec4 finalColor;

uniform vec4 color;

void main(void)
{
	finalColor = color;
}