#version 150

// WARINING: subroutines was presented first only in 4.0 glsl
//															struct
in vec3 vertex;
in vec3 normal;

//													       out data
out vec3 ReflectDir;

uniform vec3 eye;

uniform int isSkyBox;

uniform mat4x4 WVPMatrix; 
uniform mat4x4 NormalMatrix; 
uniform mat4x4 ModelMatrix;


//														  functions:
// Input vec3 vertex multiplies by WVP and divied each element by w coordinate:
vec4 WVP_PASS(in vec4 i_v)
{
	vec4 o_v = vec4(WVPMatrix * i_v);
	return  o_v / o_v.w;
}


//														main
void main(void)
{

	if (isSkyBox >= 1)
	{
		ReflectDir = vertex;

		gl_Position = WVP_PASS(vec4(vertex, 1.0));
	}
	else
	{
		vec3 worldPos = vec3(ModelMatrix * vec4(vertex, 1.f));
		vec3 worldNormal = vec3(NormalMatrix * vec4(normal, 0.f));
		vec3 viewDir = normalize(eye - worldPos);
		ReflectDir = reflect(-viewDir, worldNormal);

		gl_Position = WVP_PASS(vec4(vertex, 1.0));
	}
	
	
}
