#version 150

// in data
in vec3 vertex;
in vec3 normal;
in vec2 texel;

// out data
out vec3 Position;
out vec3 Normal;
out vec2 TexCoord;
out vec4 ShadowCoord;
out float Depth;

// uniform
uniform mat4x4 WVPMatrix; 
uniform mat4x4 ShadowMatrix;

// Main
void main(void)
{
	TexCoord = texel;

	ShadowCoord = ShadowMatrix * vec4(vertex, 1.0); 

	gl_Position = WVPMatrix * vec4(vertex, 1.0);
	Depth = gl_Position.z;

}
