#version 150

// in
in vec2 TexCoord;

// out
out vec4 FinalColor;

// uniform
uniform sampler2D Texture;
uniform sampler1D LUT;

float luma(vec3 color)
{
	return 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
}

// main function
void main(void)
{
	vec4 color = texture(Texture, TexCoord);

	FinalColor = vec4(1.0f);

	color.r = clamp(color.r, 0.f, 0.99f);
	color.g = clamp(color.g, 0.f, 0.99f);
	color.b = clamp(color.b, 0.f, 0.99f);
	
	FinalColor = vec4(1.0f);
	FinalColor.r = texture1D(LUT, color.r).r;
	FinalColor.g = texture1D(LUT, color.g).g;
	FinalColor.b = texture1D(LUT, color.b).b;
}
