#version 150

// in data
in vec3 vertex;
in vec2 texel;

// out data
out vec2 TexCoord;

// Main
void main(void)
{
	TexCoord = texel;
	gl_Position = vec4(vertex, 1.0);
}
