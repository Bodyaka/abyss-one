#version 150

// in
in vec2 TexCoord;

// out
out vec4 FinalColor;

// uniform
uniform sampler2D Texture;
uniform int   width;
uniform int   height;
uniform float threshold;

uniform vec4 edgeColor;
uniform vec4 backColor;



// compute grayscale luma part.
float luma(vec3 color)
{
	return 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
}

// main function
void main(void)
{
	vec4 color = texture(Texture, TexCoord);

	float dx = 1.f / width;
	float dy = 1.f / height;

	float s00 = luma(texture(Texture, TexCoord + vec2(-dx, -dy)).rgb);
	float s01 = luma(texture(Texture, TexCoord + vec2(0.f, -dy)).rgb);
	float s02 = luma(texture(Texture, TexCoord + vec2(dx, -dy)).rgb);

	float s10 = luma(texture(Texture, TexCoord + vec2(-dx, 0.f)).rgb);
	float s12 = luma(texture(Texture, TexCoord + vec2(dx,  0.f)).rgb);

	float s20 = luma(texture(Texture, TexCoord + vec2(-dx, dy)).rgb);
	float s21 = luma(texture(Texture, TexCoord + vec2(0.f, dy)).rgb);
	float s22 = luma(texture(Texture, TexCoord + vec2(dx,  dy)).rgb);

	float sx = s02 + 2 * s12 + s22 - (s00 + 2 * s10 + s20);
	float sy = s00 + 2 * s01 + s02 - (s20 + 2 * s21 + s22);

	float g = sx * sx + sy * sy;
	if (g > threshold)
		FinalColor = edgeColor;
	else
		FinalColor = backColor;

	FinalColor = FinalColor * color;
}
