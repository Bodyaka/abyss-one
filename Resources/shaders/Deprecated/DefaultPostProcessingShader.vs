#version 150

// in
in vec3 vertex;
in vec2 texel;

out vec2 TexCoord;

void main(void)
{
	TexCoord = texel;
	gl_Position = vec4(vertex, 1.f);
}