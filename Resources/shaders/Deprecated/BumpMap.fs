#version 150

//															struct

struct MaterialInfo
{
	vec3 ambient;		// ambient reflection (material)
	vec3 diffuse;		// diffuse reflection
	vec3 specular;		// specular reflection
	float shiness;		// shiness (from 1. to 200.)
};

struct LightInfo
{
	vec4 position;		// light position
	vec3 intensity;		// light intensity
};


//in
in vec2      TexCoord;
in vec3      LightDir;
in vec3      ViewDir;

in mat3 TBN;
// out
out vec4 finalColor;

//														   uniforms

uniform MaterialInfo material;
uniform LightInfo light;

uniform sampler2D Texture;
uniform sampler2D BumpTexture;


//														Functions:
//-------------------------------------------------------------------------------------------
// n - normal, l - light dir, v - view dir
vec4 ads(in vec3 n, in vec3 l, in vec3 v)
{
	float diff   = max(dot(l, n), 0.0);
	vec3 diffuse = material.diffuse * diff;
	vec3 specular = vec3(0.0);
	if (diff > 0.f)
	{
		vec3 r = reflect(-l, n);
		specular = material.specular * pow(max(dot(r, v), 0.0), material.shiness);
	}
	
	vec3 ambient = material.ambient;
	return vec4(ambient + diffuse + specular, 1.0f);
}
//-------------------------------------------------------------------------------------------
void main(void)
{
	vec3 Normal = texture(BumpTexture, TexCoord).xyz;
	
	Normal = normalize(2 * Normal - 1);
	vec3 l = normalize(LightDir);
	vec3 v = normalize(ViewDir);
	
	vec4 texColor = texture(Texture, TexCoord);
	finalColor = texColor * ads(Normal, l, v);
}