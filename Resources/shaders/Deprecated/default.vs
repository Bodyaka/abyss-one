#version 150

// in
in vec3 vertex;
// out
out vec4 vertexColor;

// uniforms
uniform mat4x4 WVPMatrix; 
uniform vec3 solidColor;

void main(void)
{

	vertexColor = vec4(solidColor, 1.f);
	vec4 v = vec4(vertex, 1.0);
	gl_Position = WVPMatrix * v;

}