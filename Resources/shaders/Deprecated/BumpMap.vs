#version 150

// WARINING: subroutines was presented first only in 4.0 glsl
//															struct
struct LightInfo
{
	vec4 position;		// light position
	vec3 intensity;		// light intensity
};

//														   in data:
in vec3 vertex;
in vec3 normal;
in vec2 texel;
in vec4 tangent;

//													       out data
out vec2 TexCoord;
out vec3 LightDir;
out vec3 ViewDir;

//															const
const int maxLights = 5;

//														   uniforms
uniform vec3 eye;
	// for lights:
uniform int numOfLights;
uniform LightInfo light;
	// matrices
uniform mat4x4 WVPMatrix; 
uniform mat4x4 ViewMatrix;
uniform mat4x4 NormalMatrix; 
uniform mat4x4 ModelMatrix;


//														  functions:
// map values, such as vertex and normals into coord that is needed
void mapValues(out vec3 v, out vec3 n)
{
	v = mat3(ModelMatrix) * vertex;
	n = normalize(mat3(NormalMatrix) * normal);
}

//														main
void main(void)
{

	vec3 Position;
	// sent values from vs to fs shader:

	vec3 Normal   = mat3(NormalMatrix) * normalize(normal);
	vec3 Tang     = mat3(NormalMatrix) * normalize(vec3(tangent));
	vec3 Binormal = normalize(cross(Normal, Tang));

	mat3 TangentSpace = (mat3(Tang, Binormal, Normal));
	
	// map position
	Position = (ViewMatrix * ModelMatrix * vec4(vertex, 1.0f)).xyz;
	
	// light in eye space:
	vec3 lightViewSpace = (ViewMatrix * light.position).xyz;
	vec3 eyeViewSpace   = vec3(0.f);

								// passing out data	
	TexCoord = texel;
	LightDir = normalize(TangentSpace * (lightViewSpace - Position));
	ViewDir = normalize(TangentSpace * (eyeViewSpace - Position));
	
								// passing vertex data.
	gl_Position = WVPMatrix * vec4(vertex, 1.0);
}
