#version 150

// in
in vec3 Position;
in vec3 Normal;
in vec2 TexCoord;

// out
out vec4 FinalColor;

// uniform
uniform sampler2D Texture;

uniform int width;
uniform int height;

// 0 - first pass
// 1 - second one
// 2 - third one
uniform int mode;

const int maxSmoothCount = 100;
// number of elements in array
uniform int smoothCount;

uniform float weights[maxSmoothCount];
uniform float pixOffsets[maxSmoothCount];

// just rendering texture (first pass)
void firstPass()
{
	FinalColor = texture(Texture, TexCoord);
}

// horizontal blur (second pass)
void secondPass()
{
	float dy = 1. / height;
	
	vec4 sum = texture(Texture, TexCoord) * weights[0];
	
	int count = min(maxSmoothCount, smoothCount);
	for (int i = 1; i<count; ++i)
	{
		sum += texture(Texture, TexCoord + vec2(0.f, pixOffsets[i])*dy) * weights[i];
		sum += texture(Texture, TexCoord - vec2(0.f, pixOffsets[i])*dy) * weights[i];
	}
	
	FinalColor = sum;
}

// vertical blur (third pass)
void thirdPass()
{
	float dx = 1. / width;
	
	vec4 sum = texture(Texture, TexCoord) * weights[0];
	
	int count = min(maxSmoothCount, smoothCount);
	for (int i = 1; i<count; ++i)
	{
		sum += texture(Texture, TexCoord + vec2(pixOffsets[i], 0.f)*dx) * weights[i];
		sum += texture(Texture, TexCoord - vec2(pixOffsets[i], 0.f)*dx) * weights[i];
	}
	
	FinalColor = sum;
}

// main function
void main(void)
{
	//FinalColor = vec4(1.0f, 0.f, 0.f, 0.f);

	if (mode == 0)
		secondPass();
	if (mode == 1)
		thirdPass();
}
