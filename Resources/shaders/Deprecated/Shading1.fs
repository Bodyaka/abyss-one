#version 150

//in
in vec3 WorldVertex;
in vec3 Normal;
in vec2 TexCoord;

#mine include("Helper\\lightsUniform.inl")

// Material struct:
struct Material
{
	vec4 ambient;		// ambient reflection (material)
	vec4 diffuse;		// diffuse reflection
	vec4 specular;		// specular reflection
	float shiness;		// shiness (from 1. to 200.)
};

uniform mat4x4 ViewMatrix;
uniform Material material;

out vec4 vertexData;  // World position and depth data.
out vec4 normalData;  // Normals
out vec4 colorData;	  // Final Shader color

//-------------------------------------------------------------------------------------------
// Ads type in wolrd coordinates:
vec4 ads1(in int index, in vec3 vertex, in vec3 normal)
{
	
	vec4 lightPosition = lightPositions[index];
	vec4 lightIntensity = lightIntensities[index];

	// Computation of diffuse/specular light:
	vec3 l;
		
	lightPosition = ViewMatrix * lightPosition;
	lightPosition /= lightPosition.w;
	
	if (lightPosition.w == 0.0f)
		l = normalize(vec3(lightPosition));
	else
		l = normalize(vec3(lightPosition) - vertex);

	vec3 v = normalize(-vertex);

	vec3 r = reflect(-l, normal);
	
	vec4 ambient = material.ambient;
	float diff = max(dot(normal, l), 0.0);
	vec4 diffuse = material.diffuse * diff;
	vec4 specular = vec4(0.0);
	if (diff > 0.0)
		specular = material.specular * pow(max(dot(r, v), 0.0), material.shiness);
	
	return lightIntensity * (ambient + diffuse + specular);
}

void main(void)
{
	vec4 viewVertex = ViewMatrix * vec4(WorldVertex, 1.f);
	viewVertex = viewVertex / viewVertex.w;
	vec3 n = normalize(Normal);
	vec3 v = viewVertex.xyz;

	int maxLights = min(enabledLightsCount, maxLightsCount);
	vec4 accumulator = vec4(1.f);
	for (int i = 0; i<maxLights; ++i)
		accumulator *= ads1(i, v, n);
		
	float depthValue = gl_FragCoord.z;
	
	vertexData = vec4(WorldVertex, depthValue);
	normalData = vec4(n, 1.f);
	colorData = accumulator;
}
