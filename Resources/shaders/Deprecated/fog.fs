#version 150

//in
in vec3 Position;
in vec3 Normal;
in mat4x4 modelView;

// out
out vec4 finalColor;

//															struct
struct LightInfo
{
	vec4 position;		// light position
	vec3 intensity;		// light intensity
};

struct MaterialInfo
{
	vec3 ambient;		// ambient reflection (material)
	vec3 diffuse;		// diffuse reflection
	vec3 specular;		// specular reflection
	float shiness;		// shiness (from 1. to 200.)
};

struct ADSInfo
{
	vec3 ambient;
	vec3 diffuse;
	vec3 shiness;
};
//															const
const int maxLights = 5;
//														   uniforms

uniform vec3 eye;
uniform MaterialInfo material;
uniform int adsType;
uniform int toUseCartoonShading;

// for lights:
uniform int numOfLights;
uniform LightInfo light[maxLights];

// for cartoon (if using):
const int levels = 10;
const float cartoonFactor = 1./levels;


//-------------------------------------------------------------------------------------------
vec4 ads(in int index, in vec3 mappedVertex, in vec3 n)
{
	vec3 l;
	if (light[index].position.w == 0.0f)
		l = normalize(mat3(modelView) * vec3(light[index].position));
	else
		l = normalize(mat3(modelView) * vec3(light[index].position) - mappedVertex);

	vec3 v = normalize(mat3(modelView) * eye - mappedVertex);

	vec3 r;
	
	if (adsType == 1)
		r = reflect(-l, n);	// reflected vector
	else
		r = normalize((v + l));
		

	vec3 ambient = material.ambient;
	float diff = max(dot(n, l), 0.0);
	vec3 diffuse = material.diffuse * diff;
	vec3 specular = vec3(0.0);
	if (diff > 0.0)
		specular = material.specular * pow(max(dot(r, v), 0.0), material.shiness);

	return vec4(light[index].intensity * (ambient + diffuse + specular), 1.0f);
}
//-------------------------------------------------------------------------------------------
vec4 cartoonEffect(in int index, in vec3 v, in vec3 n)
{
	vec3 l;
	if (light[index].position.w == 0.0f)
		l = normalize(mat3(modelView) * vec3(light[index].position));
	else
		l = normalize(mat3(modelView) * vec3(light[index].position) - v);

	float cosine = max(dot(n, l), 0.0);
	float cartoonEffect = floor(cosine * levels + 1) * cartoonFactor;

	vec3 ambient = material.ambient;
	vec3 diffuse = material.diffuse * cartoonEffect;

	return vec4(light[index].intensity * (ambient + diffuse), 1.0f);
}
//-------------------------------------------------------------------------------------------
void main(void)
{

	vec3 n = normalize(Normal);
	vec3 v = Position;

	finalColor = vec4(0.f);
	int num = min(numOfLights, maxLights);

	if (gl_FrontFacing)
	{
		for (int i = 0; i<num; ++i)
		{
			if (toUseCartoonShading == 1)
				finalColor += cartoonEffect(i, v, n);
			else
				finalColor += ads(i, v, n);
		}
	}
	else
		finalColor = vec4(0.0f);
	
}