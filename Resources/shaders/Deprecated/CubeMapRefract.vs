#version 150

// WARINING: subroutines was presented first only in 4.0 glsl
//															struct
struct MaterialInfo
{
	vec3 ambient;		// ambient reflection (material)
	vec3 diffuse;		// diffuse reflection
	vec3 specular;		// specular reflection
	float shiness;		// shiness (from 1. to 200.)
	float reflectFactor;
	float refractFactor;
};

// in
in vec3 vertex;
in vec3 normal;

//													       out data
out vec3  ReflectDir;
out vec3  RefractDir;
out float FresnelFactor;

uniform vec3 eye;

uniform float FresnelBias;
uniform float FresnelPower;

uniform int isSkyBox;

uniform mat4x4 WVPMatrix; 
uniform mat4x4 NormalMatrix; 
uniform mat4x4 ModelMatrix;

uniform MaterialInfo material;


//														  functions:
// Input vec3 vertex multiplies by WVP and divied each element by w coordinate:
vec4 WVP_PASS(in vec4 i_v)
{
	vec4 o_v = vec4(WVPMatrix * i_v);
	return  o_v / o_v.w;
}


//														main
void main(void)
{

	if (isSkyBox >= 1)
	{
		ReflectDir = vertex;

		gl_Position = WVP_PASS(vec4(vertex, 1.0));
	}
	else
	{
		vec3 worldPos    = vec3(ModelMatrix * vec4(vertex, 1.f));
		vec3 worldNormal = vec3(NormalMatrix * vec4(normal, 0.f));
		vec3 viewDir     = normalize(worldPos - eye);
		
		ReflectDir = reflect(viewDir, worldNormal);
		RefractDir = refract(viewDir, worldNormal, 0.7f);

		FresnelFactor = FresnelBias + (1.f - FresnelBias) * pow((1.f + dot(viewDir, worldNormal)), FresnelPower);
		gl_Position = WVP_PASS(vec4(vertex, 1.0));
	}
	
	
}
