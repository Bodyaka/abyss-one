#version 150

// in
in vec2 TexCoord;

// out
out vec4 FinalColor;

// uniform
uniform sampler2D Texture;

// main function
void main(void)
{
	FinalColor = texture(Texture, TexCoord);
}
