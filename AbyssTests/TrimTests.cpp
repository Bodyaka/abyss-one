// This test are testing trimming functionality (see https://en.wikipedia.org/wiki/Trimming_(computer_programming) )
// Trimming tests were created for getting familiar with basics of Google Testing library
#include <gtest/gtest.h>
#include "AbyssUtilities.h"


#define ABYSS_EXPECT_LTRIM(i_str, exp) \
{ \
	auto str = Abyss::Utilities::ltrimString(i_str); \
	EXPECT_EQ(str, exp); \
}

#define ABYSS_EXPECT_RTRIM(i_str, exp) \
{ \
	auto str = Abyss::Utilities::rtrimString(i_str); \
	EXPECT_EQ(str, exp); \
}

#define ABYSS_EXPECT_TRIM(i_str, exp) \
{ \
	auto str = Abyss::Utilities::trimString(i_str); \
	EXPECT_EQ(str, exp); \
}


TEST(TrimTests, LtrimTest)
{
	ABYSS_EXPECT_LTRIM(" ltest", "ltest");
	ABYSS_EXPECT_LTRIM("       ltest", "ltest");
	ABYSS_EXPECT_LTRIM("       l t e s t", "l t e s t");
	ABYSS_EXPECT_LTRIM("ltest", "ltest");
	ABYSS_EXPECT_LTRIM("     ", "");
	ABYSS_EXPECT_LTRIM(" ltest ", "ltest ");
}

TEST(TrimTests, RtrimTest)
{
	ABYSS_EXPECT_RTRIM("rtest ", "rtest");
	ABYSS_EXPECT_RTRIM("rtest       ", "rtest");
	ABYSS_EXPECT_RTRIM("r t e s t       ", "r t e s t");
	ABYSS_EXPECT_RTRIM("rtest", "rtest");
	ABYSS_EXPECT_RTRIM("     ", "");
	ABYSS_EXPECT_RTRIM(" rtest ", " rtest");
}

TEST(TrimTests, TrimTest)
{
	ABYSS_EXPECT_TRIM("test ", "test");
	ABYSS_EXPECT_TRIM("test       ", "test");
	ABYSS_EXPECT_TRIM("     t e s t       ", "t e s t");
	ABYSS_EXPECT_TRIM("test", "test");
	ABYSS_EXPECT_TRIM("     ", "");
	ABYSS_EXPECT_TRIM("", "");
	ABYSS_EXPECT_TRIM(" '1       2 3 4 5 6 7 8   9 \"  ", "'1       2 3 4 5 6 7 8   9 \"");
	ABYSS_EXPECT_TRIM(" test ", "test");
}