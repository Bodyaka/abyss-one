#pragma once

#include <gtest/gtest.h>

namespace Abyss
{
	class Window;
	class Application;
}

class TestEnvironment
	: public testing::Environment
{
public:
	TestEnvironment(int argc, char** argv);

	virtual ~TestEnvironment() {}
	// Override this to define how to set up the environment.
	virtual void SetUp() override;
	// Override this to define how to tear down the environment.
	virtual void TearDown() override;

	static Abyss::Window* getWindow();
	static Abyss::Application* getApplication();


private:
	int m_argc;
	char** m_argv;

	static Abyss::Application* m_application;
};