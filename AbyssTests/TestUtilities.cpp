#include "TestUtilities.h"

#include "SFML/Graphics/Image.hpp"
#include <SFML/Window/Event.hpp>

#include "AbyssRendering.h"
#include "TestEnvironment.h"
#include "AbyssApplication.h"
#include "AbyssApplicationContext.h"
#include "AbyssWindow.h"


namespace TestUtilities
{

	bool resizeWindow(const sf::Vector2u& newSize)
	{
		if (TestEnvironment::getWindow()->setActive()) {
			TestEnvironment::getWindow()->setSize(newSize);

			// TODO: maybe need to provide some forever-loop security:
			while (true)
			{
				sf::Event event;
				TestEnvironment::getWindow()->waitEvent(event);
				if (event.type == sf::Event::Resized)
					return TestEnvironment::getWindow()->getSize() == newSize;
			}
		}
		else {
			assert(false && "Cannot activate current window!");
		}

		return false;
	}

	void performSingleRenderOperation(sf::Image* image)
	{
		if (!TestEnvironment::getApplication()) {
			assert(false);
			return;
		}

		auto applicationContext = Abyss::ApplicationContext::getSharedApplicationContext();
		auto window = applicationContext->getWindow();

		window->clear(sf::Color(0, 0, 0, 255));
		TestEnvironment::getApplication()->render();
		window->display();

		if (image) {
			*image = window->getScreenshot();
		}
	}

	bool compareImages(const sf::Image& image1, const sf::Image& image2)
	{
		if (&image1 == &image2)
			return true;

		if (image1.getSize() != image2.getSize())
			return false;

		auto size = image1.getSize();
		for (size_t i = 0; i < size.x; ++i) 
		{
			for (size_t j = 0; j < size.y; ++j)
			{
				sf::Color color1 = image1.getPixel(i, j);
				sf::Color color2 = image2.getPixel(i, j);

				if (color1 != color2)
					return false;
			}
		}

		return true;
	}
}