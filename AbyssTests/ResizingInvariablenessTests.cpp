// This test are used for testing invariability of engine when resizing is performed
// Trimming tests were created for getting familiar with basics of Google Testing library
#include <gtest/gtest.h>
#include "AbyssUtilities.h"

#include "TestEnvironment.h"
#include "AbyssApplication.h"
#include "AbyssLight.h"
#include "AbyssMesh.h"
#include "AbyssMeshRenderable.h"
#include "AbyssSceneObject.h"
#include "AbyssSceneManager.h"
#include "AbyssMeshLoader.h"
#include "AbyssMaterial.h"
#include "AbyssCamera.h"
#include "AbyssWindow.h"

#include "TestUtilities.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Image.hpp>


using namespace Abyss;

// The fixture for testing Hell Knight.
class HellknightFixture 
	: public testing::Test {
protected:

	HellknightFixture() { }
	virtual ~HellknightFixture() { }

	virtual void SetUp() override
	{
		// Create light:
		auto sceneManager = getApplication()->getSceneManager();
		m_light = sceneManager->createLight();
		m_light->setType(LightType::DirectionalLight);
		m_light->setAttenutation(1.f, 0.8f, 0.5f);
		m_light->setDirection(Vector3D(0.f, 0.f, -1.f));

		// Create HellKnight mesh and Renderable/Scene objects:
		auto mesh = MeshLoader::getDefaultLoader()->load(Abyss::Utilities::getExecutableDirectoryPath() + "meshes/hell_knight/hellknight.md5mesh");

		auto pos = mesh->getAABB().center();
		AABB hellKnightAABB = mesh->getAABB();
		hellKnightAABB.extend(Vector3D(hellKnightAABB.width() / 2, 0.f, hellKnightAABB.depth() / 2));
		Vector3D leftTopFar = hellKnightAABB.leftTopFar();
		Vector3D rightBottomFar = hellKnightAABB.rightTopNear();


		// Creating and positoning Hell Knight's SSAO plane:
		Vector3D ssaoRectOffset = Vector3D(0.f, hellKnightAABB.height() / 5, 0.f);
		auto ssaoRect = Mesh::createRect("HellKnighter_SSAO_plane",
			hellKnightAABB.leftTopNear() - ssaoRectOffset,
			hellKnightAABB.leftTopFar() - ssaoRectOffset,
			hellKnightAABB.rightTopFar() - ssaoRectOffset,
			hellKnightAABB.rightTopNear() - ssaoRectOffset);

		auto ssaoRectRenderable = MeshRenderable::create("HellKnighterSSAORect", ssaoRect);

		// Creating actual Renderable for Hell Knight
		auto renderable = MeshRenderable::create("HellKnighter", mesh);
		renderable->setBoundingBoxEnabled(true);
		auto material = renderable->getMaterial();
		material->setShiness(100);
		renderable->setLightingEnabled(true);
		m_hellKnight = sceneManager->createSceneObject("HellKnightScenObject");
		m_hellKnight->addRenderableObject(renderable);
		m_hellKnight->addRenderableObject(ssaoRectRenderable);
		m_hellKnight->setPosition(Vector3D(-100, 0, 100));
		m_hellKnight->setScale(0.2f);

		// Then, need to zoom camera on hell knight object and perform post-setup operations:
		auto camera = sceneManager->getCamera();
		camera->focusOnObject(m_hellKnight.get());
		m_light->setPosition(camera->getEyePosition());
	}

	virtual void TearDown() {
		auto sceneManager = getApplication()->getSceneManager();
		sceneManager->deleteLight(m_light->getName());
		sceneManager->removeSceneObject(m_hellKnight);

		m_hellKnight = nullptr;
	}


	Abyss::Application* getApplication() const
	{
		return TestEnvironment::getApplication();
	}

	Abyss::LightWeakPtr m_light;
	Abyss::SceneObjectPtr m_hellKnight;
};

TEST_F(HellknightFixture, CompareImagesAfterResize)
{
	// Save original image, which will be used for comparison:
	sf::Image imageBefore;
	TestUtilities::performSingleRenderOperation(&imageBefore);
	//ASSERT_EQ(imageBefore.saveToFile(Utilities::getExecutableDirectoryPath() + "imageBefore.png"), true);

	auto oldSize = TestEnvironment::getWindow()->getSize();

	// Shrink image in size, perform additional render operation to make sure that newly created image is correct:
	auto newSize = sf::Vector2u(oldSize.x / 4, oldSize.y / 4);
	ASSERT_EQ(TestUtilities::resizeWindow(newSize), true);

	sf::Image imageIntermediate;
	TestUtilities::performSingleRenderOperation(&imageIntermediate);
	ASSERT_EQ(imageIntermediate.getSize(), newSize);

	// Revert window back to original size:
	ASSERT_EQ(TestUtilities::resizeWindow(oldSize), true);
	sf::Image imageAfter;
	TestUtilities::performSingleRenderOperation(&imageAfter);
	//ASSERT_EQ(imageAfter.saveToFile(Utilities::getExecutableDirectoryPath() + "imageAfter.png"), true);

	// Final test to check whether the images before and after resizing are still the same:
	ASSERT_EQ(TestUtilities::compareImages(imageBefore, imageAfter), true);
}