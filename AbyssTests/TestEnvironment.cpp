#include "TestEnvironment.h"
#include "Abyss.h"
#include "AbyssApplication.h"
#include "AbyssApplicationContext.h"
#include "AbyssAssimpLoader.h"
#include "AbyssWindow.h"

Abyss::Application* TestEnvironment::m_application = nullptr;

TestEnvironment::TestEnvironment(int argc, char** argv)
	: m_argc(argc)
	, m_argv(argv)
{
	}

void TestEnvironment::SetUp()
{
	srand(time(nullptr));

	if (m_application) 
		delete m_application;

	Abyss::Application::CreateOptions createOptions = { m_argc, m_argv, "Application", 0, 0, 1366, 768 };
	m_application = new Abyss::Application(createOptions);
	m_application->makeCurrent();

	Abyss::AssimpLoader::makeAsDefaultLoader();
	Abyss::ApplicationContext::getSharedApplicationContext()->polish();
}

void TestEnvironment::TearDown()
{
	delete m_application;
}

Abyss::Application* TestEnvironment::getApplication()
{
	return m_application;
}

Abyss::Window* TestEnvironment::getWindow()
{
	return Abyss::ApplicationContext::getSharedApplicationContext()->getWindow();
}
