#include <gtest/gtest.h>
#include "AbyssApplication.h"

#include "TestEnvironment.h"

int main(int argc, char * argv[])
{
	testing::InitGoogleTest(&argc, argv);

	TestEnvironment* environment = new TestEnvironment(argc, argv);
	testing::AddGlobalTestEnvironment(environment);

	RUN_ALL_TESTS();
	return 0;
}