#pragma once

#include <SFML/System/Vector2.hpp>

namespace sf 
{
	class Image;
}

namespace TestUtilities
{
	// Changes the size of window. Return true if new size of window is correctly changed - otherwise returns fasle.
	bool resizeWindow(const sf::Vector2u& newSize);
	void performSingleRenderOperation(sf::Image* image = nullptr);

	bool compareImages(const sf::Image& image1, const sf::Image& image2);
}