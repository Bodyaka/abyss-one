#include "AbyssKeyEvent.h"

ABYSS_BEGIN_NAMESPACE

KeyEvent::TKeyTypeSet KeyEvent::m_sKeys;

//-----------------------------------------------------------------------------------------------
KeyEvent::KeyEvent(KeyEvent::KeyType key, bool isPressed)
	: Event(isPressed ? KeyPressed : KeyReleased)
	, m_key(key)
{
	if (isPressed)
		m_sKeys.insert(key);
	else
		m_sKeys.erase(key);
}
//-----------------------------------------------------------------------------------------------
KeyEvent::~KeyEvent()
{

}
//-----------------------------------------------------------------------------------------------
const KeyEvent::TKeyTypeSet & KeyEvent::getKeys()
{
	return m_sKeys;
}
//-----------------------------------------------------------------------------------------------
KeyEvent::KeyType KeyEvent::fromSfmlKey(int key)
{
	// Abyss keys = SFML keys.
	return KeyType(key);
}
//-----------------------------------------------------------------------------------------------
KeyEventPtr KeyEvent::createKeyPressEvent(KeyType key)
{
	return std::make_shared<KeyEvent>(key, true);
}
//----------------------------------------------------------------------------------------------
KeyEventPtr KeyEvent::createKeyReleaseEvent(KeyType key)
{
	return std::make_shared<KeyEvent>(key, false);
}
//----------------------------------------------------------------------------------------------
bool KeyEvent::isPressed(KeyType key)
{
	return m_sKeys.find(key) != m_sKeys.end();
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE