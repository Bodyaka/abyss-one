#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class Renderer;

class RendererVisitor
{
public:

	typedef const Renderer* const  RendererWeakPtr;
public:

	RendererVisitor(RendererWeakPtr renderer);
	virtual ~RendererVisitor(){};

	virtual void visit(Mesh* mesh);
	virtual RendererWeakPtr getRenderer() const;

protected:

	RendererWeakPtr m_renderer;
};

ABYSS_END_NAMESPACE