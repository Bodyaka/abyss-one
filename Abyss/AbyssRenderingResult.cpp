#include "AbyssRenderingResult.h"
#include "AbyssTexture.h"

ABYSS_BEGIN_NAMESPACE


void RenderingResult::setTexture(const std::string& name, TexturePtr texture)
{
	m_textures[name] = texture;
}

void RenderingResult::removeTexture(const std::string& name)
{
	auto iter = m_textures.find(name);
	if (iter != m_textures.end()) 
	{
		m_textures.erase(iter);
	}
}

void RenderingResult::clearTextures()
{
	m_textures.clear();
}

TexturePtr RenderingResult::getTexture(const std::string& name) const
{
	auto iter = m_textures.find(name);
	if (iter == m_textures.end())
		return nullptr;

	return iter->second;
}

void RenderingResult::clear()
{
	m_textures.clear();
	setGBuffer(nullptr);
}

ABYSS_END_NAMESPACE