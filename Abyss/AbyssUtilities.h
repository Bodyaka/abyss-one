#pragma once

#include <vector>
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

namespace Utilities
{
	// Converts filePath string to system path. 
	String convertFilePathToUnixPath(const String & path);

	// Get current executable file path:
	std::string getExecutableFilePath();
	// Get directory path, where current executable is located:
	std::string getExecutableDirectoryPath();

	// Get directory path of given fileName.
	String getDirectoryPath(const String & path);

	// Split string in vector of strings by given delimiter:
	std::vector<String> split(const String & str, char delim, bool skipEmptyParts = true);

	// TODO: should be template method.
	float getRandomFloat(float begin = 0.f, float end = 1.f);

	// Strings operations:
	std::string ltrimString(const std::string& sourceStr);
	std::string rtrimString(const std::string& sourceStr);
	std::string trimString(const std::string& sourceStr);
	std::vector<std::string> splitString(const std::string& sourceStr, const std::string& delimeter, bool keepEmptyParts = false);

}

ABYSS_END_NAMESPACE