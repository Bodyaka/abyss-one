#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class RenderPass;
class RenderPassManager;

class RenderPassComparator final
{
public:
	RenderPassComparator(RenderPassManager* renderPassManager);

	// No need in changing RenderPass, right?
	RenderPassManager* getManager() const;

	bool operator()(const RenderPass* const first, const RenderPass* const second) const;
	bool operator()(const RenderPass& first, const RenderPass& second) const;
	
private:

	RenderPassManager* m_renderPassManager;
};

// INLINE METHODS
inline RenderPassManager* RenderPassComparator::getManager() const
{	return m_renderPassManager;	}


ABYSS_END_NAMESPACE