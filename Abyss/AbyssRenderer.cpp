#include "AbyssRenderer.h"

#include <algorithm>
#include <exception>

#include "AbyssCamera.h"
#include "AbyssMesh.h"
#include "AbyssRenderableObject.h"
#include "AbyssShaderProgram.h"
#include "AbyssShaderProgramManager.h"
#include "AbyssSceneObject.h"
#include "AbyssSceneManager.h"

#include "AbyssMaterial.h"
#include "AbyssTexture.h"
#include "AbyssRendererVisitor.h"
#include "AbyssRendererExtension.h"

ABYSS_BEGIN_NAMESPACE

namespace
{
	const std::string NORMAL_MATRIX_NAME = "NormalMatrix";
	const std::string WORLD_MATRIX_NAME = "WorldMatrix";
	const std::string VIEW_MATRIX_NAME = "ViewMatrix";
	const std::string WORLD_VIEW_MATRIX_NAME = "WorldViewMatrix";
	const std::string PROJECTION_MATRIX_NAME = "ProjectionMatrix";
	const std::string WORLD_VIEW_PROJECTION_MATRIX_NAME = "WorldViewProjectionMatrix";

}

Renderer::TRenderersContainer Renderer::sRegisteredRenderers;

//----------------------------------------------------------------------------------------------
Renderer::Renderer(const String & shaderProgramName)
	: m_program(nullptr)
{
	m_program = ShaderProgramManager::instance()->getShaderProgram(shaderProgramName);

	sRegisteredRenderers.push_back(this);
}
//----------------------------------------------------------------------------------------------
Renderer::~Renderer()
{
	// Finding renderer and remove it:
	auto iter = std::find(sRegisteredRenderers.begin(), sRegisteredRenderers.end(), this);
	sRegisteredRenderers.erase(iter);
}
//----------------------------------------------------------------------------------------------
void Renderer::render(RenderableObject * const object)
{
	beforeRender(object);

	// Prepare extensions for Rendering:
	for (auto extension : m_extensions)
		extension->prepareForRendering(object);

	performRendering(object);

	// Unprepare extensions from Rendering:
	for (auto extension : m_extensions)
		extension->unprepareFromRendering(object);

	afterRender(object);
}
//----------------------------------------------------------------------------------------------
void Renderer::performRendering(RenderableObject * const object)
{
	object->performRendering(getVisitor());
}
//----------------------------------------------------------------------------------------------
void Renderer::initAllRenderers(SceneManager * manager)
{
	// TODO:
	for (size_t i = 0; i<sRegisteredRenderers.size(); ++i)
		sRegisteredRenderers[i]->init(manager);
}
//----------------------------------------------------------------------------------------------
void Renderer::setupMatrixUniformsValues(const SceneObject * const sceneObject)
{
	if (sceneObject)
	{
		setNormalMatrix(sceneObject->getNormalMatrix());
		setWorldMatrix(sceneObject->getWorldMatrix());
		setViewMatrix(sceneObject->getSceneManager()->getCamera()->getViewMatrix());
		setWorldViewMatrix(sceneObject->getWorldViewMatrix());
		setProjectionMatrix(sceneObject->getSceneManager()->getCamera()->getProjectionMatrix());
		setWorldViewProjectionMatrix(sceneObject->getWorldViewProjectionalMatrix());
	}
	else
	{
		const Matrix4D& viewMatrix = SceneManager::instance()->getCamera()->getViewMatrix();
		const Matrix4D& wvpMatrix = SceneManager::instance()->getCamera()->getProjectionViewMatrix();
		const Matrix4D& projectionMatrix = SceneManager::instance()->getCamera()->getProjectionMatrix();

		setNormalMatrix(cIdentityMatrix3D);
		setWorldMatrix(cIdentityMatrix4D);
		setWorldMatrix(sceneObject->getWorldMatrix());
		setViewMatrix(viewMatrix);
		setWorldViewMatrix(viewMatrix);
		setProjectionMatrix(projectionMatrix);
		setWorldViewProjectionMatrix(wvpMatrix);
	}
}
//------------------------------------------------------------------------------------------------
void Renderer::setNormalMatrix(const Matrix3D & matrix)
{
	getShaderProgram()->setUniform(NORMAL_MATRIX_NAME, matrix);
}
//-------------------------------------------------------------------------------------------------
void Renderer::setWorldMatrix(const Matrix4D & matrix)
{
	getShaderProgram()->setUniform(WORLD_MATRIX_NAME, matrix);
}
//-------------------------------------------------------------------------------------------------
void Renderer::setViewMatrix(const Matrix4D & matrix)
{
	getShaderProgram()->setUniform(VIEW_MATRIX_NAME, matrix);
}
//-------------------------------------------------------------------------------------------------
void Renderer::setWorldViewMatrix(const Matrix4D & matrix)
{
	getShaderProgram()->setUniform(WORLD_VIEW_MATRIX_NAME, matrix);
}
//-------------------------------------------------------------------------------------------------
void Renderer::setProjectionMatrix(const Matrix4D & matrix)
{
	getShaderProgram()->setUniform(PROJECTION_MATRIX_NAME, matrix);
}
//-------------------------------------------------------------------------------------------------
void Renderer::setWorldViewProjectionMatrix(const Matrix4D & matrix)
{
	getShaderProgram()->setUniform(WORLD_VIEW_PROJECTION_MATRIX_NAME, matrix);
}

RendererVisitor* Renderer::getVisitor() const
{
	return m_visitor.get();
}
//------------------------------------------------------------------------------------------------
void Renderer::startRendering(RenderingResultPtr input)
{
	getShaderProgram()->bind();
}
//------------------------------------------------------------------------------------------------
void Renderer::finishRendering(RenderingResultPtr output)
{
	getShaderProgram()->release();
}
//------------------------------------------------------------------------------------------------
void Renderer::init(SceneManager* manager)
{
	if (m_isInited)
		return;

	m_visitor.reset(createVisitor());

	// Reinit all extensions:
	m_extensions.clear();
	registerExtensions();

	// TODO: should we return when m_isInited false?
	doInit(manager);

	m_isInited = true;
}
//------------------------------------------------------------------------------------------------
RendererVisitor* Renderer::createVisitor() const
{
	return new RendererVisitor(this);
}
//------------------------------------------------------------------------------------------------
void Renderer::onAttach(RenderPass* renderPass)
{

}
//------------------------------------------------------------------------------------------------
void Renderer::onDetach(RenderPass* renderPass)
{

}
//------------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE