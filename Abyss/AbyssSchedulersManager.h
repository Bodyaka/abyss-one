#pragma once

#include <vector>
#include <chrono>
#include "AbyssSingleton.h"

ABYSS_BEGIN_NAMESPACE

class Scheduler;

class SchedulersManager
	: public Singleton<SchedulersManager>
{

	friend class Singleton<SchedulersManager>;

public:

	void init();

	void registerScheduler(Scheduler* scheduler);
	void unregisterScheduler(Scheduler* scheduler);
	bool isSchedulerRegistered(Scheduler* scheduler) const; 

	const std::vector<Scheduler*>& getSchedulers() const;

	void update();

private:

	SchedulersManager();
	~SchedulersManager();

	std::chrono::high_resolution_clock::time_point m_currentTimePoint;

	std::vector<Scheduler*> m_schedulers;
};

// INLINE METHODS
inline const std::vector<Scheduler*>& SchedulersManager::getSchedulers() const
{	return m_schedulers;	}

ABYSS_END_NAMESPACE