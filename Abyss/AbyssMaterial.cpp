#include "AbyssMaterial.h"

#include "AbyssTexture.h"

ABYSS_BEGIN_NAMESPACE
//----------------------------------------------------------------------------------------------
MaterialPtr Material::create(const Vector3D & ambient, const Vector4D & diffuse, const Vector4D & specular, float shiness /* = 1.f */)
{
	Material * material = new Material();

	material->setAmbientColor(ambient);
	material->setDiffuseColor(diffuse);
	material->setSpecularColor(specular);
	material->setShiness(shiness);

	return MaterialPtr(material);
}
//----------------------------------------------------------------------------------------------
Material::Material()
	: m_ambient(Vector3D(0, 0, 0))
	, m_specular(Vector4D(1))
	, m_diffuse(Vector4D(1))
	, m_shiness(1)
	, m_diffuseIntensity(1.f)
	, m_specularIntensity(1.f)
	, m_defaultColor(Vector4D(1))
	, m_interactivityMask(InteractivityMaskType::None)
{
	setTexture(DiffuseTextureType, Texture::getDummyWhiteTexture());
}
//----------------------------------------------------------------------------------------------
Material::~Material()
{

}
//----------------------------------------------------------------------------------------------
void Material::setAmbientColor(const Vector3D & ambient)
{
	m_ambient = ambient;

	setChanged(true);
}
//----------------------------------------------------------------------------------------------
void Material::setDiffuseColor(const Vector4D & diffuse)
{
	m_diffuse = diffuse;

	setChanged(true);
}
//----------------------------------------------------------------------------------------------
void Material::setSpecularColor(const Vector4D & specular)
{
	m_specular = specular;

	setChanged(true);
}
//----------------------------------------------------------------------------------------------
void Material::setShiness(float shiness)
{
	m_shiness = shiness;

	setChanged(true);
}
//----------------------------------------------------------------------------------------------
void Material::setDiffuseIntensity(float intensity)
{
	m_diffuseIntensity = intensity;

	setChanged(true);
}
//----------------------------------------------------------------------------------------------
void Material::setSpecularIntensity(float intensity)
{
	m_specularIntensity = intensity;

	setChanged(true);
}
//----------------------------------------------------------------------------------------------
TexturePtr Material::getTexture(TextureType type) const
{
	auto iter = m_textures.find(type);
	return (iter == m_textures.end()) ? nullptr : iter->second;
}
//----------------------------------------------------------------------------------------------
void Material::setTexture(TextureType type, TexturePtr texture)
{
	if (hasTexture(type))
		m_textures[type] = texture;
	else
		m_textures.insert(TexturesContainerType::value_type(type, texture));
}
//----------------------------------------------------------------------------------------------
bool Material::hasTexture(TextureType type) const
{
	return m_textures.find(type) != m_textures.end();
}
//----------------------------------------------------------------------------------------------
void Material::setDefaultColor(const Vector4D& color)
{
	m_defaultColor = color;

	setChanged(true);
}
//----------------------------------------------------------------------------------------------
void Material::setInteractivityMask(InteractivityMaskType mask)
{
	m_interactivityMask = mask;

	setChanged(true);
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE