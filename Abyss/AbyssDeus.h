#pragma once

#include <vector>
#include <algorithm>

#include "Abyss.h"

ABYSS_BEGIN_NAMESPACE

// TODO_REVIEW: This class should be at least renamed.
// And I don't think that it should be here at all.
template <class Type>
class Deus
{
	typedef std::vector<Type*> DeusChildren;
	typedef typename DeusChildren::const_iterator ChildConstIterator;
public:
	Deus(Type * parent);
	virtual ~Deus();

	// SETTERS
	// Set parent.
	void setParent(Type* parent);
	void addChild(Type* child);
	
	// Remove child, but do not delete it.
	Type* removeChild(Type* child);
	Type* removeChild(size_t index);

	// Removes child from Deus and deletes it.
	void deleteChild(Type* child);
	void deleteChild(size_t index);

	// ACCESS
	inline size_t getChildrenCount() const;
	// Return whether deus object has children (childrenCount is greater then zero).
	inline bool hasChildren() const;
	// Get parent of DEUS.
	inline Type* getParent() const;
	// Get children:
	inline Type* getChild(size_t index) const;
	// Get vector of children:
	inline DeusChildren& getChildren();

	// INQUIRY
	inline bool containsChild(Type* child) const;

protected:

	// Deus parent.
	Type* m_parent;

	// Children.
	DeusChildren m_children;
};

// IMPLEMENTATION
#define DEUS_TEMPlATE_DEF template<class Type>
#define DEUS_TEMPLATE_TYPE Deus<Type>

//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	DEUS_TEMPLATE_TYPE::Deus(Type* parent)
	: m_parent(parent)
{}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	DEUS_TEMPLATE_TYPE::~Deus()
{
	for (size_t i = 0; i<getChildrenCount(); ++i)
		deleteChild(i);

	if (getParent())
		getParent()->removeChild((Type*)this);
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	void DEUS_TEMPLATE_TYPE::setParent(Type* parent)
{
	if (m_parent)
		m_parent->removeChild(static_cast<Type*>(this));

	m_parent = parent;

	if (m_parent)
		m_parent->addChild(static_cast<Type*>(this));
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	void DEUS_TEMPLATE_TYPE::addChild(Type* child)
{
	if (!child || containsChild(child))
		return;

	m_children.push_back(child);

	if (child->getParent() != this)
		child->setParent(static_cast<Type*>(this));
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	Type* DEUS_TEMPLATE_TYPE::removeChild(Type* child)
{
	if (!child || !containsChild(child))
		return NULL;

	ChildConstIterator p = std::find(m_children.begin(), m_children.end(), child);
	if (p != m_children.end())
	{
		m_children.erase(p);
		child->setParent(NULL);
		return child;
	}

	return NULL;
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	Type* DEUS_TEMPLATE_TYPE::removeChild(size_t index)
{
	if (index >= getChildrenCount())
		return NULL;

	Type* child = getChild(index);
	return removeChild(child);
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	void  DEUS_TEMPLATE_TYPE::deleteChild(Type* child)
{
	if (!removeChild(child))
		return;

	delete child;
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	void DEUS_TEMPLATE_TYPE::deleteChild(size_t index)
{
	Type* child = removeChild(index);
	if (!child)
		return;

	delete child;
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	size_t DEUS_TEMPLATE_TYPE::getChildrenCount() const
{
	return m_children.size();
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
bool DEUS_TEMPLATE_TYPE::hasChildren() const
{
	return getChildrenCount() > 0;
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	Type * DEUS_TEMPLATE_TYPE::getParent() const
{
	return m_parent;
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	Type* DEUS_TEMPLATE_TYPE::getChild(size_t index) const
{
	return m_children[index];
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	typename DEUS_TEMPLATE_TYPE::DeusChildren & DEUS_TEMPLATE_TYPE::getChildren()
{
	return m_children;
}
//----------------------------------------------------------------------------------------------
DEUS_TEMPlATE_DEF
	bool DEUS_TEMPLATE_TYPE::containsChild(Type* child) const
{
	ChildConstIterator p = std::find(m_children.begin(), m_children.end(), child);
	return (p != m_children.end());
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
