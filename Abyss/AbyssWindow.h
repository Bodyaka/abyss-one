#pragma once

// BEWARE: do not include any gl related stuff here!
// IN other case SFML/SFGUI will be swearing on you!
#include <string>
#include "AbyssRenderPassListener.h"
#include <SFML/Graphics/Image.hpp>
#include "SFML/Graphics/RenderWindow.hpp"


ABYSS_BEGIN_NAMESPACE

class RenderTarget;

class Window;

// This class is responsible for observing some window changes (like resizing).
class WindowListener
{
	friend class Window;
public:
	WindowListener()
		: m_window(nullptr)
	{}
	virtual ~WindowListener();

	Window* getWindow() const	{ return m_window; }

	virtual void onWindowAttached(Window* newWindow, Window* prevWindow) {}
	virtual void onWindowResize(size_t width, size_t height) = 0;

private:
	void attach(Window* window);

	Window* m_window;
};


class Window
	: public sf::RenderWindow
	, public RenderPassListener
{
	typedef sf::RenderWindow TSuper;
public:
	Window(const std::string & title, int x, int y, int width, int height);
	~Window();

	// Manage, whether fps is need to be displayed:
	void setFpsVisible(bool isVisible);

	// Getters
	inline bool isFpsVisible() const;

	// Setup of all components!
	void polish();

	// Return the size of Windows as pair (width - first, height - second)
	static sf::Vector2u getCurrentSize();

	void onRenderingStarted(RenderPass* pass, RenderingResultPtr input) override;
	void onRenderingFinished(RenderPass* pass, RenderingResultPtr output) override;

	// Add window changes listener to current window:
	void addListener(WindowListener* listener);
	// Remove window listener from current window:
	void removeWindowListener(WindowListener* listener);

	sf::Image getScreenshot() const;

protected:

	void onResize() override;
	void createFullScreenRectangle();

	Abyss::RenderTarget * m_frameBuffer;

	typedef std::vector<WindowListener*> WindowListenersContainer;
	WindowListenersContainer m_windowListeners;

};

ABYSS_END_NAMESPACE

