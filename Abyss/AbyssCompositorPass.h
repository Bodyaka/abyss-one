#pragma once

#include "AbyssRenderPass.h"

ABYSS_BEGIN_NAMESPACE

class Window;
class RenderTarget;
class CompositorPassHandler;

// Compositor of render passes. Is using to achieve post-processing effects.
// The render passes are contained in linear-like manner (vector).
class CompositorPass
	: public RenderPass
{
public:
	using PassesContainer = std::vector<RenderPassPtr>;

public:
	explicit CompositorPass(const std::string& name);
	virtual ~CompositorPass() = default;

	RenderPassPtr emplaceRenderPass(const std::string& name, Renderer* renderer);
	bool addRenderPass(RenderPassPtr renderPass);
	void removeRenderPass(const std::string& name);
	void removeRenderPass(RenderPassPtr renderPass);
	void removeAllRenderPasses();

	bool hasRenderPass(const std::string& name) const;

	RenderPassPtr getRenderPass(const std::string& name) const;
	PassesContainer getRenderPasses() const;

	void attachToWindow(Window* window);
	Window* getAttachedWindow() const;

	void setHandler(CompositorPassHandlerPtr handler);
	CompositorPassHandlerPtr getHandler() const;

public: // overriden methods:

	virtual void polish(SceneManager* sceneManager) override;
	virtual void startRendering(RenderingResultPtr input) override;
	virtual void render(RenderableObject* object) override;
	virtual void finishRendering(RenderingResultPtr output) override;

private:

	bool m_isRenderTargetDirty; // TODO: need to move to separate class!
	PassesContainer m_renderPasses;
	std::unique_ptr<RenderTarget> m_renderTarget;

	RenderingResultPtr m_cachedRenderingResult;

	CompositorPassHandlerPtr m_handler;
};

// INLINE METHODS:
//------------------------------------------------------------------------------------------------
inline CompositorPass::PassesContainer CompositorPass::getRenderPasses() const
{	return m_renderPasses;	}
//------------------------------------------------------------------------------------------------
inline void CompositorPass::setHandler(CompositorPassHandlerPtr handler)
{	m_handler = handler;	}
//------------------------------------------------------------------------------------------------
inline CompositorPassHandlerPtr CompositorPass::getHandler() const
{	return m_handler;	}
//------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE