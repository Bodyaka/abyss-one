#pragma once

#include "Abyss.h"
#include "AbyssMesh.h"
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class MeshLoader
{
public:

	MeshPtr load(const String & meshFileName, const String & meshName = "");

	static MeshLoader * getDefaultLoader();

protected:
	MeshLoader();
	MeshLoader(const MeshLoader & rhs);
	~MeshLoader();

	static void setDefaultLoader(MeshLoader * loader);

private:
	virtual MeshPtr performLoadingMeshFromFile(const String & meshFileName) = 0;

	static MeshLoader * m_loader;
};

ABYSS_END_NAMESPACE
