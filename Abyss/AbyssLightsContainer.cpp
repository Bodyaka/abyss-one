#include "AbyssLightsContainer.h"
#include "AbyssLightsManager.h"
#include "AbyssLight.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
LightsContainer::LightsContainer()
{

}
//----------------------------------------------------------------------------------------------
LightsContainer::~LightsContainer()
{
	
}
//----------------------------------------------------------------------------------------------
LightWeakPtr LightsContainer::createLight()
{
	auto lightObject = LightsManager::getInstance()->createLight();
	addLight(lightObject);

	return lightObject;
}
//----------------------------------------------------------------------------------------------
LightWeakPtr LightsContainer::createLight(const std::string& name)
{
	auto lightObject = LightsManager::getInstance()->createLight(name);
	addLight(lightObject);

	return lightObject;
}
//----------------------------------------------------------------------------------------------
void LightsContainer::addLight(LightWeakPtr light)
{
	if (hasLight(light))
		return;

	m_attachedLights.insert(light->getName());
	onLightAdded(light);
}
//----------------------------------------------------------------------------------------------
LightWeakPtr LightsContainer::removeLight(const std::string& name)
{
	if (!hasLight(name))
		return nullptr;

	m_attachedLights.erase(name);

	LightWeakPtr light = LightsManager::getInstance()->getLight(name);
	onLightRemoved(light);

	return light;
}
//----------------------------------------------------------------------------------------------
void LightsContainer::deleteLight(const std::string& name)
{
	if (auto light = removeLight(name))
		LightsManager::getInstance()->removeLight(name);
}
//----------------------------------------------------------------------------------------------
void LightsContainer::deleteAllLights()
{
	for (auto light : getLights())
	{
		deleteLight(light->getName());
	}
}
//----------------------------------------------------------------------------------------------
LightsContainerType LightsContainer::getLights() const
{
	LightsContainerType lights;

	lights.reserve(m_attachedLights.size());
	auto lightsManager = LightsManager::getInstance();
	for (auto lightName : m_attachedLights)
		lights.push_back(lightsManager->getLight(lightName));

	return lights;
}
//----------------------------------------------------------------------------------------------
Abyss::LightsContainerType LightsContainer::mergetLights(const LightsContainerType& lights1, const LightsContainerType& lights2)
{
	LightsContainerType totalLights = lights1;
	totalLights.insert(totalLights.end(), lights2.begin(), lights2.end());

	auto newEnd = std::unique(totalLights.begin(), totalLights.end());
	return LightsContainerType(totalLights.begin(), newEnd);
}
//----------------------------------------------------------------------------------------------
bool LightsContainer::hasLight(LightWeakPtr light)
{
	return hasLight(light->getName());
}
//----------------------------------------------------------------------------------------------
bool LightsContainer::hasLight(const std::string& lightName) const
{
	return m_attachedLights.find(lightName) != m_attachedLights.end();
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
