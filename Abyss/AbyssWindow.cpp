#include "AbyssRectangle.h"
#include "AbyssWindow.h"
#include "AbyssDepthBuffer.h"
#include "AbyssMeshRenderable.h"
#include "AbyssRenderTarget.h"
#include "AbyssTexture.h"
#include "AbyssMaterial.h"
#include "AbyssSceneManager.h"
#include "AbyssFullScreenRectRenderer.h"
#include "AbyssApplicationContext.h"
#include "AbyssRenderPass.h"
#include "AbyssRenderPassManager.h"
#include "AbyssRenderingResult.h"


ABYSS_BEGIN_NAMESPACE

static String cFullScreenRectSceneObject = "__Abyss_Window_FullScreen_Object__";


WindowListener::~WindowListener()
{
	if (auto window = getWindow())
		window->removeWindowListener(this);
}

void WindowListener::attach(Window* window)
{
	if (window == getWindow())
	{
		ABYSS_LOG((StringFormat("WindowLsitener [%1] is already attached to window: [%2]!") % this % window).str());
		return;
	}

	auto previousWindow = getWindow();

	m_window = window;
	
	onWindowAttached(getWindow(), previousWindow);
	if (auto newWindow = getWindow())
		onWindowResize(newWindow->getSize().x, newWindow->getSize().y);
}


//-----------------------------------------------------------------------------------------------
Window::Window(const std::string & title, int x, int y, int width, int height)
	: TSuper(sf::VideoMode(width, height), title)
	, m_frameBuffer(nullptr)
{
	setPosition(sf::Vector2i(x, y));
}
//-----------------------------------------------------------------------------------------------
Window::~Window()
{
	delete m_frameBuffer;

	SceneManager* manager = SceneManager::instance();
	manager->removeSceneObject(cFullScreenRectSceneObject);
}
//------------------------------------------------------------------------------------------------
void Window::polish()
{
	sf::Vector2u size = getSize();
	size_t width = size.x;
	size_t height = size.y;

	m_frameBuffer = new Abyss::RenderTarget(this);

	Abyss::Texture::Pointer texture(new Abyss::Texture());
	texture->setRawData(nullptr, width, height, PixelFormat::PF_UBYTE);
	texture->setFiltering(Abyss::Texture::LinearFiltering);
	texture->setWrapping(Abyss::Texture::ClampToEdgeWrapping);

	Abyss::DepthBuffer::Pointer depthBuffer(new Abyss::DepthBuffer(width, height, 0));
	m_frameBuffer->attachTexture(Abyss::RenderTarget::ColorAttachment0, texture);
	m_frameBuffer->attachDepthBuffer(depthBuffer);
	m_frameBuffer->setClearEveryFrame(false);

	createFullScreenRectangle();
}
//------------------------------------------------------------------------------------------------
void Window::onResize()
{
	TSuper::onResize();

	for (auto listener : m_windowListeners)
		listener->onWindowResize(getSize().x, getSize().y);
}
//-----------------------------------------------------------------------------------------------
void Window::createFullScreenRectangle()
{
	SceneManager* manager = SceneManager::instance();
	auto sceneObject = manager->createSceneObject(cFullScreenRectSceneObject);

	auto mesh = Abyss::Mesh::createFullScreenRect();

	MaterialPtr material(new Material());
	material->setAmbientColor(Vector3D(1.f));
	material->setTexture(DiffuseTextureType, m_frameBuffer->getAttachedTexture(Abyss::RenderTarget::ColorAttachment0));
	auto renderableObject = MeshRenderablePtr(new MeshRenderable("__ABYSS_WINDOW_FULL_RECT_RENREABLE__"));

	//auto finalRenderPass = manager->getRenderPassManager()->getRenderPass(DefaultRenderPasses::FINAL_PASS);
	auto finalRenderPass = manager->getRenderPassManager()->getRenderPass(DefaultRenderPasses::FINAL_PASS);
	renderableObject->addRenderPass(finalRenderPass);
	renderableObject->setMesh(mesh);
	renderableObject->setMaterial(material);
	renderableObject->setLightingEnabled(false);

	sceneObject->addRenderableObject(renderableObject);

	manager->getRenderPassManager()->getRenderPass(DefaultRenderPasses::DEFERRED_SHADING)->addListener(this);
	manager->getRenderPassManager()->getRenderPass(DefaultRenderPasses::PHONG_SHADING)->addListener(this);
	//manager->getRenderPassManager()->getRenderPass(DefaultRenderPasses::FINAL_PASS)->addListener(this);
}
//-----------------------------------------------------------------------------------------------
sf::Vector2u Window::getCurrentSize()
{
	Window* window = ApplicationContext::getSharedApplicationContext()->getWindow();
	return window->getSize();
}
//-----------------------------------------------------------------------------------------------
void Window::onRenderingStarted(RenderPass* pass, RenderingResultPtr input)
{
	assert(pass);

	m_frameBuffer->notifyStartRendering();

	if (pass->getName() == DefaultRenderPasses::DEFERRED_SHADING)
		m_frameBuffer->clear();
}
//-----------------------------------------------------------------------------------------------
void Window::onRenderingFinished(RenderPass* pass, RenderingResultPtr output)
{
	m_frameBuffer->notifyFinishRendering();
	
	auto attachedTexture = m_frameBuffer->getAttachedTexture(Abyss::RenderTarget::ColorAttachment0);
	output->setTexture(constants::ABYSS_MAIN_TEXTURE, attachedTexture);
}
//-----------------------------------------------------------------------------------------------
void Window::addListener(WindowListener* listener)
{
	m_windowListeners.push_back(listener);
}
//-----------------------------------------------------------------------------------------------
void Window::removeWindowListener(WindowListener* listenerToRemove)
{
	auto matchListenerFunc = [=](WindowListener * listener) -> bool
	{
		return listener == listenerToRemove;
	};

	std::remove_if(m_windowListeners.begin(), m_windowListeners.end(), matchListenerFunc);
}
//-----------------------------------------------------------------------------------------------
sf::Image Window::getScreenshot() const
{
	sf::Image image;
	auto data = m_frameBuffer->readPixels(PixelFormat::PF_UBYTE, GL_RGBA);
	image.create(m_frameBuffer->getActualWidth(), m_frameBuffer->getActualHeight(), data);

	// Because OpenGL has Y axis pointed up and in 2D world the Y axis points down, we need to flip image vertically:
	image.flipVertically();

	return image;
}
//-----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE