#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

struct RawVertexData
{
	size_t numOfVertices = 0u;
	float* verticesPositions = nullptr;
	float* verticesTexels = nullptr;
	float* verticesNormals = nullptr;
	float* verticesTangents = nullptr;
	float* verticesBitangents = nullptr;
};

ABYSS_END_NAMESPACE