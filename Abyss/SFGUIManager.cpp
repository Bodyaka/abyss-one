// TODO: window.h should be included as first due to some problem with gl.h / glew.h included first...
#include "AbyssWindow.h"
#include "SFGUIManager.h"
#include "SFGUI/SFGUI.hpp"
#include "SFGUI/Desktop.hpp"

ABYSS_BEGIN_NAMESPACE

SFGUIManager::SFGUIManager()
	: m_isEventProcessedByGUI(false)
{

}

SFGUIManager::~SFGUIManager()
{

}

void SFGUIManager::init()
{
	m_gui.reset(new sfg::SFGUI());
	m_desktop.reset(new sfg::Desktop());

	getDesktop()->SetProperty("Window#screen_window", "Gap", 0.f);
	
	m_rootWindow = sfg::Fixed::Create();
	m_rootWindow->SetId("screen_window");
	m_rootWindow->SetRequisition(sf::Vector2f(1366, 768));

	getDesktop()->Add(m_rootWindow);

	m_clock.restart();
}

bool SFGUIManager::processEvent(const sf::Event& event)
{
	if (event.type == sf::Event::Resized)
	{
		sf::Vector2f windowSize(event.size.width, event.size.height);
		m_rootWindow->SetRequisition(windowSize);
	}
	bool isEventProcessed = m_desktop->HandleEvent(event);

	bool wasEventProcessedByGUI = m_isEventProcessedByGUI;

	if (isEventProcessed)
	{
		std::function<sfg::Widget*(sfg::Widget*)> findActiveChildFunc;
		findActiveChildFunc = [&findActiveChildFunc, this](sfg::Widget* widget)->sfg::Widget*
		{
			if (!widget)
				return nullptr;

			if (widget != m_rootWindow.get() &&
				widget->IsActiveWidget() ||
				(widget->GetState() == sfg::Widget::State::ACTIVE))
				return widget;

			sfg::Container* widgetAsContainer = dynamic_cast<sfg::Container*>(widget);
			if (!widgetAsContainer)
				return nullptr;

			sfg::Widget* activeWidget = nullptr;
			for (auto child : widgetAsContainer->GetChildren())
			{
				if (auto activeChild = findActiveChildFunc(child.get()))
				{
					activeWidget = activeChild;
					break;
				}
			}

			return activeWidget;
		};

		auto activeWidget = findActiveChildFunc(getRootWindow().get());
		if (activeWidget)
		{
			std::cout << "Active widget is found: " << activeWidget->GetClass() << " ID: " << activeWidget->GetId() << " name : " << activeWidget->GetName() << "\n";
			m_isEventProcessedByGUI = true;
		}
		else
			m_isEventProcessedByGUI = false;
	}
	else
		m_isEventProcessedByGUI = false;

	if (wasEventProcessedByGUI && !m_isEventProcessedByGUI && event.type == sf::Event::MouseButtonReleased)
		return true;

	return m_isEventProcessedByGUI;
}

void SFGUIManager::update()
{
	getDesktop()->Update(m_clock.restart().asSeconds());
}

void SFGUIManager::render(Window* window)
{
	// Render SFGUI:
	getGUI()->Display(*window);
}

ABYSS_END_NAMESPACE
