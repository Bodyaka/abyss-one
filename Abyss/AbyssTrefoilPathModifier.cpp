#include "AbyssTrefoilPathModifier.h"
#include "AbyssSceneObject.h"
#include "AbyssSceneManager.h"
#include "AbyssCamera.h"

#include "AbyssPathline.h"
#include "AbyssSimpleRenderableObject.h"

ABYSS_BEGIN_NAMESPACE

TreefoilPathModifier::TreefoilPathModifier()
	: m_currentAngle(0.f)
	, m_radius(1.f)
	, m_pathRenderable(new SimpleRenderableObject("TreefoilPathModifier_PathRenderable"))
{
	setInterval(20);
	setUpdateFunc(std::bind(&TreefoilPathModifier::onPathUpdated, this, std::placeholders::_1));
	setSingleShot(false);

	m_pathRenderable->setupDefaultRenderPasses();
	m_pathRenderable->setGeometryObject(GeometryObjectPtr(new Pathline()));

	static size_t s_instancesCount = 0;
	auto sceneObject = SceneManager::instance()->createSceneObject("TreefoilPathModifier_SceneObject_" + std::to_string(s_instancesCount));
	sceneObject->addRenderableObject(m_pathRenderable);
	++s_instancesCount;
}

TreefoilPathModifier::~TreefoilPathModifier()
{

}

void TreefoilPathModifier::setSceneObjectName(const String& sceneObjectName)
{
	if (getSceneObjectName() == sceneObjectName)
		return;

	m_currentAngle = 0.f;

	m_sceneObjectName = sceneObjectName;

	if (auto object = getSceneObject())
	{
		// Need to place the object to the default positions
	}
}
//----------------------------------------------------------------------------------------------
SceneObject* TreefoilPathModifier::getSceneObject() const
{
	return SceneManager::instance()->getSceneObject(getSceneObjectName()).get();
}
//----------------------------------------------------------------------------------------------
void TreefoilPathModifier::onPathUpdated(float deltaTime)
{
	m_currentAngle += 3 * (deltaTime / getInterval());

	if (m_currentAngle >= 360)
	{
		m_currentAngle -= 360;
		getPathline()->clearVertices();
	}

	updateSceneObjectPosition();
}
//----------------------------------------------------------------------------------------------
void TreefoilPathModifier::updateSceneObjectPosition()
{
	/*auto sceneObject = getSceneObject();
	if (!sceneObject)
		return;*/

	// Code for camera for test only.
	// It should be removed before push!
	auto sceneManager = SceneManager::instance();
	auto camera = sceneManager->getCamera();

	auto angleInRadians = glm::radians(m_currentAngle);
	auto xPos = m_radius * (sin(angleInRadians) + 2 * sin(2 * angleInRadians));
	auto yPos = m_radius * (cos(angleInRadians) - 2 * cos(2 * angleInRadians));
	auto zPos = m_radius * (-sin(3 * angleInRadians));

	auto objectPos = Vector3D(xPos, yPos, zPos) + getTreefoilCenterPosition();

	getPathline()->addVertex(objectPos);
	//camera->setEyePosition(objectPos);

	auto sceneObject = getSceneObject();
	if (!sceneObject)
		return;

	//camera->setCenterPosition(sceneObject->getPosition());
	sceneObject->setPosition(objectPos);
}
//----------------------------------------------------------------------------------------------
void TreefoilPathModifier::setTreefoilRadius(float radius)
{
	// TODO: need add floating comparison:
	if (getTreefoiRadius() == radius)
		return;

	m_radius = radius;

	updateSceneObjectPosition();
}
//----------------------------------------------------------------------------------------------
void TreefoilPathModifier::setTreefoilCenterPosition(const Vector3D& pos)
{
	// TODO: need add floating comparison:
	if (getTreefoilCenterPosition() == pos)
		return;

	m_centerPosition = pos;

	updateSceneObjectPosition();
}
//----------------------------------------------------------------------------------------------
Pathline* TreefoilPathModifier::getPathline() const
{
	return dynamic_cast<Pathline*>(m_pathRenderable->getGeometryObject().get());
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE