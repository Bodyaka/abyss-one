#include "AbyssMeshUtilities.h"

#include <algorithm>
#include <chrono>

#include "AbyssMesh.h"
#include "AbyssMaterial.h"

ABYSS_BEGIN_NAMESPACE

namespace
{
	Vector2D createVector2D(const float* vertices, size_t index)
	{
		return Vector2D(vertices[index], vertices[index + 1]);
	}

	Vector3D createVector3D(const float* vertices, size_t index)
	{
		return Vector3D(vertices[index], vertices[index + 1], vertices[index + 2]);
	}
}
namespace MeshUtilities
{

	void calculateTangentsAndBitangents(const ContainerV3D& verticesPositions, const ContainerV3D& verticesNormals, const ContainerV2D& textureCoord, const ContainerUint& indices, ContainerV3D& tangents, ContainerV3D& bitangents)
	{
		size_t verticesCount = verticesPositions.size();

		if (verticesCount != verticesNormals.size() ||
			verticesCount != textureCoord.size())
			throw std::exception("Incorrect input data for tangents and bitangents calculation!");

		assert(!indices.empty());

		auto timePoint1 = std::chrono::high_resolution_clock::now();
		tangents.resize(verticesPositions.size());
		bitangents.resize(verticesCount);

		ContainerUint facesPerVertexContainer;
		facesPerVertexContainer.resize(verticesCount, 0);

		size_t indicesCount = indices.size();

		for (size_t index = 0; index != indicesCount; index += 3)
		{
			size_t vertexIndex0 = indices[index];
			size_t vertexIndex1 = indices[index + 1];
			size_t vertexIndex2 = indices[index + 2];

			Vector3D vertexPos0 = verticesPositions[vertexIndex0];
			Vector3D vertexPos1 = verticesPositions[vertexIndex1];
			Vector3D vertexPos2 = verticesPositions[vertexIndex2];

			Vector2D textureCoord0 = textureCoord[vertexIndex0];
			Vector2D textureCoord1 = textureCoord[vertexIndex1];
			Vector2D textureCoord2 = textureCoord[vertexIndex2];

			Vector3D deltaPos1 = vertexPos1 - vertexPos0;
			Vector3D deltaPos2 = vertexPos2 - vertexPos0;

			Vector2D deltaUV1 = textureCoord1 - textureCoord0;
			Vector2D deltaUV2 = textureCoord2 - textureCoord0;

			float denominator = (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
			float radius = 0.f;
			if (std::abs(denominator) > cPrecision)
				radius = 1.f / denominator;

			Vector3D tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * radius;
			Vector3D bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * radius;

			tangents[vertexIndex0] += tangent;
			tangents[vertexIndex1] += tangent;
			tangents[vertexIndex2] += tangent;

			bitangents[vertexIndex0] += bitangent;
			bitangents[vertexIndex1] += bitangent;
			bitangents[vertexIndex2] += bitangent;

			++facesPerVertexContainer[vertexIndex0];
			++facesPerVertexContainer[vertexIndex1];
			++facesPerVertexContainer[vertexIndex2];

		}
		
		for (size_t vertexIndex = 0; vertexIndex < facesPerVertexContainer.size(); ++vertexIndex)
		{
			size_t facesPerVertex = facesPerVertexContainer[vertexIndex];

			// TODO: need to check whether it is needed to perform normalization at this step!
			Vector3D bitangent = bitangents[vertexIndex];
			bitangent /= facesPerVertex;
			bitangent = glm::normalize(bitangent);

			Vector3D tangent = tangents[vertexIndex];
			tangent /= facesPerVertex;
			tangent = glm::normalize(tangent);

			Vector3D normal = verticesNormals[vertexIndex];
			tangent = glm::normalize(tangent - normal * glm::dot(normal, tangent));

			// TODO: need to check whether this is needed at all?
			/*
			if (glm::dot(glm::cross(normal, tangent), bitangent) < 0.0f)
				bitangent *= -1;
				*/

			tangents[vertexIndex] = tangent;
			bitangents[vertexIndex] = bitangent;
		}

		auto timePoint2 = std::chrono::high_resolution_clock::now();
		std::cout << "Tangents and Bitangents generation time: " << std::chrono::duration_cast<std::chrono::milliseconds>(timePoint2 - timePoint1).count() << " ms \n";
	}

	void calculateTangentsAndBitangents(const float* verticesPositions, const float* verticesNormals, const ContainerV2D& textureCoord, const size_t verticesNumber, const unsigned int* indices, const size_t indicesNumber, ContainerV3D& tangents, ContainerV3D& bitangents)
	{
		throw std::exception("This function need to be rewritten!");

		auto timePoint1 = std::chrono::high_resolution_clock::now();
		tangents.resize(verticesNumber);
		bitangents.resize(verticesNumber);

		ContainerUint facesPerVertexContainer;
		facesPerVertexContainer.resize(verticesNumber, 0);
		
		for (size_t index = 0; index != indicesNumber; index += 3)
		{
			size_t vertexIndex0 = indices[index];
			size_t vertexIndex1 = indices[index + 1];
			size_t vertexIndex2 = indices[index + 2];

			Vector3D vertexPos0 = createVector3D(verticesPositions, vertexIndex0);
			Vector3D vertexPos1 = createVector3D(verticesPositions, vertexIndex1);
			Vector3D vertexPos2 = createVector3D(verticesPositions, vertexIndex2);

			Vector2D textureCoord0 = textureCoord[vertexIndex0];
			Vector2D textureCoord1 = textureCoord[vertexIndex1];
			Vector2D textureCoord2 = textureCoord[vertexIndex2];

			Vector3D deltaPos1 = vertexPos1 - vertexPos0;
			Vector3D deltaPos2 = vertexPos2 - vertexPos0;

		
			Vector2D deltaUV1 = textureCoord1 - textureCoord0;
			Vector2D deltaUV2 = textureCoord2 - textureCoord0;

			float denominator = (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
			float radius = 0.f;
			if (std::abs(denominator) > cPrecision)
				radius = 1.f / denominator;
			
			Vector3D tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * radius;
			Vector3D bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * radius;

			tangents[vertexIndex0] += tangent;
			tangents[vertexIndex1] += tangent;
			tangents[vertexIndex2] += tangent;

			bitangents[vertexIndex0] += bitangent;
			bitangents[vertexIndex1] += bitangent;
			bitangents[vertexIndex2] += bitangent;

			++facesPerVertexContainer[vertexIndex0];
			++facesPerVertexContainer[vertexIndex1];
			++facesPerVertexContainer[vertexIndex2];

		}

		
		for (size_t vertexIndex = 0; vertexIndex < facesPerVertexContainer.size(); ++vertexIndex)
		{
			size_t facesPerVertex = facesPerVertexContainer[vertexIndex];

			// TODO: need to check whether it is needed to perform normalization at this step!
			Vector3D bitangent = bitangents[vertexIndex];
			bitangent /= facesPerVertex;
			bitangent = glm::normalize(bitangent);

			Vector3D tangent = tangents[vertexIndex];
			tangent /= facesPerVertex;
			tangent = glm::normalize(tangent);
			
			Vector3D normal = createVector3D(verticesNormals, vertexIndex);
			tangent = glm::normalize(tangent - normal * glm::dot(normal, tangent));

			if (glm::dot(glm::cross(normal, tangent), bitangent) < 0.0f)
				bitangent *= -1;

			tangents[vertexIndex] = tangent;
			bitangents[vertexIndex] = bitangent;
		}

		auto timePoint2 = std::chrono::high_resolution_clock::now();
		std::cout << "Tangents and Bitangents generation time: " << std::chrono::duration_cast<std::chrono::milliseconds>(timePoint2 - timePoint1).count() << " ms \n";
	}

	void renormalizeNormals(const float* verticesPosition, const float* loadedNormals, const size_t verticesNumber, const unsigned int* indices, const size_t indicesNumber, ContainerV3D& normals)
	{
		normals.resize(verticesNumber);

		ContainerUint facesPerVertexContainer;
		facesPerVertexContainer.resize(verticesNumber, 0);

		for (size_t index = 0; index != indicesNumber; index += 3)
		{
			size_t vertexIndex0 = indices[index];
			size_t vertexIndex1 = indices[index + 1];
			size_t vertexIndex2 = indices[index + 2];

			Vector3D vertexPos0 = createVector3D(verticesPosition, vertexIndex0);
			Vector3D vertexPos1 = createVector3D(verticesPosition, vertexIndex1);
			Vector3D vertexPos2 = createVector3D(verticesPosition, vertexIndex2);

			Vector3D deltaPos1 = vertexPos1 - vertexPos0;
			Vector3D deltaPos2 = vertexPos2 - vertexPos0;

			Vector3D normal = glm::cross(deltaPos1, deltaPos2);

			normals[vertexIndex0] += normal;
			normals[vertexIndex1] += normal;
			normals[vertexIndex2] += normal;

			++facesPerVertexContainer[vertexIndex0];
			++facesPerVertexContainer[vertexIndex1];
			++facesPerVertexContainer[vertexIndex2];
		}

		size_t numberOfIncorrectNormals = 0;
		for (size_t vertexIndex = 0; vertexIndex < facesPerVertexContainer.size(); ++vertexIndex)
		{
			Vector3D loadedNormal = glm::normalize(createVector3D(loadedNormals, vertexIndex));

			size_t facesPerVertex = facesPerVertexContainer[vertexIndex];

			// TODO: need to check whether it is needed to perform normalization at this step!
			normals[vertexIndex] /= facesPerVertex;
			normals[vertexIndex] = glm::normalize(normals[vertexIndex]);

			Vector3D computedNormal = normals[vertexIndex];

			if (loadedNormal != computedNormal)
				++numberOfIncorrectNormals;
		}

		std::cout << "Percentage of incorrect normals are = " << 100 * (double(numberOfIncorrectNormals) / verticesNumber) << "% \n";
	}


	ContainerV3D computeNormals(const ContainerV3D& verticesPositions, const ContainerUint& indices)
	{
		size_t verticesCount = verticesPositions.size();
		ContainerV3D normals;
		normals.resize(verticesCount);

		ContainerUint facesPerVertexContainer;
		facesPerVertexContainer.resize(verticesCount, 0);

		for (size_t index = 0; index != indices.size(); index += 3)
		{
			size_t vertexIndex0 = indices[index];
			size_t vertexIndex1 = indices[index + 1];
			size_t vertexIndex2 = indices[index + 2];

			Vector3D vertexPos0 = verticesPositions[vertexIndex0];
			Vector3D vertexPos1 = verticesPositions[vertexIndex1];
			Vector3D vertexPos2 = verticesPositions[vertexIndex2];

			Vector3D deltaPos1 = vertexPos1 - vertexPos0;
			Vector3D deltaPos2 = vertexPos2 - vertexPos0;

			Vector3D normal = glm::cross(deltaPos1, deltaPos2);

			normals[vertexIndex0] += normal;
			normals[vertexIndex1] += normal;
			normals[vertexIndex2] += normal;

			++facesPerVertexContainer[vertexIndex0];
			++facesPerVertexContainer[vertexIndex1];
			++facesPerVertexContainer[vertexIndex2];
		}

		size_t numberOfIncorrectNormals = 0;
		for (size_t vertexIndex = 0; vertexIndex < facesPerVertexContainer.size(); ++vertexIndex)
		{
			size_t facesPerVertex = facesPerVertexContainer[vertexIndex];

			// TODO: need to check whether it is needed to perform normalization at this step!
			normals[vertexIndex] /= facesPerVertex;
			normals[vertexIndex] = glm::normalize(normals[vertexIndex]);
		}

		return normals;
	}

	MeshPtr createRectMesh(float size)
	{
		static size_t s_instancesCount = 0;
		std::string name = "__ABYSS_RECT_MESH_" + std::to_string(s_instancesCount++);
		Mesh* mesh = new Mesh(name, GL_TRIANGLES);
		
		float halfSize = size / 2;
		Mesh::VertexData vertices;
		
		ContainerUint indices;
		indices << 0u << 1u << 2u
				<< 2u << 3u << 0u;

		// 0 ---- 3
		// |	  |
		// |	  |
		// 1 ---- 2
		vertices.positions << Vector3D(-halfSize,  halfSize, 0)
						   << Vector3D(-halfSize, -halfSize, 0)
						   << Vector3D( halfSize, -halfSize, 0)
						   << Vector3D(halfSize,	 halfSize, 0);

		vertices.texels << Vector2D(0, 1)
			            << Vector2D(0, 0)
			            << Vector2D(1, 0)
			            << Vector2D(1, 1);

		vertices.normals = computeNormals(vertices.positions, indices);

		calculateTangentsAndBitangents(vertices.positions, vertices.normals, vertices.texels, indices, vertices.tangents, vertices.bitangents);

		mesh->setVertexData(vertices);

		mesh->setIndices(indices);

		return MeshPtr(mesh);
	}
}


ABYSS_END_NAMESPACE