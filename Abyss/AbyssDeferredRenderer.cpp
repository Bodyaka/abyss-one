#include "AbyssDeferredRenderer.h"

#include "AbyssRenderer.h"
#include "AbyssSceneObject.h"
#include "AbyssRenderTarget.h"
#include "AbyssShaderProgram.h"
#include "AbyssSceneManager.h"
#include "AbyssMaterial.h"
#include "AbyssTexture.h"
#include "AbyssMeshRenderable.h"

#include "AbyssWindow.h"
#include "AbyssGBuffer.h"
#include "AbyssRenderTarget.h"
#include "AbyssSceneManager.h"

#include "AbyssMesh.h"
#include "AbyssDeferredShadingRenderer.h"
#include "AbyssRendererMaterialExtension.h"

#include "AbyssCamera.h"
#include "AbyssSceneManager.h"
#include "AbyssRenderPassManager.h"

// For testing purposes:
#include "AbyssSSAORenderer.h"
#include "AbyssRenderingResult.h"

ABYSS_BEGIN_NAMESPACE

static const std::string SHADER_NAME = "DeferredRenderer";
static const std::string POSITIONS_ATTRIBUTE = "gBufferPositionDepth";
static const std::string NORMALS_ATTRIBUTE = "gBufferNormals";
static const std::string COLORS_ATTRIBUTE = "gBufferColor";

static const std::string NEAR_PLANE_DIST_UNIFORM_NAME = "uNearPlaneDist";
static const std::string FAR_PLANE_DIST_UNIFORM_NAME = "uFarPlaneDist";

static const size_t POSITIONS_ATTRIBUTE_LOCATION = 0;
static const size_t NORMALS_ATTRIBUTE_LOCATION = 1;
static const size_t COLORS_ATTRIBUTE_LOCATION = 2;
static const size_t DIFFUSE_TEXTURE_BINDING_LOCATION = 3;
static const size_t NORMALMAP_TEXTURE_BINDING_LOCATION = 4;

DeferredRenderer::DeferredRenderer()
	: Renderer(SHADER_NAME)
	, m_gBuffer(nullptr)
{
	//m_gBufferRenderTarget.reset(new RenderTarget());

	
}
//-------------------------------------------------------------------------------------------------
DeferredRenderer::~DeferredRenderer()
{
}
//-------------------------------------------------------------------------------------------------
DeferredRenderer * DeferredRenderer::getInstance()
{
	static DeferredRenderer renderer;
	return &renderer;
}
//-------------------------------------------------------------------------------------------------
bool DeferredRenderer::isSupports(const RenderableObject * const object) const
{
	return true;
}
//-------------------------------------------------------------------------------------------------
void DeferredRenderer::beforeRender(RenderableObject * const object)
{
	setupMatrixUniformsValues(object->getBoundSceneObject());

	setupNearAndFarPlanes();
}
//-------------------------------------------------------------------------------------------------
void DeferredRenderer::afterRender(RenderableObject * const object)
{
	// Used solely as opengl breakpoint:
	// GLint params;
	// glDeleteSync(0);
}
//-------------------------------------------------------------------------------------------------
void DeferredRenderer::doInit(SceneManager* manager)
{
	initFullScreenRect(manager);
	initGBuffer(manager);
	initAttributeLocations();
}
//-------------------------------------------------------------------------------------------------
void DeferredRenderer::initGBuffer(SceneManager* manager)
{
	auto window = manager->getWindow();
	auto size = window->getSize();

	m_gBufferRenderTarget.reset(new RenderTarget(manager->getWindow()));

	m_gBuffer.reset(new GBuffer(size.x, size.y));
	m_gBufferRenderTarget->attachTexture(RenderTarget::ColorAttachment0, m_gBuffer->getTexture(GBuffer::TextureType::POSITIONS));
	m_gBufferRenderTarget->attachTexture(RenderTarget::ColorAttachment1, m_gBuffer->getTexture(GBuffer::TextureType::NORMALS));
	m_gBufferRenderTarget->attachTexture(RenderTarget::ColorAttachment2, m_gBuffer->getTexture(GBuffer::TextureType::COLORS));

	Abyss::DepthBuffer::Pointer depthBuffer(new Abyss::DepthBuffer(size.x, size.y, 0));
	m_gBufferRenderTarget->attachDepthBuffer(depthBuffer);
	m_gBufferRenderTarget->setClearColor(Vector4D(0, 0, 0, 0));

	// Attachments:
	RenderTarget::AttachmentsContainerType attachments =
	{
		RenderTarget::ColorAttachment0,
		RenderTarget::ColorAttachment1,
		RenderTarget::ColorAttachment2
	};
	m_gBufferRenderTarget->setDrawBuffers(attachments);

	assert(m_gBufferRenderTarget->isValid());
}
//-------------------------------------------------------------------------------------------------
void DeferredRenderer::initAttributeLocations()
{
	auto allAttributes = getShaderProgram()->getAttributes();
	
	getShaderProgram()->setFragmentDataLocation(POSITIONS_ATTRIBUTE, POSITIONS_ATTRIBUTE_LOCATION);
	getShaderProgram()->setFragmentDataLocation(NORMALS_ATTRIBUTE, NORMALS_ATTRIBUTE_LOCATION);
	getShaderProgram()->setFragmentDataLocation(COLORS_ATTRIBUTE, COLORS_ATTRIBUTE_LOCATION);
}
//-------------------------------------------------------------------------------------------------
void DeferredRenderer::initFullScreenRect(SceneManager* manager)
{
	auto fullScreenRendeable = manager->getFullScreenRectRenderable();
	fullScreenRendeable->setLightingEnabled(true);
	
	auto deferredShadingRenderPass = manager->getRenderPassManager()->getRenderPass(DefaultRenderPasses::DEFERRED_SHADING);
	fullScreenRendeable->addRenderPass(deferredShadingRenderPass);
}

void DeferredRenderer::registerExtensions()
{
	m_extensions.push_back(std::make_shared<RendererMaterialExtension>(this->getShaderProgram()));
}
//-------------------------------------------------------------------------------------------------
void DeferredRenderer::setupNearAndFarPlanes()
{
	auto camera = SceneManager::instance()->getCamera();

	auto nearPlaneDist = camera->getNearDistance();
	auto farPlaneDist = camera->getFarDistance();

	getShaderProgram()->setUniform(NEAR_PLANE_DIST_UNIFORM_NAME, nearPlaneDist);
	getShaderProgram()->setUniform(FAR_PLANE_DIST_UNIFORM_NAME, farPlaneDist);
}

void DeferredRenderer::startRendering(RenderingResultPtr input)
{
	Renderer::startRendering(input);

	m_gBufferRenderTarget->notifyStartRendering();
	m_gBuffer->getTexture(GBuffer::TextureType::POSITIONS)->bind(POSITIONS_ATTRIBUTE_LOCATION);
	m_gBuffer->getTexture(GBuffer::TextureType::NORMALS)->bind(NORMALS_ATTRIBUTE_LOCATION);
	m_gBuffer->getTexture(GBuffer::TextureType::COLORS)->bind(COLORS_ATTRIBUTE_LOCATION);
}

void DeferredRenderer::finishRendering(RenderingResultPtr output)
{
	m_gBuffer->getTexture(GBuffer::TextureType::POSITIONS)->release();
	m_gBuffer->getTexture(GBuffer::TextureType::NORMALS)->release();
	m_gBuffer->getTexture(GBuffer::TextureType::COLORS)->release();
	m_gBufferRenderTarget->notifyFinishRendering();

	output->setGBuffer(m_gBuffer);

	Renderer::finishRendering(output);
}

//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
