#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class VertexBufferObject
{

public:
	// Creates buffer object pointer.
	static VertexBufferObjectPtr createBufferObject(GLenum type);
	// Creates buffer object pointer.
	static VertexBufferObjectPtr createBufferObject(GLenum type, size_t count, void * data, GLenum drawMode);
	// Delete buffer object. Should be placed somewhere in other place
	~VertexBufferObject();

	// Bind this one buffer.
	void bind() const;
	// Release this one buffer.
	void release() const;
	// Fill buffer with specified data. Note, that previous binded buffer will be unbinded
	// Returning success of buffer operation:
	bool bufferData(size_t byteCount, void * data, GLenum usage);

	// ACCESS
	// Return size of buffer. Note, that buffer will be binded after call.
	size_t getSize() const;
	// Return usage of buffer (i.e. GL_STATIC_DRAW, etc). Note, that buffer will be binded after call.
	GLenum getUsage() const;
	// Return id of gl buffer object.
	inline size_t getId() const;
	// Return enum type of this one buffer object.
	inline GLenum getType() const;

	// INQUIRY
	// Return whether this one buffer is binded, or not.
	bool isBinded() const;

private:
	// Create new buffer object.
	VertexBufferObject(GLenum type);

	// Type of buffer object.
	GLenum m_type;
	// GL buffer object.
	size_t m_id;
};

// INLINE METHODS.
//---------------------------------------------------------------------------------------------
size_t VertexBufferObject::getId() const
{	return m_id;	}
//---------------------------------------------------------------------------------------------
GLenum VertexBufferObject::getType() const
{	return m_type;	}
//---------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
