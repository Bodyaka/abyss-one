#include "AbyssSchedulersManager.h"
#include "AbyssScheduler.h"

ABYSS_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------------------
SchedulersManager::SchedulersManager()
{

}
//--------------------------------------------------------------------------------------------------
SchedulersManager::~SchedulersManager()
{

}
//--------------------------------------------------------------------------------------------------
void SchedulersManager::registerScheduler(Scheduler* scheduler)
{
	assert(!isSchedulerRegistered(scheduler)); // Can't register scheduler twice

	m_schedulers.push_back(scheduler);
}
//--------------------------------------------------------------------------------------------------
void SchedulersManager::unregisterScheduler(Scheduler* scheduler)
{
	auto iter = std::find(m_schedulers.begin(), m_schedulers.end(), scheduler);
	if (iter != m_schedulers.end())
		m_schedulers.erase(iter);
	else
		assert(false && "Cannot unregister scheduler, which is not registered!");
}
//--------------------------------------------------------------------------------------------------
bool SchedulersManager::isSchedulerRegistered(Scheduler* scheduler) const
{
	return std::find(m_schedulers.begin(), m_schedulers.end(), scheduler) != m_schedulers.end();
}
//--------------------------------------------------------------------------------------------------
void SchedulersManager::update()
{
	auto currTime = std::chrono::high_resolution_clock::now();
	std::chrono::high_resolution_clock::duration nsecsDuraion = currTime - m_currentTimePoint;

	int dTime = std::chrono::duration_cast<std::chrono::milliseconds>(nsecsDuraion).count();

	// It is needed to create copy of schedulers because in update method they can unregister them from the manager.
	std::vector<Scheduler*> schedulersCopy = m_schedulers;
	for (auto iter = schedulersCopy.begin(); iter != schedulersCopy.end(); iter++)
		(*iter)->update(dTime);

	m_currentTimePoint = currTime;
}
//--------------------------------------------------------------------------------------------------
void SchedulersManager::init()
{
	m_currentTimePoint = std::chrono::high_resolution_clock::now();
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE