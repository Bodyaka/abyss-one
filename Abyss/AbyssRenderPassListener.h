#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class RenderPass;

class RenderPassListener
{
public:
	RenderPassListener() = default;
	virtual ~RenderPassListener() = default;

	virtual void onRenderingStarted(RenderPass* pass, RenderingResultPtr input) = 0;
	virtual void onRenderingFinished(RenderPass* pass, RenderingResultPtr output) = 0;
};

ABYSS_END_NAMESPACE