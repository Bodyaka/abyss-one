#include "AbyssTextureManager.h"

#include <cctype>
#include <set>
#include <fstream>
#include "SFML/Graphics/Image.hpp"
#include "AbyssUtilities.h"

ABYSS_BEGIN_NAMESPACE

TextureManager * TextureManager::getInstance()
{
	static TextureManager instance;
	return &instance;
}

TexturePtr TextureManager::createTexture(const String & path, const String & id /* = "" */)
{
	if (path.empty())
		return TexturePtr();
	
	String key = id.empty() ? path : id;
	if (hasTexture(key))
		return getTexture(key);

	sf::Image sfImage;
	if (!sfImage.loadFromFile(path))
	{
		String errStr = "Could not load file: " + path;
		showErrorMessage(errStr);
		return TexturePtr();
	}

	TexturePtr texture(new Texture());
	const void * data = sfImage.getPixelsPtr();
	sf::Vector2u imageSize = sfImage.getSize();
	if (imageSize.y == 1)
		texture->setRawData(data, imageSize.x, PF_UBYTE);
	else
		texture->setRawData(data, imageSize.x, imageSize.y, PF_UBYTE);
	
	insertTexture(key, texture);
	return texture;
}


TexturePtr TextureManager::createCubeTexture(const std::string& materialFilePath, const std::string& id /*= ""*/)
{
	if (materialFilePath.empty())
		return TexturePtr();

	String key = id.empty() ? materialFilePath : id;
	if (hasTexture(key))
	{
		auto storedTexture = getTexture(key);
		verifyStatement(storedTexture->getType() == GL_TEXTURE_CUBE_MAP,
						"Error while constructing CubeTexture with path: " + materialFilePath + " - There are already Texture with id [" + key + "] which has different type than GL_TEXTURE_CUBE_MAP!");

		return storedTexture;
	}

	std::string textureId;
	auto directionsMap = getDirectionFilePaths(materialFilePath, textureId);
	
	// Loading actual cube map images and checking whether they all share same physical space:
	std::map<Texture::CubeMapDirectionType, sf::Image> directionsImagesMap;
	for (const auto& cubeMapPair : directionsMap)
	{
		sf::Image sfImage;
		if (!sfImage.loadFromFile(cubeMapPair.second))
			verifyStatement(false, "Couldn't load cube map direction: [" + std::to_string(int(cubeMapPair.first)) + "] for file: [" + cubeMapPair.second + "]");
		else
		{
			if (!directionsImagesMap.empty())
			{
				const auto& referenceImage = directionsImagesMap.begin()->second;

				if (verifyStatement(referenceImage.getSize() == sfImage.getSize(), "DifferentSizeError: CubeMap with direction: [" + std::to_string(int(cubeMapPair.first)) + "] and file: [" + cubeMapPair.second + "] has different size!"))
					directionsImagesMap[cubeMapPair.first] = sfImage;
			}
			else
			{
				directionsImagesMap[cubeMapPair.first] = sfImage;
			}
		}
	}

	if (!verifyStatement(directionsMap.size() == 6, "Error while constructing CubeTexture with path: " + materialFilePath + "!"))
		return TexturePtr();

	std::map<Texture::CubeMapDirectionType, const void*> cubeMapData;
	for (const auto& cubeMapImagePair : directionsImagesMap)
		cubeMapData[cubeMapImagePair.first] = cubeMapImagePair.second.getPixelsPtr();

	auto cubeMapSize = directionsImagesMap.begin()->second.getSize();
	auto cubeMapTexture = TexturePtr(new Texture());
	
	auto errorBefore = glGetError();
	cubeMapTexture->setCubeMapData(cubeMapData, cubeMapSize.x, cubeMapSize.y, PF_UBYTE);
	auto errorAfter = glGetError();

	if (errorBefore == GL_NO_ERROR || errorBefore != errorAfter)
	{
		verifyStatement(errorAfter == errorBefore, "Error while setting raw data for cubeTexture: " + textureId);
	}
	insertTexture(textureId, cubeMapTexture);
	return cubeMapTexture;
}


void TextureManager::insertTexture(const std::string& textureId, TexturePtr texture)
{
#ifdef _DEBUG
	if (hasTexture(textureId))
		ABYSS_LOG("Warning: TextureManager::insertTexture - will replace existing texture with id: " + textureId);
#endif
	m_texturesMap[textureId] = texture;
}

bool TextureManager::removeTexture(const std::string& textureId)
{
	auto iter = m_texturesMap.find(textureId);
	if (iter != m_texturesMap.end())
	{
		m_texturesMap.erase(iter);
		return true;
	}

	return false;
}

bool TextureManager::hasTexture(const String& textureId) const
{
	return m_texturesMap.find(textureId) != m_texturesMap.end();
}

TexturePtr TextureManager::getTexture(const std::string& textureId) const
{
	auto iter = m_texturesMap.find(textureId);
	if (iter != m_texturesMap.end())
		return iter->second;

	return TexturePtr();
}

void TextureManager::clearAllTextures()
{
	m_texturesMap.clear();
}

std::map<Texture::CubeMapDirectionType, std::string> TextureManager::getDirectionFilePaths(const std::string& materialFilePath, std::string& textureId) const
{
	std::ifstream stream(materialFilePath);
	if (!stream.is_open())
	{
		showErrorMessage("Cannot open the file : " + materialFilePath);
		return std::map<Texture::CubeMapDirectionType, std::string>();
	}

	static std::map<std::string, Texture::CubeMapDirectionType> s_allPossibleDirections
	{
		{ "positivex", Texture::CubeMapDirectionType::Right },
		{ "posx", Texture::CubeMapDirectionType::Right },
		{ "right", Texture::CubeMapDirectionType::Right },

		{ "negativex", Texture::CubeMapDirectionType::Left },
		{ "negx", Texture::CubeMapDirectionType::Left },
		{ "left", Texture::CubeMapDirectionType::Left },

		{ "positivey", Texture::CubeMapDirectionType::Top },
		{ "posy", Texture::CubeMapDirectionType::Top },
		{ "top", Texture::CubeMapDirectionType::Top },

		{ "negativey", Texture::CubeMapDirectionType::Bottom },
		{ "negy", Texture::CubeMapDirectionType::Bottom },
		{ "bottom", Texture::CubeMapDirectionType::Bottom },

		{ "positivez", Texture::CubeMapDirectionType::Front },
		{ "posz", Texture::CubeMapDirectionType::Front },
		{ "front", Texture::CubeMapDirectionType::Front },

		{ "negativez", Texture::CubeMapDirectionType::Back },
		{ "negz", Texture::CubeMapDirectionType::Back },
		{ "back", Texture::CubeMapDirectionType::Back }
	};

	textureId.clear();
	std::string line;
	std::map<Texture::CubeMapDirectionType, std::string> cubeMapDirectionFilePaths;

	auto directoryPath = Utilities::getDirectoryPath(materialFilePath);
	while (getline(stream, line))
	{
		auto strings = Utilities::splitString(line, ":");

		if (verifyStatement(strings.size() == 2, "Incorrect format of cube map Material File: " + materialFilePath + "!"))
		{
			auto key = Utilities::trimString(strings[0]);
			auto value = Utilities::trimString(strings[1]);

			std::for_each(key.begin(), key.end(), std::tolower);

			if (key == "textureid")
			{
				verifyStatement(textureId.empty(), "TextureId for cube map Material File: " + materialFilePath + " is already exists: " + textureId);
				textureId = value;
			}
			else
			{
				auto iterDirection = s_allPossibleDirections.find(key);
				if (iterDirection != s_allPossibleDirections.end())
				{
					cubeMapDirectionFilePaths[iterDirection->second] = directoryPath + "/" + value;
				}
				else
				{
					raiseAnError("Undefined key [" + key + " for cube map Material File: " + materialFilePath);
				}
			}
		}
	}

	stream.close();

	verifyStatement(cubeMapDirectionFilePaths.size() == 6, "TextureId for cube map Material File: " + materialFilePath + " has incorrect number of directions!");
	return cubeMapDirectionFilePaths;
}

ABYSS_END_NAMESPACE