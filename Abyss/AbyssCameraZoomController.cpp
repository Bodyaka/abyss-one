#include "AbyssCameraZoomController.h"
#include "AbyssCamera.h"
#include "AbyssSceneManager.h"
#include "AbyssMouseWheelEvent.h"

ABYSS_BEGIN_NAMESPACE


CameraZoomController::CameraZoomController()
: CameraZoomController(SceneManager::instance()->getCamera())
{
	//resetCamera();
}

CameraZoomController::CameraZoomController(CameraPtr camera)
:	 m_camera(camera)
{
	if (camera)
	{
		float nearRange = camera->getNearDistance();
		float farRange = camera->getFarDistance();
		setRange(nearRange, farRange);

		setZoomStep(0.1);
	}
}

CameraZoomController::~CameraZoomController()
{

}

void CameraZoomController::resetCamera(CameraPtr camera)
{
	m_camera = camera;
}

void CameraZoomController::mouseWheelEvent(const MouseWheelEventPtr& event)
{
	if (!getCamera())
		return;

	ABYSS_LOG("The wheel is moved onto: " + std::to_string(event->getDelta()));

	Vector3D centerPos = getCamera()->getCenterPosition();
	Vector3D eyePos = getCamera()->getEyePosition();

	Vector3D centerToEyeDir = glm::normalize(eyePos - centerPos);
	auto distToCenterPos = glm::length(eyePos - centerPos);

	float zoomStep = 0;
	if (!isRelativeZoomStep())
		zoomStep = -getZoomStep() * event->getDelta();
	else
		zoomStep = - distToCenterPos * event->getDelta() * getZoomStep();

	float newDistance = distToCenterPos + zoomStep;
	newDistance = glm::clamp(newDistance, getNearestDistance(), getFurthestDistance());

	Vector3D newEyePos = centerPos + newDistance * centerToEyeDir;

	getCamera()->setEyePosition(newEyePos);
}

ABYSS_END_NAMESPACE
