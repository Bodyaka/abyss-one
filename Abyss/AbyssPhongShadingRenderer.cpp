#include "AbyssPhongShadingRenderer.h"

#include "AbyssRenderer.h"
#include "AbyssSceneObject.h"
#include "AbyssShaderProgram.h"
#include "AbyssMaterial.h"
#include "AbyssMeshRenderable.h"

#include "AbyssRendererMaterialExtension.h"
#include "AbyssRendererLightingExtension.h"


namespace
{
	const std::string SHADER_NAME = "Shading_worldSpace";

	// Misc:
	const std::string DEFAULT_COLOR_NAME = "DefaultColor";
}

ABYSS_BEGIN_NAMESPACE
//-------------------------------------------------------------------------------------------------
PhongShadingRenderer::PhongShadingRenderer()
	: Renderer(SHADER_NAME)
{

}
//-------------------------------------------------------------------------------------------------
PhongShadingRenderer::~PhongShadingRenderer()
{

}
//-------------------------------------------------------------------------------------------------
PhongShadingRenderer * PhongShadingRenderer::getInstance()
{
	static PhongShadingRenderer * renderer = nullptr;
	if (!renderer)
		renderer = new PhongShadingRenderer();

	return renderer;
}
//-------------------------------------------------------------------------------------------------
void PhongShadingRenderer::beforeRender( RenderableObject * const object )
{
	SceneObject * sceneObject = object->getBoundSceneObject();
	
	setupMatrixUniformsValues(sceneObject);
}
//-------------------------------------------------------------------------------------------------
void PhongShadingRenderer::afterRender( RenderableObject * const object )
{
}
//-------------------------------------------------------------------------------------------------
void PhongShadingRenderer::performRendering( RenderableObject * const object )
{
	setDefaultColorUniformValue(Vector4D(0));
	Renderer::performRendering(object);

	/*
	setActiveLightsCountUniformValue(0);
	setDefaultColorUniformValue(Vector4D(1));
	object->renderBoundingBox();
	*/
}
//-------------------------------------------------------------------------------------------------
void PhongShadingRenderer::setDefaultColorUniformValue(const Vector4D & color)
{
	getShaderProgram()->setUniform(DEFAULT_COLOR_NAME, color);
}
//-------------------------------------------------------------------------------------------------
bool PhongShadingRenderer::isSupports(const RenderableObject * const object) const
{
	return (dynamic_cast<const MeshRenderable* const>(object) != nullptr);
}
//-------------------------------------------------------------------------------------------------
void PhongShadingRenderer::registerExtensions()
{
	m_extensions.push_back(std::make_shared<RendererMaterialExtension>(this->getShaderProgram()));
	m_extensions.push_back(std::make_shared<RendererLightingExtension>(this->getShaderProgram()));
}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE