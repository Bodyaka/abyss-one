#include "SfguiWidgetDocker.h"


ABYSS_BEGIN_NAMESPACE

SfguiWidgetDocker::SfguiWidgetDocker(sfg::Widget* holderWidget)
	: m_holderWidget(holderWidget)
	, m_dockedWidget(nullptr)
	, m_cornerType(CornerType::None)
	, m_dockingSignal(0)
{

}


SfguiWidgetDocker::~SfguiWidgetDocker()
{
	unDock();
}


void SfguiWidgetDocker::dock(sfg::Widget* dockedWidget, CornerType cornerType)
{
	unDock();

	m_dockedWidget = dockedWidget;
	m_cornerType = cornerType;

	assert(m_dockedWidget != nullptr);

	m_dockingSignal = m_dockedWidget->GetSignal(sfg::Widget::OnSizeRequest).Connect(std::bind(&SfguiWidgetDocker::updateHolderPosition, this));
}


void SfguiWidgetDocker::unDock()
{
	if (m_dockingSignal != 0 && m_dockedWidget)
	{
		m_dockedWidget->GetSignal(sfg::Widget::OnSizeAllocate).Disconnect(m_dockingSignal);
	}
}


void SfguiWidgetDocker::updateHolderPosition()
{
	auto thisRect = m_holderWidget->GetAllocation();
	auto dockedRect = m_dockedWidget->GetAllocation();
	float thisOffset_X = ((int(m_cornerType) & int(DirectionType::Left)) != 0) ? 0.f : -thisRect.width;
	float thisOffset_Y = ((int(m_cornerType) & int(DirectionType::Top)) != 0) ? 0.f : -thisRect.height;

	float stickerPos_X = ((int(m_cornerType) & int(DirectionType::Left)) != 0) ? 0.f : dockedRect.width;
	float stickerPos_Y = ((int(m_cornerType) & int(DirectionType::Top)) != 0) ? 0.f : dockedRect.height;

	m_holderWidget->SetPosition(sf::Vector2f(stickerPos_X + thisOffset_X, stickerPos_Y + thisOffset_Y));
}

ABYSS_END_NAMESPACE

