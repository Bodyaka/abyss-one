#pragma once

#include "Abyss.h"

ABYSS_BEGIN_NAMESPACE

// This class provides changeable interface for all engine's objects
// TODO_REVIEW: makes it noncopyable??
class ChangeableObject
{
public:
	ChangeableObject();
	virtual ~ChangeableObject(){};

	void setChanged(bool changed = true);
	bool isChanged() const;

private:
	bool m_isChanged;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline ChangeableObject::ChangeableObject()
	: m_isChanged(false)
{}
//----------------------------------------------------------------------------------------------
inline void ChangeableObject::setChanged(bool changed)
{   m_isChanged = changed;	}
//----------------------------------------------------------------------------------------------
inline bool ChangeableObject::isChanged() const
{   return m_isChanged;	}
//----------------------------------------------------------------------------------------------
ABYSS_END_NAMESPACE