#pragma once

#include <vector>
#include <memory>
#include <map>
#include "AbyssRendering.h"
#include "AbyssSingleton.h"

ABYSS_BEGIN_NAMESPACE

class Light;

class LightsManager
	: public Singleton<LightsManager>
{
	friend class Singleton<LightsManager>;

public:

	LightsManager();
	~LightsManager();

	// Scene Objects:
	LightWeakPtr createLight();
	LightWeakPtr createLight(const std::string& lightName);
	LightWeakPtr getLight(const String & name) const;
	LightsContainerType getLights() const;

	bool hasLight(LightWeakPtr light) const;
	bool hasLight(const std::string& lightName) const;

	bool removeLight(const std::string& lightName);
	void removeAllLights();

	size_t getLightsSize() const;

private:
	LightsManager(const LightsManager& other) = delete;
	LightsManager& operator=(const LightsManager& other) = delete;

	typedef std::map<std::string, LightPtr> LightsInternalContainerType;
	LightsInternalContainerType m_lights;

	std::string getFreeLightName() const;

	static size_t m_nameGenerator;
	static const LightPtr s_nullLight;
};

// INCLUDE METHODS

inline size_t LightsManager::getLightsSize() const
{	return m_lights.size();	}


ABYSS_END_NAMESPACE