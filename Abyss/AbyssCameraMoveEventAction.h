#pragma once

#include "Abyss.h"
#include "AbyssRendering.h"
#include "AbyssKeyEventAction.h"

ABYSS_BEGIN_NAMESPACE

class CameraMoveEventAction
	: public KeyEventAction
{
public:
	static const float s_DefaultMoveOffset;
public:
	explicit CameraMoveEventAction(CameraPtr camera, float moveOffset = s_DefaultMoveOffset);
	~CameraMoveEventAction() {};

	void setMoveOffset(float moveOffset);
	float getMoveOffset() const;

	virtual void perform(const KeyEventPtr & event) override;

protected:
	float m_moveOffset;
	CameraPtr m_camera;
};
// INLINE METHODS
//-------------------------------------------------------------------------------------------------
inline void CameraMoveEventAction::setMoveOffset(float moveOffset)
{	m_moveOffset = moveOffset;	}
//-------------------------------------------------------------------------------------------------
inline float CameraMoveEventAction::getMoveOffset() const
{   return m_moveOffset;	}
//-------------------------------------------------------------------------------------------------
ABYSS_END_NAMESPACE