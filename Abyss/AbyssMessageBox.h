#pragma once

#include "Abyss.h"

ABYSS_BEGIN_NAMESPACE

// Some message box ))
class MessageBox
{
public:
	// Shows message box at the center of the screen.
	static void showMessageBox(const std::string & title, const std::string & message);
};

ABYSS_END_NAMESPACE