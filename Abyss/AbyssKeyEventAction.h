#pragma once

#include "AbyssEventsDef.h"
#include "AbyssEventAction.h"

ABYSS_BEGIN_NAMESPACE

class KeyEventAction:
	public EventAction
{
public:
	KeyEventAction() {};
	~KeyEventAction(){}

	virtual void perform(const KeyEventPtr & keyEvent) = 0;
};

ABYSS_END_NAMESPACE