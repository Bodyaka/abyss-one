#include "AbyssLightsManager.h"
#include "AbyssLight.h"
#include <boost/format.hpp>

ABYSS_BEGIN_NAMESPACE

size_t LightsManager::m_nameGenerator = 0;

LightsManager::LightsManager()
{

}

LightsManager::~LightsManager()
{
	removeAllLights();
}

LightWeakPtr LightsManager::createLight()
{
	return createLight(getFreeLightName());
}

LightWeakPtr LightsManager::createLight(const std::string& lightName)
{
	LightPtr light = std::make_shared<Light, std::string>(getFreeLightName());
	auto insertResult = m_lights.insert(LightsInternalContainerType::value_type(light->getName(), light));
	if (!insertResult.second)
	{
		assert(false);
		ABYSS_LOG("ERROR at: LightsManager::createLight(const std::string&) - The light with the name: " + light->getName() + " has been already inserted into LightsManager!");
	}

	return light.get();
}

std::string LightsManager::getFreeLightName() const
{
	std::string newName;

	// Picking first free Light name.
	do
	{
		std::string id = (boost::format("_Abyss_Light_%1%") % m_nameGenerator).str();
		++m_nameGenerator;

		if (!hasLight(newName))
			break;
	} while (true);

	return newName;
}

LightWeakPtr LightsManager::getLight(const String & name) const
{
	if (!hasLight(name))
		return nullptr;

	return m_lights.at(name).get();
}

LightsContainerType LightsManager::getLights() const
{
	LightsContainerType lights;

	lights.reserve(getLightsSize());
	for (auto lightPair : m_lights)
		lights.push_back(lightPair.second.get());

	return lights;
}

bool LightsManager::hasLight(LightWeakPtr light) const
{
	if (!light)
		return false;

	return hasLight(light->getName());
}

bool LightsManager::hasLight(const std::string& lightName) const
{
	return m_lights.find(lightName) != m_lights.end();
}

bool LightsManager::removeLight(const std::string& lightName)
{
	if (!hasLight(lightName))
		return false;

	m_lights.erase(m_lights.find(lightName));
	return true;
}

void LightsManager::removeAllLights()
{
	while (!m_lights.empty())
	{
		auto lightIter = m_lights.begin();
		removeLight(lightIter->second->getName());
	}
}

ABYSS_END_NAMESPACE

