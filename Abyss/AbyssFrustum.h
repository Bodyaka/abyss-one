#pragma once

#include <array>

#include "AbyssPlane.h"

ABYSS_BEGIN_NAMESPACE

class AABB;

class Frustum
{
public:
	enum PlaneId
	{
		PlaneNear = 0,	PlaneFar,	    PlaneLeft,
		PlaneRight,		PlaneBottom,	PlaneTop,
		PlaneMax
	};

	typedef std::array<Plane, PlaneMax> TPlanes;
public:
	Frustum(){};
	Frustum(const TPlanes & planes);
	~Frustum();

	void setPlanes(const TPlanes & planes);

	// Set specific plane...
	void setPlane(PlaneId plane, const Vector4D & norm);
	
	// ACCESS
	// Get plane by it's id:
	Plane getPlane(PlaneId plane) const;

	// INQUIRY
	bool contains(const Vector3D & point) const;
	
	IntersectionResult intersect(const AABB & box) const;
	// First method to check intersection:
	IntersectionResult intersect1(const AABB & box) const;
	// Second method to achieve intersection:
	IntersectionResult intersect2(const AABB & box) const;

private:

	TPlanes m_planes;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline void Frustum::setPlane(PlaneId plane, const Vector4D & norm)
{    m_planes[plane] = norm;	}
//----------------------------------------------------------------------------------------------
inline Plane Frustum::getPlane(PlaneId plane) const
{	return m_planes[plane];	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE