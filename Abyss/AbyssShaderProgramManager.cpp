#include "AbyssShaderProgramManager.h"

#include <fstream>
#include "AbyssUtilities.h"
#include "AbyssApplication.h"
#include "AbyssShaderProgram.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
ShaderProgramManager::ShaderProgramManager()
{
}
//----------------------------------------------------------------------------------------------
ShaderProgramManager::~ShaderProgramManager()
{
	for (auto iter : m_shaderPrograms)
		iter.second;
}
//----------------------------------------------------------------------------------------------
ShaderProgram * ShaderProgramManager::createShaderProgram(const String & shaderName, const String & vsFileName, const String & fsFileName)
{
	// If we have found shader with similar name, as input, just return that shader:
	auto iter = m_shaderPrograms.find(shaderName);
	if (iter != m_shaderPrograms.end())
		return iter->second;

	ShaderProgram * program = new ShaderProgram(shaderName, vsFileName, fsFileName);
	m_shaderPrograms.insert(TShaderProgramsContainer::value_type(shaderName, program));
	
	return program;
}
//----------------------------------------------------------------------------------------------
ShaderProgramManager::TShaderProgramsVector ShaderProgramManager::createShaderProgramVector(const String & directory)
{
	// TODO
	return TShaderProgramsVector();
}
//---------------------------------------------------------------------------------------------
ShaderProgramManager::TShaderProgramsVector ShaderProgramManager::createShaderProgramVectorXml(const String & shaderXmlFile)
{
	// TODO
	return TShaderProgramsVector();
}
//----------------------------------------------------------------------------------------------
ShaderProgramManager::TShaderProgramsVector ShaderProgramManager::loadShaderPrograms(const String & shaderCfgFile)
{
	TShaderProgramsVector resultShaderPrograms;

	std::ifstream file(shaderCfgFile);

	// If file could not be opened, show error message.
	if (!file.is_open())
	{
		showErrorMessage("Could not open file : " + shaderCfgFile);
		return resultShaderPrograms;
	}

	static const String cName = "name";
	static const String cEquals = "=";
	static const String cVSName = "vs";
	static const String cFSName = "fs";
	static const String cDirName = "directory";

	String cfgFileDir = Utilities::getDirectoryPath(shaderCfgFile);
	String line, shaderProgramName, vertexShaderPath, fragmentShaderPath, directoryPath;
	// Reads all lines from file and check them on tokens above:
	while (getline(file, line))
	{
		if (line.empty())
			continue;

		String localDirPath;

		// Checking, whether new line is directory format:
		if (!(localDirPath = getCfgFormatValue(line, cDirName)).empty())
		{
			directoryPath = cfgFileDir + localDirPath + "\\";
			continue;
		}

		// Check, whether new line is name of shader:
		if (!(shaderProgramName = getCfgFormatValue(line, cName)).empty())
		{
			// Getting names of vs and fs files:
			if ((vertexShaderPath = getCfgFormatValue(line, cVSName)).empty())
				continue;
			if ((fragmentShaderPath = getCfgFormatValue(line, cFSName)).empty())
				continue;

			vertexShaderPath = directoryPath + vertexShaderPath;
			fragmentShaderPath = directoryPath + fragmentShaderPath;
			
			// Creation of result program.
			if (ShaderProgram * program = createShaderProgram(shaderProgramName, vertexShaderPath, fragmentShaderPath))
			{
				resultShaderPrograms.push_back(program);
				m_shaderPrograms.insert(TShaderProgramsContainer::value_type(program->getName(), program));
			}
		}
	}

	return resultShaderPrograms;
}
//----------------------------------------------------------------------------------------------
String ShaderProgramManager::getCfgFormatValue(const String & line, const String & keyWord)
{
	String value;

	size_t id = -1;
	if ((id = line.find(keyWord)) == String::npos)
		return value;

	if ((id = line.find("=", id)) == String::npos)
		return value;

	int id1, id2;
	if ((id1 = line.find("\"", id)) == String::npos)
		return value;

	if ((id2 = line.find("\"", id1 + 1)) == String::npos)
		return value;

	int size = id2 - id1 - 1;
	return line.substr(id1 + 1, size);
}
//----------------------------------------------------------------------------------------------
ShaderProgram * ShaderProgramManager::getShaderProgram(const String & name)
{
	auto iter = m_shaderPrograms.find(name);
	if (iter == m_shaderPrograms.end())
		return nullptr;
	
	return iter->second;
}
//----------------------------------------------------------------------------------------------
bool ShaderProgramManager::hasShaderProgram(const String & name)
{
	auto iter = m_shaderPrograms.find(name);
	return iter != m_shaderPrograms.end();
}
//----------------------------------------------------------------------------------------------
ShaderProgramManager * ShaderProgramManager::instance()
{
	return Application::getCurrentApplication()->getShaderProgramManager();
}
//----------------------------------------------------------------------------------------------
ABYSS_END_NAMESPACE