#include "AbyssBlurRenderer.h"
#include "AbyssShaderProgram.h"
#include "AbyssRenderableObject.h"
#include "AbyssMaterial.h"
#include "AbyssTexture.h"

#include "AbyssRenderingResult.h"

ABYSS_BEGIN_NAMESPACE

namespace
{
	const std::string INPUT_TEXTURE_UNIFORM_NAME = "uInputTexture";
	const std::string BLUR_DIRECTION_UNIFORM_NAME = "uIsHorizontalBlur";
	const size_t inputTextureBindingPoint = 0;

}


BlurRenderer::BlurRenderer(const std::string& expectedInputTextureName, bool isHorizontalDirection)
	: Renderer("GaussianBlur")
	, m_expectedTextureInput(expectedInputTextureName)
	, m_blurDirection(isHorizontalDirection)
{

}

BlurRenderer::~BlurRenderer()
{

}

bool BlurRenderer::isSupports(const RenderableObject * const renderable) const
{
	// Does nothing. 
	return true;
}

void BlurRenderer::beforeRender(RenderableObject * const renderable)
{

}

void BlurRenderer::afterRender(RenderableObject * const renderable)
{
	
}

void BlurRenderer::startRendering(RenderingResultPtr input)
{
	Renderer::startRendering(input);

	auto inputTexture = input->getTexture(getExpectedTextureInput());
	if (inputTexture)
	{
		inputTexture->bind(inputTextureBindingPoint);
		getShaderProgram()->setUniform(INPUT_TEXTURE_UNIFORM_NAME, int(inputTextureBindingPoint));
		getShaderProgram()->setUniform(BLUR_DIRECTION_UNIFORM_NAME, getBlurDirection() ? 1 : 0);
	}
	else
	{
		raiseAnError("BlurRenderer - cannot find expected texture input!");
	}
}

void BlurRenderer::finishRendering(RenderingResultPtr output)
{
	auto inputTexture = output->getTexture(getExpectedTextureInput());
	if (inputTexture)
	{
		inputTexture->release();
	}
	else
	{
		raiseAnError("FullScreenRectRenderer - cannot find expected texture input!");
	}
}

ABYSS_END_NAMESPACE