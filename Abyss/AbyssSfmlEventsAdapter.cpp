#include "AbyssSfmlEventsAdapter.h"

// TEST TODO: remove me
#include <iostream>

#include "AbyssKeyEvent.h"
#include "AbyssMouseEvent.h"
#include "AbyssMouseWheelEvent.h"

ABYSS_BEGIN_NAMESPACE
//-----------------------------------------------------------------------------------------------
EventPtr SfmlEventsAdapter::convertToAbyssEvent(const sf::Event & sfmlEvent)
{
	switch (sfmlEvent.type)
	{
	case sf::Event::TextEntered:
		break;
	case sf::Event::KeyPressed:
		return std::static_pointer_cast<Event>(createAbyssKeyPressEvent(sfmlEvent));
	case sf::Event::KeyReleased:
		return std::static_pointer_cast<Event>(createAbyssKeyReleaseEvent(sfmlEvent));
	case sf::Event::MouseMoved:
		return std::static_pointer_cast<Event>(createAbyssMouseMoveEvent(sfmlEvent));
	case sf::Event::MouseButtonPressed:
		return std::static_pointer_cast<Event>(createAbyssMousePressEvent(sfmlEvent));
	case sf::Event::MouseButtonReleased:
		return std::static_pointer_cast<Event>(createAbyssMouseReleaseEvent(sfmlEvent));
	case sf::Event::MouseWheelMoved:
		return std::static_pointer_cast<Event>(createAbyssMouseWheelEvent(sfmlEvent));
		break;
	}
	return nullptr;
}
//-----------------------------------------------------------------------------------------------
Mouse::ButtonType SfmlEventsAdapter::fromSfmlButton(sf::Mouse::Button button)
{
	switch (button)
	{
	case sf::Mouse::Left:
		return Mouse::LeftButton;
	case sf::Mouse::Right:
		return Mouse::RightButton;
	case sf::Mouse::Middle:
		return Mouse::MiddleButton;
	}

	return Mouse::UnknownButton;
}
//--------------------------------------------------------------------------------------------------
KeyEventPtr SfmlEventsAdapter::createAbyssKeyPressEvent( const sf::Event & sfmlEvent ) 
{
	KeyEvent::KeyType key = KeyEvent::fromSfmlKey(sfmlEvent.key.code);
	return KeyEvent::createKeyPressEvent(key);
}
//-----------------------------------------------------------------------------------------------
KeyEventPtr SfmlEventsAdapter::createAbyssKeyReleaseEvent(const sf::Event & sfmlEvent) 
{
	KeyEvent::KeyType key = KeyEvent::fromSfmlKey(sfmlEvent.key.code);
	return KeyEvent::createKeyReleaseEvent(key);
}
//-----------------------------------------------------------------------------------------------
MouseEventPtr SfmlEventsAdapter::createAbyssMouseMoveEvent(const sf::Event & sfmlEvent) 
{
	int posX = sfmlEvent.mouseMove.x;
	int posY = sfmlEvent.mouseMove.y;
	Vector2D mousePosition(posX, posY);
	Mouse::setMousePosition(mousePosition);
	return MouseEvent::createMouseMoveEvent(mousePosition);
}
//-----------------------------------------------------------------------------------------------
MouseEventPtr SfmlEventsAdapter::createAbyssMousePressEvent(const sf::Event & sfmlEvent) 
{
	Mouse::ButtonType button = fromSfmlButton(sfmlEvent.mouseButton.button);
	Mouse::addToPressedButtons(button);
	int posX = sfmlEvent.mouseButton.x;
	int posY = sfmlEvent.mouseButton.y;
	return MouseEvent::createMousePressEvent(Abyss::Vector2D(posX, posY), button);
}
//-----------------------------------------------------------------------------------------------
MouseEventPtr SfmlEventsAdapter::createAbyssMouseReleaseEvent(const sf::Event & sfmlEvent) 
{
	Mouse::ButtonType button = fromSfmlButton(sfmlEvent.mouseButton.button);
	Mouse::removeFromPressedButtons(button);
	int posX = sfmlEvent.mouseButton.x;
	int posY = sfmlEvent.mouseButton.y;
	return MouseEvent::createMouseReleaseEvent(Abyss::Vector2D(posX, posY), button);
}
//-----------------------------------------------------------------------------------------------
MouseWheelEventPtr SfmlEventsAdapter::createAbyssMouseWheelEvent(const sf::Event & sfmlEvent)
{
	int posX = sfmlEvent.mouseWheel.x;
	int posY = sfmlEvent.mouseWheel.y;
	return MouseWheelEvent::createMouseWheelEvent(Abyss::Vector2D(posX, posY), sfmlEvent.mouseWheel.delta);
}
//-----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE