#pragma once

#include "AbyssRenderer.h"

ABYSS_BEGIN_NAMESPACE

class DeferredShadingRenderer
	: public Renderer
{
public:
	static DeferredShadingRenderer* getInstance();

	bool isSupports(const RenderableObject * const renderable) const override;

	virtual void startRendering(RenderingResultPtr input) override;
	virtual void finishRendering(RenderingResultPtr output) override;

private:
	DeferredShadingRenderer();

	void beforeRender(RenderableObject * const object) override;
	void afterRender(RenderableObject * const object) override;


	virtual void registerExtensions() override;

};


ABYSS_END_NAMESPACE