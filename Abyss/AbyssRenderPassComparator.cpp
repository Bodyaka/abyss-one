#include "AbyssRenderPassComparator.h"
#include "AbyssRenderPassManager.h"

ABYSS_BEGIN_NAMESPACE

RenderPassComparator::RenderPassComparator(RenderPassManager* renderPassManager)
	: m_renderPassManager(renderPassManager)
{

}

bool RenderPassComparator::operator()(const RenderPass* const first, const RenderPass* const second) const
{
	return operator()(*first, *second);
}

bool RenderPassComparator::operator()(const RenderPass& first, const RenderPass& second) const
{
	assert(getManager());

	int index1 = getManager()->getIndexOf(first.getName());
	int index2 = getManager()->getIndexOf(second.getName());

	return index1 < index2;

}

ABYSS_END_NAMESPACE