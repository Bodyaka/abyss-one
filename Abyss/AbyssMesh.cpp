#include "AbyssMesh.h"

#include "AbyssMaterial.h"
#include "AbyssRendererVisitor.h"

#include "boost/lexical_cast.hpp"

#include "AbyssVertexArrayObject.h"
#include "AbyssVertexBufferObject.h"
#include "AbyssDefaultVertexAttributes.h"

#include "AbyssMeshUtilities.h"

ABYSS_BEGIN_NAMESPACE
	
int Mesh::m_numberOfGeneratedMeshes = 0;
//--------------------------------------------------------------------------------------------------
void Mesh::VertexData::clear()
{
	// Clear all containers:
	positions.clear();
	texels.clear();
	normals.clear();
	tangents.clear();

	// Shrinking to fit (this needed for better memory consumption):
	positions.shrink_to_fit();
	texels.shrink_to_fit();
	normals.shrink_to_fit();
	tangents.shrink_to_fit();
}
//--------------------------------------------------------------------------------------------------
Mesh::Mesh(GLenum drawMode, Mesh * parent)
	: BaseMesh(parent)
	, GeometryObject(drawMode)
	, m_material(new Material())
{
	m_name = generateMeshName();
}
//--------------------------------------------------------------------------------------------------
Mesh::Mesh(const String & name, GLenum drawMode, Mesh * parent)
	: BaseMesh(parent)
	, GeometryObject(drawMode)
	, m_name(name)
	, m_material(new Material())
{
	if (m_name.empty())
		m_name = generateMeshName();
}
//--------------------------------------------------------------------------------------------------
Mesh::~Mesh()
{
	ABYSS_LOG("Mesh with name" + getName() + "has been deleted!");
}
//--------------------------------------------------------------------------------------------------
Mesh * Mesh::createSubMesh(const String & name, GLenum drawMode)
{
	auto mesh = new Mesh(name, drawMode, this);
	addChild(mesh);

	// TODO: need return by smart ptr
	return mesh;
}
//--------------------------------------------------------------------------------------------------
Mesh * Mesh::createSubMesh(const String & name, GLenum drawMode, const VertexData & data)
{
	Mesh * submesh = createSubMesh(name, drawMode);
	submesh->setVertexData(data);

	// TODO: need return by smart ptr
	return submesh;
}
//--------------------------------------------------------------------------------------------------
Mesh * Mesh::getSubMesh(const String & name)
{
	// TODO. Need hash map.
	if (m_name == name)
		return this;

	for (size_t i = 0; i<getChildrenCount(); ++i)
	{
		Mesh * mesh = getChild(i);
		if (mesh->getName() == name)
			return mesh;
	}

	return nullptr;
}
//--------------------------------------------------------------------------------------------------
void Mesh::render(RendererVisitor* visitor)
{
	visitor->visit(this);

	// Render all mesh's children
	for(size_t i = 0; i<getChildrenCount(); ++i)
		getChild(i)->render(visitor);
}
//--------------------------------------------------------------------------------------------------
void Mesh::setVertexData(const VertexData & vertexData)
{
	if (vertexData.positions.empty())
		throw std::exception("Error setup vertex data for Mesh. Vertex positions always must have at least one vertex.");

	size_t verticesSize = vertexData.positions.size();
	float * verticesPositions = (float *)vertexData.positions.data();
	float * verticesNormals = (!vertexData.normals.empty()) ? (float *)vertexData.normals.data() : nullptr;
	float * verticesTexels = (!vertexData.texels.empty()) ? (float *)vertexData.texels.data() : nullptr;
	float * verticesTangents = (!vertexData.tangents.empty()) ? (float *)vertexData.tangents.data() : nullptr;
	float * verticesBiTangents = (!vertexData.bitangents.empty()) ? (float *)vertexData.bitangents.data() : nullptr;

	setVertexData(verticesSize, verticesPositions, verticesTexels, verticesNormals, verticesTangents, verticesBiTangents);
}
//--------------------------------------------------------------------------------------------------
void Mesh::setVertexData(size_t numOfVertices, float * verticesPositions, float * verticesTexels, float * verticesNormals, float * verticesTangents, float * verticesBitangents)
{
	m_vao = VertexArrayObject::createVAO();

	m_vao->bind();
	
	size_t vector2dSize = 2 * sizeof(float);
	size_t vector3dSize = 3 * sizeof(float);

	// TODO: Problem - need to define custom anchor point. This solution is not acceptable.
	// Also, this is quite a slow solution - need increase performance.

	// Setup positions:
	if (verticesPositions)
		addBufferObject(PositionAttribute, VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER, vector3dSize * numOfVertices, (void *)verticesPositions, GL_STATIC_DRAW));

	// Setup normals
	if (verticesNormals)
		addBufferObject(NormalAttribute, VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER, vector3dSize * numOfVertices, (void *)verticesNormals, GL_STATIC_DRAW));

	// Setup texels:
	if (verticesTexels)
		addBufferObject(TexelAttribute, VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER, vector2dSize * numOfVertices, (void *)verticesTexels, GL_STATIC_DRAW));

	// Setup tangents:
	if (verticesTangents)
		addBufferObject(TangentAttribute, VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER, vector3dSize * numOfVertices, (void *)verticesTangents, GL_STATIC_DRAW));

	// Setup bitangents:
	if (verticesBitangents)
		addBufferObject(BiTangentAttribute, VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER, vector3dSize * numOfVertices, (void *)verticesBitangents, GL_STATIC_DRAW));

	m_vao->release();

	m_aabb = AABB::generateAxisAlignedBoundingBox(verticesPositions, numOfVertices);

	// Enable all attribute locations. TODO. In future need be removed:
	for (int attribute = PositionAttribute; attribute <= BiTangentAttribute; ++attribute)
		setupAttribute(attribute, attribute);

	m_verticesCount = numOfVertices;

	markBboxDirty();
	//m_indicesCount = 0;
}
//--------------------------------------------------------------------------------------------------
void Mesh::setIndices(const ContainerUint & indices)
{
	m_vao->bind();

	m_indicesCount = indices.size();

	if (!indices.empty())
		setIndicesBufferObject(VertexBufferObject::createBufferObject(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * m_indicesCount, (void *)indices.data(), GL_STATIC_DRAW));

	m_vao->release();
}
//--------------------------------------------------------------------------------------------------
MeshPtr Mesh::createFullScreenRect()
{
	static int numberOfInstances = 0;
	String numberSuffix = boost::lexical_cast<String>(numberOfInstances++);

	float side = 1.f;
	// Some hack:
	float depth = 1.f - std::numeric_limits<float>::epsilon();

	std::string meshName = "Mesh_FullScreenRect_Impl_" + numberSuffix;


	return MeshPtr(createRect(meshName, Vector3D(-side, side, depth), Vector3D(-side, -side, depth), Vector3D(side, -side, depth), Vector3D(side, side, depth)));
}
//--------------------------------------------------------------------------------------------------
MeshPtr Mesh::createRect(const std::string& meshName, const Vector3D& topLeft, const Vector3D& bottomLeft, const Vector3D& bottomRight, const Vector3D& topRight)
{
	// Creating new mesh.
	Mesh* mesh = new Mesh(meshName, GL_TRIANGLES, false);

	VertexData data;
	data.positions.push_back(topLeft);
	data.positions.push_back(bottomLeft);
	data.positions.push_back(bottomRight);
	data.positions.push_back(topRight);

	data.texels.push_back(Vector2D(0.f, 1.f));
	data.texels.push_back(Vector2D(0.f, 0.f));
	data.texels.push_back(Vector2D(1.f, 0.f));
	data.texels.push_back(Vector2D(1.f, 1.f));

	ContainerUint indices;
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(2);
	indices.push_back(3);
	indices.push_back(0);

	data.normals = MeshUtilities::computeNormals(data.positions, indices);

	mesh->setVertexData(data);
	mesh->setIndices(indices);

	return MeshPtr(mesh);
}
//--------------------------------------------------------------------------------------------------
String Mesh::generateMeshName()
{
	String generatedName = boost::lexical_cast<String>(m_numberOfGeneratedMeshes);
	++m_numberOfGeneratedMeshes;

	return generatedName;
}
//--------------------------------------------------------------------------------------------------
size_t Mesh::getVerticesSizePerPrimitive() const
{
	switch (getDrawMode())
	{
	case GL_POINTS:
		return 1;
	case GL_LINES:
		return 2;
	case GL_TRIANGLES:
		return 3;
	}

	throw std::exception("Error: Unsupported draw mode of Mesh.");
}
//--------------------------------------------------------------------------------------------------
void Mesh::markBboxDirty()
{
	m_isBoundingBoxDirty = true;

	if (auto parent = getParent())
		parent->markBboxDirty();
}
//--------------------------------------------------------------------------------------------------
const AABB& Mesh::getAABB() const
{
	if (m_isBoundingBoxDirty)
	{
		m_unitedAABB = m_aabb;

		for (size_t i = 0; i < getChildrenCount(); ++i)
		{
			m_unitedAABB.unit(getChild(i)->getAABB());
		}

		m_isBoundingBoxDirty = false;
	}

	return m_unitedAABB;
}
//----------------------------------------------------------------------------------------------



ABYSS_END_NAMESPACE
