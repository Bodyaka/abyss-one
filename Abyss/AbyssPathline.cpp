#include "AbyssPathline.h"

#include "AbyssVertexArrayObject.h"
#include "AbyssVertexBufferObject.h"
#include "AbyssDefaultVertexAttributes.h"

ABYSS_BEGIN_NAMESPACE

Pathline::Pathline()
	: GeometryObject(GL_LINE_STRIP)
	, m_isDirty(true)
{
	addBufferObject(PositionAttribute, VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER));
}


Pathline::~Pathline()
{

}

void Pathline::setVertices(const ContainerV3D& vertices)
{
	m_isDirty = true;

	m_vertices = vertices;
}

void Pathline::addVertex(const Vector3D& vertex)
{
	m_vertices.push_back(vertex);
	m_isDirty = true;
}

void Pathline::clearVertices()
{
	setVertices(ContainerV3D());
}

void Pathline::render()
{
	if (m_isDirty)
	{
		m_verticesCount = getVertices().size();

		m_isDirty = false;

		m_vao = VertexArrayObject::createVAO();

		// Binding VAO for sure:
		m_vao->bind();

		// Setup position buffer data:
		getVertexBufferObject(PositionAttribute)->bufferData(sizeof(Vector3D)* getVertices().size(), (void *)getVertices().data(), GL_STATIC_DRAW);

		// Releasing attribute:
		m_vao->release();

		// TODO

		// Enable all attribute locations. TODO. In future need be removed:
		for (int attribute = PositionAttribute; attribute <= BiTangentAttribute; ++attribute)
			setupAttribute(attribute, attribute);

	}

	drawArrays();
}

ABYSS_END_NAMESPACE