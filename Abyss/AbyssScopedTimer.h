#pragma once

#include "Abyss.h"
#include <chrono>

ABYSS_BEGIN_NAMESPACE

template <class DurationType = std::chrono::milliseconds>
class ScopedTimer
{

public:
	
	ScopedTimer(const std::string& name);
	~ScopedTimer();

	DurationType getElapsedTime() const;

private:

	typedef std::chrono::high_resolution_clock ClockType;
	typedef typename ClockType::time_point	TimePointType;
	TimePointType m_now;
	std::string m_name;
};

// TEMPLATE CLASS DEFINITION
template <class DurationType = std::chrono::milliseconds>
ScopedTimer<DurationType>::ScopedTimer(const std::string& name)
	: m_name(name)
{
	m_now = ClockType::now();
}

template <class DurationType = std::chrono::milliseconds>
ScopedTimer<DurationType>::~ScopedTimer()
{
	auto elapsedTime = getElapsedTime().count();
	std::string str = "ScopedTimer: " + m_name + " " + std::to_string(elapsedTime) + " msec";
	ABYSS_LOG(str);
}

template <class DurationType = std::chrono::milliseconds>
DurationType ScopedTimer<DurationType>::getElapsedTime() const
{
	auto now = ClockType::now();

	return std::chrono::duration_cast<DurationType>(now - m_now);
}


ABYSS_END_NAMESPACE