#include "AbyssViewingFrustum.h"
#include "AbyssAABB.h"
#include <array>


ABYSS_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------------------
ViewingFrustum::ViewingFrustum(const Params & i_params)
	: ChangeableObject()
{
	setParams(i_params);
}
//--------------------------------------------------------------------------------------------------
ViewingFrustum::~ViewingFrustum()
{

}
//--------------------------------------------------------------------------------------------------
void ViewingFrustum::setParams(const Params & i_params)
{
	m_params = i_params;

	float inverseAspect = 1.f / getAspect();

	// computing focalLength:
	m_focalLength = inverseAspect / std::tan(glm::radians(getFovY()));

	// computing FovX
	m_fovx = glm::degrees(2 * std::atan(1.f / (2 * m_focalLength)));

	// computing projection matrix:
	m_projectionMatrix = glm::perspective(getFovY(), getAspect(), getNearDistance(), getFarDistance());

	updateProjectionViewMatrix();
}
//--------------------------------------------------------------------------------------------------
Plane ViewingFrustum::getNearPlane() const
{		
	return Vector4D(0.f, 0.f, -1.f, -getNearDistance());	
}
//--------------------------------------------------------------------------------------------------
Plane ViewingFrustum::getFarPlane() const
{		
	return Vector4D(0.f, 0.f, 1.f, getFarDistance());	
}
//--------------------------------------------------------------------------------------------------
Plane ViewingFrustum::getLeftPlane() const
{
	float e = getFocalLength();
	float e2 = e * e;
	float denum = 1.f / std::sqrt(e2 + 1);
	
	return Vector4D(e * denum, 0.f, -denum, 0.f);
}
//--------------------------------------------------------------------------------------------------
Plane ViewingFrustum::getRightPlane() const
{
	float e = getFocalLength();
	float e2 = e * e;
	float denum = 1.f / std::sqrt(e2 + 1);
	
	return Vector4D( - e * denum, 0.f, -denum, 0.f);
}
//--------------------------------------------------------------------------------------------------
Plane ViewingFrustum::getBottomPlane() const
{
	float e = getFocalLength();
	float e2 = e * e;
	float a = 1 / getAspect();
	float a2 = a * a;

	float denum = 1.f / std::sqrt(e2 + a2);
	return Vector4D(0.f, e * denum, - a * denum, 0.f);
}
//--------------------------------------------------------------------------------------------------
Plane ViewingFrustum::getTopPlane() const
{
	float e = getFocalLength();
	float e2 = e * e;
	float a = 1 / getAspect();
	float a2 = a * a;

	float denum = 1.f / std::sqrt(e2 + a2);
	return Vector4D(0.f, - e * denum, - a * denum, 0.f);

}
//--------------------------------------------------------------------------------------------------
void ViewingFrustum::updateProjectionViewMatrix()
{
	m_projecionViewMatrix = m_projectionMatrix * m_viewMatrix;

	// Extracting planes from ProjectionView matrix:
	float a, b, c, d;
	const Matrix4D & m = m_projecionViewMatrix;

	Frustum::TPlanes planes;

	// Setting left plane:
	a = m[0][3] + m[0][0];
	b = m[1][3] + m[1][0];
	c = m[2][3] + m[2][0];
	d = m[3][3] + m[3][0];
	planes[Frustum::PlaneLeft] = Plane(Vector4D(a, b, c, d));

	// Setting right plane:
	a = m[0][3] - m[0][0];
	b = m[1][3] - m[1][0];
	c = m[2][3] - m[2][0];
	d = m[3][3] - m[3][0];
	planes[Frustum::PlaneRight] = Plane(Vector4D(a, b, c, d));

	// Setting bottom plane:
	a = m[0][3] + m[0][1];
	b = m[1][3] + m[1][1];
	c = m[2][3] + m[2][1];
	d = m[3][3] + m[3][1];
	planes[Frustum::PlaneBottom] = Plane(Vector4D(a, b, c, d));

	// Setting top plane:
	a = m[0][3] - m[0][1];
	b = m[1][3] - m[1][1];
	c = m[2][3] - m[2][1];
	d = m[3][3] - m[3][1];
	planes[Frustum::PlaneTop] = Plane(Vector4D(a, b, c, d));

	// Setting near plane:
	a = m[0][3] + m[0][2];
	b = m[1][3] + m[1][2];
	c = m[2][3] + m[2][2];
	d = m[3][3] + m[3][2];
	planes[Frustum::PlaneNear] = Plane(Vector4D(a, b, c, d));

	// Setting far plane:
	a = m[0][3] + m[0][2];
	b = m[1][3] + m[1][2];
	c = m[2][3] + m[2][2];
	d = m[3][3] + m[3][2];
	planes[Frustum::PlaneFar] = Plane(Vector4D(a, b, c, d));

	m_worldFrustum.setPlanes(planes);

	// Notified, that we changed.
	setChanged();
}
//--------------------------------------------------------------------------------------------------
void ViewingFrustum::setViewMatrix(const Matrix4D & viewMatrix)
{
	m_viewMatrix = viewMatrix;

	updateProjectionViewMatrix();
}
//--------------------------------------------------------------------------------------------------
Frustum ViewingFrustum::getClipSpaceFrustum() const
{
	Frustum::TPlanes planes;

	planes[Frustum::PlaneNear] = getNearPlane();
	planes[Frustum::PlaneFar]  = getFarPlane();
	planes[Frustum::PlaneLeft] = getLeftPlane();
	planes[Frustum::PlaneRight] = getRightPlane();
	planes[Frustum::PlaneBottom] = getBottomPlane();
	planes[Frustum::PlaneTop] = getTopPlane();

	return Frustum(planes);
}
//--------------------------------------------------------------------------------------------------
Vector3D ViewingFrustum::unProject(const Vector2D & windowPoint)
{
	// TODO: need proper getWindowSize calculation
	Vector2D windowSize = Vector2D(0, 0);//
	
	float x = (windowPoint.x / windowSize.x) * 2.f - 1.f;
	float y = (1.f - windowPoint.y / windowSize.y) * 2.f - 1.f;
	
	Vector4D p0(x, y, 0.f, 1.f);
	Vector4D p1(x, y, 1.f, 1.f);

	Matrix4D invProjView = glm::inverse(getProjectionMatrix());

	Vector4D nonScaled_p0 = invProjView * p0;
	Vector4D nonScaled_p1 = invProjView * p1;
	Vector3D r0 = math::swizzleProj(nonScaled_p0);
	Vector3D r1 = math::swizzleProj(nonScaled_p1);

	Matrix4D viewMx = glm::inverse(getViewMatrix());
	Vector4D v0_4d = viewMx * Vector4D(r0, 1.0f);
	Vector4D v1_4d = viewMx * Vector4D(r1, 1.0f);

	Vector3D v0 = math::swizzleProj(v0_4d);
	Vector3D v1 = math::swizzleProj(v1_4d);

	// TODO:
	/*
	Vector4D projValue = getProjectionViewMatrix() * Vector4D(camera()->eye(), 1.f);
	Vector3D projValue3D = swizzleProj(projValue);
	Vector3D rV = glm::normalize(r1 - r0);
	Vector3D result = r0 + 2.f * rV;*/

	return r0;
}
//--------------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE