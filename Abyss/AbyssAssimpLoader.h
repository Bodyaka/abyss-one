#pragma once

#include "AbyssMeshLoader.h"

struct aiMesh;
struct aiNode;
struct aiScene;
struct aiMaterial;

ABYSS_BEGIN_NAMESPACE

class AssimpLoader
	: public MeshLoader
{
public:
	static AssimpLoader * instance();

	static void makeAsDefaultLoader();

protected:
	AssimpLoader();
	~AssimpLoader();

private:

	void processAssimpNode(aiNode* node, const aiScene* scene, MeshPtr parentMesh, const String& meshFileName);

	MeshPtr performLoadingMeshFromFile(const String & meshFileName) override;

	// Return draw mode of specified mesh (i.e. triangles, lines, points, etc.)
	static GLenum getDrawMode(aiMesh * mesh);

	// TODO: temporary fix. Must return MeshPtr!!
	static Mesh *  createRawMeshFrom(aiMesh * mesh, MeshPtr parentMesh);
	static MaterialPtr createMaterialFrom(aiMaterial * material, const String & meshFileName);

private:
	// Solely for debug purposes
	// TODO: need to use them as method parameters in order to perform multi-threaded loading
	size_t m_totalVerticesCount = 0;
	size_t m_totalFacesCount = 0;
	size_t m_totalMeshesCount = 0;
	
};

ABYSS_END_NAMESPACE