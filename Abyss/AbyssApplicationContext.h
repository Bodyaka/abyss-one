#pragma once

#include "Abyss.h"

#include <string>
#include <vector>

ABYSS_BEGIN_NAMESPACE

class Window;
class FPSCounter;
class Application;

class ApplicationContext
{
	friend class Application;

	struct CreateOptions
	{
		// Console arguments and parameters:
		int argc;
		char ** argv;

		// Window's title
		std::string windowTitle;

		// Window's geometry:
		int x,		// x-position of window
			y,		// y-position of window
			width,  // width of window
			height; // height of window
	};

public:
	// Return shared application context.
	// Note, that this function does not create any application context.
	static ApplicationContext * getSharedApplicationContext();
		
	// Start running Event loop.
	virtual void go();

	// Getters:
	// Return target window, which is displaying now:
	Window * getWindow() const;
	// Return location of exe file.
	const std::string & getApplicationPath() const;

	FPSCounter* getFPSCounter() const;
	
	void polish();
private:


	// Creates application context and application, if not created.
	static ApplicationContext * create(const CreateOptions & windowParams);
	// Creates application context:
	ApplicationContext(const CreateOptions & windowParams);
	// Delete application context and br br.
	~ApplicationContext();

	// Application path.
	std::string m_applicationPath;
	// Window, which is rendered now:
	Window * m_window;

	std::unique_ptr<FPSCounter> m_fpsCounter;

	// Singleton context:
	static ApplicationContext * m_sharedApplicationContext;
};

// INLINE METHODS
//--------------------------------------------------------------------------------------------
inline const std::string & ApplicationContext::getApplicationPath() const
{	return m_applicationPath;	}
//--------------------------------------------------------------------------------------------
inline Window * ApplicationContext::getWindow() const
{	return m_window;	}
//--------------------------------------------------------------------------------------------
inline FPSCounter* ApplicationContext::getFPSCounter() const
{	return m_fpsCounter.get();	}
//--------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE