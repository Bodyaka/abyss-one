#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class CompositorPass;
class RenderTarget;

class CompositorPassHandler
{
public:
	explicit CompositorPassHandler() {};
	virtual ~CompositorPassHandler() = default;

	virtual void prepareRenderTarget(RenderTarget* renderTarget) = 0;
	
	virtual void onRenderingStarted(RenderTarget* renderTarget) = 0;
	virtual void onRenderPassStarted(RenderPass* renderPass, RenderTarget* renderTarget, RenderingResultPtr input) = 0;
	virtual void onRenderPassFinished(RenderPass* renderPass, RenderTarget* renderTarget, RenderingResultPtr input) = 0;
	virtual void onFinishRendering(RenderTarget* renderTarget, RenderingResultPtr output) = 0;

private:
	CompositorPassHandler(const CompositorPassHandler& other) = delete;
	CompositorPassHandler& operator = (const CompositorPassHandler& other) = delete;
};

// -------- class CompositorPassDefaultHandler ------------

class CompositorPassDefaultHandler
	: public CompositorPassHandler
{
public:
	// Specifies input texture name, which will be used as main input for post-processing
	// For more info, @see setExpectedInputTextureName
	CompositorPassDefaultHandler(const std::string& textureName);
	virtual ~CompositorPassDefaultHandler() {};

	void setExpectedInputTextureName(const std::string& textureName);
	const std::string& getExpectedInputTextureName() const;

	virtual void prepareRenderTarget(RenderTarget* renderTarget) override;

	virtual void onRenderingStarted(RenderTarget* renderTarget) override;
	virtual void onRenderPassStarted(RenderPass* renderPass, RenderTarget* renderTarget, RenderingResultPtr input) override;
	virtual void onRenderPassFinished(RenderPass* renderPass, RenderTarget* renderTarget, RenderingResultPtr input) override;
	virtual void onFinishRendering(RenderTarget* renderTarget, RenderingResultPtr output) override;

protected:

	static void swapTextureWithRenderTarget(RenderingResultPtr result, const std::string& textureName, RenderTarget* renderTarget);

	std::string m_textureName;
	Abyss::TexturePtr m_createdTexture;
};

// INLINE METHODS;
//-------------------------------------------------------------------------------------------------
inline void CompositorPassDefaultHandler::setExpectedInputTextureName(const std::string& textureName)
{
	m_textureName = textureName;
}
//-------------------------------------------------------------------------------------------------
inline const std::string& CompositorPassDefaultHandler::getExpectedInputTextureName() const
{
	return m_textureName;
}
//-------------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE