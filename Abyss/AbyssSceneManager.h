#pragma once

#include <unordered_map>

#include "AbyssSceneObject.h"
#include "AbyssChangeableObject.h"
#include "AbyssLightsContainer.h"

ABYSS_BEGIN_NAMESPACE

class Camera;
class SceneObject;
class SceneDivider;
class Window;
class RenderQueue;
class RenderPassManager;

class SceneManager
	: public ChangeableObject
	, public LightsContainer
{
	friend class SceneObject;

public:
	typedef std::unordered_map<String, SceneObjectPtr> SceneObjectsContainer;

public:
	SceneManager();
	~SceneManager();

	// Calling init happens only at startup.
	// You shouldn't call this method unless you know what are you doing.
	void init();

	// update all scene objects.
	void update();

	// Rendering:
	// draw all scene objects.
	void render();

	// Add listener for global rendering events
	void addListener(SceneManagerListener* listener);
	// Remove specific rendering listener;
	void removeListener(SceneManagerListener* listener);

	void onStartRendering();
	void onFinishRendering();

	// Scene Objects:
	SceneObjectPtr createSceneObject();
	SceneObjectPtr createSceneObject(const String & name);
	SceneObjectPtr getSceneObject(const String & name) const;
	const SceneObjectsContainer & getSceneObjects() const;

	bool hasSceneObject(SceneObjectPtr object) const;
	bool hasSceneObject(const String & name) const;

	bool removeSceneObject(const String & name);
	bool removeSceneObject(SceneObjectPtr objectPtr);
	void removeAllSceneObjects();
	
	size_t getSceneObjectsSize() const;

	// Return axis aligned bounding box for this one scene manager.
	// Note, that axis aligned bounding box will be updated only once before rendering. 
	// Unless you pass true as parameter:
	const AABB & getAxisAlignedBoundingBox(bool recompute = false) const;

	// Camera:
	void setCamera(CameraPtr camera);
	CameraPtr getCamera() const;

	// Post processing:
	// Post-processing methods should be moved to separate model class.
	void setSSAOEnabled(bool isEnabled);
	bool isSSAOEnabled() const;

	TexturePtr getSSAOTexture() const;
	RenderableObjectPtr getFullScreenRectRenderable() const;

	// Rendering features:
	RenderQueue* getRenderQueue() const;
	RenderPassManager* getRenderPassManager() const;

	// Returns window, where this manager attached to.
	Window* getWindow() const;

	static SceneManager * instance();

	/*// TODO!!!
	void removeLight(LightPtr light);
	void removeLight(const String & name);
	void removeAllLights();

	// Set current scene divider for scene manager. Return previous scene divider:
	SceneDivider * setSceneDivider(SceneDivider * divider);

	// Return render queue of manager:
	RenderQueue & getRenderQueue();
	// Return scene divider of scene manager. By default no scene divider is using:
	SceneDivider * getSceneDivider() const;
	const LightsMap & getLights() const;*/
	
private:	
	// Updates axis aligned bounding box 
	void updateAxisAlignedBoundingBox();

	String findFreeSceneObjectName() const;

	void prepareSceneObjetsForRendering();
	void createFullScreenRectObject();

private: // members

	typedef std::vector<SceneManagerListener*> RenderingListenerContainer;

	AABB m_aabb;
	CameraPtr m_camera;
	SceneObjectsContainer m_sceneObjects;
	RenderingListenerContainer m_listeners;

	// Number of default elements.
	static size_t m_nameGenerator;

	std::unique_ptr<GBuffer> m_gBuffer;
	std::unique_ptr<RenderQueue> m_renderQueue;
	std::unique_ptr<RenderPassManager> m_renderPassManager;

	TexturePtr m_ssaoTexture;

	bool m_isSSAOEnabled;
	
};

// INLINE METHODS.
//----------------------------------------------------------------------------------------------
inline size_t SceneManager::getSceneObjectsSize() const
{	return m_sceneObjects.size();	}
//----------------------------------------------------------------------------------------------
inline const SceneManager::SceneObjectsContainer & SceneManager::getSceneObjects() const
{   return m_sceneObjects;	}
//----------------------------------------------------------------------------------------------
inline CameraPtr SceneManager::getCamera() const
{	return m_camera;	}
//----------------------------------------------------------------------------------------------
inline RenderQueue* SceneManager::getRenderQueue() const
{	return m_renderQueue.get();	}
//----------------------------------------------------------------------------------------------
inline Abyss::TexturePtr SceneManager::getSSAOTexture() const
{	return m_ssaoTexture;	}
//-----------------------------------------------------------------------------------------------
inline RenderPassManager* SceneManager::getRenderPassManager() const
{	return m_renderPassManager.get();	}
//-----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE