#pragma once

#include <map>
#include "AbyssRendering.h"
#include "AbyssPixelFormat.h"

ABYSS_BEGIN_NAMESPACE

class Texture
{
public:
	enum WrappingType
	{
		ClampToEdgeWrapping = 0,
		ClampToBorderWrapping,
		RepeatWrapping,
		MirroredRepeatWrapping
	};

	enum FilteringType
	{
		NearestFiltering = 0,
		LinearFiltering
	};

	enum class CubeMapDirectionType
	{
		Right,		// PositiveX
		Left,		// NegativeX
		Top,		// PositiveY
		Bottom,		// NegativeY
		Back,		// PositiveZ
		Front		// NegativeZ
	};

	DECLARE_POINTER(Texture)

public:
	Texture();
	~Texture();

	// Bind texture to first binding point
	void bind();
	// Bind texture to specified binding point
	void bind(size_t bindingPoint);
	// Release texture.
	void release();

	// Return binding point, which this texture is binded to:
	size_t getBindingPoint() const;
	// Return type of texture (suitable only for GL): 1d, 2d, 3d.
	GLenum getType() const;
	// Return pixel format of texture's data.
	PixelFormat getPixelFormat() const;

	// Return OpenGL ID of textureL
	size_t getId() const;

	// Change size of texture. Old data will be deleted. Note, that texture should be created before using this method. In other case, it will do nothing.
	void setSize(size_t width);
	void setSize(size_t width, size_t height);
	void setSize(size_t width, size_t height, size_t depth);

	// Return width of the texture.
	size_t getWidth() const;
	// Return height of the texture. In case of 1D texture will return only 1.
	size_t getHeight() const;
	// Return depth of the texture. In case of 1D and 3D texture will return 1.
	size_t getDepth() const;

	// Return total size of texture (i.e. width * height * depth):
	size_t getSize() const;

	// Set data as 1D texture:
	bool setRawData(const void * data, 
					size_t width, 
					PixelFormat pixelFormat, 
					GLenum inTextureFormat = GL_RGBA, 
					GLenum gpuTextureFormat = GL_RGBA);

	// Set data as 2D texture (event if height is 1, the texture will be 2d):
	bool setRawData(const void * data, 
					size_t width, 
					size_t height, 
					PixelFormat pixelFormat,
					GLenum inTextureFormat = GL_RGBA,
					GLenum gpuTextureFormat = GL_RGBA);

	// Set data as 3D texture:
	bool setRawData(const void * data, 
					size_t width, 
					size_t height, 
					size_t depth, 
					PixelFormat pixelFormat,
					GLenum inTextureFormat = GL_RGBA,
					GLenum gpuTextureFormat = GL_RGBA);

	// Set data as Cube Texture:
	bool setCubeMapData(const std::map<CubeMapDirectionType, const void*>& cubeMapData, 
						size_t width,
						size_t height,
						PixelFormat pixelFormat = PF_UBYTE,
						GLenum inTextureFormat = GL_RGB,
						GLenum gpuTextureFormat = GL_RGB);

	// Some parameters:
	void setWrapping(WrappingType wrapping);
	void setWrapping(WrappingType wrapS, WrappingType wrapT, WrappingType wrapR);

	// Return wrapping for s,t,r axes
	WrappingType getWrappingS() const;
	WrappingType getWrappingT() const;
	WrappingType getWrappingR() const;

	// Set filtering for texture and mipmaps (if they are enabled)
	void setFiltering(FilteringType filtering);
	FilteringType getFiltering() const;

	// Compare physical space of two textures:
	bool isSamePhysicalSpace(Texture* otherTexture) const;

	GLenum getInternalFormat() const;

	// Return dummy white texture (i.e. texture with size 1x1,
	static TexturePtr getDummyWhiteTexture();

	static GLenum getCurrentActiveTexture();
	static GLint getCurrentBoundTexture(GLenum textureType);

private:

	Texture(const Texture & other);
	Texture & operator = (const Texture & other);

	// Update texture parameters (like filtering, wrapping, etc.)
	void updateParameters();
	void deleteTexture();
	bool setRawDataImpl(GLenum type, 
						const void * data, 
						size_t width, 
						size_t height, 
						size_t depth, 
						PixelFormat pixelFormat,
						GLenum inTextureFormat = GL_RGBA,
						GLenum gpuTextureFormat = GL_RGBA);

	static size_t generateTexture(GLenum type);

	GLenum m_type;
	GLenum m_internalFormat;
	// OpenGL id of texture;
	size_t m_id;  // OpenGL id of texture;
	// Current binding point (texture unit)
	size_t m_bindingPoint;

	// Size of texture:
	size_t m_width,
		   m_height,
		   m_depth;

	WrappingType m_wrappingS,
				 m_wrappingT,
				 m_wrappingR;

	FilteringType m_filtering;

	PixelFormat m_pixelFormat;
};

// INLINE METHODS
//-------------------------------------------------------------------------------------------------
inline GLenum Texture::getType() const
{	return m_type;	}
//-------------------------------------------------------------------------------------------------
inline size_t Texture::getBindingPoint() const
{	return m_bindingPoint;	}
//-------------------------------------------------------------------------------------------------
inline size_t Texture::getId() const
{	return m_id;	}
//-------------------------------------------------------------------------------------------------
inline size_t Texture::getWidth() const
{	return m_width;	}
//-------------------------------------------------------------------------------------------------
inline size_t Texture::getHeight() const
{	return m_height;}
//-------------------------------------------------------------------------------------------------
inline size_t Texture::getDepth() const
{	return m_depth;	}
//-------------------------------------------------------------------------------------------------
inline size_t Texture::getSize() const
{	return getWidth() * getHeight() * getDepth();	}
//-------------------------------------------------------------------------------------------------
inline Texture::WrappingType Texture::getWrappingS() const
{	return m_wrappingS;	}
//-------------------------------------------------------------------------------------------------
inline Texture::WrappingType Texture::getWrappingT() const
{	return m_wrappingT;	}
//-------------------------------------------------------------------------------------------------
inline Texture::WrappingType Texture::getWrappingR() const
{	return m_wrappingR;	}
//-------------------------------------------------------------------------------------------------
inline Texture::FilteringType Texture::getFiltering() const
{	return m_filtering;	}
//-------------------------------------------------------------------------------------------------
inline PixelFormat Texture::getPixelFormat() const
{	return m_pixelFormat;	}
//-------------------------------------------------------------------------------------------------
inline GLenum Texture::getInternalFormat() const
{	return m_internalFormat;	}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE