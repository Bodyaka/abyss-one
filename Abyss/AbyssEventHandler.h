#pragma once

#include "Abyss.h"
#include "AbyssEvent.h"

ABYSS_BEGIN_NAMESPACE

class EventHandler
{
public:
	EventHandler();
	// Destructor.
	virtual ~EventHandler();
	// events:

	// General event for all events:
	void event(const EventPtr & event);

protected:
	// events:
	// when moving mouse (with pressed or not keys) inside focused object
	virtual void mouseMoveEvent(const MouseEventPtr & event);
	// when mouse button is pressed:
	virtual void mousePressEvent(const MouseEventPtr & event);
	// when mouse button is released:
	virtual void mouseReleaseEvent(const MouseEventPtr & event);
	// when mouse is double clicked (also, press event before this is emitting).
	virtual void mouseDoubleClickEvent(const MouseEventPtr & event);
	// when mouse wheel is changed:
	virtual void mouseWheelEvent(const MouseWheelEventPtr& event);

	// when key is pressed:
	virtual void keyPressEvent(const KeyEventPtr & event);
	// when key is released:
	virtual void keyReleaseEvent(const KeyEventPtr & event);

private:
	// put your code here.
	virtual void onEvent(const EventPtr & event);
};

ABYSS_END_NAMESPACE