#pragma once

#include <string>
#include <memory>
#include <boost/format.hpp>
#include "AbyssLogging.h"
#include "AbyssConfig.h"

// Some typedefs and other data:
ABYSS_BEGIN_NAMESPACE

	// Represents enum for intersections:
	enum IntersectionResult
	{
		IR_NoIntersection = 0,
		IR_Inside,
		IR_Intersects
	};


	// Typedefs
	typedef std::string String;
	typedef boost::format StringFormat;

	// Displays error message. 
	// Now just prints in console the message, but in future will be placed output to some window.
	void showErrorMessage(const String & message);

	// This will call assert in debug and exception in release.
	void raiseAnError(const String & message);

	// This will Verify statement and if it is false, it will raise and error (see raiseAnError). 
	bool verifyStatement(bool statement, const String & message);

	#define DECLARE_POINTER(Type) \
	public:	\
		typedef std::shared_ptr<Type> Pointer;

	class Exception
		: public std::exception
	{
	public:
		Exception(const std::string& str);

		virtual const char* what() const override;

	private:
		std::string m_what;

	};

ABYSS_END_NAMESPACE