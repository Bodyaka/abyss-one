#pragma once

#include "AbyssGlm.h"
#include "AbyssRendering.h"
#include "AbyssChangeableObject.h"
#include <map>

ABYSS_BEGIN_NAMESPACE

// Specifies different types of textures.
enum TextureType
{
	UnknownTextureType = 0,
	DiffuseTextureType,
	SpecularTextureType,
	AmbientTextureType,
	EmissiveTextureType,
	HeightTextureType,
	NormalMapTextureType,
	ShinessTextureType,
	OpacityTextureType,
	DisplacementTextureType,
	LightMapTextureType,
	ReflectionTextureType,
	UserTextureType
};

enum class InteractivityMaskType : unsigned char
{
	None = 0,
	ItemHovered = 0 << 1,
	ItemSelected = 0 << 2
};


class Material
	: public ChangeableObject
{
public:

	static MaterialPtr create(const Vector3D & ambient, const Vector4D & diffuse, const Vector4D & specular, float shiness = 1.f);

	Material();
	~Material();

	// Set specified ambient color.
	void setAmbientColor(const Vector3D & ambient);
	// Set specified diffuse color.
	void setDiffuseColor(const Vector4D & diffuse);
	// Set specified specular color.
	void setSpecularColor(const Vector4D & specular);
	// Set shiness level of material:
	void setShiness(float shiness);

	void setInteractivityMask(InteractivityMaskType flag);
	InteractivityMaskType getInteractivityMask() const;

	// Some default color, which is used is simple renderers:
	void setDefaultColor(const Vector4D& color);

	// For debug purposes:
	// Intensity of diffuse light - will be multiplied by diffuse color.
	void setDiffuseIntensity(float intensity);
	// Intensity of specular light - will be multiplied by specular color.
	void setSpecularIntensity(float intensity);

	// Get specified texture:
	TexturePtr getTexture(TextureType type) const;
	// Set texture for specified type:
	void setTexture(TextureType type, TexturePtr texture);
	// Check, whether there is a texture of given type:
	bool hasTexture(TextureType type) const;

	// Getters:
	// Return ambient color of material.
	const Vector3D & getAmbientColor() const;
	// Return diffuse color of material.
	const Vector4D & getDiffuseColor() const;
	// Return specular color of material.
	const Vector4D & getSpecularColor() const;
	// Return shiness level:
	float getShiness() const;

	// Returns default color value:
	const Vector4D& getDefaultColor() const;

	// For debug purposes
	// Intensity of diffuse light - will be multiplied by diffuse color.
	float getDiffuseIntensity() const;
	// Intensity of specular light - will be multiplied by specular color.
	float getSpecularIntensity() const;

private:

	typedef std::map<TextureType, TexturePtr> TexturesContainerType;

	TexturesContainerType m_textures;

	Vector3D m_ambient;
	InteractivityMaskType m_interactivityMask;

	Vector4D m_diffuse,
			 m_specular;

	Vector4D m_defaultColor;

	float m_shiness;

	float m_diffuseIntensity;
	float m_specularIntensity;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline const Vector3D & Material::getAmbientColor() const
{	return m_ambient;	}
//----------------------------------------------------------------------------------------------
inline const Vector4D & Material::getDiffuseColor() const
{	return m_diffuse;	}
//----------------------------------------------------------------------------------------------
inline const Vector4D & Material::getSpecularColor() const
{	return m_specular;	}
//----------------------------------------------------------------------------------------------
inline float Material::getShiness() const
{	return m_shiness;	}
//----------------------------------------------------------------------------------------------
inline float Material::getDiffuseIntensity() const
{	return m_diffuseIntensity;	}
//----------------------------------------------------------------------------------------------
inline float Material::getSpecularIntensity() const
{	return m_specularIntensity;	}
//----------------------------------------------------------------------------------------------
inline const Vector4D& Material::getDefaultColor() const
{	return m_defaultColor;	}
//----------------------------------------------------------------------------------------------
inline InteractivityMaskType Material::getInteractivityMask() const
{	return m_interactivityMask;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE