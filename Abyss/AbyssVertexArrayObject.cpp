#include "AbyssVertexArrayObject.h"

ABYSS_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------------------
VertexArrayObjectPtr VertexArrayObject::createVAO()
{
	return VertexArrayObjectPtr(new VertexArrayObject());
}
//--------------------------------------------------------------------------------------------------
VertexArrayObject::VertexArrayObject()
{
	glGenVertexArrays(1, &m_id);
}
//--------------------------------------------------------------------------------------------------
VertexArrayObject::~VertexArrayObject()
{
	glDeleteVertexArrays(1, &m_id);
}
//--------------------------------------------------------------------------------------------------
void VertexArrayObject::bind() const
{
	glBindVertexArray(m_id);
}
//--------------------------------------------------------------------------------------------------
void VertexArrayObject::release() const
{
	glBindVertexArray(0);
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE