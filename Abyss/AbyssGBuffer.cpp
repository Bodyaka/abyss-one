#include "AbyssTexture.h"
#include "AbyssGBuffer.h"

ABYSS_BEGIN_NAMESPACE


GBuffer::GBuffer(int width, int height)
{
	auto createTexture = [=](GLenum inTextureFormat, GLenum outTextureFormat, PixelFormat pixelFormat) -> TexturePtr
	{
		TexturePtr texture = TexturePtr(new Texture());
		texture->setRawData(nullptr, width, height, pixelFormat, inTextureFormat, outTextureFormat);
		texture->setFiltering(Texture::FilteringType::NearestFiltering);

		return texture;
	};

	// TODO: huge load on GPU memory bus!
	// There is a strange artifact with SSAO implementation!
	// When storing G-Buffer in World Space and convert it to ViewSpace in FragmentShader of SSAO, there will arise some artifacts, which are caused by precision lost (after couple of tests).
	// If store positions in ViewSpace, this problem is eliminated, BUT there will be a problem with world space lighting and calculation of motion blur.
	// Thus, currently, this approach will be used. However, it should be revised. Maybe, it is a problem with drivers...
	auto positionsTexture = createTexture(GL_RGBA, GL_RGBA32F, PF_FLOAT);
	positionsTexture->setFiltering(Texture::FilteringType::NearestFiltering);
	positionsTexture->setWrapping(Texture::ClampToEdgeWrapping);

	m_textures[TextureType::POSITIONS] = positionsTexture;

	// Shiness level is used as "a" parameter of normals texture. It can't be represented as 8-byte value. It should be stored as 16F value.
	// Also, we should use compact data as possible. Thus, shiness level can't be used as "a" parameter of colors texture,
	// because in this case, we have to store colors texture internally as GL_RGBA16F, i.e. each channel is 2x larger!
	// In order to compact data, we are using "a" parameter of normals texture instead.
	m_textures[TextureType::NORMALS] = createTexture(GL_RGBA, GL_RGBA16F, PF_FLOAT);
	m_textures[TextureType::COLORS] = createTexture(GL_RGBA, GL_RGBA, PF_UBYTE);
}

GBuffer::~GBuffer()
{
	// Do nothing.
}

Abyss::TexturePtr GBuffer::getTexture(TextureType texture)
{
	if (m_textures.find(texture) == m_textures.end())
		return nullptr;

	return m_textures[texture];
}

void GBuffer::bind(size_t bindingPoint)
{
	for (const auto& texturePair : m_textures)
	{
		texturePair.second->bind(bindingPoint);
		++bindingPoint;
	}
}

ABYSS_END_NAMESPACE