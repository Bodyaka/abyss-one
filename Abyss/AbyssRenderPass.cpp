#include "AbyssRenderPass.h"
#include "AbyssRenderer.h"

#include "AbyssRenderPassListener.h"

ABYSS_BEGIN_NAMESPACE


RenderPass::RenderPass(const std::string& name, Renderer* renderer)
	: m_name(name)
	, m_renderer(renderer)
	, m_isPostProcessingEffect(false)
{
}

void RenderPass::startRendering(RenderingResultPtr input)
{
	if (!getRenderer())
	{
		//assert(false);
		return;
	}

	m_isLocked = true;

	// TODO: should be listener called before or after startRendering(...) line?
	for (auto listener : m_listeners)
		listener->onRenderingStarted(this, input);

	getRenderer()->startRendering(input);
}

void RenderPass::finishRendering(RenderingResultPtr output)
{
	if (!getRenderer())
	{
		//assert(false);
		return;
	}

	m_isLocked = false;

	assert(getRenderer());
	getRenderer()->finishRendering(output);

	for (auto listener : m_listeners)
		listener->onRenderingFinished(this, output);
}

void RenderPass::setRenderer(Renderer* renderer)
{
	if (m_isLocked)
	{
		ABYSS_LOG("ERROR: couldn't change renderer for pass with name: " + getName() + " because it is in rendering process!");
		assert(false);

		return;
	}

	auto oldRenderer = getRenderer();

	m_renderer = renderer;

	if (oldRenderer != getRenderer())
	{
		if (oldRenderer)
			oldRenderer->onDetach(this);

		if (auto newRenderer = getRenderer())
			newRenderer->onAttach(this);
	}
}

void RenderPass::render(RenderableObject* object)
{
	if (!getRenderer())
	{
		//assert(false);
		return;
	}

	getRenderer()->render(object);
}

void RenderPass::addListener(RenderPassListener* listener)
{
	m_listeners.push_back(listener);
}

void RenderPass::removeListener(RenderPassListener* listener)
{
	m_listeners.erase(std::remove(m_listeners.begin(), m_listeners.end(), listener));
}

void RenderPass::polish(SceneManager* sceneManager)
{

}

ABYSS_END_NAMESPACE