#include "AbyssUtilities.h"

#include <windows.h>
#include <random>
#include <cctype>

#include "AbyssGlm.h"
#include "AbyssTexture.h"

#include <sstream>
#include <algorithm>

ABYSS_BEGIN_NAMESPACE

static char windowsDirSlash = '\\';
static char unixDirSlash    =  '/';

namespace Utilities
{
	//-------------------------------------------------------------------------------------------
	String convertFilePathToSystemPath(const String & path)
	{
		String newPath = path;
		std::replace(newPath.begin(), newPath.end(), windowsDirSlash, unixDirSlash);

		return newPath;
	}
	//-------------------------------------------------------------------------------------------
	String getDirectoryPath(const String & path)
	{
		int windowsSlashLastPos = path.find_last_of(windowsDirSlash);
		int unixSlashLastPos	   = path.find_last_of(unixDirSlash);

		String dirSlash = (windowsSlashLastPos == String::npos) ? &unixDirSlash : &windowsDirSlash;
		int slashPos = std::max(windowsSlashLastPos, unixSlashLastPos);

		return path.substr(0, slashPos + 1); // 1 - size of slash. Be careful - size doesn't count escape character
	}
	//-------------------------------------------------------------------------------------------
	std::vector<String> split( const String & str, char delim, bool skipEmptyParts)
	{
		String item;
		std::stringstream stream(str);
		std::vector<String> splittedStrings;

		while (std::getline(stream, item, delim))
		{
			if (skipEmptyParts && item.empty())
				continue;

			splittedStrings.push_back(item);
		}

		return splittedStrings;
	}

	float getRandomFloat(float begin /*= 0.f*/, float end /*= 0.f*/)
	{
		// TODO: should be used some seeding. I just forgot how to set it properly :(
		std::uniform_real_distribution<float> uniformGenerator(begin, end);
		std::default_random_engine engine;

		static unsigned long long s_seeder = 0;
		engine.seed(++s_seeder);

		return uniformGenerator(engine);
	}

	std::string ltrimString(const std::string& sourceStr)
	{
		std::string trimmedString = sourceStr;
		auto iterToFirstNotSpace = std::find_if_not(trimmedString.begin(), trimmedString.end(), [](char ch) -> bool
		{
			return isspace(ch);
		});

		if (iterToFirstNotSpace == trimmedString.end())
		{
			// Checking corner case, when input string consists only from spaces:
			if (!trimmedString.empty() && isspace(*trimmedString.rbegin()))
				return "";

			return trimmedString;
		}
				
		trimmedString.erase(trimmedString.begin(), iterToFirstNotSpace);
		return trimmedString;
	}

	std::string rtrimString(const std::string& sourceStr)
	{
		std::string trimmedString = sourceStr;
		
		auto revereseIterToFirstNotSpace = std::find_if_not(trimmedString.rbegin(), trimmedString.rend(), [](char ch) -> bool
		{
			return isspace(ch);
		}); 
		
		if (revereseIterToFirstNotSpace == trimmedString.rend())
		{
			// Checking corner case, when input string consists only from spaces:
			if (!trimmedString.empty() && isspace(*trimmedString.begin()))
				return "";

			return trimmedString;
		}

		// Converting reverse iterator to normal iterator:
		auto iterToFirstNotSpace = revereseIterToFirstNotSpace.base();
		trimmedString.erase(iterToFirstNotSpace, trimmedString.end());
		return trimmedString;
	}

	std::string trimString(const std::string& sourceStr)
	{
		return rtrimString(ltrimString(sourceStr));
	}

	std::vector<std::string> splitString(const std::string& sourceStr, const std::string& delimeter, bool keepEmptyParts /*= false*/)
	{
		std::vector<std::string> strings;

		std::string::size_type currentPos = 0;
		std::string::size_type prevPos = 0;

		// Helper method to check whether given string is considered to be empty:
		auto isStringEmpty = [keepEmptyParts](const std::string& str) -> bool
		{
			if (keepEmptyParts)
				return false;

			if (str.empty())
				return true;

			bool areAllCharactersSpaces = true;
			for (const auto& ch : str)
			{
				if (std::isspace(ch))
				{
					areAllCharactersSpaces = false;
					break;
				}
			}

			return areAllCharactersSpaces;
		};
		
		while ((currentPos = sourceStr.find(delimeter, prevPos)) != std::string::npos)
		{
			auto subStr = sourceStr.substr(prevPos, currentPos - prevPos);
			prevPos = currentPos + 1;

			if (isStringEmpty(subStr))
				continue;
			
			strings.push_back(subStr);
		}

		// To get the last substring (or only, if delimiter is not found):
		auto lastSubStr = sourceStr.substr(prevPos);
		if (!isStringEmpty(lastSubStr))
			strings.push_back(lastSubStr);

		return strings;
	}

	std::string getExecutableFilePath()
	{
#ifdef _WIN32 

		char buffer[MAX_PATH];
		int bytes = GetModuleFileName(NULL, buffer, MAX_PATH);
		if (bytes == 0)
			return std::string();
		else
			return convertFilePathToSystemPath(std::string(buffer));
#else
		char szTmp[32];
		sprintf(szTmp, "/proc/%d/exe", getpid());
		int bytes = MIN(readlink(szTmp, pBuf, len), len - 1);
		if (bytes >= 0)
			pBuf[bytes] = '\0';
		return bytes;
#endif
	}

	std::string getExecutableDirectoryPath()
	{
		return getDirectoryPath(getExecutableFilePath());
	}

}

ABYSS_END_NAMESPACE