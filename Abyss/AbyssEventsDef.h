#pragma once

#include "Abyss.h"
#include <memory>

ABYSS_BEGIN_NAMESPACE

// Forward declarations:
class Event;
class KeyEvent;
class MouseEvent;
class MouseWheelEvent;

typedef std::shared_ptr<Event> EventPtr;
typedef std::shared_ptr<KeyEvent> KeyEventPtr;
typedef std::shared_ptr<MouseEvent> MouseEventPtr;
typedef std::shared_ptr<MouseWheelEvent> MouseWheelEventPtr;

ABYSS_END_NAMESPACE