#pragma once

#include "AbyssRenderer.h"

ABYSS_BEGIN_NAMESPACE

class RenderTarget;

class SSAOBlurRenderer
	: public Renderer
{
public:
	static SSAOBlurRenderer* getInstance();

	void setKernelSize(size_t kernelSize);
	size_t getKernelSize() const;

	virtual void startRendering(RenderingResultPtr input) override;
	virtual void finishRendering(RenderingResultPtr output) override;
	
	virtual bool isSupports(const RenderableObject * const object) const override;

protected:
	virtual void doInit(SceneManager* manager);
	virtual void beforeRender(RenderableObject * const object);
	virtual void afterRender(RenderableObject * const object);

	void initRenderTarget(SceneManager* manager);
	void initFullScreenRect(SceneManager* manager);

	// Ctor and assignment:
	SSAOBlurRenderer();
	~SSAOBlurRenderer();

	SSAOBlurRenderer(const SSAOBlurRenderer& other) = delete;
	SSAOBlurRenderer& operator=(const SSAOBlurRenderer& other) = delete;

private:

	TexturePtr m_tempTexture; // Texture, where results of rendering will be assigned.
	std::unique_ptr<RenderTarget> m_renderTarget;

	size_t m_kernelSize;
};


// INLINE METHODS:
//--------------------------------------------------------------------------------------------
inline size_t SSAOBlurRenderer::getKernelSize() const
{	return m_kernelSize;	}
//--------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE