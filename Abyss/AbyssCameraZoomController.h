#pragma once

#include "Abyss.h"
#include "AbyssEventHandler.h"
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class CameraZoomController
	: public EventHandler
{
public:
	// Will use default camera of SceneManager:
	explicit CameraZoomController();
	explicit CameraZoomController(CameraPtr camera);
	~CameraZoomController();

	void resetCamera(CameraPtr camera);
	CameraPtr getCamera() const;

	// Change the zoom step:
	// if relative, the zoomStep will multiples by the distance.
	// If is not relative, will zoomStep will be added to current distance.
	void setZoomStep(float zoomStep, bool relative = true);
	float getZoomStep() const;
	bool isRelativeZoomStep() const;

	void setRange(float nearestDist, float furthestDist);
	float getNearestDistance() const;
	float getFurthestDistance() const;


private:
	virtual void mouseWheelEvent(const MouseWheelEventPtr& event);

	std::pair<float, float> m_range;

	bool m_isRelativeZoomStep;
	float m_zoomStep;

	CameraPtr m_camera;
};

// INLINE METHODS
//--------------------------------------------------------------------------------------------------
inline CameraPtr CameraZoomController::getCamera() const
{	return m_camera;	}
//--------------------------------------------------------------------------------------------------
inline void CameraZoomController::setZoomStep(float zoomStep, bool relative /* = true */)
{
	m_zoomStep = zoomStep;
	m_isRelativeZoomStep = relative;
}
//--------------------------------------------------------------------------------------------------
inline float CameraZoomController::getZoomStep() const
{
	return m_zoomStep;
}
//--------------------------------------------------------------------------------------------------
inline bool CameraZoomController::isRelativeZoomStep() const
{
	return m_isRelativeZoomStep;
}
//--------------------------------------------------------------------------------------------------
inline void CameraZoomController::setRange(float nearestDist, float furthestDist)
{
	m_range.first = nearestDist;
	m_range.second = furthestDist;
}
//--------------------------------------------------------------------------------------------------
inline float CameraZoomController::getNearestDistance() const
{
	return m_range.first;
}
//--------------------------------------------------------------------------------------------------
inline float CameraZoomController::getFurthestDistance() const
{
	return m_range.second;
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE