#pragma once

#include <vector>
#include "Abyss.h"

ABYSS_BEGIN_NAMESPACE

template <class ListenerType>
class Notifier
{
public:
	typedef std::vector<ListenerType*> ListenersContainerType;
public:
	Notifier(){};
	virtual ~Notifier(){};

	void removeAllListeners();
	void addListener(ListenerType* listener);
	void removeListener(ListenerType* listener);

	bool hasListener(ListenerType* listener) const;
	const ListenersContainerType& getListeners() const;

	// TODO: need to implement correct version of Notify via variadic templates
	// void notify();

private:
	ListenersContainerType m_listeners;

};  

// TEMPLATE METHODS IMPLEMENTATION:
//----------------------------------------------------------------------------------------------
template <class ListenerType>
void Notifier<ListenerType>::removeAllListeners()
{
	m_listeners.clear();
}
//----------------------------------------------------------------------------------------------
template <class ListenerType>
void Notifier<ListenerType>::addListener(ListenerType* listener)
{
	if (hasListener(listener))
	{
		assert(false && "Listener already presents in Notifier!");
		return;
	}
	m_listeners.push_back(listener);
}
//----------------------------------------------------------------------------------------------
template <class ListenerType>
void Notifier<ListenerType>::removeListener(ListenerType* listener)
{
	m_listeners.erase(std::remove(m_listeners.begin(), m_listeners.end(), listener));
}
//----------------------------------------------------------------------------------------------
template <class ListenerType>
bool Notifier<ListenerType>::hasListener(ListenerType* listener) const
{
	return std::find(m_listeners.begin(), m_listeners.end(), listener) != m_listeners.end();
}
//----------------------------------------------------------------------------------------------
template <class ListenerType>
inline const typename Notifier<ListenerType>::ListenersContainerType& Notifier<ListenerType>::getListeners() const
{	return m_listeners;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE