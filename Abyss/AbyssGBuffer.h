#pragma once

#include <map>
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class GBuffer final
{
public:
	GBuffer(int width, int height);
	~GBuffer();

	enum class TextureType
	{
		POSITIONS,
		NORMALS,
		COLORS // rgb - diffuse; a - specularity.
	};

	// Binds postiion, normals and color texture starting from specified binding point
	// I.e. Positions will be bound to 'bindingPoint', Normals to 'bindingPoint + 1' and Colors - to 'bindingPoint + 2'
	void bind(size_t bindingPoint);
	void release();

	TexturePtr getTexture(TextureType texture);

private:
	GBuffer(const GBuffer& other) = delete;
	GBuffer& operator=(const GBuffer& other) = delete;

	std::map<TextureType, TexturePtr> m_textures;
};

ABYSS_END_NAMESPACE