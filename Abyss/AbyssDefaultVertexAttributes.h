#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

enum DefaultVertexAttribute
{
	PositionAttribute,
	TexelAttribute,
	NormalAttribute,
	TangentAttribute,
	BiTangentAttribute
};

namespace Utilities
{
	// Return attribute's name in shader by specified attribute id
	const String & getAttributeName(int attribute);

	// Get attribute location by attribute name in shader
	int getAttributeLocation(const String & attributName);
}

ABYSS_END_NAMESPACE