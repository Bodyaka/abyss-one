#pragma once

#include <set>
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class LightsContainer
{
public:

	LightsContainer();
	virtual ~LightsContainer();

	// Function for lighting:
	LightWeakPtr createLight();
	LightWeakPtr createLight(const std::string& name);

	void addLight(LightWeakPtr light);
	LightWeakPtr removeLight(const std::string& name);

	void deleteLight(const std::string& name);
	void deleteAllLights();

	bool hasLight(LightWeakPtr light);
	bool hasLight(const std::string& lightName) const;

	virtual LightsContainerType getLights() const;

	// Merge lights into single container (with removing duplicates):
	static LightsContainerType mergetLights(const LightsContainerType& lights1, const LightsContainerType& lights2);

	protected:
		virtual void onLightAdded(LightWeakPtr light) {};
		virtual void onLightRemoved(LightWeakPtr light) {};
private:


	LightsContainer(const LightsContainer& other) = delete;
	LightsContainer& operator = (const LightsContainer& other) = delete;

	std::set<std::string> m_attachedLights;
};

ABYSS_END_NAMESPACE