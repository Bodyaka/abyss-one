#include "AbyssShaderProgram.h"

#include "AbyssUtilities.h"
#include "AbyssDefaultVertexAttributes.h"

#include <fstream>
#include "AbyssFileSystem.h"
// TODO: rebuild boost:
//#include <boost/filesystem.hpp>

ABYSS_BEGIN_NAMESPACE
	
//----------------------------------------------------------------------------------------------
ShaderProgram::ShaderProgram(const String & name, const String & vsName, const String & fsName)
	: m_programId(0)
	, m_name(name)
	, m_valid(false)
	, m_isNeedRelink(false)
{
	setup(vsName, fsName);
}
//----------------------------------------------------------------------------------------------
ShaderProgram::~ShaderProgram()
{
	release();
	
	if (m_valid)
	{
		glDeleteProgram(getProgramId());
	}
}
//---------------------------------------------------------------------------
void ShaderProgram::setup(const String & vsName, const String & fsName)
{
	m_shadersFileNames[GL_VERTEX_SHADER] = vsName;
	m_shadersFileNames[GL_FRAGMENT_SHADER] = fsName;

	rebuild();
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::compileShader(GLenum shaderType, const std::string& shaderFileName)
{
	if (shaderFileName.empty() || (getShaderId(shaderType) == INVALID_ID))
		return false;

	String shaderSourceCode = getShaderSourceCode(shaderFileName);
	addCustomDefinesToSourceCode(shaderType, shaderSourceCode);

#ifdef ABYSS_ENABLE_SHADER_DEBUGGING
	writeDebugInfo(shaderFileName, shaderSourceCode);
#endif

	if (!compileShaderSourceCode(shaderSourceCode, shaderType))
		return false;

	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::compileShaderSourceCode(const String & code, GLenum shaderType)
{
	if (code.empty())
		return false;

	const char * cCode = code.c_str();

	auto shaderHandle = getShaderId(shaderType);
	assert(shaderHandle != INVALID_ID);

	// Set source code for shader and compiles it:
	glShaderSource(shaderHandle, 1, &cCode, NULL);
	glCompileShader(shaderHandle);

	// Check, whether compile was successful. If not, delete newly created shader.
	if (!checkCompileStatus(shaderHandle, shaderType))
	{
		printToLogFile(code);
		std::string assertStr = "Shader compilation of shader: " + getShaderFileName(shaderType) + " is failed!";
		assert(false && assertStr.c_str());
	}

	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::checkCompileStatus(GLuint shaderId, GLenum shaderType) const
{
	// Get compiling status:
	GLint compileStatus;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compileStatus);

	if (compileStatus != GL_FALSE)
		return true;

	GLint infoLength;	

	glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLength);
	char * log = new char[infoLength];
	glGetShaderInfoLog(shaderId, infoLength, &infoLength, log);

	std::string shaderTypeName = "UNDEFINED";
	switch (shaderType)
	{
		case GL_FRAGMENT_SHADER:
			shaderTypeName = "FRAGMENT";
			break;
		case GL_VERTEX_SHADER:
			shaderTypeName = "VERTEX";
			break;
		default:
			assert(false && "Undefined shader type!");
			break;
	}
	String errorString = "Compile of " + shaderTypeName + " shader with name: [" + m_name + "] is FAILED : " + String(log);
	showErrorMessage(errorString);

	// Delete heap-allocated memory:
	delete[] log;

	return false;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::checkLinkStatus(GLuint programId) const
{
	GLint linkingStatus;
	glGetProgramiv(programId, GL_LINK_STATUS, &linkingStatus);

	if (linkingStatus != GL_FALSE)
		return true;

	// Print info about bad program linkage:
	GLint infoLength;
	glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLength);

	// Get log of linkage
	char * log = new char[infoLength];
	glGetProgramInfoLog(programId, infoLength, &infoLength, log);

	String errorString = "Linkage of shader program with name: [" + m_name + "] is FAILED : " + String(log);
	showErrorMessage(errorString);
	
	// Delete heap-allocated memory.
	delete[] log;

	return false;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::link()
{
	auto isDeferredRenderer = getName() == "DeferredRenderer";
	m_isNeedRelink = false;

	glLinkProgram(m_programId);

	auto returnStatus = checkLinkStatus(m_programId);
	assert(returnStatus && "Linkage of program is failed!");
	return returnStatus;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::rebuild()
{
	// Destroys old program:
	if (getProgramId() != INVALID_ID)
		glDeleteProgram(getProgramId());


	for (auto shaderType : getSuportedShaderTypes())
	{
		// Create and compile VertexShader:
		m_shaderIds[shaderType] = glCreateShader(shaderType);

		auto shaderFileName = getShaderFileName(shaderType);
		if (!compileShader(shaderType, shaderFileName))
		{
			showErrorMessage("An error occurs while compiling " + shaderFileName + " shader!");
			deleteShader(shaderType);
			return false;
		}
	}
	
	// Create shader program.
	m_programId = glCreateProgram();
	for (auto shaderType : getSuportedShaderTypes())
		glAttachShader(m_programId, getShaderId(shaderType));

	// If linkage was failed, delete shader program
	if (!link())
	{
		release();
		glDeleteProgram(m_programId);

		return false;
	}

	setupDataLocations();

	// We marks vertex and fragment shader as deletable, so, next time, when vertex/fragment shader will be detached
	// from shader program it will be deleted.
	for (auto shaderType : getSuportedShaderTypes())
	{
		// WARNING: DO NOT USE deleteShader() here! Because it will reset shader's id!
		glDeleteShader(getShaderId(shaderType));
	}

	return true;
}
//---------------------------------------------------------------------------------------------
ShaderProgram::DataMapConstIterator ShaderProgram::findUniform(const String & str) const
{
	DataMapConstIterator p = m_uniforms.find(str);

	//	if (p == m_uniforms.end())
	//		ERR_COUT("ERROR : Cannot find such uniform - " + str);

	return p;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String & name, const float value)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniform1f(p->second, value);

	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String & name, const int value)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniform1i(p->second, value);
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String & name, const Vector2D & vec)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniform2fv(p->second, 1, &vec[0]);
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String & name, const Vector3D & vec)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniform3fv(p->second, 1, &vec[0]);
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String & name, const Vector4D & vec)
{
	GLint location = glGetUniformLocation(getProgramId(), name.c_str());
	if (location == -1)
		return false;

	glUniform4fv(location, 1, &vec[0]);
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String & name, const Matrix2D & matrix)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniformMatrix2fv(p->second, 1, false, &matrix[0][0]);
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String & name, const Matrix3D & matrix)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniformMatrix3fv(p->second, 1, false, &matrix[0][0]);
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String & name, const Matrix4D & matrix)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniformMatrix4fv(p->second, 1, false, &matrix[0][0]);
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String& name, const Vector2D* data, size_t size)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniform2fv(p->second, size, reinterpret_cast<const GLfloat*>(data));
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String& name, const Vector3D* data, size_t size)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniform3fv(p->second, size, reinterpret_cast<const GLfloat*>(data));
	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::setUniform(const String& name, const Vector4D* data, size_t size)
{
	DataMapConstIterator p = findUniform(name);

	if (p == m_uniforms.end())
		return false;

	glUniform4fv(p->second, size, reinterpret_cast<const GLfloat*>(data));
	return true;
}
//---------------------------------------------------------------------------------------------
String ShaderProgram::getShaderSourceCode(const String & shaderFilePath)
{
	std::ifstream stream(shaderFilePath);
	if (!stream.is_open())
	{
		showErrorMessage("Cannot open the file : " + shaderFilePath);
		return "";
	}

	std::string sourceCode;
	std::string line;
	while (getline(stream, line))
		sourceCode += line + '\n';
	
	stream.close();

	// Post processing on data:
	// In current version, works only on #mine statements:

	return getConvertedNativeSourceCode(sourceCode, shaderFilePath);
}
//---------------------------------------------------------------------------------------------
String ShaderProgram::getConvertedNativeSourceCode(const String & sourceCode, const String & shaderFilePath)
{
	String directoryPath = Utilities::getDirectoryPath(shaderFilePath);

	static String sAbyssKeyword = "#abyss";
	static String sIncludeKeyword = "include";
	static size_t npos = String::npos;

	size_t seekPos = 0;
	String convertedSourceCode = sourceCode;
	while ((seekPos = convertedSourceCode.find(sAbyssKeyword, seekPos)) != npos)
	{
		// Note, that #abyss only works on single line:
		size_t newLinePos = convertedSourceCode.find_first_of("\n", seekPos);
		if (newLinePos == npos) // Some ugly behavior
			break;

		std::string line = convertedSourceCode.substr(seekPos, newLinePos - seekPos);

		// Some fool's defense
		if (line.empty())
			continue;

		// All abyss preprocessor is doing with () statesment:
		size_t firstBracketPos = line.find_first_of("(", 0);
		size_t lastBracketPos = line.find_last_of(")");

		if ((firstBracketPos == npos) || (lastBracketPos == npos))
			break;

		// Include statement (#abyss include("blah.vs") or #abyss include(@"D:\blah.vs")).
		// @ symbol before string indicates, that this one fileName is full file name.
		// Without @ symbol before string indicates, that compiler will take fileName respective to 
		// parent fileName location.
		// TODO : Need some cache improvement.

		size_t intersetPos = 0;
		if ((intersetPos = line.find(sIncludeKeyword)) != npos)
		{
			size_t firstQuotesMarkPos = line.find_first_of("\"", intersetPos);
			size_t lastQuotesMarkPos  = line.find_last_of("\"");

			if (firstQuotesMarkPos == npos)
				break;

			std::string includedFileName = line.substr(firstQuotesMarkPos + 1, lastQuotesMarkPos - firstQuotesMarkPos - 1);

			size_t ampersandPos = line.find_first_of("@", firstBracketPos);

			// Getting file name by its full path
			if (ampersandPos == npos)
				includedFileName = directoryPath + includedFileName;

			// Getting contents of fileName:
			std::string contents = getShaderSourceCode(includedFileName);

			// And replacing it our data by new contents:
			convertedSourceCode.replace(seekPos, newLinePos - seekPos, contents);
		}
	}

	return convertedSourceCode;
}
//---------------------------------------------------------------------------------------------
int ShaderProgram::getAttributeLocation(const String & name) const
{
	DataMapConstIterator p = m_attributes.find(name);
	if (p == m_attributes.end())
		return -1;

	return p->second;
}
//---------------------------------------------------------------------------------------------
int ShaderProgram::getUniformLocation(const String & name) const
{
	DataMapConstIterator p = m_uniforms.find(name);
	if (p != m_uniforms.end())
		return p->second;

	return -1;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::hasUniform(const String & name) const
{
	return (m_uniforms.end() != m_uniforms.find(name));
}
//---------------------------------------------------------------------------------------------
void ShaderProgram::setupDataLocations()
{
	setupActiveUniformsLocations();
	setupVertexAttributesLocations();
}
//-----------------------------------------------------------------------------------------------
void ShaderProgram::setupActiveUniformsLocations()
{
	m_uniforms.clear();

	GLint uniformsCount = 0;
	GLsizei maxUniformLength = 0;

	glGetProgramiv(getProgramId(), GL_ACTIVE_UNIFORMS, &uniformsCount);
	glGetProgramiv(getProgramId(), GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxUniformLength);

	GLchar* uniformName = new GLchar[maxUniformLength];

	for (int i = 0; i<uniformsCount; ++i)
	{
		GLsizei length;
		GLint   size;
		GLenum  type;

		glGetActiveUniform(getProgramId(), i, maxUniformLength, &length, &size, &type, uniformName);

		GLint uniformLocation = glGetUniformLocation(getProgramId(), uniformName);
		m_uniforms.insert(DataMapValueType(String(uniformName), uniformLocation));

	}

	delete[] uniformName;
}
//-----------------------------------------------------------------------------------------------
void ShaderProgram::setupVertexAttributesLocations()
{
	m_attributes.clear();

	GLint attributesCount = 0;
	GLsizei maxAttributeLength = 0;

	glGetProgramiv(getProgramId(), GL_ACTIVE_ATTRIBUTES, &attributesCount);
	glGetProgramiv(getProgramId(), GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxAttributeLength);

	bool toRelink = false;
	
	GLchar* attributeName = new GLchar[maxAttributeLength];
	for (int i = 0; i<attributesCount; ++i)
	{
		GLsizei length;
		GLint   size;
		GLenum  type;

		glGetActiveAttrib(getProgramId(), i, maxAttributeLength, &length, &size, &type, attributeName);

		GLint attribLocation = glGetAttribLocation(getProgramId(), attributeName);

		String strAttrib(attributeName);
		
		// We need define default location if they are not meet out conditions:
		int defaultLocation = Utilities::getAttributeLocation(strAttrib);
		if(defaultLocation != attribLocation)
		{
			toRelink = true;
			attribLocation = defaultLocation;
			glBindAttribLocation(getProgramId(), attribLocation, attributeName);
		}

		// Adding attributes:
		m_attributes.insert(DataMapValueType(strAttrib, attribLocation));
	}

	delete[] attributeName;

	// We need relinking program due to specified vertex attribute locations:
	if (toRelink)
		link();
}
//-----------------------------------------------------------------------------------------------
void ShaderProgram::setFragmentDataLocation(const String & name, size_t location)
{
	int loc = getFragmentDataLocation(name);
	if (loc == location || loc == -1)
		return;

	glBindFragDataLocation(getProgramId(), location, name.c_str());

	m_isNeedRelink = true;
}
//---------------------------------------------------------------------------------------------
size_t ShaderProgram::getFragmentDataLocation(const String& name)
{
	return glGetFragDataLocation(getProgramId(), name.c_str());
}
//---------------------------------------------------------------------------------------------
void ShaderProgram::bind()
{	
	if (m_isNeedRelink)
		link();

	glUseProgram(getProgramId());	
}
//---------------------------------------------------------------------------------------------
void ShaderProgram::release()
{	
	glUseProgram(0);
}
//---------------------------------------------------------------------------------------------
void ShaderProgram::bindAttributeLocation(const String & attributeName, int attributeLocation)
{
	glBindAttribLocation(getProgramId(), attributeLocation, attributeName.c_str());
}
//---------------------------------------------------------------------------------------------
void ShaderProgram::writeDebugInfo(const std::string& shaderFilePath, const std::string& sourceCode)
{
	auto fileSystem = FileSystem::getInstance();
	std::string dirPath = fileSystem->getDirectory(shaderFilePath);
	std::string fileName = fileSystem->getFileName(shaderFilePath);

	std::string strDebugFile = dirPath;
	fileSystem->appendPathEntry(strDebugFile, "debug");
	fileSystem->appendPathEntry(strDebugFile, fileName);

	if (!fileSystem->writeToFile(strDebugFile, sourceCode, true))
		ABYSS_LOG("ERROR: Cannot create debug file " + strDebugFile + "!")
}
//---------------------------------------------------------------------------------------------
std::string ShaderProgram::getShaderFileName(GLenum shaderType) const
{
	auto iter = m_shadersFileNames.find(shaderType);
	if (iter == m_shadersFileNames.end())
		return std::string();

	return iter->second;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::addCustomDefine(GLenum shaderType, const std::string& strDefine, bool isNeedRecompile /*= true*/)
{
	if (isDefined(shaderType, strDefine))
		return false;

	auto& customDefinesSet = m_customDefines[shaderType];

	bool isInserted = customDefinesSet.insert(strDefine).second;
	if (isInserted && isNeedRecompile)
	{
		rebuild();
	}

	return isInserted;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::removeCustomDefine(GLenum shaderType, const std::string& strDefine, bool isNeedRecompile /*= true*/)
{
	if (!isDefined(shaderType, strDefine))
		return false;

	auto& customDefinesSet = m_customDefines[shaderType];

	auto iter = customDefinesSet.find(strDefine);
	if (iter == customDefinesSet.end())
		return false;

	customDefinesSet.erase(iter);

	if (isNeedRecompile)
		rebuild();

	return true;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::isDefined(GLenum shaderType, const std::string& customDefineStr) const
{
	const auto& customDefinesSet = getAllCustomDefines(shaderType);
	return customDefinesSet.find(customDefineStr) != customDefinesSet.end();
}
//---------------------------------------------------------------------------------------------
const std::set<std::string>& ShaderProgram::getAllCustomDefines(GLenum shaderType) const
{
	static const std::set<std::string> s_emptyCustomDefines;
	auto iter = m_customDefines.find(shaderType);
	if (iter != m_customDefines.end())
		return iter->second;

	return s_emptyCustomDefines;
}
//---------------------------------------------------------------------------------------------
const std::set<GLenum>& ShaderProgram::getSuportedShaderTypes() const
{
	static std::set<GLenum> s_allSupportedShaderTypes =
	{
		GL_VERTEX_SHADER,
		GL_FRAGMENT_SHADER
	};

	return s_allSupportedShaderTypes;
}
//---------------------------------------------------------------------------------------------
GLuint ShaderProgram::getShaderId(GLenum shaderType) const
{
	auto iter = m_shaderIds.find(shaderType);
	if (iter == m_shaderIds.end())
		return INVALID_ID;

	return iter->second;
}
//---------------------------------------------------------------------------------------------
bool ShaderProgram::deleteShader(GLenum shaderType)
{
	auto iter = m_shaderIds.find(shaderType);
	if (iter == m_shaderIds.end())
		return false;

	if (iter->second != INVALID_ID)
	{
		glDeleteShader(iter->second);
	}

	m_shaderIds.erase(iter);
	return true;
}
//---------------------------------------------------------------------------------------------
void ShaderProgram::addCustomDefinesToSourceCode(GLenum shaderType, std::string& shaderSourceCode) const
{
	const auto& customDefinesSet = getAllCustomDefines(shaderType);
	if (customDefinesSet.empty())
		return;

	// Find position where to insert. Note, that first string should be always version, if it is defined in shader program:
	// From GLSL specification: "The #version directive must occur in a shader before anything else, except for comments and white space".
	auto insertPos = shaderSourceCode.find("#version");
	if (insertPos != std::string::npos)
	{
		insertPos = shaderSourceCode.find_first_of('\n', insertPos);
		insertPos += 1; // for ('\n') symbol;
	}
	else
		insertPos = std::string::size_type(0u); // beginning of the string

	for (const auto& customDefineStr : customDefinesSet)
	{
		std::string defineStr = "#define " + customDefineStr + "\n";
		shaderSourceCode.insert(insertPos, defineStr);

		insertPos += defineStr.size();
	}
}
//---------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE