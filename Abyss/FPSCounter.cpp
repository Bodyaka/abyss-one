#include "FPSCounter.h"

ABYSS_BEGIN_NAMESPACE

FPSCounter::FPSCounter()
	: Notifier<FPSCounterListener>()
	, m_frameRate(0.f)
	, m_framesCount(0)
	, m_startTime(0)
	, m_updatesCount(0)
	, m_combinedFramesCount(0.)
{

}

FPSCounter::~FPSCounter()
{

}

void FPSCounter::onFrameStarted()
{
	// It is needed in order to properly calculate 
	if (m_startTime == 0)
		m_startTime = clock();
}

void FPSCounter::onFrameEnded()
{
	clock_t endFrame = clock();
	update(endFrame - m_startTime);
}

void FPSCounter::update(clock_t elapsedTime)
{
	++m_framesCount;
	auto deltaTime = clock() - m_startTime;

	if (deltaTime > CLOCKS_PER_SEC)
	{
		m_lastFrameRate = m_framesCount;

		// Calculation of average frame rate is done via next algorithm:
		// http://stackoverflow.com/a/28544279
		m_frameRate = m_frameRate * 0.5f + m_lastFrameRate * 0.5f;

		// "True" average of frameRate:
		++m_updatesCount;
		m_combinedFramesCount += m_framesCount;

		for (FPSCounterListener* listener : getListeners())
		{
			listener->onFPSChanged(this);
		}

		// Reset counter variables:
		m_startTime = clock();
		m_framesCount = 0;
	}
}

float FPSCounter::getAverageFrameTimeInMilliseconds() const
{
	if (getAverageFrameRate() == 0.f)
		return std::numeric_limits<float>::min();

	return 1000.f / getAverageFrameRate();
}

float FPSCounter::getAverageFrameRate2() const
{
	return m_combinedFramesCount / m_updatesCount;
}

ABYSS_END_NAMESPACE