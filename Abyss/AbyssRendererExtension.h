#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class ShaderProgram;

class RendererExtension
{
public:
	RendererExtension(ShaderProgram* program)
		: m_program(program)
	{ };

	virtual ~RendererExtension() {};

	virtual void prepareForRendering(RenderableObject * const object) = 0;
	virtual void unprepareFromRendering(RenderableObject * const object) = 0;

	virtual void beforeRendering(Mesh* mesh) = 0;
	virtual void afterRendering(Mesh* mesh) = 0;

	ShaderProgram* getShaderProgram() const;

protected:
	ShaderProgram* m_program;

private:

	RendererExtension(const RendererExtension& other) = delete;
	RendererExtension& operator = (const RendererExtension& other) = delete;
};

inline ShaderProgram* RendererExtension::getShaderProgram() const
{	return m_program;	}


ABYSS_END_NAMESPACE