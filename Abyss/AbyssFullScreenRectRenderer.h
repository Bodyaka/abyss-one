#pragma once

#include "AbyssRenderer.h"

ABYSS_BEGIN_NAMESPACE

class FullScreenRectRenderer :
	public Renderer
{
public:
	explicit FullScreenRectRenderer(const std::string& expectedTextureName);
	~FullScreenRectRenderer();

	virtual bool isSupports(const RenderableObject * const renderable) const override;

	void setExpectedTextureInput(const std::string& textureName);
	const std::string& getExpectedTextureInput() const;

	virtual void startRendering(RenderingResultPtr input) override;
	virtual void finishRendering(RenderingResultPtr output) override;

private:
	virtual void beforeRender(RenderableObject * const renderable) override;
	virtual void afterRender(RenderableObject * const renderable) override;

	std::string m_expectedTextureInput;
};

// INLINE METHODS:
//-----------------------------------------------------------------------------------------------
inline void FullScreenRectRenderer::setExpectedTextureInput(const std::string& textureName)
{	m_expectedTextureInput = textureName;	}
//-----------------------------------------------------------------------------------------------
inline const std::string& FullScreenRectRenderer::getExpectedTextureInput() const
{	return m_expectedTextureInput;	}
//-----------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE

