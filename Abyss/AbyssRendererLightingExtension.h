#pragma once

#include "AbyssRendererExtension.h"

ABYSS_BEGIN_NAMESPACE

class SceneObject;

class RendererLightingExtension
	: public RendererExtension
{
public:
	RendererLightingExtension(ShaderProgram* program);
	~RendererLightingExtension();

	virtual void prepareForRendering(RenderableObject * const object) override;
	virtual void unprepareFromRendering(RenderableObject * const object) override;
	
	virtual void beforeRendering(Mesh* mesh) override;
	virtual void afterRendering(Mesh* mesh) override;

protected:
	void setActiveLightsCountUniformValue(int lightsCount);

	// TODO: these two methods need be removed, when Light object will be added.
	void setLightPositionUniformValue(int lightIndex, const Vector3D & position);
	void setLightDirectionUniformValue(int lightIndex, const Vector3D& direction);
	void setLightColorUniformValue(int lightIndex, const Vector4D & intensity);
	void setLightType(int lightIndex, int lightType);
	void setSpotlightUniformValues(int lightIndex, float innerCutoffAngle, float outerCutoffAngle);
	// X - constant attenuation; Y - linear attenuation; Z - quadratic attenuation
	void setLightAttenuationUniformValue(int lightIndex, const Vector3D& attenuationConstants);

	void setupLightsUniformsValues(const SceneObject* const sceneObject);

};

ABYSS_END_NAMESPACE