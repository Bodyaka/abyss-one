#pragma once

#include "Abyss.h"

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
//TODO: file is missing.#include <glm/gtc/swizzle.hpp>
#include <glm/gtc/matrix_transform.hpp>

ABYSS_BEGIN_NAMESPACE

typedef glm::vec2 Vector2D;
typedef glm::vec3 Vector3D;
typedef glm::vec4 Vector4D;
typedef glm::mat2 Matrix2D;
typedef glm::mat3 Matrix3D;
typedef glm::mat4 Matrix4D;

typedef std::vector<Vector2D> ContainerV2D;
typedef std::vector<Vector3D> ContainerV3D;
typedef std::vector<Vector4D> ContainerV4D;
typedef std::vector<unsigned int>     ContainerUint;

template <class ContainerValueType>
std::vector<ContainerValueType>& operator << (std::vector<ContainerValueType>& container, const ContainerValueType& value)
{
	container.push_back(value);
	return container;
}

const Vector3D One3D(1.f);
const Vector3D cAxisX(1.f, 0.f, 0.f);
const Vector3D cAxisY(0.f, 1.f, 0.f);
const Vector3D cAxisZ(0.f, 0.f, 1.f);
const Matrix3D cIdentityMatrix3D(1.f);
const Matrix4D cIdentityMatrix4D(1.f);
const float cPrecision = 0.000001f;


// Converts other vectors to Vector3D:
template <class VectorType>
inline Vector3D toVector3D(const VectorType& vec)
{
	return Vector3D(vec);
}

template <>
inline Vector3D toVector3D(const Vector2D& vec)
{
	return Vector3D(vec.x, vec.y, 0.f);
}

template <>
inline Vector3D toVector3D(const Vector4D& vec)
{
	return Vector3D(vec.x, vec.y, vec.z);
}

// Converts other vectors to Vector4D:
template <class VectorType>
inline Vector4D toVector4D(const VectorType& vec)
{
	return Vector4D(vec);
}

template <>
inline Vector4D toVector4D(const Vector2D& vec)
{
	return Vector4D(vec.x, vec.y, 0.f, 0.f);
}

template <>
inline Vector4D toVector4D(const Vector3D& vec)
{
	return Vector4D(vec.x, vec.y, vec.z, 0.f);
}


ABYSS_END_NAMESPACE
