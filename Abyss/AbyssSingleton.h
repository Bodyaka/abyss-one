#pragma once

#include "Abyss.h"

ABYSS_BEGIN_NAMESPACE

template <class Type>
class Singleton
{
public:
	static Type* getInstance();

protected:
	Singleton();
	virtual ~Singleton();

private:
	template <class OtherType>
	Singleton(const Singleton<OtherType>& rhs) = delete;

	// TODO: need define operator = as deleted.
// 	template <class OtherType>
// 	Singleton<Type>& operator = Singleton(const Singleton<OtherType>& rhs) = delete;
};

// Implementation:
//--------------------------------------------------------------------------------------------------
template <class Type>
Singleton<Type>::Singleton()
{	}
//--------------------------------------------------------------------------------------------------
template <class Type>
Singleton<Type>::~Singleton()
{	}
//--------------------------------------------------------------------------------------------------
template <class Type>
Type* Singleton<Type>::getInstance()
{
	static Type instance;
	return &instance;
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE