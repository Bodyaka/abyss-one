#pragma once

#include "AbyssGlm.h"
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

namespace MeshUtilities
{
	void calculateTangentsAndBitangents(const ContainerV3D& verticesPositions, 
										const ContainerV3D& verticesNormals, 
										const ContainerV2D& textureCoord,
										const ContainerUint& indices,
										ContainerV3D& tangents,
										ContainerV3D& bitangents);

	// Input should contains only 3-vertex faces!
	void calculateTangentsAndBitangents(const float* verticesPositions,
										const float* verticesNormals,
										const ContainerV2D& textureCoord,
										const size_t verticesNumber,
										const unsigned int* indices,
										const size_t indicesNumber,
										ContainerV3D& tangents,
										ContainerV3D& bitangents);

	ContainerV3D computeNormals(const ContainerV3D& verticesPositions, const ContainerUint& indices);

	void renormalizeNormals(const float* verticesPositions, const float* loadedNormals, const size_t verticesNumber, const unsigned int* indices, const size_t indicesNumber, ContainerV3D& normals);

	MeshPtr createRectMesh(float size);


}

ABYSS_END_NAMESPACE