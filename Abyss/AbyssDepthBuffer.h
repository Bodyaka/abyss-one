#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class DepthBuffer
{
public:

	DECLARE_POINTER(DepthBuffer)

	DepthBuffer(size_t width, size_t height, size_t depth);
	~DepthBuffer();

	void bind();
	static void release();

	void setSize(size_t width, size_t height);

	GLuint getHandle() const;

	size_t getWidth() const;
	size_t getHeight() const;
	size_t getDepth() const;

private:

	void setupBufferStorage();

	GLuint m_handle;

	size_t m_width,
		   m_height,
		   m_depth;
};

// INLINE METHODS
//--------------------------------------------------------------------------------------------------
inline GLuint DepthBuffer::getHandle() const
{	return m_handle;	}
//--------------------------------------------------------------------------------------------------
inline size_t DepthBuffer::getWidth() const
{	return m_width;	}
//--------------------------------------------------------------------------------------------------
inline size_t DepthBuffer::getHeight() const
{	return m_height;	}
//--------------------------------------------------------------------------------------------------
inline size_t DepthBuffer::getDepth() const
{	return m_depth;	}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE