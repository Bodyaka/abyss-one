#include "AbyssRendererExtension.h"

ABYSS_BEGIN_NAMESPACE

class RendererMaterialExtension
	: public RendererExtension
{
public:
	RendererMaterialExtension(ShaderProgram* program);

	virtual void prepareForRendering(RenderableObject * const object) override;
	virtual void unprepareFromRendering(RenderableObject * const object) override;

	virtual void beforeRendering(Mesh* mesh) override;
	virtual void afterRendering(Mesh* mesh) override;

	// Some customization for extension.
	void setDiffuseTextureBindingPoint(size_t location);
	size_t getDiffuseTextureBindingPoint() const;

	void setNormalTextureBindingPoint(size_t location);
	size_t getNormalTextureBindingPoint() const;

	
protected:
	void setupMaterialUniformValues(const Material* const material);

	void bindMaterialTextureUniformValues(const Material* const material, int diffuseBindingPoint, int normalsBindingPoint);
	void releaseMaterialTextureUniformValues(const Material* const material);

	size_t m_diffuseTextureBindingPoint = 0,
		   m_normalTextureBindingPoint = 1;

};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline size_t RendererMaterialExtension::getDiffuseTextureBindingPoint() const
{	return m_diffuseTextureBindingPoint;	}
//----------------------------------------------------------------------------------------------
inline size_t RendererMaterialExtension::getNormalTextureBindingPoint() const
{	return m_normalTextureBindingPoint;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE