#pragma once

#include <map>
#include "AbyssRendering.h"
#include "AbyssTexture.h"

ABYSS_BEGIN_NAMESPACE

class TextureManager
{
public:
	TextureManager(){};
	~TextureManager(){};

	// MUTATORS:
	// Creates texture from given path. If id is no specified, it will generate automatically based on the path of texture.
	TexturePtr createTexture(const std::string& path, const std::string& id = "");
	TexturePtr createCubeTexture(const std::string& materialFilePath, const std::string& id = "");

	// Inserts texture with specified Id. If old one texture exists with same id, it will be replaced!
	void insertTexture(const std::string& textureId, TexturePtr texture);
	// Remove texture with specified Id. 
	bool removeTexture(const std::string& textureId);

	void clearAllTextures();

	// INQUIRY and ACCESSORS:
	TexturePtr getTexture(const std::string& textureId) const;
	bool hasTexture(const std::string& textureId) const;

	// Return default texture manager.
	static TextureManager * getInstance();

private:

	TextureManager(const TextureManager& other) = delete;
	TextureManager& operator=(const TextureManager& other) = delete;

	// Retrieve file paths for given material directions:
	std::map<Texture::CubeMapDirectionType, std::string> getDirectionFilePaths(const std::string& materialFilePath, std::string& textureId) const;

private:

	std::map<std::string, TexturePtr> m_texturesMap;

};

ABYSS_END_NAMESPACE