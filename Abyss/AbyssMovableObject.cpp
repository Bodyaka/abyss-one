#include "AbyssMovableObject.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
MovableObject::MovableObject()
	: TransformableObject()
	, m_position(Vector3D(0.f, 0.f, 0.f))
{

}
//----------------------------------------------------------------------------------------------
MovableObject::~MovableObject()
{

}
//----------------------------------------------------------------------------------------------
void MovableObject::setPosition(const Vector3D & position)
{
	m_position = position;

	updateModelMatrix_impl();
}
//----------------------------------------------------------------------------------------------
void MovableObject::move(const Vector3D & offset)
{
	m_position += offset;

	updateModelMatrix_impl();
}
//----------------------------------------------------------------------------------------------
void MovableObject::updateModelMatrix_impl()
{
	Matrix4D identityMatrix;
	setModelMatrix(glm::translate(identityMatrix, getPosition()));
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE