#pragma once

#include "Abyss.h"
#include "SFGUI/Widget.hpp"

ABYSS_BEGIN_NAMESPACE


class SfguiWidgetDocker
{
public:
	enum class DirectionType
	{
		Top = 1,
		Bottom = 1 << 1,
		Left = 1 << 2,
		Right = 1 << 3
	};

	enum class CornerType
	{
		None = 0,
		TopLeft = int(DirectionType::Top) | int(DirectionType::Left),
		TopRight = int(DirectionType::Top) | int(DirectionType::Right),
		BottomLeft = int(DirectionType::Bottom) | int(DirectionType::Left),
		BottomRight = int(DirectionType::Bottom) | int(DirectionType::Right)
	};
	

public:
	SfguiWidgetDocker(sfg::Widget* holderWidget);
	virtual ~SfguiWidgetDocker();

	void dock(sfg::Widget* dockedWidget, CornerType cornerType);
	void unDock();

protected:

	void updateHolderPosition();

	sfg::Widget* m_holderWidget;

	sfg::Widget* m_dockedWidget;
	CornerType m_cornerType;

	size_t m_dockingSignal;

};

ABYSS_END_NAMESPACE