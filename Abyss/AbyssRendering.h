#pragma once

#include <vector>
#include "AbyssGL.h"

#include "AbyssWeakPtr.h"
#include <memory>

ABYSS_BEGIN_NAMESPACE

// Some const:
const GLuint INVALID_ID = 0;
const int    INVALID_LOCATION = -1;

// FORWARD DECLARATIONS
class Mesh;
class Light;
class Camera;
class Texture;
class Material;
class RenderPass;
class UniformBlock;
class RenderableObject;
class MeshRenderable;
class GeometryObject;
class VertexArrayObject;
class VertexBufferObject;
class UniformBufferObject;
class SimpleRenderableObject;
class SceneManagerListener;

class GBuffer;
class Renderer;
class RenderingResult;
class CompositorPass;
class CompositorPassHandler;

// Render-To-Texture
class DepthBuffer;

// TYPEDEF
// Strong Ptr
typedef std::shared_ptr<Mesh>	MeshPtr;
typedef std::shared_ptr<Light> LightPtr;
typedef std::shared_ptr<Camera> CameraPtr;
typedef std::shared_ptr<Texture> TexturePtr;
typedef std::shared_ptr<Material> MaterialPtr;
typedef std::shared_ptr<UniformBlock> UniformBlockPtr;
typedef std::shared_ptr<GeometryObject> GeometryObjectPtr;
typedef std::shared_ptr<RenderableObject> RenderableObjectPtr;
typedef std::shared_ptr<MeshRenderable> MeshRenderablePtr;
typedef std::shared_ptr<VertexArrayObject> VertexArrayObjectPtr;
typedef std::shared_ptr<VertexBufferObject> VertexBufferObjectPtr;
typedef std::shared_ptr<UniformBufferObject> UniformBufferObjectPtr;
typedef std::shared_ptr<SimpleRenderableObject> SimpleRenderableObjectPtr;
typedef std::shared_ptr<RenderPass> RenderPassSharedPtr;
typedef std::shared_ptr<GBuffer> GBufferPtr;
typedef std::shared_ptr<RenderingResult> RenderingResultPtr;
typedef std::shared_ptr<RenderPass> RenderPassPtr;
typedef std::shared_ptr<CompositorPassHandler> CompositorPassHandlerPtr;
typedef std::shared_ptr<CompositorPass> CompositorPassPtr;


// Weak Ptr:
typedef Light* LightWeakPtr;
typedef WeakPtr<RenderPass> RenderPassWeakPtr;

typedef std::vector<LightWeakPtr> LightsContainerType;

// Define for testing SSAO rendering.
// When rendering of SSAO will be done, this define should be removed.
//#define ABYSS_USE_SSAO_RENDERING


ABYSS_END_NAMESPACE
