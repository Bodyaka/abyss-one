#include "AbyssSceneManagerListener.h"
#include "AbyssSceneManager.h"

ABYSS_BEGIN_NAMESPACE

SceneManagerListener::SceneManagerListener(SceneManager* manager)
: m_sceneManager(nullptr)
{
	setSceneManager(manager);
}

SceneManagerListener::~SceneManagerListener()
{
	setSceneManager(nullptr);
}

void SceneManagerListener::setSceneManager(SceneManager* manager)
{
	if (auto oldManager = getSceneManager())
		oldManager->removeListener(this);

	m_sceneManager = manager;

	if (auto newManager = getSceneManager())
		newManager->addListener(this);
}

ABYSS_END_NAMESPACE