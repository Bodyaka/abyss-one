#pragma once

#include <string>

// TODO_REVIEW: need to move this to Abyss or separate class.
namespace Abyss
{
	void printToLog(const std::string& message);
	void clearLogFile();
	void printToLogFile(const std::string& message);
}

#define ABYSS_LOG(message)\
	Abyss::printToLog(message);