#include "AbyssSimpleRenderableObject.h"

#include "AbyssGeometryObject.h"
#include "AbyssSimpleRenderer.h"
#include "AbyssSceneManager.h"
#include "AbyssRenderPassManager.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
SimpleRenderableObjectPtr SimpleRenderableObject::create(const String & name, GeometryObjectPtr geometryObject)
{
	SimpleRenderableObject* renderable = new SimpleRenderableObject(name);
	renderable->setGeometryObject(geometryObject);

	return SimpleRenderableObjectPtr(renderable);
}
//----------------------------------------------------------------------------------------------
SimpleRenderableObject::SimpleRenderableObject(const String & name)
	: RenderableObject(name)
	, m_geometryObject()
{}
//----------------------------------------------------------------------------------------------
SimpleRenderableObject::~SimpleRenderableObject()
{}
//----------------------------------------------------------------------------------------------
GeometryObjectPtr SimpleRenderableObject::getGeometryObject() const
{
	return m_geometryObject;
}
//----------------------------------------------------------------------------------------------
AABB SimpleRenderableObject::getAABB() const
{
	return AABB();
}
//----------------------------------------------------------------------------------------------
void SimpleRenderableObject::setupDefaultRenderPasses()
{
	// Ugly AF
	auto renderPass = SceneManager::instance()->getRenderPassManager()->getRenderPass(DefaultRenderPasses::PHONG_SHADING);
	addRenderPass(renderPass);
}
//----------------------------------------------------------------------------------------------
void SimpleRenderableObject::performRendering(RendererVisitor* visitor)
{
	getGeometryObject()->draw();
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE