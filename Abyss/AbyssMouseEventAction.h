#pragma once

#pragma once

#include "AbyssEventsDef.h"
#include "AbyssEventAction.h"

ABYSS_BEGIN_NAMESPACE

class MouseEventAction:
	public EventAction
{
public:
	MouseEventAction() {};
	~MouseEventAction(){}

	virtual void perform(const MouseEventPtr & mouseEvent) = 0;
};

ABYSS_END_NAMESPACE