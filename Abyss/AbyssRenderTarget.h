#pragma once

#include <vector>
#include <map>

#include "AbyssGlm.h"
#include "AbyssRendering.h"
#include "AbyssTexture.h"
#include "AbyssDepthBuffer.h"

ABYSS_BEGIN_NAMESPACE

class Window;

class RenderTarget
{
public:
	enum Attachment
	{
		InvalidAttachment = -1,

		ColorAttachment0 = GL_COLOR_ATTACHMENT0,
		ColorAttachment1 = GL_COLOR_ATTACHMENT1,
		ColorAttachment2 = GL_COLOR_ATTACHMENT2,
		ColorAttachment3 = GL_COLOR_ATTACHMENT3,
		ColorAttachment4 = GL_COLOR_ATTACHMENT4,
		ColorAttachment5 = GL_COLOR_ATTACHMENT5,
		ColorAttachment6 = GL_COLOR_ATTACHMENT6,
		ColorAttachment7 = GL_COLOR_ATTACHMENT7,
		ColorAttachment8 = GL_COLOR_ATTACHMENT8,
		ColorAttachment9 = GL_COLOR_ATTACHMENT9,
		ColorAttachment10 = GL_COLOR_ATTACHMENT10,
		ColorAttachment11 = GL_COLOR_ATTACHMENT11,
		ColorAttachment12 = GL_COLOR_ATTACHMENT12,
		ColorAttachment13 = GL_COLOR_ATTACHMENT13,
		ColorAttachment14 = GL_COLOR_ATTACHMENT14,
		ColorAttachment15 = GL_COLOR_ATTACHMENT15,
		DepthAttachment = GL_DEPTH_ATTACHMENT,
		StencilAttachment = GL_STENCIL_ATTACHMENT,
		DepthAndStencilAttachment = GL_DEPTH_STENCIL_ATTACHMENT
	};

public:
	typedef std::vector<Attachment> AttachmentsContainerType;

public:
	
	DECLARE_POINTER(RenderTarget)

	RenderTarget(Window * window, float left = 0.f, float top = 0.f, float width = 1.f, float height = 1.f);
	~RenderTarget();

	void bind();
	static void release();

	// Render Target should be bound before calling clear method!
	void clear();

	GLuint getHandle() const;

	void setMetrics(float left, float top, float width, float height);
	void getMetrics(float & left, float & top, float * width, float & height);

	size_t getActualLeftOffset() const;
	size_t getActualTopOffset() const;
	size_t getActualWidth() const;
	size_t getActualHeight() const;

	// Setting attachment to nullptr will detach specified attachment.
	void attachTexture(Attachment attachmentPoint, Texture::Pointer texture);
	void deattachTexture(Texture::Pointer texture);
	bool hasAttachment(Attachment attachmentPoint) const;
	bool isTextureAttached(Texture::Pointer texture) const;

	Attachment getTextureAttachment(Texture::Pointer texture) const;
	Texture::Pointer getAttachedTexture(Attachment attachmentPoint) const;

	void attachDepthBuffer(DepthBuffer::Pointer buffer);
	void deattachDepthBuffer();
	DepthBuffer::Pointer getDepthBuffer() const;
	bool hasAttachedDepthBuffer() const;

	// Setup, which attachment to draw. This represents binding between shader's output variable
	// and attachment point, where the shader's ouput variable (frag data location) is the index in container.
	void setDrawBuffers(const AttachmentsContainerType & buffersToDraw);
	// Return draw buffer container.
	const AttachmentsContainerType & getDrawBuffers() const;

	// Check status of FrameBuffer. 
	// The error message will be written in debugString, if it was defined.
	bool isValid(std::string * debugString = nullptr) const;

	void notifyStartRendering();
	void notifyFinishRendering();

	void setClearEveryFrame(bool toClear);
	bool isClearEveryFrame() const;
	void setClearColor(const Vector4D& clearColor);
	const Vector4D& getClearColor() const;

	void update();
	void reinit();

	bool blitTo(RenderTarget* renderTarget, bool withDepthBuffer = true);

	Window* getAttachedWindow() const;

	// Will try to get pixels from 0-s color attachment:
	unsigned char* readPixels() const;
	unsigned char* readPixels(PixelFormat pixelFormat, GLenum internalFormat) const;
	unsigned char* readPixels(int x, int y, int width, int height, PixelFormat pixelFormat, GLenum internalFormat) const;

private:

	// TODO: Should be deleted, when DSA will be integrated into project
	bool isValid_impl(std::string * debugString = nullptr) const;

	bool m_needToReinit,
		 m_isClearEveryFrame;

	GLuint m_handle;

	float m_left,
		  m_top,
		  m_width,
		  m_height;

	DepthBuffer::Pointer m_depthBuffer;

	typedef std::map<Attachment, Texture::Pointer> AttachedTexturesContainer;
	AttachedTexturesContainer m_attachedTextures;

	AttachmentsContainerType m_drawBuffers;

	Window* m_attachedWindow;

	Vector4D m_clearColor;
};

// INLINE METHODS
//--------------------------------------------------------------------------------------------------
inline GLuint RenderTarget::getHandle() const
{	return m_handle;	}
//--------------------------------------------------------------------------------------------------
inline DepthBuffer::Pointer RenderTarget::getDepthBuffer() const
{	return m_depthBuffer;	}
//--------------------------------------------------------------------------------------------------
inline const RenderTarget::AttachmentsContainerType & RenderTarget::getDrawBuffers() const
{	return m_drawBuffers; }
//--------------------------------------------------------------------------------------------------
inline void RenderTarget::setClearEveryFrame(bool toClear)
{	m_isClearEveryFrame = toClear;	}
//-------------------------------------------------------------------------------------------------
inline bool RenderTarget::isClearEveryFrame() const
{	return m_isClearEveryFrame;	}
//--------------------------------------------------------------------------------------------------
inline Window* RenderTarget::getAttachedWindow() const
{	return m_attachedWindow;	}
//--------------------------------------------------------------------------------------------------
inline const Vector4D& RenderTarget::getClearColor() const
{	return m_clearColor;	}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE