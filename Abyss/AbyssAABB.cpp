#include "AbyssAABB.h"

#include "AbyssPlane.h"

ABYSS_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------------------
Vector3D AABB::leftBottomFar() const
{
	return begin();
}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::leftTopFar() const
{
	return Vector3D(minX(), maxY(), minZ());
}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::rightTopFar() const
{
	return Vector3D(maxX(), maxY(), minZ());
}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::rightBottomFar() const
{
	return Vector3D(maxX(), minY(), minZ());
}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::leftBottomNear() const
{
	return Vector3D(minX(), minY(), maxZ());
}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::leftTopNear() const
{
	return Vector3D(minX(), maxY(), maxZ());
}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::rightTopNear() const
{
	return end();
}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::rightBottomNear() const
{
	return Vector3D(maxX(), minY(), maxZ());
}
//--------------------------------------------------------------------------------------------------
std::vector<AABB> AABB::split(size_t w, size_t h, size_t d) const
{
	// Computing size of splited aabbs:
	size_t size = w*h*d;
	
	float dx = width()  / w;
	float dy = height() / h;
	float dz = depth()  / d;
	
	float x0 = minX();
	float y0 = minY();
	float z0 = minZ();

	std::vector<AABB> splitedAabbs;
	splitedAabbs.reserve(size);

	for (size_t x = 0; x < w; ++x)
		for (size_t y = 0; y < h; ++y)
			for (size_t z = 0; z< d; ++z)
			{
				Vector3D begin = Vector3D(x0 + x*dx, y0 + y*dy, z0 + z*dz);
				Vector3D end   = Vector3D(x0 + (x+1)*dx, y0 + (y+1)*dy, z0 + (z+1)*dz);
				splitedAabbs.push_back(AABB(begin, end));
			}

	return splitedAabbs;
}
//--------------------------------------------------------------------------------------------------
bool AABB::contains(const Vector3D & point) const
{
	if (!(point.x >= minX() && point.x <= maxX())) return false;
	if (!(point.y >= minY() && point.y <= maxY())) return false;
	if (!(point.z >= minZ() && point.z <= maxZ())) return false;

	return true;
}
//--------------------------------------------------------------------------------------------------
bool AABB::contains(const AABB & aabb) const
{
	if (!(aabb.minX() >= minX() && aabb.maxX() <= maxX())) return false;
	if (!(aabb.minY() >= minY() && aabb.maxY() <= maxY())) return false;
	if (!(aabb.minZ() >= minZ() && aabb.maxZ() <= maxZ())) return false;

	return true;
}
//--------------------------------------------------------------------------------------------------
ContainerV3D AABB::points() const
{
	ContainerV3D pointsContainer;
	pointsContainer.reserve(8);

	pointsContainer.push_back(rightBottomFar());
	pointsContainer.push_back(rightTopFar());
	pointsContainer.push_back(leftTopFar());
	pointsContainer.push_back(leftBottomFar());

	pointsContainer.push_back(leftTopNear());
	pointsContainer.push_back(leftBottomNear());
	pointsContainer.push_back(rightBottomNear());
	pointsContainer.push_back(rightTopNear());

	return pointsContainer;
}
//--------------------------------------------------------------------------------------------------
AABB AABB::transform(const Matrix4D & matrix) const
{
	ContainerV3D vectors = transformed(matrix);

	float minx, miny, minz, maxx, maxy, maxz;
	Vector3D first = vectors.front();
	minx = maxx = first.x;
	miny = maxy = first.y;
	minz = maxz = first.z;

	for (size_t i = 1; i<vectors.size(); ++i)
	{
		Vector3D elem = vectors.at(i);

		minx = std::min<float>(elem.x, minx);
		miny = std::min<float>(elem.y, miny);
		minz = std::min<float>(elem.z, minz);

		maxx = std::max<float>(elem.x, maxx);
		maxy = std::max<float>(elem.y, maxy);
		maxz = std::max<float>(elem.z, maxz);
	}
	
	return AABB(Vector3D(minx, miny, minz), Vector3D(maxx, maxy, maxz));
}
//--------------------------------------------------------------------------------------------------
ContainerV3D AABB::transformed(const Matrix4D & matrix) const
{
	ContainerV3D vectors;
	vectors.reserve(8);

	vectors.push_back(Vector3D(matrix * Vector4D(rightBottomFar(), 1.f)));
	vectors.push_back(Vector3D(matrix * Vector4D(rightTopFar(), 1.f)));
	vectors.push_back(Vector3D(matrix * Vector4D(leftTopFar(), 1.f)));
	vectors.push_back(Vector3D(matrix * Vector4D(leftBottomFar(), 1.f)));

	vectors.push_back(Vector3D(matrix * Vector4D(leftTopNear(), 1.f)));
	vectors.push_back(Vector3D(matrix * Vector4D(leftBottomNear(), 1.f)));
	vectors.push_back(Vector3D(matrix * Vector4D(rightBottomNear(), 1.f)));
	vectors.push_back(Vector3D(matrix * Vector4D(rightTopNear(), 1.f)));

	return vectors;
}
//--------------------------------------------------------------------------------------------------
IntersectionResult AABB::intersect(const Plane & plane) const
{
	Vector3D c = center();
	Vector3D h = diagonal();

	Vector3D n = glm::abs(plane.n());

	float e = glm::dot(h, n);
	float d = plane.signedDistance(c);

	if (d - e > 0)
		return IR_Inside;
	if (d + e < 0)
		return IR_NoIntersection;
	
	return IR_Intersects;
}
//--------------------------------------------------------------------------------------------------
IntersectionResult AABB::intersect(const AABB & aabb) const
{
	bool containsBegin = contains(aabb.begin());
	bool containsEnd   = contains(aabb.end());

	if (containsBegin && containsEnd)
		return IR_Inside;
	if (containsBegin || containsEnd)
		return IR_Intersects;
		
	return IR_NoIntersection;
	
	/*int spaceHits = 0;

	if (aabb.minX() >= minX() && aabb.maxX() <= maxX()) ++spaceHits;
	if (aabb.minY() >= minY() && aabb.maxY() <= maxY()) ++spaceHits;
	if (aabb.minZ() >= minZ() && aabb.maxZ() <= maxZ()) ++spaceHits;

	// If no space hits - return outside:
	if (spaceHits == 0)
		return EIR_OUTSIDE;
	// If 3 space hits - return inside:
	if (spaceHits == 3)
		return EIR_INSIDE;
	// Else - return intersects:
	return EIR_INTERSECT;*/
}
//--------------------------------------------------------------------------------------------------
AABB AABB::generateAxisAlignedBoundingBox(const ContainerV3D & vertices)
{
	return generateAxisAlignedBoundingBox((float*)vertices.data(), vertices.size());
}
//-------------------------------------------------------------------------------------------------
AABB AABB::generateAxisAlignedBoundingBox(float * positions, size_t verticesSize)
{
	if (!positions || verticesSize == 0)
		return AABB();

	Vector3D frontVec(positions[0], positions[1], positions[2]);
	Vector3D begin = frontVec, end = frontVec;

	for (size_t i = 1; i < verticesSize; ++i)
	{
		size_t vertexIndex = i * 3;

		float x = positions[vertexIndex];
		float y = positions[vertexIndex + 1];
		float z = positions[vertexIndex + 2];

		begin.x = std::min(begin.x, x);
		begin.y = std::min(begin.y, y);
		begin.z = std::min(begin.z, z);

		end.x = std::max(end.x, x);
		end.y = std::max(end.y, y);
		end.z = std::max(end.z, z);
	}

	return AABB(begin, end);
}
//-------------------------------------------------------------------------------------------------
void AABB::unit(const AABB & other)
{
	float newMinX = std::min(minX(), other.minX());
	float newMaxX = std::max(maxX(), other.maxX());

	float newMinY = std::min(minY(), other.minY());
	float newMaxY = std::max(maxY(), other.maxY());

	float newMinZ = std::min(minZ(), other.minZ());
	float newMaxZ = std::max(maxZ(), other.maxZ());

	Vector3D newBegin = Vector3D(newMinX, newMinY, newMinZ);
	Vector3D newEnd = Vector3D(newMaxX, newMaxY, newMaxZ);

	m_bbox.first = newBegin;
	m_bbox.second = newEnd;
}
//--------------------------------------------------------------------------------------------------
void AABB::extend(const Vector3D & extendSize)
{
	m_bbox.first -= extendSize;
	m_bbox.second += extendSize;
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE