#pragma once

#include "AbyssMesh.h"

ABYSS_BEGIN_NAMESPACE

class Rectangle
	: public Mesh
{
public:
	Rectangle(const Vector2D & position, const Vector2D & size);

	void setPosition(const Vector2D & pos);
	void setSize(const Vector2D & size);

	const Vector2D & getPosition() const;
	const Vector2D & getSize() const;

private:
	Vector2D m_position,
			 m_size;
};


inline const Vector2D & Abyss::Rectangle::getPosition() const
{	return m_position;	}

inline const Vector2D & Abyss::Rectangle::getSize() const
{	return m_size;	}

ABYSS_END_NAMESPACE