#pragma once

#include <string>
#include <map>
#include "AbyssRendering.h"


ABYSS_BEGIN_NAMESPACE

// Need to move it to Constants.h
namespace constants
{
	const std::string ABYSS_SSAO_TEXTURE = "ABYSS_SSAO";
	const std::string ABYSS_MAIN_TEXTURE = "ABYSS_MAIN_TEXTURE";
}

class RenderingResult final
{
public:
	RenderingResult() = default;
	RenderingResult(const RenderingResult& other) = default;
	RenderingResult& operator=(const RenderingResult& other) = default;
	~RenderingResult() = default;

	// Eliminates textures and GBuffer:
	void clear();

	void setTexture(const std::string& name, TexturePtr texture);
	void removeTexture(const std::string& name);
	void clearTextures();
	
	TexturePtr getTexture(const std::string& name) const;
	std::map<std::string, TexturePtr> getTextures() const;

	void setGBuffer(GBufferPtr gBuffer);
	GBufferPtr getGBuffer() const;


private:
	std::map<std::string, TexturePtr> m_textures;

	GBufferPtr m_gBuffer;
};

// INLINE METHODS:
//------------------------------------------------------------------------------------------------
inline std::map<std::string, TexturePtr> RenderingResult::getTextures() const
{	return m_textures;	}
//------------------------------------------------------------------------------------------------
inline void RenderingResult::setGBuffer(GBufferPtr gBuffer)
{	m_gBuffer = gBuffer;	}
//------------------------------------------------------------------------------------------------
inline GBufferPtr RenderingResult::getGBuffer() const
{	return m_gBuffer;	}
//------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE