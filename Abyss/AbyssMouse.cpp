#include "AbyssMouse.h"

#include <set>

ABYSS_BEGIN_NAMESPACE

// Struct-implemtor of Mouse data.
struct MouseImpl
{
	// Return implementor, which stores actual mouse data.
	static MouseImpl * getSharedInstance()
	{
		static MouseImpl * sharedMouseImplemetor = nullptr;

		if (sharedMouseImplemetor == nullptr)
			sharedMouseImplemetor = new MouseImpl();

		return sharedMouseImplemetor;
	}


	typedef std::set<Mouse::ButtonType> TButtonsContainer;

	TButtonsContainer m_pressedButtons;

	Vector2D m_currentPosition,
		     m_previousPosition;

	bool m_isAlreadyMoved;

private:
	MouseImpl()
		: m_isAlreadyMoved(false)
	{};

	~MouseImpl(){};
};
//-------------------------------------------------------------------------------------------------
const Vector2D & Mouse::getCurrentPosition()
{
	return MouseImpl::getSharedInstance()->m_currentPosition;
}
//-------------------------------------------------------------------------------------------------
const Vector2D & Mouse::getPreviousPosition()
{
	return MouseImpl::getSharedInstance()->m_previousPosition;
}
//-------------------------------------------------------------------------------------------------
bool Mouse::isButtonPressed(ButtonType button)
{
	MouseImpl * implemetor = MouseImpl::getSharedInstance();
	auto iter = implemetor->m_pressedButtons.find(button);

	return iter != implemetor->m_pressedButtons.end();
}
//-------------------------------------------------------------------------------------------------
void Mouse::addToPressedButtons(ButtonType button)
{
	MouseImpl::getSharedInstance()->m_pressedButtons.insert(button);
}
//-------------------------------------------------------------------------------------------------
void Mouse::removeFromPressedButtons(ButtonType button)
{
	MouseImpl::getSharedInstance()->m_pressedButtons.erase(button);
}
//-------------------------------------------------------------------------------------------------
void Mouse::setMousePosition(const Vector2D & position)
{
	MouseImpl * implementor = MouseImpl::getSharedInstance();
	if (!implementor->m_isAlreadyMoved)
	{
		implementor->m_isAlreadyMoved = true;
		implementor->m_currentPosition = implementor->m_previousPosition = position;
		return;
	}

	implementor->m_previousPosition = implementor->m_currentPosition;
	implementor->m_currentPosition = position;
}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE