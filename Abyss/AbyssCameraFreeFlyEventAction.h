#pragma once

#include "Abyss.h"
#include "AbyssRendering.h"
#include "AbyssMouseEventAction.h"

ABYSS_BEGIN_NAMESPACE

class CameraFreeFlyEventAction
	: public MouseEventAction
{
public:
	// Create camera free fly event action (i.e. noclip)
	// @camera - pointer to camera object;
	// @cameraRotatesMultiplier - Value, which used how map single pixel offset in mouse into radians.
	// I.e. this means, that each mouse offset will multiplies on that value to produce on which camera will rotates.
	// The higher this value the faster camera will rotates. 
	explicit CameraFreeFlyEventAction(CameraPtr camera, float cameraRotatesMultiplier = 1 / 40.f);
	~CameraFreeFlyEventAction() {};

	void setCameraRotatesMultiplier(float newValue);
	float getCameraRotatesMultiplier() const;

	virtual void perform(const MouseEventPtr & event) override;

protected:
	float m_cameraRotatesMultiplier;
	CameraPtr m_camera;
};
// INLINE METHODS
//-------------------------------------------------------------------------------------------------
inline void CameraFreeFlyEventAction::setCameraRotatesMultiplier(float newValue)
{    m_cameraRotatesMultiplier = newValue;	}
//-------------------------------------------------------------------------------------------------
inline float CameraFreeFlyEventAction::getCameraRotatesMultiplier() const
{	return m_cameraRotatesMultiplier;	}
//-------------------------------------------------------------------------------------------------
ABYSS_END_NAMESPACE