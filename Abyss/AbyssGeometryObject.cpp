#include "AbyssGeometryObject.h"

#include "AbyssVertexBufferObject.h"
#include "AbyssVertexArrayObject.h"
#include "AbyssDefaultVertexAttributes.h"

ABYSS_BEGIN_NAMESPACE
//--------------------------------------------------------------------------------------------------
GeometryObject::GeometryObject(GLenum drawMode)
	: m_drawMode(drawMode)
	, m_vao(VertexArrayObject::createVAO())
	, m_indicesBufferObject(VertexBufferObjectPtr())
	, m_verticesCount(0)
	, m_indicesCount(0)
{
}
//--------------------------------------------------------------------------------------------------
GeometryObject::~GeometryObject()
{
}
//--------------------------------------------------------------------------------------------------
void GeometryObject::draw()
{
	(getIndicesCount() == 0) ? drawArrays() : drawElements();

}
//--------------------------------------------------------------------------------------------------
void GeometryObject::drawArrays()
{
	if (getVerticesCount() == 0)
		return;

	m_vao->bind();
		glDrawArrays(m_drawMode, 0, getVerticesCount());
	m_vao->release();
}
//--------------------------------------------------------------------------------------------------
void GeometryObject::drawElements()
{
	if (getIndicesCount() == 0)
		return;

	m_vao->bind();
	getIndicesBufferObject()->bind();
		glDrawElements(m_drawMode, getIndicesCount(), GL_UNSIGNED_INT, (GLvoid*)0);
	getIndicesBufferObject()->release();
	m_vao->release();				
}
//--------------------------------------------------------------------------------------------------
void GeometryObject::addBufferObject(int attribute, VertexBufferObjectPtr buffer)
{
	m_attributeBuffers.insert(TAttributeBuffersContainer::value_type(attribute, buffer));
}
//--------------------------------------------------------------------------------------------------
void GeometryObject::removeBufferObject(int attribute)
{
	if (!hasAttribute(attribute))
		return;

	m_attributeBuffers.erase(attribute);
}
//--------------------------------------------------------------------------------------------------
bool GeometryObject::hasAttribute(int attribute) const
{
	auto findedPointer = m_attributeBuffers.find(attribute);

	return findedPointer != m_attributeBuffers.end();
}
//--------------------------------------------------------------------------------------------------
bool GeometryObject::setupAttribute(int attribute, int location)
{
	if (m_vao == nullptr || !hasAttribute(attribute) || location == INVALID_LOCATION)
		return false;
	
	int elementsCount = (attribute == TexelAttribute) ? 2 : 3;

	// Bind VAO:
	m_vao->bind();
	
	// Bind VBO:
	getVertexBufferObject(attribute)->bind();

	glEnableVertexAttribArray(location);
	glVertexAttribPointer(location, elementsCount, GL_FLOAT, GL_FALSE, 0, (GLvoid*)nullptr);

	// Release VBO:
	getVertexBufferObject(attribute)->release();

	// Release VAO:
	m_vao->release();

	return true;
}
//--------------------------------------------------------------------------------------------------
void GeometryObject::setIndicesBufferObject(VertexBufferObjectPtr indicesBufferObject)
{
	m_indicesBufferObject = indicesBufferObject;
}
//--------------------------------------------------------------------------------------------------
VertexBufferObjectPtr GeometryObject::getVertexBufferObject(int attribute) const
{
	if (!hasAttribute(attribute))
		return VertexBufferObjectPtr();

	return m_attributeBuffers.at(attribute);
}
//--------------------------------------------------------------------------------------------------
const AABB & GeometryObject::getAABB() const
{
	return m_aabb;
}
//--------------------------------------------------------------------------------------------------
ABYSS_END_NAMESPACE
