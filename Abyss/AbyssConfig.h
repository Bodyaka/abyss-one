#pragma once

// Defines For namespace:
#ifndef ABYSS_BEGIN_NAMESPACE
	#define ABYSS_BEGIN_NAMESPACE namespace Abyss {
#endif

#ifndef ABYSS_END_NAMESPACE
	#define ABYSS_END_NAMESPACE }
#endif

// Defines for shader debugging:
#ifndef ABYSS_ENABLE_SHADER_DEBUGGING
	#define ABYSS_ENABLE_SHADER_DEBUGGING
#endif


// Defines for DLL_export and DLL_import:
#if defined( _WIN32 ) && !defined( ABYSS_STATIC )
	#if defined( ABYSS_EXPORT )
		#define ABYSS_API __declspec( dllexport )
	#else
		#define ABYSS_API __declspec( dllimport )
#endif
#else
	#define ABYSS_API
#endif


// Rendering defines:

// This define is used for enabling Blur rendering
//#define ABYSS_ENABLE_BLUR 
