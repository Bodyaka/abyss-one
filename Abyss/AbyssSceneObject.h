#pragma once

#include <unordered_map>
#include <boost/shared_ptr.hpp>

#include "AbyssDeus.h"
#include "AbyssAABB.h"
#include "AbyssRenderableObject.h"
#include "AbyssFreeTransformableObject.h"
#include "AbyssLightsContainer.h"

ABYSS_BEGIN_NAMESPACE

class RenderPass;
class SceneManager;
class RenderQueueVisitor;

class SceneObject
	: public Deus<SceneObject>
	, public FreeTransformableObject
	, public LightsContainer
{
protected:
	typedef Deus<SceneObject> TSuperDeus;
	typedef std::unordered_map<String, RenderableObjectPtr> TRenderableObjectsContainer;
public:
	SceneObject(const String & name, SceneManager * manager, SceneObject * parent = nullptr);
	~SceneObject();

	void prepareForRendering(RenderQueueVisitor* visitor);

	void addRenderableObject(RenderableObjectPtr object);
	void removeRenderableObject(const String & objectName);
	void removeRenderableObject(RenderableObjectPtr object);
	size_t getRenderableObjectsSize() const;
	RenderableObjectPtr getRenderableOject(const String & name) const;
	std::vector<RenderableObjectPtr> getRenderableObjects() const;
	bool hasRenderableObject(const String & name) const;
	bool hasRenderableObject(RenderableObjectPtr object) const;

	const String & getName() const;

	void setVisible(bool visibility);
	bool isVisible() const;

	// TODO: need add enum parameter to control which AABB to return.
	// Returns the local AABB without any local transformations.
	inline const AABB& getLocalAABB() const;
	// Return global AABB with all of it's transformations.
	inline const AABB& getWorldAABB() const;

	SceneManager * getSceneManager() const;

	void onStartRendering();
	void onFinishRendering();

	virtual LightsContainerType getLights() const override;

public:
	virtual void updateTransform();

protected:

	virtual void onLightAdded(LightWeakPtr light) override;
	virtual void onLightRemoved(LightWeakPtr light) override;

	bool m_isVisible;
	AABB m_worldAABB,
		 m_localAABB;


	String m_name;
	SceneManager * m_manager;
	TRenderableObjectsContainer m_renderableObjects;

private:

	void updateAABB();

	void updateRenderableObjectsAABB();

};

// TYPEDEF
typedef boost::shared_ptr<SceneObject> SceneObjectPtr;

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline const String & SceneObject::getName() const
{	return m_name;	}
//----------------------------------------------------------------------------------------------
inline bool SceneObject::isVisible() const
{	return m_isVisible;	}
//----------------------------------------------------------------------------------------------
inline const AABB & SceneObject::getLocalAABB()	const
{	return m_localAABB;	}
//----------------------------------------------------------------------------------------------
inline const AABB & SceneObject::getWorldAABB()	const
{	return m_worldAABB;	}
//----------------------------------------------------------------------------------------------
inline SceneManager * SceneObject::getSceneManager() const
{	return m_manager;		}// TODO	
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE