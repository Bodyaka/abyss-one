#include "AbyssFullScreenRectRenderer.h"

#include "AbyssRenderer.h"
#include "AbyssSceneObject.h"
#include "AbyssShaderProgram.h"
#include "AbyssSceneManager.h"
#include "AbyssMaterial.h"
#include "AbyssTexture.h"
#include "AbyssMeshRenderable.h"

#include "AbyssRenderingResult.h"

ABYSS_BEGIN_NAMESPACE

namespace
{
	const std::string SHADER_NAME = "FullScreenRect";
	const std::string INPUT_TEXTURE_NAME = "uInputTexture";

	const size_t diffuseTextureBindingPoint = 0;
}


FullScreenRectRenderer::FullScreenRectRenderer(const std::string& expectedTextureName)
	: Renderer(SHADER_NAME)
	, m_expectedTextureInput(expectedTextureName)
{
}
//-------------------------------------------------------------------------------------------------
FullScreenRectRenderer::~FullScreenRectRenderer()
{
}
//-------------------------------------------------------------------------------------------------
void FullScreenRectRenderer::beforeRender(RenderableObject * const renderable)
{
	/*
	MaterialPtr material = renderable->getMaterial();

	getShaderProgram()->setUniform(MATERIAL_DIFFUSE_TEXTURE_NAME, int(diffuseTextureBindingPoint));
	getShaderProgram()->setUniform(MATERIAL_AMBIENT_NAME, material->getAmbientColor());

	if (material->hasTexture(DiffuseTextureType))
		material->getTexture(DiffuseTextureType)->bind(diffuseTextureBindingPoint);
	*/
}
//-------------------------------------------------------------------------------------------------
void FullScreenRectRenderer::afterRender(RenderableObject * const renderable)
{
	/*
	MaterialPtr material = renderable->getMaterial();

	if (material->hasTexture(DiffuseTextureType))
		material->getTexture(DiffuseTextureType)->release();
		*/
}
//-------------------------------------------------------------------------------------------------
bool FullScreenRectRenderer::isSupports(const RenderableObject * const object) const
{
	return true;
}
//-------------------------------------------------------------------------------------------------
void FullScreenRectRenderer::startRendering(RenderingResultPtr input)
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Renderer::startRendering(input);

if (!getExpectedTextureInput().empty()) 
	{
		if (auto texture = input->getTexture(getExpectedTextureInput()))
		{
			getShaderProgram()->setUniform(INPUT_TEXTURE_NAME, int(diffuseTextureBindingPoint));
			texture->bind(diffuseTextureBindingPoint);

			glDeleteSync(0);

		}
		else 
		{
			raiseAnError("FullScreenRectRenderer - cannot find expected texture input!");
		}
	}
}
//-------------------------------------------------------------------------------------------------
void FullScreenRectRenderer::finishRendering(RenderingResultPtr output)
{
	Renderer::finishRendering(output);

	if (!getExpectedTextureInput().empty())
	{
		if (auto texture = output->getTexture(getExpectedTextureInput()))
		{
			texture->release();
			glDeleteSync(0);
		}
		else
			raiseAnError("FullScreenRectRenderer - cannot find expected texture input!");
	}
}
//-------------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE
