#include "AbyssTexture.h"

#include "AbyssGlm.h"
#include "AbyssRendering.h"
#include "AbyssTextureManager.h"

static size_t s_invalidBindingPoint = -1;

ABYSS_BEGIN_NAMESPACE

namespace
{

	GLenum convertWrappingTypeToGL(Texture::WrappingType wrapping)
	{
		switch (wrapping)
		{
		case Texture::ClampToBorderWrapping:
			return GL_CLAMP_TO_BORDER;
		case Texture::ClampToEdgeWrapping:
			return GL_CLAMP_TO_EDGE;
		case Texture::RepeatWrapping:
			return GL_REPEAT;
		case Texture::MirroredRepeatWrapping:
			return GL_MIRRORED_REPEAT;
		}

		assert(false);
		return GL_NONE;
	}


	GLenum convertCubeMapDirectionTypeToGL(Texture::CubeMapDirectionType directionType)
	{
		switch (directionType)
		{
		case Texture::CubeMapDirectionType::Right:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
		case Texture::CubeMapDirectionType::Left:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
		case Texture::CubeMapDirectionType::Top:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
		case Texture::CubeMapDirectionType::Bottom:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
		case Texture::CubeMapDirectionType::Back:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
		case Texture::CubeMapDirectionType::Front:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
		default:
			assert(false && "Unknown CubeMapDirectionType!");
			return GL_NONE;
		}
	}

}

//-------------------------------------------------------------------------------------------------
Texture::Texture()
	: m_type(GL_NONE)
	, m_id(INVALID_ID)
	, m_bindingPoint(s_invalidBindingPoint)
	, m_wrappingS(RepeatWrapping)
	, m_wrappingT(RepeatWrapping)
	, m_wrappingR(RepeatWrapping)
	, m_filtering(NearestFiltering)
	, m_pixelFormat(PF_UNKNOWN)
	, m_internalFormat(GL_NONE)
{
}
//-------------------------------------------------------------------------------------------------
Texture::~Texture()
{
	deleteTexture();
}
//-------------------------------------------------------------------------------------------------
void Texture::bind()
{
	bind(0);
}
//-------------------------------------------------------------------------------------------------
void Texture::bind(size_t bindingPoint)
{
	m_bindingPoint = bindingPoint;

	glActiveTexture(GL_TEXTURE0 + bindingPoint);
	glBindTexture(getType(), getId());
}
//-------------------------------------------------------------------------------------------------
void Texture::release()
{
	if (m_bindingPoint == s_invalidBindingPoint)
		return;

	glActiveTexture(GL_TEXTURE0 + m_bindingPoint);
	glBindTexture(getType(), 0);
	m_bindingPoint = s_invalidBindingPoint;
}
//-------------------------------------------------------------------------------------------------
bool Texture::setRawDataImpl(GLenum type, const void * data, size_t width, size_t height, size_t depth, PixelFormat pixelFormat, GLenum inTextureFormat, GLenum gpuTextureFormat)
{
	if (pixelFormat == PF_UNKNOWN)
		return false;

	// Creating texture of specified type, if needed.
	if (type != getType())
	{
		deleteTexture();
		m_id = generateTexture(type);
		m_type = type;
	}

	bind();

	m_width = width;
	m_height = height;
	m_depth = depth;

	m_pixelFormat = pixelFormat;

	GLenum glPixelType = convertPixelFormatToGL(pixelFormat);

	switch (type)
	{
	case GL_TEXTURE_1D:
		glTexImage1D(getType(), 0, gpuTextureFormat, width, 0, inTextureFormat, glPixelType, data);
		break;
	case GL_TEXTURE_2D:
		glTexImage2D(getType(), 0, gpuTextureFormat, width, height, 0, inTextureFormat, glPixelType, data);
		break;
	case GL_TEXTURE_3D:
		glTexImage3D(getType(), 0, gpuTextureFormat, width, height, depth, 0, inTextureFormat, glPixelType, data);
		break;
	}

	updateParameters();
	return checkGLErrorStatus();
}
//-------------------------------------------------------------------------------------------------
bool Texture::setCubeMapData(const std::map<CubeMapDirectionType, const void*>& cubeMapData, size_t width, size_t height, PixelFormat pixelFormat /*= PF_UBYTE*/, GLenum inTextureFormat /*= GL_RGB*/, GLenum gpuTextureFormat /*= GL_RGB*/)
{
	if (pixelFormat == PF_UNKNOWN)
		return false;

	auto type = GL_TEXTURE_CUBE_MAP;

	// Creating texture of specified type, if needed.
	if (type != getType())
	{
		deleteTexture();
		m_id = generateTexture(type);
		m_type = type;
	}

	m_width = width;
	m_height = height;
	m_depth = 0;
	m_internalFormat = gpuTextureFormat;

	bind();

	GLenum glPixelType = convertPixelFormatToGL(pixelFormat);
	for (const auto& mapDataPair : cubeMapData)
	{
		auto cubeTextureBindingPoint = convertCubeMapDirectionTypeToGL(mapDataPair.first);
		auto pixelData = mapDataPair.second;
		assert(pixelData != nullptr); // Check for correct data!

		glTexImage2D(cubeTextureBindingPoint, 0, gpuTextureFormat, width, height, 0, inTextureFormat, glPixelType, pixelData);
	}

	updateParameters();

	return checkGLErrorStatus();
}
//-------------------------------------------------------------------------------------------------
bool Texture::setRawData(const void * data, size_t width, PixelFormat pixelFormat, GLenum inTextureFormat, GLenum gpuTextureFormat)
{
	return setRawDataImpl(GL_TEXTURE_1D, data, width, 1, 1, pixelFormat, inTextureFormat, gpuTextureFormat);
}
//-------------------------------------------------------------------------------------------------
bool Texture::setRawData(const void * data, size_t width, size_t height, PixelFormat pixelFormat, GLenum inTextureFormat, GLenum gpuTextureFormat)
{
	return setRawDataImpl(GL_TEXTURE_2D, data, width, height, 1, pixelFormat, inTextureFormat, gpuTextureFormat);
}
//-------------------------------------------------------------------------------------------------
bool Texture::setRawData(const void * data, size_t width, size_t height, size_t depth, PixelFormat pixelFormat, GLenum inTextureFormat, GLenum gpuTextureFormat)
{
	return setRawDataImpl(GL_TEXTURE_3D, data, width, height, depth, pixelFormat, inTextureFormat, gpuTextureFormat);
}
//-------------------------------------------------------------------------------------------------
void Texture::setSize(size_t width)
{
	if (getType() == GL_TEXTURE_1D && getWidth() == width)
		return;
	
	setRawData(nullptr, width, getPixelFormat());
}
//-------------------------------------------------------------------------------------------------
void Texture::setSize(size_t width, size_t height)
{
	if (getType() == GL_TEXTURE_2D && getWidth() == width && getHeight() == height)
		return;

	setRawData(nullptr, width, height, getPixelFormat());
}
//-------------------------------------------------------------------------------------------------
void Texture::setSize(size_t width, size_t height, size_t depth)
{
	if (getType() == GL_TEXTURE_3D && getWidth() == width && getHeight() == height && getDepth() == depth)
		return;

	setRawData(nullptr, width, height, depth, getPixelFormat());
}
//-------------------------------------------------------------------------------------------------
void Texture::deleteTexture()
{
	glDeleteTextures(1, &m_id);
}
//-------------------------------------------------------------------------------------------------
size_t Texture::generateTexture(GLenum type)
{
	GLuint id;
	glGenTextures(1, &id);
	return id;
}
//-------------------------------------------------------------------------------------------------
void Texture::setWrapping(WrappingType wrap)
{
	setWrapping(wrap, wrap, wrap);
}
//-------------------------------------------------------------------------------------------------
void Texture::setWrapping(WrappingType wrapS, WrappingType wrapT, WrappingType wrapR)
{
	// TODO: should we manually bind texture in order to change the wrapping value?
	bind();

	m_wrappingS = wrapS;
	m_wrappingT = wrapT;
	m_wrappingR = wrapR;

	glTexParameteri(getType(), GL_TEXTURE_WRAP_S, convertWrappingTypeToGL(wrapS));
	glTexParameteri(getType(), GL_TEXTURE_WRAP_T, convertWrappingTypeToGL(wrapT));
	glTexParameteri(getType(), GL_TEXTURE_WRAP_R, convertWrappingTypeToGL(wrapR));

	release();
}
//-------------------------------------------------------------------------------------------------
void Texture::setFiltering(FilteringType filtering)
{
	// TODO: should we manually bind texture in order to change the filtering value?
	bind();

	m_filtering = filtering;

	GLenum glFiltering = (filtering == NearestFiltering) ? GL_NEAREST : GL_LINEAR;
	
	glTexParameteri(getType(), GL_TEXTURE_MIN_FILTER, glFiltering);
	glTexParameteri(getType(), GL_TEXTURE_MAG_FILTER, glFiltering);

	// TODO: need add filtering for mipmaps

	release();
}
//-------------------------------------------------------------------------------------------------
void Texture::updateParameters()
{
	//setWrapping(getWrappingS(), getWrappingT(), getWrappingR());
	setFiltering(getFiltering());

}
//-------------------------------------------------------------------------------------------------
TexturePtr Texture::getDummyWhiteTexture()
{
	static TexturePtr texture;
	
	if (!texture)
	{
		texture.reset(new Texture());
		Vector4D whiteTexel(1);
		texture->setRawData(&whiteTexel[0], 1, 1, Abyss::PF_FLOAT);
		TextureManager::getInstance()->insertTexture("__AbyssDummyWhiteTexture", texture);
	}

	return texture;
}
//-------------------------------------------------------------------------------------------------
GLenum Texture::getCurrentActiveTexture()
{
	GLint s_currentActiveTexture = 0;
	glGetIntegerv(GL_ACTIVE_TEXTURE, &s_currentActiveTexture);

	return GLenum(s_currentActiveTexture);
}
//-------------------------------------------------------------------------------------------------
GLint Texture::getCurrentBoundTexture(GLenum textureType)
{
	GLenum textureBindingType = 0;

	switch (textureType)
	{
	case GL_TEXTURE_1D:
		textureBindingType = GL_TEXTURE_BINDING_1D;
		break;
	case GL_TEXTURE_2D:
		textureBindingType = GL_TEXTURE_BINDING_2D;
		break;
	case GL_TEXTURE_3D:
		textureBindingType = GL_TEXTURE_BINDING_3D;
		break;
	default:
		assert(false && "Incorrect input Texture's type!");
		return -1;
		break;
	}

	GLint textureId;
	glGetIntegerv(textureBindingType, &textureId);

	return textureId;
}
//-------------------------------------------------------------------------------------------------
bool Texture::isSamePhysicalSpace(Texture* otherTexture) const
{
	assert(otherTexture);

	return (getType() == otherTexture->getType() &&
			getWidth() == otherTexture->getWidth() &&
			getHeight() == otherTexture->getHeight() &&
			getDepth() == otherTexture->getDepth());
		
}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE