#include "AbyssRenderTarget.h"
#include "AbyssWindow.h"
#include <stack>

ABYSS_BEGIN_NAMESPACE

class RenderTargetWindowListener :
	public WindowListener
{
public:

	RenderTargetWindowListener(RenderTarget* observingTarget)
		: m_renderTarget(observingTarget)
	{}

	virtual void onWindowResize(size_t width, size_t height)
	{
		m_renderTarget->update();
	}

private:
	RenderTarget* m_renderTarget;

};

static std::stack<RenderTarget*> s_BoundRenderTargetsStack;

//-------------------------------------------------------------------------------------------------
RenderTarget::RenderTarget(Window * window, float left /*= 0.f*/, float top /*= 0.f*/, float width /*= 1.f*/, float height /*= 1.f*/)
	: m_left(left)
	, m_top(top)
	, m_width(width)
	, m_height(height)
	, m_depthBuffer(nullptr)
	, m_needToReinit(false)
	, m_isClearEveryFrame(true)
	, m_attachedWindow(window)
	, m_clearColor(Vector4D(0, 0, 0, 1))
{
	glGenFramebuffers(1, &m_handle);

	getAttachedWindow()->addListener(new RenderTargetWindowListener(this));
}
//-------------------------------------------------------------------------------------------------
RenderTarget::~RenderTarget()
{
	glDeleteFramebuffers(1, &m_handle);
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::attachDepthBuffer(DepthBuffer::Pointer buffer)
{
	if (buffer == getDepthBuffer())
		return;

	m_depthBuffer = buffer;

	m_needToReinit = true;
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::deattachDepthBuffer()
{
	if (hasAttachedDepthBuffer())
	{
		m_depthBuffer.reset();
		m_needToReinit = true;
	}
}
//-------------------------------------------------------------------------------------------------
bool RenderTarget::hasAttachedDepthBuffer() const
{
	return m_depthBuffer != nullptr;
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, getHandle());

	if (m_needToReinit)
	{
		reinit();
		m_needToReinit = false;
	}
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::release()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
//------------------------------------------------------------------------------------------------
void RenderTarget::clear()
{
	glColorMask(true, true, true, true);
	glClearColor(getClearColor().r, getClearColor().g, getClearColor().b, getClearColor().a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
//--------------------------------------------------------------------------------------------------
void RenderTarget::setMetrics(float left, float top, float width, float height)
{
	m_left = left;
	m_top = top;
	m_width = width;
	m_height = height;

	m_needToReinit = true;
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::attachTexture(Attachment attachmentPoint, Texture::Pointer texture)
{
	if (!texture)
	{
		Texture::Pointer attachedTexture = getAttachedTexture(attachmentPoint);
		deattachTexture(attachedTexture);
		return;
	}

	verifyStatement(attachmentPoint != InvalidAttachment, "Attachment point for RenderBuffer must be valid!");

	texture->setSize(getActualWidth(), getActualHeight());
	texture->release();

	m_attachedTextures.insert(AttachedTexturesContainer::value_type(attachmentPoint, texture));

	m_needToReinit = true;
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::deattachTexture(Texture::Pointer texture)
{
	if (!isTextureAttached(texture))
	{
		showErrorMessage("Input texture isn't attached in specified RenderTarget!");
		return;
	}

	GLenum textureType = texture->getType();
	Attachment attachment = getTextureAttachment(texture);

	bind();
	glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture->getId(), 0);
	release();

	m_attachedTextures.erase(attachment);
}
//-------------------------------------------------------------------------------------------------
bool RenderTarget::isTextureAttached(Texture::Pointer texture) const
{
	return getTextureAttachment(texture) != InvalidAttachment;
}
//-------------------------------------------------------------------------------------------------
bool RenderTarget::hasAttachment(Attachment attachmentPoint) const
{
	// TODO: need to solve conflict with render buffer and texture at same attachment point.
	return getAttachedTexture(attachmentPoint) != nullptr;
}
//-------------------------------------------------------------------------------------------------
Texture::Pointer RenderTarget::getAttachedTexture(Attachment attachmentPoint) const
{
	auto foundIter = m_attachedTextures.find(attachmentPoint);

	return foundIter != m_attachedTextures.end() ? foundIter->second : nullptr;
}
//-------------------------------------------------------------------------------------------------
RenderTarget::Attachment RenderTarget::getTextureAttachment(Texture::Pointer texture) const
{
	for (const auto & iter : m_attachedTextures)
	{
		if (iter.second == texture)
			return iter.first;
	}

	return InvalidAttachment;
}
//-------------------------------------------------------------------------------------------------
bool RenderTarget::isValid(std::string * debugString) const
{
	// TODO: Ugly-hack. Yes, I know =(
	const_cast<RenderTarget*>(this)->bind();
	bool isOk = isValid_impl(debugString);
	release();
	return isOk;
}
//-------------------------------------------------------------------------------------------------
bool RenderTarget::isValid_impl(std::string * debugString /*= nullptr*/) const
{
	bool isOk = true;

	GLenum fbStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (fbStatus != GL_FRAMEBUFFER_COMPLETE)
		isOk = false;

	if (debugString)
	{
		switch (fbStatus)
		{
		case GL_FRAMEBUFFER_UNDEFINED:
			*debugString = "Target is the default framebuffer, but he default framebuffer doesn't exist!";
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			*debugString = "Some framebuffer attachment are incompleted!";
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			*debugString = "FrameBuffer doesn't have at least one image attached to it.";
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			*debugString = "The value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for any color attachment point(s) named by GL_DRAW_BUFFER!";
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			*debugString = "GL_READ_BUFFER is not GL_NONE and the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for the color attachment point named by GL_READ_BUFFER!";
			break;
		case GL_FRAMEBUFFER_UNSUPPORTED:
			*debugString = "The combinations of internal formats of the attached images violates implementation-dependent set of restrictions!";
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
			*debugString = "The value of GL_RENDERBUFFER_SAMPLES is not the same for all attached renderbuffers; OR  the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not the same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not GL_TRUE for all attached textures";
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
			*debugString = "The any of FrameBuffer attachment is layered, and any populated attachment is not layered, or if all populated color attachments are not from textures of the same target!";
			break;
		default:
			*debugString = "All is OK!";
		}
	}

	return isOk;
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::notifyStartRendering()
{
	bind();

	glViewport(getActualLeftOffset(), getActualTopOffset(), getActualWidth(), getActualHeight());

	if (isClearEveryFrame())
		clear();

	s_BoundRenderTargetsStack.push(this);
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::notifyFinishRendering()
{
	release();

	s_BoundRenderTargetsStack.pop();

	if (s_BoundRenderTargetsStack.empty())
	{
		auto size = Window::getCurrentSize();
		glViewport(0, 0, size.x, size.y);
	}
	else
	{
		auto renderTarget = s_BoundRenderTargetsStack.top();
		s_BoundRenderTargetsStack.pop();
		renderTarget->bind();
	}
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::setDrawBuffers(const AttachmentsContainerType & buffersToDraw)
{
	if (getDrawBuffers() == buffersToDraw)
		return;

	m_drawBuffers = buffersToDraw;

	m_needToReinit = true;
}
//-------------------------------------------------------------------------------------------------
size_t RenderTarget::getActualLeftOffset() const
{
	return getAttachedWindow()->getCurrentSize().x * m_left;
}
//-------------------------------------------------------------------------------------------------
size_t RenderTarget::getActualTopOffset() const
{
	return getAttachedWindow()->getCurrentSize().y * m_top;
}
//-------------------------------------------------------------------------------------------------
size_t RenderTarget::getActualWidth() const
{
	return getAttachedWindow()->getCurrentSize().x * m_width;
}
//-------------------------------------------------------------------------------------------------
size_t RenderTarget::getActualHeight() const
{
	return getAttachedWindow()->getCurrentSize().y * m_height;
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::update()
{
	m_needToReinit = true;
}

void RenderTarget::reinit()
{
	GLuint depthBufferHandle = getDepthBuffer() ? getDepthBuffer()->getHandle() : 0;

	if (getDepthBuffer())
		getDepthBuffer()->setSize(getActualWidth(), getActualHeight());

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBufferHandle);

	// TODO: strange... maybe should be deleted...
	if (!m_attachedTextures.empty())
	{
		for (auto iter = m_attachedTextures.cbegin(); iter != m_attachedTextures.cend(); ++iter)
		{
			Attachment attachmentPoint = iter->first;
			TexturePtr texture = iter->second;

			texture->setSize(getActualWidth(), getActualHeight());
			glFramebufferTexture2D(GL_FRAMEBUFFER, attachmentPoint, GL_TEXTURE_2D, texture->getId(), 0);
		}

		if (m_drawBuffers.empty())
		{
			GLenum attachmentPoint = m_attachedTextures.begin()->first;
			glDrawBuffers(1, &attachmentPoint);
		}
		else
		{
			GLenum* attachments = new GLenum[m_drawBuffers.size()];
			for (size_t i = 0; i < m_drawBuffers.size(); ++i)
				attachments[i] = m_drawBuffers[i];

			glDrawBuffers(m_drawBuffers.size(), attachments);

			delete[] attachments;
		}
	}

	String fbStatus;
	bool isOk = isValid_impl(&fbStatus);
	verifyStatement(isOk, fbStatus);

}
//-------------------------------------------------------------------------------------------------
bool RenderTarget::blitTo(RenderTarget* renderTarget, bool withDepthBuffer)
{
	auto previousError = glGetError();

	glBindFramebuffer(GL_READ_FRAMEBUFFER, getHandle());
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, renderTarget->getHandle());

	auto thisRT_Left = this->getActualLeftOffset();
	auto thisRT_Top = this->getActualTopOffset();
	auto thisRT_Right = thisRT_Left + this->getActualWidth();
	auto thisRT_Bottom = thisRT_Top + this->getActualHeight();

	auto otherRT_Left = renderTarget->getActualLeftOffset();
	auto otherRT_Top = renderTarget->getActualTopOffset();
	auto otherRT_Right = otherRT_Left + renderTarget->getActualWidth();
	auto otherRT_Bottom = otherRT_Top + renderTarget->getActualHeight();

	// Perform blitting:
	GLbitfield bitmask = GL_COLOR_BUFFER_BIT;
	if (withDepthBuffer && hasAttachedDepthBuffer())
	{
		bitmask |= GL_DEPTH_BUFFER_BIT;
	}

	glBlitFramebuffer(thisRT_Left, thisRT_Top, thisRT_Right, thisRT_Bottom,
				      otherRT_Left, otherRT_Top, otherRT_Right, otherRT_Bottom, 
					  bitmask, GL_LINEAR);

	auto newError = glGetError();
	if (newError != GL_NO_ERROR && newError != previousError)
	{
		std::string strErrorDescription;
		switch (newError)
		{
		case GL_INVALID_OPERATION:
			strErrorDescription = "INVALID_OPERATION!";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			strErrorDescription = "INVALID_FRAMEBUFFER_OPERATION";
			break;
		default:
			strErrorDescription = "UNEXPECTED ERROR";
		}
		ABYSS_LOG("ERROR: RenderTarget::blitTo() is finished with errors: " + strErrorDescription);

		assert(false);
	}

	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	return newError == GL_NO_ERROR;
}
//-------------------------------------------------------------------------------------------------
void RenderTarget::setClearColor(const Vector4D& clearColor)
{
	m_clearColor = clearColor;
}
//-------------------------------------------------------------------------------------------------
unsigned char* RenderTarget::readPixels() const
{
	if (!verifyStatement(hasAttachment(ColorAttachment0), "Cannot readPixels for RenderTarget - it doesn't have first color attachment!"))
		return nullptr;

	TexturePtr attachedTexture = getAttachedTexture(ColorAttachment0);

	return readPixels(attachedTexture->getPixelFormat(), attachedTexture->getInternalFormat());
}
//-------------------------------------------------------------------------------------------------
unsigned char* RenderTarget::readPixels(PixelFormat pixelFormat, GLenum internalFormat) const
{
	return readPixels(0, 0, getActualWidth(), getActualHeight(), pixelFormat, internalFormat);
}
//-------------------------------------------------------------------------------------------------
unsigned char* RenderTarget::readPixels(int x, int y, int width, int height, PixelFormat pixelFormat, GLenum internalFormat) const
{
	size_t totalSize = width * height;
	size_t componentsSize = 0;
	switch (internalFormat) 
	{
		case GL_BGR:
		case GL_RGB:
			componentsSize = 3; 
		break;

		case GL_BGRA:
		case GL_RGBA:
			componentsSize = 4; 
		break;

		case GL_ALPHA:
		case GL_LUMINANCE:
			componentsSize = 1; 
		break;
	}

	unsigned char* data = (unsigned char*)calloc(componentsSize * totalSize, sizeof(unsigned char));

	auto mutableThis = const_cast<RenderTarget*>(this);
	mutableThis->bind();

	GLenum glPixelFormat = convertPixelFormatToGL(pixelFormat);
	glReadPixels(0, 0, getActualWidth(), getActualHeight(), internalFormat, glPixelFormat, data);

	mutableThis->release();

	return data;
}
//-------------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE