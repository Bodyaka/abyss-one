#include "AbyssBoundingBox.h"

#include "AbyssVertexArrayObject.h"
#include "AbyssVertexBufferObject.h"
#include "AbyssDefaultVertexAttributes.h"

ABYSS_BEGIN_NAMESPACE
//--------------------------------------------------------------------------------------------------
BoundingBox::BoundingBox()
	: GeometryObject(GL_LINE_STRIP)
{
	initBoundingBox();
}
//--------------------------------------------------------------------------------------------------
BoundingBox::BoundingBox(const AABB & box)
	: GeometryObject(GL_LINE_STRIP)
{
	initBoundingBox();

	setAABB(box);
}
//--------------------------------------------------------------------------------------------------
BoundingBox::~BoundingBox()
{

}
//--------------------------------------------------------------------------------------------------
void BoundingBox::setAABB(const AABB & box)
{
	m_aabb = box;

	// Get data from aabb:
	ContainerV3D position = createPositionsContainer();
	ContainerUint indices = createIndicesContainer();

	// Setup indices count:
	m_indicesCount = indices.size();
	m_verticesCount = position.size();
	
	setupVertexData(position, indices);
}
//--------------------------------------------------------------------------------------------------
ContainerV3D BoundingBox::createPositionsContainer() const
{
	ContainerV3D positions;

	// Vertices' positions creation. 
	positions.push_back(m_aabb.leftBottomNear());
	positions.push_back(m_aabb.rightBottomNear());
	positions.push_back(m_aabb.rightTopNear());
	positions.push_back(m_aabb.leftTopNear());

	positions.push_back(m_aabb.rightBottomFar());
	positions.push_back(m_aabb.leftBottomFar());
	positions.push_back(m_aabb.leftTopFar());
	positions.push_back(m_aabb.rightTopFar());

	return std::move(positions);
}
//--------------------------------------------------------------------------------------------------
ContainerUint BoundingBox::createIndicesContainer() const
{
	ContainerUint indices;

	// first loop (front face)
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(3);
	indices.push_back(0);

	// next loop (left face):
	indices.push_back(3);
	indices.push_back(6);
	indices.push_back(5);
	indices.push_back(0);
	indices.push_back(5);

	// next loop (back face):
	indices.push_back(6);
	indices.push_back(7);
	indices.push_back(4);
	indices.push_back(5);
	indices.push_back(4);

	// next loop (right face):
	indices.push_back(7);
	indices.push_back(2);
	indices.push_back(1);
	indices.push_back(4);

	return std::move(indices);
}
//--------------------------------------------------------------------------------------------------
void BoundingBox::setupVertexData(const ContainerV3D & positions, const ContainerUint & indices)
{
	m_vao = VertexArrayObject::createVAO();

	// Binding VAO for sure:
	m_vao->bind();

	// Setup position buffer data:
	getVertexBufferObject(PositionAttribute)->bufferData(sizeof(Vector3D) * positions.size(), (void *)positions.data(), GL_STATIC_DRAW);

	// Setup indices buffer data:
	getIndicesBufferObject()->bufferData(sizeof(GLuint) * indices.size(), (void *)indices.data(), GL_STATIC_DRAW);
	
	// Releasing attribute:
	m_vao->release();

	// TODO

	// Enable all attribute locations. TODO. In future need be removed:
	for (int attribute = PositionAttribute; attribute <= BiTangentAttribute; ++attribute)
		setupAttribute(attribute, attribute);
}
//--------------------------------------------------------------------------------------------------
void BoundingBox::initBoundingBox()
{
	// Creating vertex positions VBO:
	addBufferObject(PositionAttribute, VertexBufferObject::createBufferObject(GL_ARRAY_BUFFER));
	// Create index buffer object:
	setIndicesBufferObject(VertexBufferObject::createBufferObject(GL_ELEMENT_ARRAY_BUFFER));
}
//--------------------------------------------------------------------------------------------------
void BoundingBox::draw()
{
	drawElements();
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
