#pragma once

#include "AbyssGL.h"

ABYSS_BEGIN_NAMESPACE

enum PixelFormat
{
	PF_UNKNOWN = 0, // Wrong pixel format.
	PF_UBYTE,
	PF_BYTE, 
	PF_USHORT,
	PF_SHORT,
	PF_UINT, 
	PF_INT, 
	PF_FLOAT,
	PF_UBYTE_R3G3B2,
	PF_USHORT_R5G6B5,
	PF_USHORT_R4G4B4A4,
	PF_USHORT_R5G5B5A1,
	PF_UINT_R8G8B8A8, 
	PF_UINT_R10G10B10A2
};

// Helper methods:
GLenum		convertPixelFormatToGL(PixelFormat pixelFormat);
PixelFormat convertPixelFormatFromGL(GLenum glFormat);



ABYSS_END_NAMESPACE