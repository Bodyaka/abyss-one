#pragma once

#include "AbyssRenderer.h"

ABYSS_BEGIN_NAMESPACE

class GBuffer;
class RenderTarget;

class DeferredRenderer
	: public Renderer
{

public:
	
	static DeferredRenderer * getInstance();

	bool isSupports(const RenderableObject * const renderable) const override;
	virtual void startRendering(RenderingResultPtr input) override;
	virtual void finishRendering(RenderingResultPtr output) override;

private:

	DeferredRenderer();
	~DeferredRenderer();

	void initFullScreenRect(SceneManager* manager);
	void initGBuffer(SceneManager* manager);
	void initAttributeLocations();
	void setupNearAndFarPlanes();

	virtual void doInit(SceneManager* manager) override;
	
	void beforeRender(RenderableObject * const object) override;
	void afterRender(RenderableObject * const object) override;

	virtual void registerExtensions() override;

private: // Members section:

	std::unique_ptr<RenderTarget> m_gBufferRenderTarget;

	GBufferPtr m_gBuffer;

};

ABYSS_END_NAMESPACE