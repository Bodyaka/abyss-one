#include "AbyssLogging.h"
#include <iostream>
#include <iomanip>
#include <chrono>
#include <ctime>

#include <fstream>

namespace Abyss
{
	void printToLog(const std::string& message)
	{
		/*auto now = std::chrono::system_clock::now();
		auto nowTime_t = std::chrono::system_clock::to_time_t(now);*/

		std::cout << "[ABYSS]: " << message << "\n";
	}

	void printToLogFile(const std::string& message)
	{
		std::ofstream logStream("Abyss.log", std::ios::out | std::ios::app);
		
		if (!logStream.is_open())
			logStream.open("Abyss.log", std::ios::out | std::ios::app);

		logStream << message;
		logStream.close();
	}

	void clearLogFile()
	{
		std::ifstream logStream("Abyss.log", std::ios::out | std::ios::trunc);
	}

}
