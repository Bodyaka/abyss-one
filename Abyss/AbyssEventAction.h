#pragma once

#include "Abyss.h"

ABYSS_BEGIN_NAMESPACE

class EventAction
{
public:
	EventAction(){};
	virtual ~EventAction() {};

private:
	EventAction(const EventAction &);
	EventAction & operator = (const EventAction &);

};

ABYSS_END_NAMESPACE