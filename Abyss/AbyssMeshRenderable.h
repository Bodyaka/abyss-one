#pragma once

#include "AbyssRenderableObject.h"

ABYSS_BEGIN_NAMESPACE

class MeshRenderable
	: public RenderableObject
{
public:

	// Creates mesh renderable object with material from mesh.
	static MeshRenderablePtr create(const String & name, MeshPtr mesh);
	// Creates mesh renderable object pointer with specified data.
	static MeshRenderablePtr create(const String & name, MeshPtr mesh, MaterialPtr material);

	// Creates mesh renderable object with specified name.
	MeshRenderable(const String & name);
	~MeshRenderable();
	
	// SETTERS
	// Change mesh for this one rednerable object.
	void setMesh(MeshPtr mesh);

	// GETTERS
	MeshPtr getMesh() const;
	virtual GeometryObjectPtr getGeometryObject() const override;

protected:
	virtual AABB getAABB() const override;
	virtual void performRendering(RendererVisitor* visitor) override;

private: // Methods

	// Main data:
	MeshPtr m_mesh;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline MeshPtr MeshRenderable::getMesh() const
{	return m_mesh;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE