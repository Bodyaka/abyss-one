#pragma once

#include "Abyss.h"
#include "AbyssEventHandler.h"
#include "AbyssRendering.h"
#include "AbyssSceneObject.h"

ABYSS_BEGIN_NAMESPACE

class Scheduler;

class TrackBallRotationController
	: public EventHandler
{
public:
	TrackBallRotationController();
	TrackBallRotationController(const String& sceneObjectName);
	~TrackBallRotationController();

	void setSceneObjectName(const String& sceneObjectName);
	const String& getSceneObjectName() const;


protected:

	virtual void mousePressEvent(const MouseEventPtr & event) override;
	virtual void mouseMoveEvent(const MouseEventPtr & event) override;
	virtual void mouseReleaseEvent(const MouseEventPtr & event) override;
	SceneObject* getSceneObject() const;

	Vector2D getObjectCenterInNDCSpace() const;

	Vector2D convertToNDCSpace(const Vector2D& mousePos) const;
	Vector3D convertToObjectVirtualSphereSpace(const Vector2D& ndcPos) const;

	void computeRotationParameters(const Vector2D& prevMousePosition, 
								   const Vector2D& currMousePosition, 
								   Vector3D& rotationAxis, 
								   float& rotationAngle) const;


	Vector3D getObjectRotationCenter() const;
	void initScheduler();

	String m_sceneObjectName;

	bool m_isActive;
	float m_radius;

	Vector2D m_currentPosition,
			 m_prevPosition;

	Vector3D m_objectPosInWorldSpace;

	float m_inersionRotationAngle;
	float m_inersionRotationDelta;
	Vector3D m_inersionRotationAxis;
	std::unique_ptr<Scheduler> m_inersionScheduler;
};


// INLINE METHODS
inline const String& TrackBallRotationController::getSceneObjectName() const
{	return m_sceneObjectName;	}


ABYSS_END_NAMESPACE