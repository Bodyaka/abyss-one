#include "AbyssSceneManager.h"

#include "boost/lexical_cast.hpp"

#include "AbyssCamera.h"
#include "AbyssRenderer.h"
#include "AbyssSceneObject.h"
#include "AbyssApplication.h"
#include "AbyssRenderPass.h"
#include "AbyssSceneManagerListener.h"
#include "AbyssLight.h"
#include "AbyssLightsManager.h"

#include "AbyssGBuffer.h"
#include "AbyssWindow.h"
#include "AbyssApplicationContext.h"

#include "AbyssRenderQueue.h"
#include "AbyssRenderPassManager.h"

// For post-processing:
#include "AbyssMesh.h"
#include "AbyssMeshRenderable.h"
#include "AbyssTexture.h"

#include "AbyssSSAORenderer.h"
#include "AbyssSSAOBlurRenderer.h"

ABYSS_BEGIN_NAMESPACE

size_t SceneManager::m_nameGenerator = 0;
static const std::string s_fullScreenRectName = "_ABYSS_SCENE_MANAGER_FULL_SCREEN_RECT";
static const std::string s_fullScreenRectRenderableName = s_fullScreenRectName + "_RENDERABLE";
static const std::string s_fullScreenRectSceneObjectName = s_fullScreenRectName + "_SCNE_OBJECT";

//----------------------------------------------------------------------------------------------
SceneManager::SceneManager()
	: m_isSSAOEnabled(true)
{
	
}
//----------------------------------------------------------------------------------------------
SceneManager::~SceneManager()
{
	for (auto listener : m_listeners)
		listener->setSceneManager(nullptr);
}
//----------------------------------------------------------------------------------------------
void SceneManager::update()
{
	auto iter = m_sceneObjects.begin();
	while (iter != m_sceneObjects.end())
	{
		iter->second->updateTransform();
		++iter;
	}

	// Update lights:
	auto lights = LightsManager::getInstance()->getLights();
	for (auto light : lights)
		light->updateTransform();
}
//----------------------------------------------------------------------------------------------
void SceneManager::render()
{
	onStartRendering();
	
	m_renderQueue->performRendering();

	onFinishRendering();
}
//----------------------------------------------------------------------------------------------
SceneObjectPtr SceneManager::createSceneObject()
{
	String newName;
	
	String sceneObjectName = findFreeSceneObjectName();

	return createSceneObject(sceneObjectName);
}
//----------------------------------------------------------------------------------------------
SceneObjectPtr SceneManager::createSceneObject(const String & name)
{
	SceneObjectPtr object(new SceneObject(name, this));
	
	m_sceneObjects.insert(SceneObjectsContainer::value_type(name, object));

	return object;
}
//----------------------------------------------------------------------------------------------
bool SceneManager::removeSceneObject(const String & name)
{
	auto iter = m_sceneObjects.find(name);

	if (iter == m_sceneObjects.end())
		return false;

	m_sceneObjects.erase(iter);

	return true;
}
//----------------------------------------------------------------------------------------------
bool SceneManager::removeSceneObject(SceneObjectPtr object)
{
	if (!object)
		return false;

	String name = object->getName();
	
	return removeSceneObject(name);
}
//----------------------------------------------------------------------------------------------
void SceneManager::removeAllSceneObjects()
{
	m_sceneObjects.clear();
}
//----------------------------------------------------------------------------------------------
bool SceneManager::hasSceneObject(SceneObjectPtr object) const
{
	return hasSceneObject(object->getName());
}
//----------------------------------------------------------------------------------------------
bool SceneManager::hasSceneObject(const String & name) const
{
	auto iter = m_sceneObjects.find(name);

	return iter != m_sceneObjects.end();
}
//----------------------------------------------------------------------------------------------
void SceneManager::setCamera(CameraPtr camera)
{
	m_camera = camera;
}
//----------------------------------------------------------------------------------------------
void SceneManager::onStartRendering()
{
	// When camera is changed, we need update all scene objects.
	if (getCamera()->isChanged())
		update();

	if (isChanged())
		updateAxisAlignedBoundingBox();

	for (auto listener : m_listeners)
		listener->onRenderingStarted(this);

	prepareSceneObjetsForRendering();
}
//----------------------------------------------------------------------------------------------
void SceneManager::onFinishRendering()
{
	for (auto iter : m_sceneObjects)
		iter.second->onFinishRendering();

	setChanged(false);
	getCamera()->setChanged(false);

	for (auto listener : m_listeners)
		listener->onRenderingFinished(this);
}
//----------------------------------------------------------------------------------------------
const AABB & SceneManager::getAxisAlignedBoundingBox(bool recompute /* = false */) const
{
	if (recompute)
		const_cast<SceneManager*>(this)->updateAxisAlignedBoundingBox();

	return m_aabb;
}
//----------------------------------------------------------------------------------------------
void SceneManager::updateAxisAlignedBoundingBox()
{
	m_aabb = AABB();

	if (size_t size = getSceneObjectsSize())
	{
		bool isFirstObject = true;
		for (auto iter : m_sceneObjects)
		{
			SceneObjectPtr object = iter.second;
			const AABB & aabb = object->getWorldAABB();
			
			if (isFirstObject)
			{
				m_aabb = aabb;
				isFirstObject = false;
			}
			else
				m_aabb.unit(aabb);
		}
	}
}
//----------------------------------------------------------------------------------------------
String SceneManager::findFreeSceneObjectName() const
{
	String newName;

	do 
	{
		String id = boost::lexical_cast<String>(m_nameGenerator++);
		newName = "SceneObject_" + id;
		if (!hasSceneObject(newName))
			break;

	} while (true);

	return newName;
}
//----------------------------------------------------------------------------------------------
SceneManager * SceneManager::instance()
{
	return Application::getCurrentApplication()->getSceneManager();
}
//----------------------------------------------------------------------------------------------
void SceneManager::addListener(SceneManagerListener* listener)
{
	m_listeners.push_back(listener);
}
//----------------------------------------------------------------------------------------------
void SceneManager::removeListener(SceneManagerListener* listenerToRemove)
{
	auto matchListenerFunc = [=](SceneManagerListener * listener) -> bool
	{		
		return listener == listenerToRemove;	
	};

	std::remove_if(m_listeners.begin(), m_listeners.end(), matchListenerFunc);
}
//----------------------------------------------------------------------------------------------
SceneObjectPtr SceneManager::getSceneObject(const String & name) const
{
	auto iter = m_sceneObjects.find(name);
	if (iter == m_sceneObjects.end())
		return nullptr;

	return iter->second;
}
//----------------------------------------------------------------------------------------------
void SceneManager::init()
{
	// TODO: need redefine frustum on resize.
	auto windowSize = getWindow()->getSize();

	ViewingFrustum::Params params = { 45.f, float(windowSize.x) / windowSize.y, 1.f, 500.f };
	Camera * camera = new Camera(Vector3D(0.f, 0.f, 10.f), Vector3D(0.f, 0.f, 0.f), Vector3D(0.f, 1.0f, 0.f), params);
	camera->stickToWindow(getWindow());
	setCamera(CameraPtr(camera));
	
	// Init Rendering system:
	m_renderPassManager.reset(new RenderPassManager(this));
	m_renderQueue.reset(new RenderQueue(this));

	m_ssaoTexture.reset(new Texture());

	// Rect is dependent on initRenderPasses method!
	createFullScreenRectObject();
	Renderer::initAllRenderers(this);
}

Window* SceneManager::getWindow() const
{
	return ApplicationContext::getSharedApplicationContext()->getWindow();
}
//----------------------------------------------------------------------------------------------
void SceneManager::prepareSceneObjetsForRendering()
{
	m_renderQueue->clear();

	for (auto iter : m_sceneObjects)
	{
		auto sceneObject = iter.second.get();
		m_renderQueue->prepareForRendering(sceneObject);
	}
}
//----------------------------------------------------------------------------------------------
RenderableObjectPtr SceneManager::getFullScreenRectRenderable() const
{
	if (auto sceneObject = getSceneObject(s_fullScreenRectSceneObjectName))
		return sceneObject->getRenderableOject(s_fullScreenRectRenderableName);

	return RenderableObjectPtr();
}
//----------------------------------------------------------------------------------------------
void SceneManager::createFullScreenRectObject()
{
	// Setup post-processing stuff:
	auto fullScreenRect = Mesh::createFullScreenRect();
	auto meshRenderable = MeshRenderable::create(s_fullScreenRectRenderableName, fullScreenRect);
	meshRenderable->removeAllRenderPasses();
	auto sceneObject = createSceneObject(s_fullScreenRectSceneObjectName);
	sceneObject->addRenderableObject(meshRenderable);
}
//----------------------------------------------------------------------------------------------
bool SceneManager::isSSAOEnabled() const
{
	return m_isSSAOEnabled;
}
//----------------------------------------------------------------------------------------------
void SceneManager::setSSAOEnabled(bool isEnabled)
{
	if (isSSAOEnabled() == isEnabled)
		return;

	m_isSSAOEnabled = isEnabled;

	auto ssaoRenderer = isSSAOEnabled() ? SSAORenderer::getInstance() : nullptr;
	auto ssaoBlurRenderer = isSSAOEnabled() ? SSAOBlurRenderer::getInstance() : nullptr;

	getRenderPassManager()->getRenderPass(DefaultRenderPasses::SSAO)->setRenderer(ssaoRenderer);
	getRenderPassManager()->getRenderPass(DefaultRenderPasses::SSAO_BLUR)->setRenderer(ssaoBlurRenderer);
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
