#pragma once

#include "AbyssMath.h"

#include <utility>
#include <vector>

ABYSS_BEGIN_NAMESPACE

class Plane;

class AABB
{
public:
	AABB(){};
	inline AABB(const Vector3D & i_rightBottomFar, const Vector3D & i_leftTopNear);

	/// Far section
	Vector3D rightBottomFar() const;
	Vector3D rightTopFar() const;
	Vector3D leftTopFar() const;
	Vector3D leftBottomFar() const;

	Vector3D leftTopNear() const;
	Vector3D leftBottomNear() const;
	Vector3D rightBottomNear() const;
	Vector3D rightTopNear() const;

	std::vector<AABB> split(size_t w, size_t h, size_t d) const;

	inline void setBegin(const Vector3D & begin);
	inline void setEnd(const Vector3D & end);

	// (i.e. beginning point with least x, y, z values). 
	inline Vector3D begin() const;
	// (i.e. ending point with least x, y, z values). 
	inline Vector3D end() const;

	inline Vector3D center() const;
	// Return diagonal from center to TopMost point.
	inline Vector3D diagonal() const;

	/// SIZES:
	inline float width() const;
	inline float height() const;
	inline float depth() const;
	// POSITIONS:
	/// Getting min/max x coordinate of AABB.
	inline float minX() const;
	inline float maxX() const;

	/// Getting min/max y coordinate of AABB.
	inline float minY() const;
	inline float maxY() const;

	/// Getting min/max z coordinate of AABB.
	inline float minZ() const;
	inline float maxZ() const;

	/// Return vectorbbox (8 points):
	ContainerV3D points() const;

	/// drawing wireframe bbox
	void draw();

	/// Unit result with specified AABB.
	void unit(const AABB & other);
	/// Extend this one bbox on specified size:
	void extend(const Vector3D & extendSize);


	// INQUIRY

	/// EIntersectResult
	// Main intersection:
	IntersectionResult intersect(const Plane & plane) const;
	IntersectionResult intersect(const AABB & box) const;

	/// Return true, if this one AABB contains specified point:
	bool contains(const Vector3D & point) const;
	/// Return true, if this one AABB contains specified aabb.
	bool contains(const AABB & aabb) const;

	/// Return modified ABBB that multiplied by given matrix.
	AABB transform(const Matrix4D & matrix) const;

	/// Return modified VectorBB that multiplied by give matrix:
	ContainerV3D transformed(const Matrix4D & matrix) const;

	// Generate AABB by specified vertices container:
	static AABB generateAxisAlignedBoundingBox(const ContainerV3D & vertices);
	// Generate AABB by specified raw vertices array (array of 3 float components):
	static AABB generateAxisAlignedBoundingBox(float * positions, size_t verticesSize);

private:
	typedef std::pair<Vector3D, Vector3D> TPair;
	// Box itself: contains from begin point and end point:
	TPair m_bbox;
};

//--------------------------------------------------------------------------------------------------
AABB::AABB(const Vector3D & begin, const Vector3D & end)
	: m_bbox(TPair(begin, end))
{

}
//--------------------------------------------------------------------------------------------------
void AABB::setBegin(const Vector3D & begin)
{	m_bbox.first = begin;	}
//--------------------------------------------------------------------------------------------------
void AABB::setEnd(const Vector3D & end)
{	m_bbox.second = end;	}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::begin() const
{	return m_bbox.first;	}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::end() const
{	return m_bbox.second;	}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::center() const
{	return (m_bbox.first + m_bbox.second) / Vector3D(2.f); }
//--------------------------------------------------------------------------------------------------
float AABB::minX() const
{	return begin().x;	}
//--------------------------------------------------------------------------------------------------
float AABB::maxX() const
{	return end().x;	}
//--------------------------------------------------------------------------------------------------
float AABB::minY() const
{	return begin().y;	}
//--------------------------------------------------------------------------------------------------
float AABB::maxY() const
{	return end().y;	}
//--------------------------------------------------------------------------------------------------
float AABB::minZ() const
{	return begin().z;	}
//--------------------------------------------------------------------------------------------------
float AABB::maxZ() const
{	return end().z;	}
//--------------------------------------------------------------------------------------------------
float AABB::width() const
{	return maxX() - minX();	}
//--------------------------------------------------------------------------------------------------
float AABB::height() const
{	return maxY() - minY();	}
//--------------------------------------------------------------------------------------------------
float AABB::depth() const
{	return maxZ() - minZ();	}
//--------------------------------------------------------------------------------------------------
Vector3D AABB::diagonal() const
{	return end() - begin() / 2.f;	}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
