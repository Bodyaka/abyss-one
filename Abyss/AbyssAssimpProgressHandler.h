#pragma once

#include "Abyss.h"
#include <assimp/ProgressHandler.hpp>

ABYSS_BEGIN_NAMESPACE

class AssimpProgressHandler
	: public Assimp::ProgressHandler
{
public:
	AssimpProgressHandler();

	virtual bool Update(float percentage /* = -1.f */) override;
};

ABYSS_END_NAMESPACE