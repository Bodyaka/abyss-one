#pragma once

#include <map>
#include "AbyssRendering.h"
#include "AbyssAABB.h"

ABYSS_BEGIN_NAMESPACE

class RendererVisitor;

class GeometryObject
{
public:

	typedef std::map<int, VertexBufferObjectPtr> TAttributeBuffersContainer;

public:
	// Draw mode. Ho-ho. It is something like triangles, lines, etc.
	GeometryObject(GLenum drawMode);
	~GeometryObject();

	// Render Interface.
	virtual void draw();

	// Add buffer object to specified attribute:
	void addBufferObject(int attribute, VertexBufferObjectPtr buffer);

	// Remove given buffer object (with specified attribute).
	void removeBufferObject(int attribute);

	// Change draw mode of geometry object:
	void setDrawMode(GLenum drawMode);

	// Setup default attribute.
	// Note, that this one method will bind VAO, so the previous one object will be released.
	bool setupAttribute(int attribute, int location);

	// Set index buffer for renderable object:
	void setIndicesBufferObject(VertexBufferObjectPtr indicesBufferObject);

	// ACCESS:
	// Return number of vertices of given geometry object:
	size_t getVerticesCount() const;
	// Return number of indices of given geometry object:
	size_t getIndicesCount() const;

	// Return attributes and their buffers:
	TAttributeBuffersContainer getAttributeBuffersContainer() const;
	// Return buffer at given attribute:
	VertexBufferObjectPtr getVertexBufferObject(int attribute) const;

	// Return indices buffer object:
	VertexBufferObjectPtr getIndicesBufferObject() const;

	// Return VAO:
	VertexArrayObjectPtr getVertexArrayObject() const;

	// Return draw mode of geometry object:
	GLenum getDrawMode() const;
	virtual const AABB & getAABB() const;
	
	// INQUIRY:
	// Check, whether object has given attribute:
	bool hasAttribute(int attribute) const;

protected: // methods:
	// Draw arrays as monolith data.
	void drawArrays();
	// Draw elements with specified draw mode.
	void drawElements();

protected: // members.

	// Draw mode of abyss mesh.
	GLenum m_drawMode;
	// Vertex array object.
	VertexArrayObjectPtr m_vao;
	
	size_t m_verticesCount,
		   m_indicesCount;

	AABB m_aabb;

private:

	// Indices buffer:
	VertexBufferObjectPtr m_indicesBufferObject;
	// Vertex attributes VBO container:
	TAttributeBuffersContainer m_attributeBuffers;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline GeometryObject::TAttributeBuffersContainer GeometryObject::getAttributeBuffersContainer() const
{	return m_attributeBuffers;	}
//----------------------------------------------------------------------------------------------
inline VertexArrayObjectPtr GeometryObject::getVertexArrayObject() const
{	return m_vao;	}
//----------------------------------------------------------------------------------------------
inline VertexBufferObjectPtr GeometryObject::getIndicesBufferObject() const
{   return m_indicesBufferObject;	}
//----------------------------------------------------------------------------------------------
inline void GeometryObject::setDrawMode(GLenum drawMode)
{   m_drawMode = drawMode;	}
//----------------------------------------------------------------------------------------------
inline GLenum GeometryObject::getDrawMode() const
{   return m_drawMode;	}
//----------------------------------------------------------------------------------------------
inline size_t GeometryObject::getVerticesCount() const
{   return m_verticesCount;	}
//----------------------------------------------------------------------------------------------
inline size_t GeometryObject::getIndicesCount() const
{   return m_indicesCount;	}
//----------------------------------------------------------------------------------------------
ABYSS_END_NAMESPACE