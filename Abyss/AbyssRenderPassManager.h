#pragma once

#include <list>
#include <vector>
#include "AbyssRenderPass.h"
#include "AbyssSceneManagerListener.h"

ABYSS_BEGIN_NAMESPACE

class SceneManager;

// Represents default render passes, which cannot be removed from the system, but, can be changed (re-grouped). 
namespace DefaultRenderPasses
{
	static const std::string DEFERRED_RENDERING = "ABYSS_DEFERRED_RENDERING";
	static const std::string SSAO = "ABYSS_SSAO";
	static const std::string SSAO_BLUR = "ABYSS_SSAO_BLUR";
	static const std::string DEFERRED_SHADING = "ABYSS_DEFERRED_SHADING";
	static const std::string FORWARD_RENDERING = "ABYSS_FORWARD_RENDERING";
	static const std::string PHONG_SHADING = "ABYSS_PHONG_SHADING";
	static const std::string DEBUG_RENDERING = "ABYSS_DEBUG_RENDERING";
	static const std::string GAUSSIAN_BLUR_RENDER_PASS = "ABYSS_GAUSSIAN_BLUR";
	static const std::string FINAL_PASS = "ABYSS_FINAL_PASS";
}

class RenderPassManager final
	: public SceneManagerListener
{
public:
	 
	using RenderPassContainerType = std::vector<RenderPassWeakPtr>;
public:
	RenderPassManager(SceneManager* manager);
	~RenderPassManager();

	// Adds RenderPass before specified renderPassBefore, which should be setup in system.
	// If renderPass is defined in the system, false will be returned.
	bool addRenderPass(RenderPassWeakPtr renderPassBefore, RenderPassSharedPtr renderPass);
	bool addRenderPass(const std::string& renderPassBefore, RenderPassSharedPtr renderPass);

	bool addRenderPassToBegin(RenderPassSharedPtr renderPass);
	bool addRenderPassToEnd(RenderPassSharedPtr renderPass);

	// Removes RenderPass with given name. Removing RenderPass, which defined in DefaultRenderPasses namespace will generate exception.
	bool removeRenderPass(const std::string& renderPassToRemove);

	bool hasRenderPass(const std::string& renderPassName) const;
	RenderPassWeakPtr getRenderPass(const std::string& renderPassName) const;

	// Get all RenderPasses:
	const RenderPassContainerType& getAllRenderPasses() const;

	// Return index of RenderPass in manager's list.
	// Will return -1 if the result is incorrect!
	int getIndexOf(RenderPassWeakPtr renderPassPtr) const;
	int getIndexOf(const std::string& renderPassName) const;
	
public: // overridden:
	virtual void onRenderingStarted(SceneManager* manager) override;
	virtual void onRenderingFinished(SceneManager* manager) override;

private:

	typedef std::list<RenderPassSharedPtr> RenderPassInternalContainerType;
	bool addRenderPass_impl(RenderPassInternalContainerType::const_iterator iterToInsert, RenderPassSharedPtr renderPass);

	RenderPassManager(const RenderPassManager& other) = delete;
	RenderPassManager& operator = (const RenderPassManager& other) = delete;

	void initDefaultRenderPasses();

private:

	mutable bool m_isChanged = false;
	mutable RenderPassContainerType m_cachedRenderPassesVector;

	RenderPassContainerType m_cachedDirtyRenderPasses;
	RenderPassInternalContainerType m_renderPasses;
};
ABYSS_END_NAMESPACE