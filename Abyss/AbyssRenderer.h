#pragma once

#include <vector>
#include "AbyssGlm.h"
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class SceneObject;
class SceneManager;
class ShaderProgram;
class RenderableObject;
class RendererVisitor;
class RendererExtension;

class Renderer
{
	// Another way to deal with this friendship - add to API getExtenstions() method.
	friend class RendererVisitor;

public:

	typedef std::vector<Renderer *> TRenderersContainer;
public:
	explicit Renderer(const String & shaderProgramName);
	virtual ~Renderer();

	// Return name of the renderer.
	inline ShaderProgram * getShaderProgram() const;

	// Init renderer on initialization state.
	// There are you can place some program recompilation, or initialization buffers.
	// Note, that you are not needed to call this method by hand.
	void init(SceneManager* manager);

	// Preparing for rendering. Place here some special code for rendering:
	virtual void startRendering(RenderingResultPtr input);
	virtual void finishRendering(RenderingResultPtr output);

	virtual void render(RenderableObject * const );

	// Check, whether this one renderer can support specified renderable object.
	virtual bool isSupports(const RenderableObject * const object) const = 0;

	// Some hack method. Need be replaced by RenderManager:
	static void initAllRenderers(SceneManager * manager);

	RendererVisitor* getVisitor() const;

	static const TRenderersContainer& getRegisteredRenderers()	{ return sRegisteredRenderers; }


	virtual void onAttach(RenderPass* renderPass);
	virtual void onDetach(RenderPass* renderPass);


protected:
	
	virtual void registerExtensions(){};
	virtual void doInit(SceneManager* manager) {};

	virtual RendererVisitor* createVisitor() const;

	//NVI
	// TODO: maybe, this method haven't be pure virtual, it should be tested clearly. 
	virtual void beforeRender(RenderableObject * const object) = 0;
	
	// Just perform rendering of given renderable object.
	// This process consists only from two parts: rendering mesh and rendering bounding box.
	// TODO: maybe, this one should be removed. We will see in future about usage of this method.
	virtual void performRendering(RenderableObject * const object);

	// TODO: maybe, this method haven't be pure virtual, it should be tested clearly. 
	virtual void afterRender(RenderableObject * const object) = 0;

	// Renderer's shader.
	ShaderProgram * m_program;

	void setupMatrixUniformsValues(const SceneObject * const sceneObject);
	
	void setNormalMatrix(const Matrix3D & matrix);
	void setWorldMatrix(const Matrix4D & matrix);
	void setViewMatrix(const Matrix4D & matrix);
	void setWorldViewMatrix(const Matrix4D & matrix);
	void setProjectionMatrix(const Matrix4D & matrix);
	void setWorldViewProjectionMatrix(const Matrix4D & matrix);


protected:

	std::unique_ptr<RendererVisitor> m_visitor;
	std::vector<std::shared_ptr<RendererExtension>> m_extensions;

private:

	bool m_isInited = false;

	// Container of registered renderers:
	static TRenderersContainer sRegisteredRenderers;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
ShaderProgram * Renderer::getShaderProgram() const
{	return m_program;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE