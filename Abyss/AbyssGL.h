#pragma once

#include "Abyss.h"

#include "GL/glew.h"

ABYSS_BEGIN_NAMESPACE

// Check, whether there is an error in OpenGL.
inline bool checkGLErrorStatus()
{	return glGetError() == GL_NO_ERROR;	}

ABYSS_END_NAMESPACE