#include "AbyssLight.h"
#include "AbyssCamera.h"
#include "AbyssSceneObject.h"
#include "AbyssRenderableObject.h"
#include "AbyssSceneManager.h"
#include "AbyssShaderProgram.h"
#include "AbyssRendererLightingExtension.h"

#include <boost/format.hpp>

ABYSS_BEGIN_NAMESPACE

namespace
{
	const int MAX_LIGHTS_COUNT = 10;

	// LIGHTS
	const std::string ACTIVE_LIGHT_COUNT_NAME = "activeLightsCount";
	const std::string LIGHT_NAME = "lights";
	const std::string LIGHT_TYPE_NAME = "type";
	const std::string LIGHT_POSITION_NAME = "position";
	const std::string LIGHT_DIRECTION_NAME = "direction";
	const std::string LIGHT_COLOR_NAME = "color";
	const std::string LIGHT_CONSTANT_ATTENUATION_NAME = "constantAttenuation";
	const std::string LIGHT_LINEAR_ATTENUATION_NAME = "linearAttenuation";
	const std::string LIGHT_QUADRADTIC_ATTENUATION_NAME = "quadraticAttenuation";
	const std::string LIGHT_ENABLED_NAME = "enableLighting";
	const std::string LIGHT_INNER_CUTOFF_COSINE = "innerCutOffCosine";
	const std::string LIGHT_OUTER_CUTOFF_COSINE = "outerCutOffCosine";
}

RendererLightingExtension::RendererLightingExtension(ShaderProgram* program)
	: RendererExtension(program)
{

}

RendererLightingExtension::~RendererLightingExtension()
{

}

void RendererLightingExtension::prepareForRendering(RenderableObject * const object)
{
	SceneObject * sceneObject = object->getBoundSceneObject();
	getShaderProgram()->setUniform(LIGHT_ENABLED_NAME, object->isLightingEnabled());

	if (object->isLightingEnabled())
	{
		setupLightsUniformsValues(sceneObject);
	}
}

void RendererLightingExtension::unprepareFromRendering(RenderableObject * const object)
{

}

void RendererLightingExtension::beforeRendering(Mesh* mesh)
{
	// Does nothing
}

void RendererLightingExtension::afterRendering(Mesh* mesh)
{
	// Does nothing
}

void RendererLightingExtension::setupLightsUniformsValues(const SceneObject* const sceneObject)
{
	// TODO: Need add real light object.
	//const int activeLightsCount = std::min(MAX_LIGHTS_COUNT, 1);
	//const Vector4D lightPosition = Vector4D(0.f, 0.f, 1.f, 0.f);
	//const Vector4D lightIntensity = Vector4D(1.f, 1.f, 1.f, 1.f);

	SceneManager* sceneManager = sceneObject->getSceneManager();
	auto sceneManagerLights = sceneManager->getLights();
	auto sceneObjectLights = sceneObject->getLights();
	auto lights = LightsContainer::mergetLights(sceneManagerLights, sceneObjectLights);

	auto lightsSize = lights.size();
	if (lightsSize > MAX_LIGHTS_COUNT)
	{
		assert(false);
		ABYSS_LOG("ERROR at RendererLightingExtension::setupLightsUniformsValues(...) - the lights count cannot be larger than MAX_LIGHTS_COUNT const.");
		lightsSize = MAX_LIGHTS_COUNT;
	}

	for (size_t lightIndex = 0; lightIndex < lightsSize; ++lightIndex)
	{
		LightWeakPtr light = lights[lightIndex];
		Vector4D lightPosition4D = light->getWorldMatrix() * Vector4D(0.f, 0.f, 0.f, 1.f);

		// TODO: should we divide by w?
		lightPosition4D /= lightPosition4D.w;
		Vector3D lightPosition = toVector3D(lightPosition4D);

		Vector3D lightDirection = light->getDirection();
		Vector4D lightColor = light->getColor();

		setLightAttenuationUniformValue(lightIndex, light->getAttenuation());
		setLightPositionUniformValue(lightIndex, lightPosition);
		setLightDirectionUniformValue(lightIndex, lightDirection);
		setLightColorUniformValue(lightIndex, lightColor);
		setLightType(lightIndex, int(light->getLightType()));
		setSpotlightUniformValues(lightIndex, light->getInnerCutOffAngle(), light->getOuterCutOffAngle());
	}

	setActiveLightsCountUniformValue(lightsSize);

	getShaderProgram()->setUniform("eyePosition_WorldSpace", sceneManager->getCamera()->getEyePosition());
}

void RendererLightingExtension::setLightPositionUniformValue(int lightIndex, const Vector3D & position)
{
	std::string uniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_POSITION_NAME) % lightIndex).str();
	getShaderProgram()->setUniform(uniformName, position);
}

void RendererLightingExtension::setLightDirectionUniformValue(int lightIndex, const Vector3D & direction)
{
	std::string uniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_DIRECTION_NAME) % lightIndex).str();
	getShaderProgram()->setUniform(uniformName, direction);
}

void RendererLightingExtension::setLightColorUniformValue(int lightIndex, const Vector4D & color)
{
	std::string uniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_COLOR_NAME) % lightIndex).str();
	getShaderProgram()->setUniform(uniformName, color);
}

void RendererLightingExtension::setActiveLightsCountUniformValue(int lightsCount)
{
	getShaderProgram()->setUniform(ACTIVE_LIGHT_COUNT_NAME, lightsCount);
}

void RendererLightingExtension::setLightAttenuationUniformValue(int lightIndex, const Vector3D& attenuationConstants)
{
	std::string constantAttenuationUniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_CONSTANT_ATTENUATION_NAME) % lightIndex).str();
	std::string linearAttenuationUniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_LINEAR_ATTENUATION_NAME) % lightIndex).str();
	std::string quadraticAttenuationUniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_QUADRADTIC_ATTENUATION_NAME) % lightIndex).str();

	getShaderProgram()->setUniform(constantAttenuationUniformName, attenuationConstants.x);
	getShaderProgram()->setUniform(linearAttenuationUniformName, attenuationConstants.y);
	getShaderProgram()->setUniform(quadraticAttenuationUniformName, attenuationConstants.z);
}

void RendererLightingExtension::setLightType(int lightIndex, int lightType)
{
	std::string uniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_TYPE_NAME) % lightIndex).str();
	getShaderProgram()->setUniform(uniformName, lightType);
}

void RendererLightingExtension::setSpotlightUniformValues(int lightIndex, float innerCutoffAngle, float outerCutoffAngle)
{
	std::string innerCutoffCosineUniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_INNER_CUTOFF_COSINE) % lightIndex).str();
	std::string outerCutoffCosineUniformName = (boost::format(LIGHT_NAME + "[%1%]." + LIGHT_OUTER_CUTOFF_COSINE) % lightIndex).str();

	getShaderProgram()->setUniform(innerCutoffCosineUniformName, cos(innerCutoffAngle));
	getShaderProgram()->setUniform(outerCutoffCosineUniformName, cos(outerCutoffAngle));
}

ABYSS_END_NAMESPACE