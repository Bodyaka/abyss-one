#pragma once

#include "AbyssRendering.h"
#include <set>
#include <map>

ABYSS_BEGIN_NAMESPACE

class SceneManager;
class RenderPassListener;

class RenderPass
{
public:

	// LIFECYCLE:
	explicit RenderPass(const std::string& name, Renderer* renderer = nullptr);
	virtual ~RenderPass() = default;

	// Polish is called when the object is created and ready to use.
	// This can be used to defer some initialization logic.
	virtual void polish(SceneManager* sceneManager);

	// Changes Renderer of RenderPass:
	void setRenderer(Renderer* renderer);
	Renderer* getRenderer() const;

	virtual void startRendering(RenderingResultPtr input);
	virtual void render(RenderableObject* object);
	virtual void finishRendering(RenderingResultPtr output);

	// Returns RenderPass priority:
	const std::string& getName() const;

	// LISTENERS:
	void addListener(RenderPassListener* listener);
	void removeListener(RenderPassListener* listener);

	void setIsPostProcessingEffect(bool isPostProcessing);
	bool isPostProcessingEffect() const;

private:

	bool m_isLocked = false;

	std::string m_name;
	Renderer* m_renderer = false;

	std::vector<RenderPassListener*> m_listeners;

	bool m_isPostProcessingEffect;

	// DELETED METHODS:
private:
	RenderPass(const RenderPass& pass) = delete;
	RenderPass& operator = (const RenderPass& pass) = delete;
};

// INLINE METHODS
inline const std::string& RenderPass::getName() const
{	return m_name; }

inline Renderer* RenderPass::getRenderer() const
{	return m_renderer;	}

inline void RenderPass::setIsPostProcessingEffect(bool isPostProcessing)
{	m_isPostProcessingEffect = isPostProcessing;	}

inline bool RenderPass::isPostProcessingEffect() const
{	return m_isPostProcessingEffect;	}

ABYSS_END_NAMESPACE

