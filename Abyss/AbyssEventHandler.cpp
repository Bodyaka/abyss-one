#include "AbyssEventHandler.h"

#include "AbyssKeyEvent.h"
#include "AbyssMouseEvent.h"
#include "AbyssMouseWheelEvent.h"

ABYSS_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------------------
EventHandler::EventHandler()
{}
//--------------------------------------------------------------------------------------------------
EventHandler::~EventHandler()
{}
//--------------------------------------------------------------------------------------------------
void EventHandler::mouseMoveEvent(const MouseEventPtr & event)
{}
//--------------------------------------------------------------------------------------------------
void EventHandler::mousePressEvent(const MouseEventPtr & event)
{}
//--------------------------------------------------------------------------------------------------
void EventHandler::mouseReleaseEvent(const MouseEventPtr & event)
{}
//--------------------------------------------------------------------------------------------------
void EventHandler::mouseWheelEvent(const MouseWheelEventPtr& event)
{}
//--------------------------------------------------------------------------------------------------
void EventHandler::mouseDoubleClickEvent(const MouseEventPtr & event)
{}
//--------------------------------------------------------------------------------------------------
void EventHandler::keyPressEvent(const Abyss::KeyEventPtr & event)
{}
//--------------------------------------------------------------------------------------------------
void EventHandler::keyReleaseEvent(const KeyEventPtr & event)
{}
//--------------------------------------------------------------------------------------------------
void EventHandler::event(const EventPtr & i_event)
{
	switch (i_event->getType())
	{
	case Event::MouseMoved:
		mouseMoveEvent(std::static_pointer_cast<MouseEvent>(i_event));
		break;
	case Event::MousePressed:
		mousePressEvent(std::static_pointer_cast<MouseEvent>(i_event));
		break;
	case Event::MouseReleased:
		mouseReleaseEvent(std::static_pointer_cast<MouseEvent>(i_event));
		break;
	case Event::MouseDoubleClick:
		mouseDoubleClickEvent(std::static_pointer_cast<MouseEvent>(i_event));
		break;
	case Event::MouseWheel:
		mouseWheelEvent(std::static_pointer_cast<MouseWheelEvent>(i_event));
		break;
	case Event::KeyPressed:
		keyPressEvent(std::static_pointer_cast<KeyEvent>(i_event));
		break;
	case Event::KeyReleased:
		keyReleaseEvent(std::static_pointer_cast<KeyEvent>(i_event));
		break;
	}

	onEvent(i_event);
}
//--------------------------------------------------------------------------------------------------
void EventHandler::onEvent(const EventPtr & event)
{

}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
