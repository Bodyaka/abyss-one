#include "AbyssMouseWheelEvent.h"

ABYSS_BEGIN_NAMESPACE

MouseWheelEvent::MouseWheelEvent(const Vector2D & position, int delta)
	: MouseEvent(position, Mouse::NoneButton, MouseWheel)
	, m_delta(delta)
{

}

MouseWheelEventPtr MouseWheelEvent::createMouseWheelEvent(const Vector2D & position, int delta)
{
	auto wheelEvent = new MouseWheelEvent(position, delta);
	return MouseWheelEventPtr(wheelEvent);
}

ABYSS_END_NAMESPACE