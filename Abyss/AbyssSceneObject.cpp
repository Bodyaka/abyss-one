#include "AbyssSceneObject.h"

#include <cassert>

#include "AbyssLight.h"
#include "AbyssMesh.h"
#include "AbyssCamera.h"
#include "AbyssSceneManager.h"

ABYSS_BEGIN_NAMESPACE

//-------------------------------------------------------------------------------------------------
SceneObject::SceneObject(const String & name, SceneManager * manager, SceneObject * parent)
	: TSuperDeus(parent)
	, FreeTransformableObject()
	, m_isVisible(true)
	, m_name(name)
	, m_manager(manager)
{
}
//-------------------------------------------------------------------------------------------------
SceneObject::~SceneObject()
{

}
//-------------------------------------------------------------------------------------------------
void SceneObject::prepareForRendering(RenderQueueVisitor* visitor)
{
	if (!isVisible())
		return;

	for (auto object : m_renderableObjects)
		object.second->prepareForRendering(visitor);
}
//-------------------------------------------------------------------------------------------------
void SceneObject::addRenderableObject(RenderableObjectPtr object)
{
	assert(object);

	m_renderableObjects[object->getName()] = object;
	
	// We need bind renderable object to given scene object.
	object->bindTo(this);

	updateAABB();
}
//-------------------------------------------------------------------------------------------------
void SceneObject::removeRenderableObject(const String & objectName)
{
	if (!hasRenderableObject(objectName))
		return;

	// We need release (unbind) deleted renderable object from this one scene object.
	RenderableObjectPtr object = m_renderableObjects.at(objectName);
	object->release();

	m_renderableObjects.erase(objectName);

	updateAABB();
}
//-------------------------------------------------------------------------------------------------
void SceneObject::removeRenderableObject(RenderableObjectPtr object)
{
	removeRenderableObject(object->getName());
}
//-------------------------------------------------------------------------------------------------
RenderableObjectPtr SceneObject::getRenderableOject(const String & name) const
{
	if (m_renderableObjects.find(name) == m_renderableObjects.end())
		return RenderableObjectPtr();

	return m_renderableObjects.at(name);
}
//-------------------------------------------------------------------------------------------------
size_t SceneObject::getRenderableObjectsSize() const
{
	return m_renderableObjects.size();
}
//-------------------------------------------------------------------------------------------------
bool SceneObject::hasRenderableObject(const String & name) const
{
	return m_renderableObjects.find(name) != m_renderableObjects.end();
}
//-------------------------------------------------------------------------------------------------
bool SceneObject::hasRenderableObject(RenderableObjectPtr object) const
{
	return hasRenderableObject(object->getName());
}
//-------------------------------------------------------------------------------------------------
void SceneObject::setVisible(bool visibility)
{
	m_isVisible = visibility;

	for (auto & object : m_renderableObjects)
		object.second->setVisible(visibility);

	// Change visibility value for scene object children:
	for (auto object : m_children)
		object->setVisible(visibility);
}
//-------------------------------------------------------------------------------------------------
void SceneObject::updateAABB()
{
	m_worldAABB = AABB();

	if (getRenderableObjectsSize() == 0)
		return;

	// TODO: Maybe, here is needed some improvement: 
	// We should not compute AABB's of mesh objects, because most of time they are static.
	updateRenderableObjectsAABB();

	m_worldAABB = getLocalAABB().transform(getWorldMatrix());
}
//-------------------------------------------------------------------------------------------------
void SceneObject::updateRenderableObjectsAABB()
{
	if (!isAutoUpdateEnabled())
		return;

	m_localAABB = AABB();

	bool firstObject = true;
	for (auto & object : m_renderableObjects)
	{
		AABB renderableObjectAABB = object.second->getGeometryObject()->getAABB();

		if (firstObject)
		{
			firstObject = false;
			m_localAABB = renderableObjectAABB;
			continue;
		}

		m_localAABB.unit(renderableObjectAABB);
	}
}
//-------------------------------------------------------------------------------------------------
void SceneObject::updateTransform()
{
	CameraPtr camera = getSceneManager()->getCamera();

	const Matrix4D & viewMatrix = camera->getViewMatrix();
	const Matrix4D & projMatrix = camera->getProjectionMatrix();
	
	SceneObject * parent = getParent();
	
	// TODO: some stincky code, need some better solution:
	if (!parent)
		FreeTransformableObject::updateTransform(projMatrix, viewMatrix);
	else
		FreeTransformableObject::updateTransform(projMatrix, viewMatrix, parent->getModelMatrix());

	updateAABB();
}
//-------------------------------------------------------------------------------------------------
void SceneObject::onStartRendering()
{

}
//-------------------------------------------------------------------------------------------------
void SceneObject::onFinishRendering()
{

}
//-------------------------------------------------------------------------------------------------
std::vector<RenderableObjectPtr> SceneObject::getRenderableObjects() const
{
	std::vector<RenderableObjectPtr> renderableObjects;
	renderableObjects.reserve(getRenderableObjectsSize());

	for (auto iterValue : m_renderableObjects)
	{
		renderableObjects.push_back(iterValue.second);
	}

	return renderableObjects;
}
//-------------------------------------------------------------------------------------------------
LightsContainerType SceneObject::getLights() const
{
	auto ownLights = LightsContainer::getLights();

	LightsContainerType parentLights;
	if (auto parentSceneObject = getParent())
		parentLights = parentSceneObject->getLights();

	return LightsContainer::mergetLights(ownLights, parentLights);
}
//-------------------------------------------------------------------------------------------------
void SceneObject::onLightAdded(LightWeakPtr light)
{
	light->attachToSceneObject(this);
}
//-------------------------------------------------------------------------------------------------
void SceneObject::onLightRemoved(LightWeakPtr light)
{
	light->deattachFromSceneObject();
}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE