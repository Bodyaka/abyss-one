#pragma once

#include "AbyssScheduler.h"
#include "AbyssGlm.h"
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class Pathline;
class SceneObject;

class TreefoilPathModifier
	: public Scheduler
{
public:
	TreefoilPathModifier();
	~TreefoilPathModifier();

	void setSceneObjectName(const String& sceneObjectName);
	const String& getSceneObjectName() const;

	void setTreefoilRadius(float radius);
	float getTreefoiRadius() const;

	void setTreefoilCenterPosition(const Vector3D& pos);
	const Vector3D& getTreefoilCenterPosition() const;


protected:

	Pathline* getPathline() const;

	virtual void onPathUpdated(float deltaTime);

	void updateSceneObjectPosition();

	SceneObject* getSceneObject() const;

	String m_sceneObjectName;
	float m_currentAngle;
	float m_radius;

	Vector3D m_centerPosition;

	SimpleRenderableObjectPtr m_pathRenderable;
};

// INLINE METHODS
inline const String& TreefoilPathModifier::getSceneObjectName() const
{	return m_sceneObjectName;	}

inline float TreefoilPathModifier::getTreefoiRadius() const
{	return m_radius; }

inline const Vector3D& TreefoilPathModifier::getTreefoilCenterPosition() const
{	return m_centerPosition;	}

ABYSS_END_NAMESPACE