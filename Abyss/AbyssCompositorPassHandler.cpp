#include "AbyssCompositorPassHandler.h"

#include "AbyssRenderTarget.h"
#include "AbyssTexture.h"
#include "AbyssDepthBuffer.h"
#include "AbyssRenderingResult.h"

ABYSS_BEGIN_NAMESPACE

// -------- class CompositorPassDefaultHandler ------------


CompositorPassDefaultHandler::CompositorPassDefaultHandler(const std::string& textureName)
	: m_textureName(textureName)
{	}

void CompositorPassDefaultHandler::prepareRenderTarget(RenderTarget* renderTarget)
{
	if (!renderTarget->getAttachedTexture(RenderTarget::ColorAttachment0))
	{
		auto width = renderTarget->getActualWidth();
		auto height = renderTarget->getActualHeight();

		m_createdTexture.reset(new Abyss::Texture());
		m_createdTexture->setRawData(nullptr, width, height, PixelFormat::PF_UBYTE);
		m_createdTexture->setFiltering(Abyss::Texture::LinearFiltering);
		m_createdTexture->setWrapping(Abyss::Texture::ClampToEdgeWrapping);

		Abyss::DepthBuffer::Pointer depthBuffer(new Abyss::DepthBuffer(width, height, 0));
		renderTarget->attachTexture(Abyss::RenderTarget::ColorAttachment0, m_createdTexture);
		renderTarget->attachDepthBuffer(depthBuffer);
	}
}

void CompositorPassDefaultHandler::onRenderPassStarted(RenderPass* renderPass, RenderTarget* renderTarget, RenderingResultPtr input)
{
	//renderTarget->clear();
}

void CompositorPassDefaultHandler::onRenderPassFinished(RenderPass* renderPass, RenderTarget* renderTarget, RenderingResultPtr input)
{
	// Simulate ping-pong effect:
	swapTextureWithRenderTarget(input, m_textureName, renderTarget);
}

void CompositorPassDefaultHandler::onFinishRendering(RenderTarget* renderTarget, RenderingResultPtr output)
{
	auto attachedTexture = renderTarget->getAttachedTexture(RenderTarget::ColorAttachment0);

	// Output must contain original input texture pointer, that was in input (see onRenderingStarted)
	if (attachedTexture != m_createdTexture)
	{
		// Because main texture is attached to render target, this means, that it contains last rendered image from compositor chain
		// Thus, it is needed only to swap attached texture with main texture in output:
		swapTextureWithRenderTarget(output, m_textureName, renderTarget);
	}

	// TODO: this blitting might be very time consuming! Need to provide another approach, or use some caching instead!
	// Copy last rendered texture into main texture:
	std::unique_ptr<RenderTarget> blitRenderTarget(new RenderTarget(renderTarget->getAttachedWindow()));
	blitRenderTarget->attachTexture(RenderTarget::ColorAttachment0, output->getTexture(m_textureName));
	//Abyss::DepthBuffer::Pointer depthBuffer(new Abyss::DepthBuffer(blitRenderTarget->getActualWidth(), blitRenderTarget->getActualHeight(), 0));
	//blitRenderTarget->attachDepthBuffer(depthBuffer);
	blitRenderTarget->bind();
	blitRenderTarget->release();
		
	renderTarget->bind();
	if (!renderTarget->blitTo(blitRenderTarget.get(), false))
	{
		// TODO: somehow when application is closed, the pass handler generated this exception!
		ABYSS_LOG("CompositorPassHandler - cannot blit Attached texture to main renderTarget!");
	}
	renderTarget->release();
}

void CompositorPassDefaultHandler::swapTextureWithRenderTarget(RenderingResultPtr result, const std::string& textureName, RenderTarget* renderTarget)
{
	TexturePtr mainTexture = result->getTexture(textureName);

	auto attachedTexture = renderTarget->getAttachedTexture(RenderTarget::ColorAttachment0);
	renderTarget->deattachTexture(attachedTexture);
	renderTarget->attachTexture(RenderTarget::ColorAttachment0, mainTexture);
	//renderTarget->reinit();

	result->setTexture(textureName, attachedTexture);
}

void CompositorPassDefaultHandler::onRenderingStarted(RenderTarget* renderTarget)
{
	// Nothing To Do
}



ABYSS_END_NAMESPACE