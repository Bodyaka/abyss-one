#include "AbyssPixelFormat.h"

#include "boost/bimap.hpp"

ABYSS_BEGIN_NAMESPACE

typedef boost::bimap<GLenum, PixelFormat> PixelFormatMapperType;
typedef PixelFormatMapperType::value_type PixelFormatMapperValueType;
static PixelFormatMapperType s_pixelFormatMapper;

//-----------------------------------------------------------------------------------------------
void initializePixelFormatMapper()
{
	if (!s_pixelFormatMapper.empty())
		return;
		
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_BYTE,		PF_UBYTE));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_BYTE,				PF_BYTE));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_SHORT,	PF_USHORT));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_SHORT,				PF_SHORT));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_INT,		PF_UINT));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_INT,				PF_INT));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_FLOAT,				PF_FLOAT));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_BYTE_3_3_2,		PF_UBYTE_R3G3B2));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_SHORT_5_6_5,		PF_USHORT_R5G6B5));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_SHORT_4_4_4_4,	PF_USHORT_R4G4B4A4));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_SHORT_5_5_5_1,	PF_USHORT_R5G5B5A1));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_INT_8_8_8_8,		PF_UINT_R8G8B8A8));
	s_pixelFormatMapper.insert(PixelFormatMapperValueType(GL_UNSIGNED_INT_10_10_10_2,	PF_UINT_R10G10B10A2));
}
//-----------------------------------------------------------------------------------------------
GLenum convertPixelFormatToGL(PixelFormat pixelFormat)
{
	initializePixelFormatMapper();
	auto iter = s_pixelFormatMapper.right.find(pixelFormat);
	return (iter == s_pixelFormatMapper.right.end()) ? GL_NONE : iter->second;
}
//-----------------------------------------------------------------------------------------------
PixelFormat convertPixelFormatFromGL(GLenum glFormat)
{
	initializePixelFormatMapper();
	auto iter = s_pixelFormatMapper.left.find(glFormat);
	return (iter == s_pixelFormatMapper.left.end()) ? PF_UNKNOWN : iter->second;
}
//-----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE