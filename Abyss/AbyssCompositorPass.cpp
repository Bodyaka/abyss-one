#include "AbyssCompositorPass.h"

#include "AbyssSceneManager.h"
#include "AbyssRenderTarget.h"
#include "AbyssCompositorPassHandler.h"
#include "AbyssRenderingResult.h"
#include "AbyssRenderTarget.h"

ABYSS_BEGIN_NAMESPACE

CompositorPass::CompositorPass(const std::string& name)
	: RenderPass(name, nullptr)
	, m_isRenderTargetDirty(false)
{
	setIsPostProcessingEffect(true);
}

void CompositorPass::polish(SceneManager* sceneManager)
{
	RenderPass::polish(sceneManager);

	// Attach to current window, if needed:
	if (getAttachedWindow() == nullptr)
	{
		auto window = sceneManager->getWindow();
		attachToWindow(window);
	}

	// Specify default handler:
	if (getHandler() == nullptr)
	{
		m_handler.reset(new CompositorPassDefaultHandler(constants::ABYSS_MAIN_TEXTURE));
	}
}

void CompositorPass::startRendering(RenderingResultPtr input)
{
	if (m_isRenderTargetDirty)
	{
		getHandler()->prepareRenderTarget(m_renderTarget.get());
		m_isRenderTargetDirty = false;
	}

	m_cachedRenderingResult = input;
	getHandler()->onRenderingStarted(m_renderTarget.get());
}

void CompositorPass::render(RenderableObject* object)
{
	m_renderTarget->notifyStartRendering();
	
	for (auto renderPass : getRenderPasses())
	{
		glDeleteSync(0);

		m_handler->onRenderPassStarted(renderPass.get(), m_renderTarget.get(), m_cachedRenderingResult);

		renderPass->startRendering(m_cachedRenderingResult);
		renderPass->render(object);
		renderPass->finishRendering(m_cachedRenderingResult);

		m_handler->onRenderPassFinished(renderPass.get(), m_renderTarget.get(), m_cachedRenderingResult);
		
		glDeleteSync(0);
	}

	m_renderTarget->notifyFinishRendering();
}

void CompositorPass::finishRendering(RenderingResultPtr output)
{
	m_handler->onFinishRendering(m_renderTarget.get(), output);
}

RenderPassPtr CompositorPass::emplaceRenderPass(const std::string& name, Renderer* renderer)
{
	auto iter = std::find_if(m_renderPasses.begin(), m_renderPasses.end(), [name](const RenderPassPtr& renderPass) -> bool
	{
		return renderPass->getName() == name;
	});

	if (iter != m_renderPasses.end())
		return *iter;

	RenderPassPtr renderPass = std::make_shared<RenderPass>(name, renderer);
	m_renderPasses.push_back(renderPass);
	return renderPass;
}


bool CompositorPass::addRenderPass(RenderPassPtr renderPass)
{
	if (hasRenderPass(renderPass->getName()))
		return false;

	m_renderPasses.push_back(renderPass);
	return true;
}

void CompositorPass::removeRenderPass(const std::string& name)
{
	auto iter = std::find_if(m_renderPasses.begin(), m_renderPasses.end(), [name](const RenderPassPtr& renderPass) -> bool
	{
		return renderPass->getName() == name;
	});

	if (iter != m_renderPasses.end())
		m_renderPasses.erase(iter);
}

void CompositorPass::removeRenderPass(RenderPassPtr renderPass)
{
	auto iter = std::find(m_renderPasses.begin(), m_renderPasses.end(), renderPass);

	if (iter != m_renderPasses.end())
		m_renderPasses.erase(iter);
}

void CompositorPass::removeAllRenderPasses()
{
	m_renderPasses.clear();
}

bool CompositorPass::hasRenderPass(const std::string& name) const
{
	auto iter = std::find_if(m_renderPasses.begin(), m_renderPasses.end(), [name](const RenderPassPtr& renderPass) -> bool
	{
		return renderPass->getName() == name;
	});

	return iter != m_renderPasses.end();
}

RenderPassPtr CompositorPass::getRenderPass(const std::string& name) const
{
	auto iter = std::find_if(m_renderPasses.begin(), m_renderPasses.end(), [name](const RenderPassPtr& renderPass) -> bool 
	{
		return renderPass->getName() == name;
	});

	return iter != m_renderPasses.end() ? *iter : nullptr;

}

void CompositorPass::attachToWindow(Window* window)
{
	if (getAttachedWindow() == window)
		return;

	m_renderTarget.reset(new RenderTarget(window));
	m_isRenderTargetDirty = true;
}

Window* CompositorPass::getAttachedWindow() const
{
	return m_renderTarget != nullptr ? m_renderTarget->getAttachedWindow()
									 : nullptr;
}

ABYSS_END_NAMESPACE