#include "AbyssCameraMoveEventAction.h"

#include "AbyssCamera.h"
#include "AbyssKeyEvent.h"

ABYSS_BEGIN_NAMESPACE

const float CameraMoveEventAction::s_DefaultMoveOffset = 0.33f;

//-------------------------------------------------------------------------------------------------
CameraMoveEventAction::CameraMoveEventAction(CameraPtr camera, float moveOffset /* = s_DefaultMoveOffset */)
	: KeyEventAction()
	, m_camera(camera)
	, m_moveOffset(moveOffset)
{

}
//-------------------------------------------------------------------------------------------------
void CameraMoveEventAction::perform(const KeyEventPtr & event)
{
	// Camera's data:
	const Vector3D & up = m_camera->getUpVector();
	const Vector3D & eye = m_camera->getEyePosition();
	const Vector3D & center = m_camera->getCenterPosition();

	// Camera's direction data:
	Vector3D eyeDir = glm::normalize(center - eye);
	Vector3D sideDir = glm::cross(eyeDir, up);

	// Offset (which is used for final offset and direction).
	Vector3D offset, dir;

	if (event->isPressed(KeyEvent::W))
		offset += eyeDir * getMoveOffset();
	if (event->isPressed(KeyEvent::S))
		offset -= eyeDir * getMoveOffset();
	if (event->isPressed(KeyEvent::A))
		offset -= sideDir * getMoveOffset();
	if (event->isPressed(KeyEvent::D))
		offset += sideDir * getMoveOffset();

	m_camera->setup(eye + offset, center + offset, up);
}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE