#pragma once

#include "AbyssAABB.h"
#include "AbyssGeometryObject.h"
#include "AbyssGlm.h"

ABYSS_BEGIN_NAMESPACE

// Bounding box for drawing some data.
class Pathline
	: public GeometryObject
{
public:
	// Create Bbox.
	Pathline();
	// Destructor.
	~Pathline();

	void setVertices(const ContainerV3D& vertices);
	void addVertex(const Vector3D& vertex);
	void clearVertices();

	const ContainerV3D& getVertices() const;

	// Render bbox:
	void render();

private:

	ContainerV3D m_vertices;

	bool m_isDirty;
};

// INLINE METHODS
inline const ContainerV3D& Pathline::getVertices() const
{	return m_vertices;	}

ABYSS_END_NAMESPACE

