#pragma once

#include <ctime>
#include "Abyss.h"
#include "Notifier.h"

ABYSS_BEGIN_NAMESPACE

class FPSCounter;

class FPSCounterListener
{
public:
	virtual ~FPSCounterListener(){};

	virtual void onFPSChanged(FPSCounter* counter) = 0;
};

// Simple CPU FPS counter.
class FPSCounter
	: public Notifier<FPSCounterListener>
{
public:
	FPSCounter();
	virtual ~FPSCounter();

	virtual void onFrameStarted();
	virtual void onFrameEnded();

	size_t getLastFrameRate() const;
	float getAverageFrameRate() const;
	float getAverageFrameRate2() const;
	float getAverageFrameTimeInMilliseconds() const;

protected:

	void update(clock_t elapsedTime);

private:

	clock_t m_startTime;
			
	size_t m_framesCount;
	size_t m_lastFrameRate;
	float m_frameRate;

	size_t m_updatesCount;
	double m_combinedFramesCount;
};

// INLINE METHODS
//--------------------------------------------------------------------------------------------
inline float FPSCounter::getAverageFrameRate() const
{	return m_frameRate;	}
//--------------------------------------------------------------------------------------------
inline size_t FPSCounter::getLastFrameRate() const
{	return m_lastFrameRate;	}
//--------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE