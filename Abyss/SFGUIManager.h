#pragma once

#include <memory>
#include "Abyss.h"
#include "AbyssSingleton.h"
#include "SFML/System/Clock.hpp"
#include "SFGUI/Window.hpp"
#include "SFGUI/Fixed.hpp"

namespace sfg
{
	class SFGUI;
	class Desktop;
}

namespace sf
{
	class Event;
}

ABYSS_BEGIN_NAMESPACE

class Window;

class SFGUIManager
	: public Singleton<SFGUIManager>
{
	typedef Singleton<SFGUIManager> TSuper;
	friend class TSuper;

public:

	sfg::Desktop* getDesktop() const;
	sfg::SFGUI* getGUI() const;
	sfg::Fixed::Ptr getRootWindow() const;

	void update();
	void render(Window* window);
	void init();
	bool processEvent(const sf::Event& event);

private:

	SFGUIManager();
	~SFGUIManager();

	sf::Clock m_clock;
	std::unique_ptr<sfg::SFGUI> m_gui;
	std::unique_ptr<sfg::Desktop> m_desktop;

	sfg::Fixed::Ptr m_rootWindow;

	bool m_isEventProcessedByGUI;

};

inline sfg::SFGUI* SFGUIManager::getGUI() const
{	return m_gui.get();	}

inline sfg::Desktop* SFGUIManager::getDesktop() const
{	return m_desktop.get();	}

inline sfg::Fixed::Ptr SFGUIManager::getRootWindow() const
{	return m_rootWindow; }

ABYSS_END_NAMESPACE