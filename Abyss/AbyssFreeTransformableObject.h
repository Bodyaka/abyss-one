#pragma once

#include "AbyssMovableObject.h"

ABYSS_BEGIN_NAMESPACE

class FreeTransformableObject
	: public MovableObject
{
public:
	FreeTransformableObject();
	~FreeTransformableObject();

	// Rotate along own Y axis.
	void yaw(float angle);
	// Rotate along own Z axis.
	void roll(float angle);
	// Rotate along own X axis.
	void pitch(float angle);

	// Rotate by angle along specified axis
	void rotate(float angle, const Vector3D & axis);
	// Rotate by angle along specified self axis.
	void rotateAlongSelfAxis(float angle, const Vector3D & axis);

	// Scale by specified value:
	void setScale(const Vector3D & scale);
	// Scale all axes by specified value:
	void setScale(float value);
	// Return scale.
	const Vector3D & getScale() const;

	// Set rotation matrix (should not use this method):
	void setRotationMatrix(const Matrix4D& matrix);
	// Return rotation matrix (without scale).
	const Matrix4D & getRotationMatrix() const;

protected:

	// Updates transform of object.
	virtual void updateModelMatrix_impl() override;

private:
	
	// Rotation matrix.
	Matrix4D m_rotationMatrix;

	// Scale value.
	Vector3D m_scale;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline const Matrix4D & FreeTransformableObject::getRotationMatrix() const
{	return m_rotationMatrix;	}
//----------------------------------------------------------------------------------------------
inline const Vector3D & FreeTransformableObject::getScale() const
{	return m_scale;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
