#pragma once

#include "AbyssRenderer.h"

ABYSS_BEGIN_NAMESPACE

class BlurRenderer : public	Renderer
{
public:
	explicit BlurRenderer(const std::string& expectedInputTextureName, bool isHorizontalDirection);
	~BlurRenderer();

	static BlurRenderer* getInstance();

	// Specifies the expected input texture name from input RenderingResult, which will be blurred:
	void setExpectedTextureInput(const std::string& textureName);
	const std::string& getExpectedTextureInput() const;

	// Specifies whether blurring is occur in horizontal or vertical direction:
	void setBlurDirection(bool isHorizontalDirection);
	bool getBlurDirection() const;

	virtual bool isSupports(const RenderableObject * const renderable) const override;
	virtual void beforeRender(RenderableObject * const renderable) override;
	virtual void afterRender(RenderableObject * const renderable) override;

	virtual void startRendering(RenderingResultPtr input) override;
	virtual void finishRendering(RenderingResultPtr output) override;


private:
	bool m_blurDirection;
	std::string m_expectedTextureInput;
};


// INLINE METHODS:
//-------------------------------------------------------------------------------------------------
inline void BlurRenderer::setBlurDirection(bool isHorizontalDirection)
{	m_blurDirection = isHorizontalDirection;	}
//-------------------------------------------------------------------------------------------------
inline bool BlurRenderer::getBlurDirection() const
{	return m_blurDirection;	}
//-------------------------------------------------------------------------------------------------
inline void BlurRenderer::setExpectedTextureInput(const std::string& textureName)
{	m_expectedTextureInput = textureName;	}
//-----------------------------------------------------------------------------------------------
inline const std::string& BlurRenderer::getExpectedTextureInput() const
{	return m_expectedTextureInput;	}
//-----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE