#include "Abyss.h"

#include <cassert>
#include <iostream>

ABYSS_BEGIN_NAMESPACE

Exception::Exception(const std::string& str)
	: m_what(str)
{
	
}

const char* Exception::what() const
{
	return m_what.c_str();
}

void showErrorMessage(const String & message)
{
	std::cout << "ERROR: " << message << "\n";
}

void raiseAnError(const String & message)
{
#ifdef _DEBUG
	assert(!message.c_str());
#else
	throw Exception(message);
#endif
}

bool verifyStatement(bool statement, const String & message)
{
	if (!statement)
		raiseAnError(message);

	return statement;
}


ABYSS_END_NAMESPACE