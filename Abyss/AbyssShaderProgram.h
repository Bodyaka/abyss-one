#pragma once

#include <set>
#include <vector>
#include <map>

#include "AbyssGlm.h"
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class ShaderProgram
{
	friend class ShaderProgramManager;
public:
	typedef std::map<String, GLint> DataMap;
	typedef DataMap::value_type		DataMapValueType;
	typedef DataMap::const_iterator DataMapConstIterator;

public:
	// Begin shader program.
	void bind();
	// End shader Program.
	void release();

	// Some gl operations on program. Relinks program, if some changes are made.
	// Return true, if linking succeed, else - false.
	bool link();
	// Rebuild's shader program (recompiles program and relink).
	bool rebuild();

	// All uniform setters will check whether uniform value is set good.
	// Sets uniform by specified float value.
	bool setUniform(const String & name, const float value);
	// Sets uniform int value.
	bool setUniform(const String & name, const int value);
	// Sets uniform Vector2d data.
	bool setUniform(const String & name, const Vector2D & vec);
	// Sets uniform Vector3d data.
	bool setUniform(const String & name, const Vector3D & vec);
	// Sets uniform Vector4d data.
	bool setUniform(const String & name, const Vector4D & vec);
	// Sets matrix2d data:
	bool setUniform(const String & name, const Matrix2D & matrix);
	// Sets matrix3d data:
	bool setUniform(const String & name, const Matrix3D & matrix);
	// Sets matrix4d data:
	bool setUniform(const String & name, const Matrix4D & matrix);
	// Set array of Vector2D data.
	bool setUniform(const String& name, const Vector2D* data, size_t size);
	// Set array of Vector3D data.
	bool setUniform(const String& name, const Vector3D* data, size_t size);
	// Set array of Vector4D data.
	bool setUniform(const String& name, const Vector4D* data, size_t size);

	// Set specified frag data location.
	// After that, you can relink program via linkProgram explicity, 
	// or after another begin clause, program links automatically
	void setFragmentDataLocation(const String & name, size_t location);
	size_t getFragmentDataLocation(const String& name);

	// Return uniform location of specified uniform value.
	int getUniformLocation(const String & name) const;

	// Find Specified uniform.
	DataMapConstIterator findUniform(const String & str) const;
	// Return uniforms.
	const DataMap & getUniforms() const;

	// Check, whether this ShaderProgram has specified Uniform.
	bool hasUniform(const String & str) const;

	// ATTRIBUTES
	// Bind attribute to specified location.
	void bindAttributeLocation(const String & attributeName, int attributeLocation);
	// Return attribute location of specified attribute.
	int getAttributeLocation(const String & name) const;
	// Return attributes:
	const DataMap & getAttributes() const;

	// Return OpenGL program id.
	int getProgramId() const;
	// Return whether this one ShaderProgram is valid or not.
	bool isValid() const;
	// Name of the shader.
	const String & getName() const;

	// FileName of shader's file, where it is compiled.
	std::string getShaderFileName(GLenum shaderType) const;

	// Adds custom define for shader program. If such define already presents in the ShaderProgram, false will be returned and nothing happens.
	// In other case, the defines will be added right at the beginning (or after "version" keyword). And if isNeedRecompile is true, program will be immediately recompiled.
	// In second case, this method will return true.
	bool addCustomDefine(GLenum shaderType, const std::string& strDefine, bool isNeedRecompile = true);
	// Removes custom define from shader program. Method's signature same as in "addCustomDefine".
	bool removeCustomDefine(GLenum shaderType, const std::string& strDefine, bool isNeedRecompile = true);
	// Check whether specified custom define is defined in the program:
	bool isDefined(GLenum shaderType, const std::string& customDefineStr) const;
	
	// Returns set of all custom defines. 
	const std::set<std::string>& getAllCustomDefines(GLenum shaderType) const;

	// Returns supported shader types of this ShaderProgram:
	const std::set<GLenum>& getSuportedShaderTypes() const;

	// Get data of specified shader file.
	static String getShaderSourceCode(const String & shaderFilePath);
	// Converts specified shader source from Abyss pragma processor into native OpenGL:
	static String getConvertedNativeSourceCode(const String & sourceCode, const String & shaderFilePath);

private:

	void writeDebugInfo(const std::string& shaderFilePath, const std::string& sourceCode);

	// Shader program constructor. This constructor is called only by shader Manager.
	ShaderProgram(const String & name, const String & vsName, const String & fsName);
	// Delete shader program.
	~ShaderProgram();

	// Creating shader with specified attributes.
	void setup(const String & vsName, const String & fsName);
	
	bool compileShader(GLenum shaderType, const std::string& shaderFileName);
	// Compile shader source code and return whether shader is compiled successfully.
	bool compileShaderSourceCode(const String & code, GLenum shaderType);
	
	// Check, whether compiling of specified shader was ok:
	bool checkCompileStatus(GLuint shaderId, GLenum shaderType) const;
	// Check whether program linking was successful:
	bool checkLinkStatus(GLuint programId) const;

	// Setup locations (handles, ids) of uniforms and vertex attributes:
	void setupDataLocations();
	// Acquire all active uniforms locations.
	void setupActiveUniformsLocations();
	// Acquire all vertex attribute locations in shader program:
	void setupVertexAttributesLocations();

	GLuint getShaderId(GLenum shaderType) const;

	// Deletes specified shader and returns whether shader was successfully deleted.
	bool deleteShader(GLenum shaderType);

	void addCustomDefinesToSourceCode(GLenum shaderType, std::string& shaderSourceCode) const;


private: // Members section:
	// Program handle.
	GLuint m_programId;
	std::map<GLenum, GLuint> m_shaderIds;

	// Name of the Shader Program.
	String m_name;
	// Valid status of ShaderProgram.
	bool m_valid;
	// Some flags:
	bool m_isNeedRelink;
	// Map of Uniforms
	DataMap m_uniforms;
	// Map of attributes
	DataMap m_attributes;

	// Names of shader's file sources:
	std::map<GLenum, std::string> m_shadersFileNames;

	// Set of all defines:
	typedef std::set<std::string> CustomDefinesSetType;
	std::map<GLenum, CustomDefinesSetType> m_customDefines;
};

// INLINE METHODS.
//---------------------------------------------------------------------------------------------
inline int ShaderProgram::getProgramId() const
{	return m_programId;	}
//---------------------------------------------------------------------------------------------
inline const String & ShaderProgram::getName() const
{	return m_name;	}
//---------------------------------------------------------------------------------------------
inline bool ShaderProgram::isValid() const
{	return m_valid;	}
//---------------------------------------------------------------------------------------------
inline const ShaderProgram::DataMap & ShaderProgram::getUniforms() const
{	return m_uniforms;	}
//---------------------------------------------------------------------------------------------
inline const ShaderProgram::DataMap & ShaderProgram::getAttributes() const
{	return m_attributes;	}
//---------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
