#include "AbyssCameraFreeFlyEventAction.h"

#include "AbyssCamera.h"
#include "AbyssMouseEvent.h"
#include "boost/math/constants/constants.hpp"

ABYSS_BEGIN_NAMESPACE

//-------------------------------------------------------------------------------------------------
CameraFreeFlyEventAction::CameraFreeFlyEventAction(CameraPtr camera, float cameraRotatesMultiplier)
	: MouseEventAction()
	, m_camera(camera)
	, m_cameraRotatesMultiplier(cameraRotatesMultiplier)
{
}
//-------------------------------------------------------------------------------------------------
void CameraFreeFlyEventAction::perform(const MouseEventPtr & event)
{
	if (!Mouse::isButtonPressed(Mouse::LeftButton))
		return;

	const Vector2D & offset = event->getMouseMoveOffset();
	float moveOffsetMultiplier = getCameraRotatesMultiplier() / boost::math::constants::pi<float>();
	float dYaw  = -offset.x * moveOffsetMultiplier;
	float dRoll = offset.y * moveOffsetMultiplier;

	const Vector3D & eye = m_camera->getEyePosition();
	const Vector3D & center = m_camera->getCenterPosition();

	Vector3D distance = center - eye;

	float x = distance.x;
	float y = distance.y;
	float z = distance.z;

	float xx = x*x;
	float yy = y*y; 
	float zz = z*z; 

	float radius = sqrt(xx + yy + zz);

	static const float epsilon = 0.00001f;
	if (std::abs(z) < epsilon)
		z = epsilon;
	if (std::abs(radius) < epsilon)
		radius = epsilon;

	float newYaw = atan2(x, z) + dYaw;
	float newRoll = acos(y/radius) + dRoll;

	float sinYaw = sin(newYaw);
	float cosYaw = cos(newYaw);
	float sinRoll = sin(newRoll);
	float cosRoll = cos(newRoll);

	z = radius * sinRoll * cosYaw;
	x = radius * sinRoll * sinYaw;
	y = radius * cosRoll;

	m_camera->setCenterPosition(eye + Vector3D(x, y, z));
}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE