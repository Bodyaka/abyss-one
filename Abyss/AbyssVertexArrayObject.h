#pragma once

#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class VertexArrayObject
{
public:
public:
	// Create new Vertex array object
	static VertexArrayObjectPtr createVAO();
	// Destructor:
	~VertexArrayObject();

	// Bind current VAO
	void bind() const;
	// Release VAO (similar to bind(0)
	void release() const;

	// ACCESS
	// Return VAO index in OpenGL context.
	size_t getId() const;

	// INQUIRY

private:
	// Private Constructor
	VertexArrayObject();
	// Purposely non-implemented
	VertexArrayObject(const VertexArrayObject & other);
	// Purposely non-implemented
	VertexArrayObject & operator=(const VertexArrayObject & other);

	// Id of VAO in OpenGL content
	size_t m_id;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline size_t VertexArrayObject::getId() const
{	return m_id;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE