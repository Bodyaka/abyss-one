#pragma once

#include "Abyss.h"
#include "AbyssGlm.h"

ABYSS_BEGIN_NAMESPACE

class ApplicationContext;

// Mouse object itself.
// Can return current and previous positions of Mouse and pressed buttons.
class Mouse
{
	friend class SfmlEventsAdapter;
public:

	enum ButtonType
	{
		UnknownButton = -1,
		NoneButton = 0,
		LeftButton,
		MiddleButton,
		RightButton
	};

	static const Vector2D & getCurrentPosition();
	static const Vector2D & getPreviousPosition();
	static bool isButtonPressed(ButtonType button);

private:
	static void addToPressedButtons(ButtonType button);
	static void removeFromPressedButtons(ButtonType button);
	static void setMousePosition(const Vector2D & position);

	Mouse();
	~Mouse();
};

ABYSS_END_NAMESPACE