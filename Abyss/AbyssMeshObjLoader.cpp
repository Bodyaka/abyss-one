#include "AbyssMeshObjLoader.h"

#include <vector>
#include <fstream>
#include "boost/lexical_cast.hpp"

#include "AbyssMesh.h"
#include "AbyssUtilities.h"

ABYSS_BEGIN_NAMESPACE

namespace
{
	const int    VERTEX_COMPONENTS_NUMBER = 4;
	const String OTHER_OBJ_DELIM = "o";
	const String VERTEX_POSITION_DELIM = "v";
	const String VERTEX_TEXEL_DELIM	= "vt";
	const String VERTEX_NORMAL_DELIM = "vn";
	const String FACE_DELIM = "f";

	GLenum getDrawModeByVerticesPerFace(size_t verticesPerFace)
	{
		switch(verticesPerFace)
		{
			case 1:
				return GL_POINTS;
			case 2:
				return GL_LINES;
			case 3:
				return GL_TRIANGLES;
			case 4:
				return GL_QUADS;
		}

		assert("Invalid vertices count per face");
		return GL_NONE;
	}

}


//----------------------------------------------------------------------------------------------
MeshObjLoader * MeshObjLoader::instance()
{
	static MeshObjLoader sInstance;

	return &sInstance;
}
//----------------------------------------------------------------------------------------------
MeshPtr MeshObjLoader::performLoadingMeshFromFile(const String & meshFileName)
{
	return loadDataFromFile(meshFileName);
}
//----------------------------------------------------------------------------------------------
MeshPtr MeshObjLoader::loadDataFromFile(const String & meshFileName)
{
	Mesh::VertexData tempData, outputData;
	std::vector<size_t> facesIndices;

	std::ifstream file(meshFileName);

	// If file could not be opened, show error message.
	if (!file.is_open())
		throw std::exception(String("Could not open file : " + meshFileName).c_str());

	String line;
	size_t totalVerticesCount = 0,
		   verticesCountPerFace = 0;
		   
	MeshPtr mesh = MeshPtr(new Mesh(GL_TRIANGLES));
	while (getline(file, line))
	{
		if (isComment(line))
			continue;

		std::vector<String> splittedStrings = Utilities::split(line, ' ', true);

		if (!isValidObjLines(splittedStrings))
			continue;
		
		const String & keyString = splittedStrings.at(0);
		if (keyString == VERTEX_POSITION_DELIM)
			tempData.positions.push_back(readVector3D(splittedStrings.at(1), splittedStrings.at(2), splittedStrings.at(3)));
		else if (keyString == VERTEX_TEXEL_DELIM)
			tempData.texels.push_back(readVector2D(splittedStrings.at(1), splittedStrings.at(2)));
		else if (keyString == VERTEX_NORMAL_DELIM)
			tempData.normals.push_back(readVector3D(splittedStrings.at(1), splittedStrings.at(2), splittedStrings.at(3)));
		else if (keyString == FACE_DELIM)
		{
			// TODO: need some better solution.
			static const int faceVertexPositionId = 0;
			static const int faceVertexTexelId  = 1;
			static const int faceVertexNormalId = 2;
			
			size_t oldVerticesCountPerFace = verticesCountPerFace;
			// Gets vertices' count per face. We doesn't want include face delimeter here.
			verticesCountPerFace = splittedStrings.size() - 1;

			// Check, whether given file is valid:
			if (!outputData.positions.empty() && verticesCountPerFace != oldVerticesCountPerFace)
				throw std::exception("Bad obj file format: Number of vertices per face is different!");

			for (size_t i = 1; i<splittedStrings.size(); ++i)
			{
				std::vector<String> faces = Utilities::split(splittedStrings[i], '/', false);

				// Obj lines are stored as
				// f: v v v
				// f: v/t// v/t// v/t//
				// f: v//n  v//n  v//n
				// f: v/t/n v/t/n v/t/n
				// see http://uk.wikipedia.org/wiki/Obj
				// Processing vertex position index.
				if (!faces[faceVertexPositionId].empty())
				{
					int vertexPositionIndex = boost::lexical_cast<int>(faces[faceVertexPositionId]);
					// Converts vertex position index from global into local index.
					vertexPositionIndex -= totalVerticesCount + 1; 
					const Vector3D & position = tempData.positions.at(vertexPositionIndex);
					outputData.positions.push_back(position);
				}
				// Processing texels index
				if (faces.size() > faceVertexTexelId && !faces[faceVertexTexelId].empty())
				{
					int texelIndex = boost::lexical_cast<int>(faces[faceVertexTexelId]) - 1;
					const Vector2D & texel = tempData.texels.at(texelIndex);
					outputData.texels.push_back(texel);
				}
				// Processing vertex normal index
				if (faces.size() > faceVertexNormalId && !faces[faceVertexNormalId].empty())
				{
					int normalIndex = boost::lexical_cast<int>(faces[faceVertexNormalId]) - 1;
					const Vector3D & normal = tempData.normals.at(normalIndex);
					outputData.normals.push_back(normal);
				}
				
			}
		}
		else if (keyString == OTHER_OBJ_DELIM && !outputData.positions.empty()) // 
		{
			Mesh * subMesh = createSubMesh(outputData, mesh);
			subMesh->setDrawMode(getDrawModeByVerticesPerFace(verticesCountPerFace));

			totalVerticesCount += outputData.positions.size();

			// Clearing parameters.
			tempData.clear();
			outputData.clear();
			verticesCountPerFace = 0;
		}
	}

	setDataForMesh(outputData, mesh);
	mesh->setDrawMode(getDrawModeByVerticesPerFace(verticesCountPerFace));

	return mesh;
}
//--------------------------------------------------------------------------------------------------
bool MeshObjLoader::isComment(const String & fileLine) const
{
	return fileLine[0] == '#';
}
//--------------------------------------------------------------------------------------------------
bool MeshObjLoader::isValidObjLines(const std::vector<String> & strings) const
{
	if (strings.empty())
		return false;
	
	return (strings.at(0) == VERTEX_POSITION_DELIM ||
			strings.at(0) == VERTEX_TEXEL_DELIM ||
			strings.at(0) == VERTEX_NORMAL_DELIM ||
			strings.at(0) == FACE_DELIM);
}
//--------------------------------------------------------------------------------------------------
Vector2D MeshObjLoader::readVector2D(const String & string_x, const String & string_y) const
{
	float x = boost::lexical_cast<float, String>(string_x);
	float y = boost::lexical_cast<float, String>(string_y);

	return Vector2D(x, y);
}
//--------------------------------------------------------------------------------------------------
Vector3D MeshObjLoader::readVector3D(const String & string_x, const String & string_y, const String & string_z) const
{
	float x = boost::lexical_cast<float, String>(string_x);
	float y = boost::lexical_cast<float, String>(string_y);
	float z = boost::lexical_cast<float, String>(string_z);

	return Vector3D(x, y, z);
}
//--------------------------------------------------------------------------------------------------
Mesh * MeshObjLoader::createSubMesh(const Mesh::VertexData & data, MeshPtr mesh)
{
	size_t childrenSize = mesh->getChildren().size();
	const String & subMeshName = mesh->getName() + boost::lexical_cast<String>(childrenSize);

	return mesh->createSubMesh(subMeshName, GL_STATIC_DRAW, data);
}
//--------------------------------------------------------------------------------------------------
void MeshObjLoader::setDataForMesh(const Mesh::VertexData & data, MeshPtr mesh)
{
	if (mesh->hasChildren())
		createSubMesh(data, mesh);
	else
		mesh->setVertexData(data);
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE