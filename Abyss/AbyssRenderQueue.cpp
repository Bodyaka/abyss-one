#include "AbyssRenderQueue.h"
#include "AbyssSceneObject.h"
#include "AbyssRenderPass.h"
#include "AbyssSceneManager.h"
#include "AbyssRenderPassManager.h"
#include "AbyssRenderingResult.h"

ABYSS_BEGIN_NAMESPACE



RenderQueue::RenderQueue(SceneManager* manager)
	: m_manager(manager)
	, m_visitor(new RenderQueueVisitor(this))
	, m_renderQueueGroups(RenderPassComparator(manager->getRenderPassManager()))
	, m_renderingResult(new RenderingResult())
{

}

void RenderQueue::addRenderable(RenderPass* pass, RenderableObject* renderable)
{
	m_renderQueueGroups[pass].push_back(renderable);
}

RenderQueueVisitor* RenderQueue::getVisitor() const
{
	return m_visitor.get();
}

void RenderQueue::prepareForRendering(SceneObject* object)
{
	object->prepareForRendering(getVisitor());
}

void RenderQueue::performRendering()
{
	m_renderingResult->clear();

	for (const auto& renderPassIter : m_renderQueueGroups)
	{
		auto renderPass = renderPassIter.first;
		const auto& renderables = renderPassIter.second;

		renderPass->startRendering(m_renderingResult);

		for (const auto& renderable : renderables)
			renderPass->render(renderable);

		renderPass->finishRendering(m_renderingResult);
	}
}

void RenderQueue::clear()
{
	m_renderQueueGroups.clear();
}


// ----------------------------- RenderQueueVisitor ------------------------------


RenderQueueVisitor::RenderQueueVisitor(RenderQueue* queue)
	: m_renderQueue(queue)
{

}

void RenderQueueVisitor::visit(RenderPass* pass, RenderableObject* object)
{
	m_renderQueue->addRenderable(pass, object);
}

ABYSS_END_NAMESPACE