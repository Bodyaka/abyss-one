#pragma once

#include "Abyss.h"
#include "AbyssEvent.h"
#include "AbyssMouse.h"
#include "SFML/Window/Event.hpp"
#include "SFML/Window/Mouse.hpp"

ABYSS_BEGIN_NAMESPACE

class SfmlEventsAdapter
{
public:
	// Converts input sfml event into Abyss event.
	// If no event can be handled, return nullptr event.
	static EventPtr convertToAbyssEvent(const sf::Event & smflEvent);
	
	// Creates button from SFML button type.
	// In future will be deleted.
	static Mouse::ButtonType fromSfmlButton(sf::Mouse::Button button);

private:
	static KeyEventPtr createAbyssKeyPressEvent(const sf::Event & sfmlEvent);
	static KeyEventPtr createAbyssKeyReleaseEvent(const sf::Event & sfmlEvent);
	static MouseEventPtr createAbyssMouseMoveEvent(const sf::Event & sfmlEvent);
	static MouseEventPtr createAbyssMousePressEvent(const sf::Event & sfmlEvent);
	static MouseEventPtr createAbyssMouseReleaseEvent(const sf::Event & sfmlEvent);
	static MouseWheelEventPtr createAbyssMouseWheelEvent(const sf::Event & sfmlEvent);

	// TODO: need make it deleted
	SfmlEventsAdapter();
	// TODO: need make it deleted
	~SfmlEventsAdapter();
};

ABYSS_END_NAMESPACE