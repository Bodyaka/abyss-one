#pragma once

#include "AbyssTransformableObject.h"

ABYSS_BEGIN_NAMESPACE

// Be carefully with this object.
// Setting position will change only model matrix of transform object.
// If you want change other matrices, you need to update manually, calling updateTransform or calling SceneManager
class MovableObject
	: public TransformableObject
{
public:
	MovableObject();
	~MovableObject();

	void move(const Vector3D & offset);

	void setPosition(const Vector3D & position);
	
	const Vector3D & getPosition() const;

protected:

	virtual void updateModelMatrix_impl();

	Vector3D m_position;
};

// INLINE
//----------------------------------------------------------------------------------------------
inline const Vector3D & MovableObject::getPosition() const
{	return m_position;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE