#include "AbyssLight.h"
#include "AbyssLightsManager.h"
#include "AbyssSceneObject.h"
#include "AbyssCamera.h"
#include "AbyssSceneManager.h"

ABYSS_BEGIN_NAMESPACE

Light::Light(const std::string& name)
	: m_name(name)
	, m_attenutation(1.f, 1.f, 1.f)
	, m_color(Vector4D(1.f))
	, m_direction(Vector3D(0.f, 0.f, 1.f))
	, m_isActive(true)
	, m_isVisible(true)
	, m_lightType(LightType::PointLight)
	, m_creator(nullptr)
	, m_attachedSceneObject(nullptr)
	, m_innerCutOffAngle(glm::radians(12.5f))
	, m_outerCutOffAngle(glm::radians(17.5f))
{
}

Light::~Light()
{

}

void Light::setAttenutation(float constantFactor, float linearFactor, float quadraticFactor)
{
	m_attenutation = Vector3D(constantFactor, linearFactor, quadraticFactor);

	setChanged(true);
}

void Light::setColor(const Vector4D& color)
{
	m_color = color;

	setChanged(true);
}

void Light::setActive(bool activeFlag)
{
	if (isActive() == activeFlag)
		return;

	m_isActive = activeFlag;

	setChanged(true);
}

void Light::setVisible(bool visibility)
{
	if (isVisible() == visibility)
		return;

	m_isVisible = visibility;

	setChanged(true);
}

void Light::setType(LightType lightType)
{
	if (getLightType() == lightType)
		return;

	m_lightType = lightType;

	setChanged(true);
}

void Light::setCreator(LightsManager* manager)
{
	m_creator = manager;
}

void Light::updateTransform()
{
	SceneManager* sceneManager = SceneManager::instance();
	auto camera = sceneManager->getCamera();

	auto attachedSceneObject = getAttachedSceneObject();
	Matrix4D worldMatrix = attachedSceneObject ? attachedSceneObject->getWorldMatrix() : cIdentityMatrix4D;

	TransformableObject::updateTransform(camera->getProjectionMatrix(), camera->getViewMatrix(), worldMatrix);
	
	// Should we do anything?
}

void Light::setRenderableObject(RenderableObjectPtr object)
{
	m_renderableObject = object;
}

void Light::setDirection(const Vector3D& direction)
{
	m_direction = direction;
}

void Light::attachToSceneObject(SceneObject* object)
{
	if (object == getAttachedSceneObject())
		return;

	if (auto oldAttachedSceneObject = getAttachedSceneObject())
		oldAttachedSceneObject->removeLight(getName());
	
	m_attachedSceneObject = object;

	if (auto newAttachedSceneObject = getAttachedSceneObject())
		newAttachedSceneObject->addLight(this);
}

void Light::deattachFromSceneObject()
{
	attachToSceneObject(nullptr);
}

void Light::setInnerCutOffAngle(float radians)
{
	if (getInnerCutOffAngle() == radians) // TODO: need floating point comparison!
		return;

	m_innerCutOffAngle = radians;
	
	setChanged(true);
}

void Light::setOuterCutOffAngle(float radians)
{
	if (getOuterCutOffAngle() == radians) // TODO: need floating point comparison!
		return;

	m_outerCutOffAngle = radians;

	setChanged(true);
}

ABYSS_END_NAMESPACE