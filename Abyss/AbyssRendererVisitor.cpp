#include "AbyssRendererVisitor.h"
#include "AbyssMesh.h"

#include "AbyssRenderer.h"
#include "AbyssRendererExtension.h"

ABYSS_BEGIN_NAMESPACE


RendererVisitor::RendererVisitor(RendererWeakPtr renderer)
	: m_renderer(renderer)
{

}

void RendererVisitor::visit(Mesh* mesh)
{
	for (auto extension : getRenderer()->m_extensions)
		extension->beforeRendering(mesh);

	mesh->draw();

	for (auto extension : getRenderer()->m_extensions)
		extension->afterRendering(mesh);
}

RendererVisitor::RendererWeakPtr RendererVisitor::getRenderer() const
{
	return m_renderer;
}

ABYSS_END_NAMESPACE