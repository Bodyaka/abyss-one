#pragma once

#include "AbyssGlm.h"
#include "AbyssEvent.h"
#include "AbyssMouse.h"

ABYSS_BEGIN_NAMESPACE

class MouseEvent
	: public Event
{

public:

	// FABRICS
	static MouseEventPtr createMouseMoveEvent(const Vector2D & position, Mouse::ButtonType button = Mouse::NoneButton);
	static MouseEventPtr createMousePressEvent(const Vector2D & position, Mouse::ButtonType button = Mouse::NoneButton);
	static MouseEventPtr createMouseReleaseEvent(const Vector2D & position, Mouse::ButtonType button = Mouse::NoneButton);
	static MouseEventPtr createMouseDoubleClickEvent(const Vector2D & position, Mouse::ButtonType button = Mouse::NoneButton);

	// Return current mouse position.
	inline const Vector2D & getMousePosition() const;

	// Return pressed mouse button.
	inline Mouse::ButtonType getMouseButton() const;

	// Return offset between last mouse position and current position.
	// Meaningful only for mouseMove.
	inline const Vector2D & getMouseMoveOffset() const;

protected:

	// Constructor
	MouseEvent(const Vector2D & position, Mouse::ButtonType button, Type eventType);

	Vector2D m_mousePosition,
			 m_mouseMoveOffset;

	// Pressed button:
	Mouse::ButtonType m_mouseButton;
};

//-------------------------------------------------------------------------------------------------
const Vector2D & MouseEvent::getMousePosition() const
{	return m_mousePosition; }
//-------------------------------------------------------------------------------------------------
const Vector2D & MouseEvent::getMouseMoveOffset() const
{   return m_mouseMoveOffset;	}
//-------------------------------------------------------------------------------------------------
Mouse::ButtonType MouseEvent::getMouseButton() const
{	return m_mouseButton;	}
//-------------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE