#pragma once

#include <map>
#include <queue>
#include <vector>
#include "AbyssRendering.h"
#include "AbyssRenderPass.h"
#include "AbyssRenderPassComparator.h"

ABYSS_BEGIN_NAMESPACE

class Renderer;
class SceneManager;
class SceneObject;

class RenderQueue
{
	friend class RenderQueueVisitor;

public:
	RenderQueue(SceneManager* manager);

	void prepareForRendering(SceneObject* object);
	void performRendering();
	void clear();

	RenderQueueVisitor* getVisitor() const;

private:

	void addRenderable(RenderPass* pass, RenderableObject* renderable);

	typedef std::vector<RenderableObject*> RenderQueueGroupType;

	std::unique_ptr<RenderQueueVisitor> m_visitor;
	SceneManager* m_manager;
	std::map<RenderPass*, RenderQueueGroupType, RenderPassComparator> m_renderQueueGroups;

	RenderingResultPtr m_renderingResult;
};


class RenderQueueVisitor
{
public:
	RenderQueueVisitor(RenderQueue* queue);

	void visit(RenderPass* pass, RenderableObject* object);

private:
	RenderQueue* m_renderQueue;
};

ABYSS_END_NAMESPACE