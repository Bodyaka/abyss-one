#include "AbyssMeshLoader.h"

#include "AbyssMeshObjLoader.h"

ABYSS_BEGIN_NAMESPACE

MeshLoader * MeshLoader::m_loader = nullptr;

MeshLoader::MeshLoader()
{}

MeshLoader::~MeshLoader()
{}

void MeshLoader::setDefaultLoader(MeshLoader * loader)
{
	m_loader = loader;
}

MeshLoader * MeshLoader::getDefaultLoader()
{
	if (!m_loader)
		m_loader = MeshObjLoader::instance();
	return m_loader;
}

MeshPtr MeshLoader::load(const String & meshFileName, const String & meshName /* = "" */)
{
	MeshPtr mesh(nullptr);
	try
	{
		return performLoadingMeshFromFile(meshFileName);
	}
	catch (const std::exception & exc)
	{
		showErrorMessage(exc.what());
	}

	return MeshPtr();
}

ABYSS_END_NAMESPACE