#include "AbyssTransformableObject.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
TransformableObject::TransformableObject()
	: ChangeableObject()
	, m_modelMatrix(cIdentityMatrix4D)
	, m_worldMatrix(cIdentityMatrix4D)
	, m_normalMatrix(cIdentityMatrix3D)
	, m_worldViewMatrix(cIdentityMatrix4D)
	, m_wvpMatrix(cIdentityMatrix4D)
	, m_isAutoUpdateEnabled(true)
{
}
//----------------------------------------------------------------------------------------------
void TransformableObject::updateTransform(const Matrix4D & projectionMatrix, const Matrix4D & viewMatrix, const Matrix4D & worldMatrix)
{
	m_worldMatrix = worldMatrix * m_modelMatrix;
	m_worldViewMatrix = viewMatrix * m_worldMatrix;
	Matrix3D worldViewSubMatrix = Matrix3D(getWorldViewMatrix());
	m_normalMatrix = glm::transpose(glm::inverse(worldViewSubMatrix));
	m_wvpMatrix = projectionMatrix * m_worldViewMatrix;

	setChanged();
}
//----------------------------------------------------------------------------------------------
void TransformableObject::setModelMatrix(const Matrix4D & matrix)
{
	m_modelMatrix = matrix;

	if (isAutoUpdateEnabled())
		updateTransform();

	setChanged();
}
//----------------------------------------------------------------------------------------------
bool TransformableObject::setAutoUpdateEnabled(bool flag)
{
	bool oldValue = isAutoUpdateEnabled();
	m_isAutoUpdateEnabled = flag;

	if (m_isAutoUpdateEnabled)
		updateTransform();

	return oldValue;
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE