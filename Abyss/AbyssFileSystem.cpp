#include "AbyssFileSystem.h"
#include <fstream>

#include <windows.h>

ABYSS_BEGIN_NAMESPACE


FileSystem::FileSystem()
{

}

FileSystem::~FileSystem()
{

}

std::string FileSystem::getDirectory(const std::string& filePath) const
{
	std::string dirPath = filePath;
	auto location = dirPath.find_last_of(getDelimeter());
	if (location != std::string::npos)
		return dirPath.substr(0, location);
	
	return std::string("");
}


std::string FileSystem::getFileName(const std::string& filePath) const
{
	std::string dirPath = getDirectory(filePath);
	if (dirPath.empty())
		return filePath;

	return filePath.substr(dirPath.size() + 1); // 1 is for delimeter, because in other case it will be included in the fileName!
}


char FileSystem::getDelimeter()
{
#if defined(_WIN32) || defined(_WIN64)
	return '\\';
#else
	return '/';
#endif
}

void FileSystem::appendPathEntry(std::string& filePath, const std::string& pathEntry) const
{
	filePath += getDelimeter() + pathEntry;
}

bool FileSystem::writeToFile(const std::string& filePath, const std::string& contents, bool overwrite /*= true*/)
{
	// OFSTREAM doesn't work, when filePath contains directories, which doesn't exists!
	auto dir = getDirectory(filePath);
	CreateDirectory(dir.c_str(), NULL);

	std::ofstream fileStream(filePath);
	
	/*
	if (overwrite)
		fileStream.open(filePath);
	else
		fileStream.open(filePath, std::ios::out | std::ios::app);
		*/

	if (!fileStream.is_open())
		return false;

	fileStream << contents;
	fileStream.close();

	return true;

}

ABYSS_END_NAMESPACE