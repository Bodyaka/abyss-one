#include "AbyssAssimpLoader.h"

#include <boost/format.hpp>
#include <fstream>

#include <assimp/Importer.hpp>	//OO version Header!
#include <assimp/PostProcess.h>
#include <assimp/Scene.h>
#include <assimp/material.h>

#include "AbyssMaterial.h"
#include "AbyssTextureManager.h"
#include "AbyssUtilities.h"
#include "AbyssMeshUtilities.h"
#include "AbyssAssimpProgressHandler.h"

ABYSS_BEGIN_NAMESPACE

AssimpLoader::AssimpLoader()
{
}

AssimpLoader::~AssimpLoader()
{
}

AssimpLoader * AssimpLoader::instance()
{
	static AssimpLoader * sInstance = nullptr;
	if (!sInstance)
		sInstance = new AssimpLoader();

	return sInstance;
}

void AssimpLoader::makeAsDefaultLoader()
{
	setDefaultLoader(instance());
}

MeshPtr AssimpLoader::performLoadingMeshFromFile(const String & meshFileName)
{
	std::ifstream file(meshFileName);

	// If file could not be opened, show error message.
	if (!file.is_open())
		throw std::exception(String("Could not open file : " + meshFileName).c_str());

	// Perform importing. Note, that if no loading is possible - it will show a message.
	Assimp::Importer importer;
	// Note, that progress handler will be deleted in Importer's dtor:
	AssimpProgressHandler* progressHandler = new AssimpProgressHandler();
	importer.SetProgressHandler(progressHandler);
	
	const aiScene * scene = importer.ReadFile(meshFileName, aiProcessPreset_TargetRealtime_Quality);
	if (!scene)
		throw std::exception(importer.GetErrorString());

	// TODO: Need rewrite loading functions in such manner, so, they will return not only on mesh object, but several ones.
	assert(scene->mNumMeshes > 0);

	m_totalVerticesCount = 0;
	m_totalFacesCount = 0;
	m_totalMeshesCount = scene->mNumMeshes;

	MeshPtr resultMesh = MeshPtr(new Mesh(GL_TRIANGLES));
	processAssimpNode(scene->mRootNode, scene, resultMesh, meshFileName);

	ABYSS_LOG((boost::format("Loading of file: %1%; VerticesCount: %2%; FacesCount: %3%; MeshesCount: %4%") % meshFileName
																											% m_totalVerticesCount
																											% m_totalFacesCount
																											% m_totalMeshesCount).str());

	return resultMesh;
}

GLenum AssimpLoader::getDrawMode(aiMesh * mesh)
{
	switch (mesh->mPrimitiveTypes)
	{
	case (aiPrimitiveType::aiPrimitiveType_POINT) :
		return GL_POINTS;
	case (aiPrimitiveType::aiPrimitiveType_LINE) :
		return GL_LINES;
	case (aiPrimitiveType::aiPrimitiveType_TRIANGLE) :
		return GL_TRIANGLES;
	}

	throw std::exception("Error: assimp mesh has unsupported primitive type.");
}

Mesh * AssimpLoader::createRawMeshFrom(aiMesh * mesh, MeshPtr parentMesh)
{
	GLenum drawMode = getDrawMode(mesh);

	Mesh * resultMesh = nullptr;
	
	if (parentMesh)
	{
		auto childrenCount = parentMesh->getChildren().size();
		resultMesh = parentMesh->createSubMesh(std::string(mesh->mName.C_Str()) + "_" + std::to_string(childrenCount), drawMode);
	}
	else
	{
		resultMesh = new Mesh(drawMode);
	}

	float * verticesPosition = nullptr,
		  * verticesTexels   = nullptr,
		  * verticesNormals  = nullptr,
		  * verticesTangents = nullptr,
		  * verticesBitangents = nullptr;

	if (mesh->HasPositions())
		verticesPosition = (float*)mesh->mVertices;

	if (mesh->HasNormals())
		verticesNormals = (float*)mesh->mNormals;

	// TODO: Need check on different texture coordinates set.
	std::vector<Vector2D> verticesTexelsVector;
	if (mesh->HasTextureCoords(0))
	{
		verticesTexelsVector.reserve(mesh->mNumVertices);
		for (unsigned int k = 0; k < mesh->mNumVertices; ++k)
		{
			Vector2D vertexTexel;
			vertexTexel.x = mesh->mTextureCoords[0][k].x;
			// We need invert texture coordinates, because OpenGL texture coordinates are from down to up.
			vertexTexel.y = 1 - mesh->mTextureCoords[0][k].y;

			verticesTexelsVector.push_back(vertexTexel);
		}

		//verticesTexels = (float*)mesh->mTextureCoords;
		verticesTexels = (float*)verticesTexelsVector.data();
	}

	int numOfIndices = mesh->mNumFaces * resultMesh->getVerticesSizePerPrimitive();
	ContainerUint indices;
	indices.reserve(numOfIndices);
	for (size_t f = 0; f < mesh->mNumFaces; ++f)
	{
		const aiFace & face = mesh->mFaces[f];
		assert(face.mNumIndices == 3);
		for (size_t i = 0; i < face.mNumIndices; ++i)
			indices.push_back(face.mIndices[i]);
	}

	
	// TODO: maybe, bitangents need to check here too.
	if (mesh->HasTangentsAndBitangents())
	{
		verticesTangents = (float*)mesh->mTangents;
		verticesBitangents = reinterpret_cast<float*>(mesh->mBitangents);
	}
	
	
	resultMesh->setVertexData(mesh->mNumVertices,
						   verticesPosition,
						   verticesTexels,
						   verticesNormals,
						   verticesTangents,
						   verticesBitangents);

	

	resultMesh->setIndices(indices);

	return resultMesh;
}

MaterialPtr AssimpLoader::createMaterialFrom(aiMaterial * assimpMaterial, const String & meshFileName)
{
	if (!assimpMaterial)
		return MaterialPtr();

	MaterialPtr material(new Material());

	// TODO: We are loading only diffusion texture.
	// In future it is need to load all types of maps.
	aiString texturePath;
	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath))
	{
		String path = Utilities::getDirectoryPath(meshFileName) + String(texturePath.C_Str());
		TexturePtr texture = TextureManager::getInstance()->createTexture(path);
		if (texture)
			material->setTexture(DiffuseTextureType, texture);
	}

	// TODO: need to define own normal map in material file!
	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_NORMALS, 0, &texturePath))
	{
		String path = Utilities::getDirectoryPath(meshFileName) + String(texturePath.C_Str());
		TexturePtr texture = TextureManager::getInstance()->createTexture(path);
		if (texture)
			material->setTexture(NormalMapTextureType, texture);
	}

	aiColor4D diffuse;
	if (AI_SUCCESS == aiGetMaterialColor(assimpMaterial, AI_MATKEY_COLOR_DIFFUSE, &diffuse))
		material->setDiffuseColor(Vector4D(diffuse.r, diffuse.g, diffuse.b, diffuse.a));

	// TODO: need to check whether it is ok to skip alpha value of ambient channel of material...
	aiColor4D ambient;
	if (AI_SUCCESS == aiGetMaterialColor(assimpMaterial, AI_MATKEY_COLOR_AMBIENT, &ambient))
		material->setAmbientColor(Vector3D(ambient.r, ambient.g, ambient.b));

	aiColor4D specular;
	if (AI_SUCCESS == aiGetMaterialColor(assimpMaterial, AI_MATKEY_COLOR_SPECULAR, &specular))
		material->setSpecularColor(Vector4D(specular.r, specular.g, specular.b, specular.a));

	// TODO: need add emission color for material:
	/*aiColor4D emission;
	if (AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_EMISSIVE, &emission))
		color4_to_float4(&emission, c);*/

	float shininess = 0.0;
	unsigned int max;
	if (AI_SUCCESS == aiGetMaterialFloatArray(assimpMaterial, AI_MATKEY_SHININESS, &shininess, &max))
		material->setShiness(shininess);
	
	return material;
}

void AssimpLoader::processAssimpNode(aiNode* node, const aiScene* scene, MeshPtr parentMesh, const String& meshFileName)
{
	m_totalMeshesCount += node->mNumMeshes;

	for (size_t i = 0; i < node->mNumMeshes; ++i)
	{
		aiMesh* assimpMesh = scene->mMeshes[node->mMeshes[i]];
		auto transformation = node->mTransformation;

		// TODO: need add support of non-identity transforms
		assert(transformation.IsIdentity());
		if (!transformation.IsIdentity())
		{
			std::string assimpTransformationStr;
			assimpTransformationStr += std::to_string(transformation.a1) + " " +
									   std::to_string(transformation.a2) + " " +
									   std::to_string(transformation.a3) + " " +
									   std::to_string(transformation.a4) + " \n";

			assimpTransformationStr += std::to_string(transformation.b1) + " " +
									   std::to_string(transformation.b2) + " " +
									   std::to_string(transformation.b3) + " " +
									   std::to_string(transformation.b4) + " \n";

			assimpTransformationStr += std::to_string(transformation.c1) + " " +
									   std::to_string(transformation.c2) + " " +
									   std::to_string(transformation.c3) + " " +
									   std::to_string(transformation.c4) + " \n";

			assimpTransformationStr += std::to_string(transformation.d1) + " " +
									   std::to_string(transformation.d2) + " " +
									   std::to_string(transformation.d3) + " " +
									   std::to_string(transformation.d4) + " \n";
		
			ABYSS_LOG(assimpTransformationStr);
		}
		
		
		Mesh* rawMesh = createRawMeshFrom(assimpMesh, parentMesh);;

		size_t materialIndex = assimpMesh->mMaterialIndex;
		aiMaterial * material = scene->mMaterials[materialIndex];
		rawMesh->setMaterial(createMaterialFrom(material, meshFileName));

		m_totalVerticesCount += assimpMesh->mNumVertices;
		m_totalFacesCount += assimpMesh->mNumFaces;
	}

	// TODO: need to create tree-like mesh structure.
	for (size_t i = 0; i < node->mNumChildren; ++i)
	{
		aiNode* child = node->mChildren[i];
		processAssimpNode(child, scene, parentMesh, meshFileName);
	}
}


ABYSS_END_NAMESPACE