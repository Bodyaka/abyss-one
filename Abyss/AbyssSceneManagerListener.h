#pragma once

#include "Abyss.h"

ABYSS_BEGIN_NAMESPACE

class SceneManager;

class SceneManagerListener
{
public:
	SceneManagerListener(SceneManager* manager);
	virtual ~SceneManagerListener();

	virtual void onRenderingStarted(SceneManager* manager) = 0;
	virtual void onRenderingFinished(SceneManager* manager) = 0;

	void setSceneManager(SceneManager* manager);
	SceneManager* getSceneManager() const;

private:
	SceneManager* m_sceneManager;
};

// INLINE METHODS
inline SceneManager* SceneManagerListener::getSceneManager() const
{
	return m_sceneManager;
}

ABYSS_END_NAMESPACE