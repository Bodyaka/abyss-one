#pragma once

#include "AbyssAABB.h"
#include "AbyssGeometryObject.h"

ABYSS_BEGIN_NAMESPACE

// Bounding box for drawing some data.
class BoundingBox
	: public GeometryObject
{
public:
	// Create Bbox.
	BoundingBox();
	// Create Bbox from specified AABB.
	BoundingBox(const AABB & box);
	// Destructor.
	~BoundingBox();

	// Set AABB for given BoundingBox.
	void setAABB(const AABB & box);

	// Render bbox:
	virtual void draw() override;

private:
	// Initialize VAO of rendereable object.
	void initBoundingBox();

	// Create and return positions container:
	ContainerV3D createPositionsContainer() const;
	// Create and return indices container:
	ContainerUint createIndicesContainer() const;

	// Setup attributes for bounding box container:
	void setupVertexData(const ContainerV3D & positions, const ContainerUint & indices);
};

// INLINE METHODS

ABYSS_END_NAMESPACE

