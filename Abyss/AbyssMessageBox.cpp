#include "AbyssMessageBox.h"

#ifdef _WIN32
#include <windows.h>
#ifdef MessageBox
#undef MessageBox
#endif
#endif

ABYSS_BEGIN_NAMESPACE

void MessageBox::showMessageBox(const std::string & title, const std::string & message)
{
#ifdef _WIN32
	MessageBoxA(nullptr, message.c_str(), title.c_str(), MB_OK | MB_ICONERROR);
#endif
}

ABYSS_END_NAMESPACE
