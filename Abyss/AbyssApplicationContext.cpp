
#include "GL/glew.h"
#include "SFGUI/Context.hpp"

#include "AbyssApplicationContext.h"

#include <time.h>
#include "SFML/Window/Event.hpp"

#include "AbyssUtilities.h"
#include "AbyssWindow.h"
#include "AbyssApplication.h"
#include "AbyssSfmlEventsAdapter.h"
#include "AbyssSchedulersManager.h"
#include "SFGUIManager.h"
#include "AbyssShaderProgramManager.h"
#include "FPSCounter.h"

#include "AbyssRendering.h"

#include "AbyssTexture.h"

static const std::string sWindowsFolderDelim = "\\";

ABYSS_BEGIN_NAMESPACE

ApplicationContext * ApplicationContext::m_sharedApplicationContext = nullptr;

//-------------------------------------------------------------------------------------------------
ApplicationContext::ApplicationContext(const CreateOptions & windowParams)
{
	srand((unsigned int)time(NULL));

	// Construction of application path:
	std::string applicationPath = std::string(windowParams.argv[0]);
	size_t lastIndex = applicationPath.find_last_of(sWindowsFolderDelim);
	m_applicationPath = applicationPath.substr(0, lastIndex + sWindowsFolderDelim.size());

	// Construction of window:
	m_window = new Window(windowParams.windowTitle, 
					      windowParams.x, 
						  windowParams.y, 
						  windowParams.width, 
						  windowParams.height);

	m_fpsCounter.reset(new FPSCounter());
}
//-------------------------------------------------------------------------------------------------
ApplicationContext::~ApplicationContext()
{
	// something very helpful here.
	delete m_window;
}
//-------------------------------------------------------------------------------------------------
ApplicationContext * ApplicationContext::getSharedApplicationContext()
{
	return m_sharedApplicationContext;
}
//-------------------------------------------------------------------------------------------------
ApplicationContext * ApplicationContext::create(const CreateOptions & windowParams)
{
	if (!m_sharedApplicationContext)
		m_sharedApplicationContext = new ApplicationContext(windowParams);

	return m_sharedApplicationContext;
}
//-------------------------------------------------------------------------------------------------
void ApplicationContext::go()
{
	polish();
	Application * app = Application::getCurrentApplication();

	while (getWindow()->isOpen())
	{
		sf::Event event;
		while (getWindow()->pollEvent(event))
		{
			
			if (event.type == sf::Event::Closed)
				getWindow()->close();

			bool isEventProcessedByGui = SFGUIManager::getInstance()->processEvent(event);
			if (!isEventProcessedByGui)
			{
				EventPtr abyssEventPtr = SfmlEventsAdapter::convertToAbyssEvent(event);
				app->processEvent(abyssEventPtr);
			}

		}

		// Update GUI:
		SFGUIManager::getInstance()->update();

		// Update all scheduler:
		SchedulersManager::getInstance()->update();

		// Clear all depth, color and stencil buffers:
		getWindow()->clear(sf::Color(0, 0, 0, 255));

		// Prepare for rendering:
		getFPSCounter()->onFrameStarted();
		app->render();
		getFPSCounter()->onFrameEnded();

		glPushAttrib(GL_ALL_ATTRIB_BITS);
		getWindow()->resetGLStates();

		// WARNING!!!
		// There is a bug, that when Current active texture is not GL_TEXTURE0 (in that bug it was GL_TEXTURE2), 
		// rendering of SFGUI's text was incorrect. It was blurred. See "$(ABYSS_ROOT)/Screenshots/Bugs/Blurred_Text.png" image fore more details.
		// Primary cause of this bug was incorrect RenderPass order in Texture, which caused that ActiveTexture was not GL_TEXTURE0.
		// However, make sure, that releasing of textures happened in reverse order to their binding!
		// TODO: move me to unit tests library!
		assert(Texture::getCurrentActiveTexture() == GL_TEXTURE0);
		assert(Texture::getCurrentBoundTexture(GL_TEXTURE_1D) == 0);
		assert(Texture::getCurrentBoundTexture(GL_TEXTURE_2D) == 0);
		assert(Texture::getCurrentBoundTexture(GL_TEXTURE_3D) == 0);

		SFGUIManager::getInstance()->render(getWindow());
		
		// End all rendering:
		getWindow()->display();

		glPopAttrib();
	}
}
//-------------------------------------------------------------------------------------------------
void ApplicationContext::polish()
{
	clearLogFile();

	getWindow()->setActive();

	SFGUIManager::getInstance()->init();
	getWindow()->setActive();

	auto shaderProgramManager = ShaderProgramManager::instance();

	// TODO: need to add possibility to customly specify shaders configuration file.
	auto cfgFileName = Utilities::getExecutableDirectoryPath() + "ApplicationShaders.cfg";
	auto shaders = shaderProgramManager->loadShaderPrograms(cfgFileName);

	Application * app = Application::getCurrentApplication();
	app->setup();

	getWindow()->polish();

	SchedulersManager::getInstance()->init();
}
//-------------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE