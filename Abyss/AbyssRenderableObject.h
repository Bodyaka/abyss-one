#pragma once

#include <vector>
#include <map>
#include "AbyssGlm.h"
#include "AbyssRendering.h"
#include "AbyssRenderPass.h"
#include "AbyssAABB.h"

ABYSS_BEGIN_NAMESPACE

class Renderer;
class SceneObject;
class BoundingBox;
class SimpleRenderableObject;
class RendererVisitor;
class RenderQueueVisitor;

class RenderableObject
{
	// TODO. Maybe, there are some better solution
	friend class Renderer;
	friend class SceneObject;

public:

	typedef std::function<void(RenderableObject*)> RenderPassesSetupFunc;

public:

	// Creates renderable object with specified name.
	RenderableObject(const String & name);
	~RenderableObject();

	// Push this one entity in render queue
	// TODO
	//void render(RenderQueue & queue, SceneObject * object);

	// Indicates, whether lighting is enabled/disabled.
	void setLightingEnabled(bool isEnabled);
	bool isLightingEnabled() const;

	// Change material for this one renderable object.
	void setMaterial(MaterialPtr material);
	MaterialPtr getMaterial() const;

	// SETTERS
	// Set, whether this renderable object should be rendered or not.
	void setVisible(bool isVisible);

	static void setDefaultRenderPassesInitializer(RenderPassesSetupFunc setupFunc);
	static RenderPassesSetupFunc getDefaultRenderPassesInitializer();

	// Add rendering pass to this one renderable object with renderer, which should be render this one object's data. 
	void addRenderPass(RenderPassWeakPtr pass);
	// Remove specified render pass from renderable object.
	void removeRenderPass(RenderPassWeakPtr pass);
	// Removes all render passes at all.
	void removeAllRenderPasses();
	// Init default render passes:
	virtual void setupDefaultRenderPasses();


	// GETTERS
	bool hasRenderPass(RenderPassWeakPtr pass) const;
	// Get name of renderable object
	const String & getName() const;
	// Get scene object, which this one renderable object is bound.
	SceneObject * getBoundSceneObject() const;

	// Return geometry object, which will be renderer with this one renderable object.
	virtual GeometryObjectPtr getGeometryObject() const = 0;

	const Vector4D & getPickColor() const;

	// INQUIRY
	// Check, whether this renderable object is visible.
	bool isVisible() const;

	// Sets bounding box enabled:
	void setBoundingBoxEnabled(bool enable);
	// Return, whether bounding box is enabled, or not:
	bool isBoundingBoxEnabled() const;

protected:
	
	virtual AABB getAABB() const = 0;

// Preparing rendering this renderable
	virtual void prepareForRendering(RenderQueueVisitor* visitor);

	// Actual perform drawing of the data:
	virtual void performRendering(RendererVisitor* visitor) = 0;

protected: // Members

	bool m_isBoundingBoxDirty;
	
private: // Methods

	// Bind this one renderable object to specified scene object.
	void bindTo(SceneObject * object);
	// Release this on renderable object from bound scene object
	void release();

	void constructBoundingBox();

	static Vector4D generateNewPickColor();



private: // Members

	// Scene object, which this one renderable object is bound.
	SceneObject * m_boundSceneObject;

	// Name of entity.
	String m_name;

	// Visibility flags
	bool m_isVisible;
	// Object's renderer
	Renderer * m_renderer;

	// Picking.
	// Some pick color (unique for each entity).
	Vector4D m_pickColor;

	// Set of reserved colors.
	typedef std::vector<Vector4D> PickSet;
	static PickSet m_sReservedPickColors;
	
	bool m_isLightingEnabled;

	std::set<RenderPassWeakPtr> m_renderPasses;

	static RenderPassesSetupFunc m_defaultRenderPassesInitializer;

	// BBOX:
	bool m_isBoundingBoxEnabled;
	std::unique_ptr<SimpleRenderableObject> m_bboxRenderable;

	// Material:
	MaterialPtr m_material;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline void RenderableObject::setVisible(bool visible)
{	m_isVisible = visible;	}
//----------------------------------------------------------------------------------------------
inline const String & RenderableObject::getName() const
{   return m_name;	}
//----------------------------------------------------------------------------------------------
inline const Vector4D & RenderableObject::getPickColor() const
{	return m_pickColor;	}
//----------------------------------------------------------------------------------------------
inline bool RenderableObject::isVisible() const
{	return m_isVisible;	}
//----------------------------------------------------------------------------------------------
inline void RenderableObject::release()
{   m_boundSceneObject = nullptr;	}
//----------------------------------------------------------------------------------------------
inline SceneObject * RenderableObject::getBoundSceneObject() const
{   return m_boundSceneObject;	}
//----------------------------------------------------------------------------------------------
inline bool RenderableObject::isLightingEnabled() const
{	return m_isLightingEnabled;	}
//----------------------------------------------------------------------------------------------
inline bool RenderableObject::isBoundingBoxEnabled() const
{	return m_isBoundingBoxEnabled;	}
//----------------------------------------------------------------------------------------------
inline void RenderableObject::setMaterial(MaterialPtr material)
{	m_material = material;	}
//----------------------------------------------------------------------------------------------
inline MaterialPtr RenderableObject::getMaterial() const
{	return m_material;	}
//----------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE