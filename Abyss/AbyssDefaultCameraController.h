#pragma once

#include "Abyss.h"
#include "AbyssEventHandler.h"
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class KeyEventAction;
class MouseEventAction;

class DefaultCameraController
	: public EventHandler
{
public:
	explicit DefaultCameraController(CameraPtr camera);
	~DefaultCameraController();
	
	void resetMouseEventAction(MouseEventAction * eventAction);
	void resetKeyEventAction(KeyEventAction * eventAction);

	virtual void mouseMoveEvent(const MouseEventPtr & event) override;
	virtual void keyPressEvent(const Abyss::KeyEventPtr & event) override;

	CameraPtr getCamera() const;

private:
	KeyEventAction * m_keyEventAction;
	MouseEventAction * m_mouseEventAction;
	CameraPtr m_camera;
};

// INLINE METHODS
//--------------------------------------------------------------------------------------------------
inline CameraPtr DefaultCameraController::getCamera() const
{	return m_camera;	}
//--------------------------------------------------------------------------------------------------
ABYSS_END_NAMESPACE