#pragma once

#include "AbyssMeshLoader.h"

ABYSS_BEGIN_NAMESPACE

// This class is responsible for mesh loading operation.
class MeshObjLoader
	: public MeshLoader
{

public:
	static MeshObjLoader * instance();

	static void makeAsDefaultLoader();

protected:
	MeshObjLoader(){};
	~MeshObjLoader(){};

private:
	virtual MeshPtr performLoadingMeshFromFile(const String & meshFileName) override;


	// Actual load implementation of loader:
	// Return whether loading data from file was succeeded:
	MeshPtr loadDataFromFile(const String & meshFileName);

	// Create SubMesh for current mesh from given data:
	Mesh * createSubMesh(const Mesh::VertexData & data, MeshPtr parentMesh);
	// Set data for current mesh.
	void setDataForMesh(const Mesh::VertexData & data, MeshPtr mesh);

	// Check, whether given line is comment.
	bool isComment(const String & fileLine) const;

	bool isValidObjLines(const std::vector<String> & strings) const;

	Vector2D readVector2D(const String & string_x, const String & string_y) const;
	Vector3D readVector3D(const String & string_x, const String & string_y, const String & string_z) const;
};

ABYSS_END_NAMESPACE