#pragma once

#include "AbyssRenderer.h"
#include "AbyssRendererVisitor.h"

ABYSS_BEGIN_NAMESPACE

class PhongShadingRenderer
	: public Renderer
{
public:
	static PhongShadingRenderer * getInstance();

	PhongShadingRenderer();
	~PhongShadingRenderer();
	
	virtual bool isSupports(const RenderableObject * const object) const override;

protected:

	virtual void registerExtensions() override;

	virtual void beforeRender( RenderableObject * const object ) override;
	virtual void afterRender( RenderableObject * const object ) override;
	virtual void performRendering(RenderableObject * const object) override;

	void setDefaultColorUniformValue(const Vector4D & color);

	

protected:

};

ABYSS_END_NAMESPACE