#include "AbyssFreeTransformableObject.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
FreeTransformableObject::FreeTransformableObject()
	: MovableObject()
	, m_rotationMatrix(cIdentityMatrix4D)
	, m_scale(Vector3D(1.f))
{

}
//----------------------------------------------------------------------------------------------
FreeTransformableObject::~FreeTransformableObject()
{

}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::yaw(float angle)
{
	rotateAlongSelfAxis(angle, cAxisY);
}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::roll(float angle)
{
	rotateAlongSelfAxis(angle, cAxisZ);
}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::pitch(float angle)
{
	rotateAlongSelfAxis(angle, cAxisX);
}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::rotateAlongSelfAxis(float angle, const Vector3D & axis)
{
	// TODO:
	/*
	// We need turn off auto updates for a while - we don't want updates 3 times, only 1 time.
	bool oldValue = setAutoUpdateEnabled(false);

	// First, we need move our object to the origin.
	Vector3D pos = getPosition();
	move(-pos);

	rotate(angle, axis);

	// And move it to the initial position
	move(pos);

	setAutoUpdateEnabled(oldValue);
	*/
	/*const Matrix4D & matrix = getModelMatrix();
	Vector4D axis4d = matrix * Vector4D(axis, 1.0f);
	Vector3D axis3d = glm::normalize(swizzleProj(axis4d));
	rotate(angle, axis3d);*/
	/*
	const Vector3D & pos = getPosition();

	Matrix4D matrix = getModelMatrix();
	matrix = glm::translate(matrix, -pos);
	matrix = glm::rotate(matrix, angle, axis);
	matrix = glm::translate(matrix, pos);

	setModelMatrix(matrix);*/

	rotate(angle, axis);
}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::rotate(float angle, const Vector3D & axis)
{
	m_rotationMatrix = glm::rotate(m_rotationMatrix, angle, axis);

	updateModelMatrix_impl();
}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::setRotationMatrix(const Matrix4D& matrix)
{
	m_rotationMatrix = matrix;

	updateModelMatrix_impl();
}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::setScale(const Vector3D & scale)
{
	m_scale = scale;

	updateModelMatrix_impl();
}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::setScale(float scale)
{
	setScale(Vector3D(scale, scale, scale));
}
//----------------------------------------------------------------------------------------------
void FreeTransformableObject::updateModelMatrix_impl()
{
	// Order of transformations should be revisited.
	// Note, that now it next: Scale, Rotation, Translation.
	Matrix4D matrix = Matrix4D(1.0f);
	matrix = glm::translate(matrix, getPosition());
	matrix = matrix * m_rotationMatrix;
	matrix = glm::scale(matrix, getScale());

	setModelMatrix(matrix);
}
//----------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE