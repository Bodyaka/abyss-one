#include "AbyssDefaultVertexAttributes.h"

#include <map>

namespace
{
	typedef std::map<int, Abyss::String>  TAttributeNameMap;
	typedef TAttributeNameMap::value_type TAttributeNameValue;
	
	TAttributeNameMap sAttributeNamesMap;

	// In future will be replaced by std::initializer_list.
	void setupAttributeNamesMap()
	{
		if (!sAttributeNamesMap.empty())
			return;

		sAttributeNamesMap.insert(TAttributeNameValue(Abyss::PositionAttribute, "VertexPosition"));
		sAttributeNamesMap.insert(TAttributeNameValue(Abyss::TexelAttribute,	"VertexTexel"));
		sAttributeNamesMap.insert(TAttributeNameValue(Abyss::NormalAttribute,   "VertexNormal"));
		sAttributeNamesMap.insert(TAttributeNameValue(Abyss::TangentAttribute,  "VertexTangent"));
		sAttributeNamesMap.insert(TAttributeNameValue(Abyss::BiTangentAttribute,"VertexBiTangent"));
	}
}

ABYSS_BEGIN_NAMESPACE

namespace Utilities
{
	//-----------------------------------------------------------------------------------------------
	const String & getAttributeName(int attribute)
	{
		setupAttributeNamesMap();

		return sAttributeNamesMap.at(attribute);
	}
	//-----------------------------------------------------------------------------------------------
	int getAttributeLocation(const String & attributName)
	{
		setupAttributeNamesMap();

		for (auto iter = sAttributeNamesMap.begin(); iter != sAttributeNamesMap.end(); ++iter)
		{
			if (iter->second == attributName)
				return iter->first;
		}

		return INVALID_LOCATION;
	}
	//-----------------------------------------------------------------------------------------------
}

ABYSS_END_NAMESPACE