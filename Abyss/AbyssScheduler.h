#pragma once

#include "Abyss.h"
#include <functional>

ABYSS_BEGIN_NAMESPACE

class Scheduler
{
public:

	typedef std::function<void(float)> UpdateFuncType;
	typedef std::function<bool(void)> StopConditionFuncType;

public:

	Scheduler();
	virtual ~Scheduler();

	static Scheduler* scheduleOnce(int msecs, const UpdateFuncType& updateFunc);
	static Scheduler* scheduleForever(int msecs, const UpdateFuncType& updateFunc);
	static Scheduler* scheduleUntil(int msecs, const UpdateFuncType& updateFunc, const StopConditionFuncType& stopFunc);

	void setInterval(int msecs);
	int getInterval() const;

	void update(int msecs);

	void start();
	void pause();

	bool isPaused() const;

	void setDeleteOnStop(bool isDelete);
	bool isDeleteOnStop() const;

	void setSingleShot(bool singleShot);
	bool isSingleShot() const;

	void setUpdateFunc(const UpdateFuncType& func);
	const UpdateFuncType& getUpdateFunc() const;

	void setStopConditionFunc(const StopConditionFuncType& func);
	const StopConditionFuncType& getStopConditionFunc() const;

protected:

	void stop();

	int m_currTimeInMsecs,
		m_targetTimeInMsecs;

	bool m_isPaused;
	bool m_isSingleShot;
	bool m_isNeedDeleteOnStop;
	
	StopConditionFuncType m_stopConditionFunc;
	UpdateFuncType m_updateFunc;

private:
	Scheduler(const Scheduler& scheduler) = delete;
	Scheduler& operator = (const Scheduler& scheduler) = delete;
};

// INLINE METHODS:
//----------------------------------------------------------------------------------------------
inline bool Scheduler::isPaused() const
{	return m_isPaused;	}
//----------------------------------------------------------------------------------------------
inline void Scheduler::setDeleteOnStop(bool isDelete)
{	m_isNeedDeleteOnStop = isDelete;	}
//----------------------------------------------------------------------------------------------
inline bool Scheduler::isDeleteOnStop() const
{	return m_isNeedDeleteOnStop;	}
//----------------------------------------------------------------------------------------------
inline void Scheduler::setSingleShot(bool singleShot)
{	m_isSingleShot = singleShot;	}
//----------------------------------------------------------------------------------------------
inline bool Scheduler::isSingleShot() const
{	return m_isSingleShot;	}
//----------------------------------------------------------------------------------------------
inline void Scheduler::setUpdateFunc(const UpdateFuncType& func)
{	m_updateFunc = func;	}
//----------------------------------------------------------------------------------------------
inline const Scheduler::UpdateFuncType& Scheduler::getUpdateFunc() const
{	return m_updateFunc;	}
//----------------------------------------------------------------------------------------------
inline void Scheduler::setStopConditionFunc(const StopConditionFuncType& func)
{	m_stopConditionFunc = func;	}
//----------------------------------------------------------------------------------------------
inline const Scheduler::StopConditionFuncType& Scheduler::getStopConditionFunc() const
{	return m_stopConditionFunc;	}
//----------------------------------------------------------------------------------------------
inline int Scheduler::getInterval() const
{	return m_targetTimeInMsecs;	}
//----------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE