#include "AbyssMouseEvent.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
MouseEvent::MouseEvent(const Vector2D & position, Mouse::ButtonType button, Type eventType)
	: Event(eventType)
	, m_mousePosition(position)
	, m_mouseButton(button)
	, m_mouseMoveOffset(position - Mouse::getPreviousPosition())
{
}
//------------------------------------------------------------------------------------------------
MouseEventPtr MouseEvent::createMouseMoveEvent(const Vector2D & position, Mouse::ButtonType button /* = NoneButton */)
{
	return MouseEventPtr(new MouseEvent(position, button, MouseMoved));
}
//--------------------------------------------------------------------------------------------------
MouseEventPtr MouseEvent::createMousePressEvent(const Vector2D & position, Mouse::ButtonType button /* = NoneButton */)
{
	return MouseEventPtr(new MouseEvent(position, button, MousePressed));
}
//--------------------------------------------------------------------------------------------------
MouseEventPtr MouseEvent::createMouseReleaseEvent(const Vector2D & position, Mouse::ButtonType button /* = NoneButton */)
{
	return MouseEventPtr(new MouseEvent(position, button, MouseReleased));
}
//--------------------------------------------------------------------------------------------------
MouseEventPtr MouseEvent::createMouseDoubleClickEvent(const Vector2D & position, Mouse::ButtonType button)
{
	return MouseEventPtr(new MouseEvent(position, button, MouseDoubleClick));
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE