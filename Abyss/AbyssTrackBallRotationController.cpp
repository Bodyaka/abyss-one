#include "AbyssTrackBallRotationController.h"
#include "AbyssSceneManager.h"
#include "AbyssCamera.h"
#include "AbyssMouseEvent.h"
#include "AbyssScheduler.h"

#include "AbyssApplicationContext.h"
#include "AbyssWindow.h"

static const float DEFAULT_RADIUS_SIZE_IN_NDC = 1.;

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
TrackBallRotationController::TrackBallRotationController()
	: m_isActive(false)
	, m_radius(DEFAULT_RADIUS_SIZE_IN_NDC)
{
	initScheduler();
}
//----------------------------------------------------------------------------------------------
TrackBallRotationController::TrackBallRotationController(const String& sceneObjectName)
	: m_isActive(false)
	, m_radius(DEFAULT_RADIUS_SIZE_IN_NDC)
{
	setSceneObjectName(sceneObjectName);
	initScheduler();
}
//----------------------------------------------------------------------------------------------
TrackBallRotationController::~TrackBallRotationController()
{

}
//----------------------------------------------------------------------------------------------
void TrackBallRotationController::setSceneObjectName(const String& sceneObjectName)
{
	if (getSceneObjectName() == sceneObjectName)
		return;

	m_sceneObjectName = sceneObjectName;

	if (auto object = getSceneObject())
	{
		auto camera = SceneManager::instance()->getCamera();
		camera->focusOnObject(object);

		const auto& objectAABB = object->getWorldAABB();
		Vector3D objectPos = objectAABB.center();
		m_objectPosInWorldSpace = objectPos;
	}
}
//----------------------------------------------------------------------------------------------
SceneObject* TrackBallRotationController::getSceneObject() const
{
	return SceneManager::instance()->getSceneObject(getSceneObjectName()).get();
}
//----------------------------------------------------------------------------------------------
void TrackBallRotationController::mousePressEvent(const MouseEventPtr & event)
{
	if (event->getMouseButton() == Mouse::LeftButton)
	{
		m_inersionScheduler->pause();

		m_isActive = true;
		m_prevPosition = event->getMousePosition();
	}
}
//----------------------------------------------------------------------------------------------
void TrackBallRotationController::mouseMoveEvent(const MouseEventPtr & event)
{
	if (!m_isActive)
		return;
	
	m_currentPosition = event->getMousePosition();

	Vector3D rotationAxis;
	float rotationAngle;
	computeRotationParameters(m_prevPosition, m_currentPosition, rotationAxis, rotationAngle);
	
	if (rotationAngle != 0.)
	{
		Matrix4D currRotationMatrix;
		currRotationMatrix = glm::rotate(currRotationMatrix, rotationAngle, rotationAxis);
		currRotationMatrix = math::getRotationMatrixAroundObjectCenter(getSceneObject(), currRotationMatrix);

		Matrix4D newRotationMatrix = getSceneObject()->getRotationMatrix() * currRotationMatrix;
		getSceneObject()->setRotationMatrix(newRotationMatrix);
	}

	m_prevPosition = m_currentPosition;
}
//----------------------------------------------------------------------------------------------
void TrackBallRotationController::mouseReleaseEvent(const MouseEventPtr & event)
{
	if (event->getMouseButton() == Mouse::LeftButton)
	{
		Vector2D offset = event->getMouseMoveOffset();
		Vector2D prevPosition = m_prevPosition;
		Vector2D currPosition = event->getMousePosition();

		static const float maxRotationLength = 250;
		static const float lengthToBeginInersion = 5;
		float length = glm::length(offset);

		if (length > lengthToBeginInersion)
		{
			// TODO: Problem - the event's position is the same as previous position (either m_prevPostion or m_currPosition).
			Vector2D currPosition = event->getMousePosition();
			Vector2D prevPostion = event->getMousePosition() - offset;
			computeRotationParameters(prevPostion, currPosition, m_inersionRotationAxis, m_inersionRotationAngle);

			// TODO: Problem - when moving outside sphere's radius, the rotation inersion becomes unexpected.
			// In order to properly define inersion, we have to define some magic numbers and simple relaxation
			length = std::min<int>(maxRotationLength, length);
			float angleBefore = m_inersionRotationAngle;
			float multiplier = length / lengthToBeginInersion;
			multiplier = std::min<float>(multiplier, 5);
			m_inersionRotationAngle *= multiplier;

			//std::cout << " Multipler = " << multiplier << "; Angle: before = " << angleBefore << " after: " << angleBefore << "\n";

			m_inersionRotationDelta = m_inersionRotationAngle / length;

			m_inersionScheduler->setInterval(20);
			m_inersionScheduler->start();
		}

	}
	
	m_isActive = false;
}
//----------------------------------------------------------------------------------------------
Abyss::Vector2D TrackBallRotationController::getObjectCenterInNDCSpace() const
{
	SceneObject* object = getSceneObject();
	assert(object);

	Vector3D pos = getObjectRotationCenter();
	const Matrix4D& wvpMatrix = object->getWorldViewProjectionalMatrix();

	Vector4D hPos = wvpMatrix * Vector4D(pos, 1);

	hPos = hPos / hPos.w;
	return Vector2D(hPos.x, hPos.y);
}
//----------------------------------------------------------------------------------------------
Vector2D TrackBallRotationController::convertToNDCSpace(const Vector2D& mousePos) const
{
	auto window = ApplicationContext::getSharedApplicationContext()->getWindow();
	const auto& windowSize = window->getCurrentSize();

	auto x = 2. * mousePos.x / windowSize.x - 1.;
	auto y = -(2. * mousePos.y / windowSize.y - 1.);

	return Vector2D(x, y);
}
//----------------------------------------------------------------------------------------------
Vector3D TrackBallRotationController::convertToObjectVirtualSphereSpace(const Vector2D& ndcPos) const
{
	Vector2D spherePos = getObjectCenterInNDCSpace();
	Vector2D pos = ndcPos - spherePos;

	Vector3D posInSphereSpace;

	float squaredLength = pos.x * pos.x + pos.y * pos.y;
	float squaredRadius = m_radius * m_radius;
	
	if (squaredLength <= squaredRadius)
	{
		float zPos = sqrt(squaredRadius - squaredLength);
		posInSphereSpace = Vector3D(pos.x, pos.y, zPos);
	}
	else
	{
		float multiplier = m_radius / sqrt(squaredLength);
		posInSphereSpace = multiplier * Vector3D(pos.x, pos.y, 0);
	}

	return posInSphereSpace;
}
//----------------------------------------------------------------------------------------------
void TrackBallRotationController::computeRotationParameters(const Vector2D& prevMousePosition, const Vector2D& currMousePosition, Vector3D& rotationAxis, float& rotationAngle) const
{
	Vector2D prevNDCPos = convertToNDCSpace(prevMousePosition);
	Vector2D currNDCPos = convertToNDCSpace(currMousePosition);

	Vector3D prevSpherePoint = convertToObjectVirtualSphereSpace(prevNDCPos);
	Vector3D currSpherePoint = convertToObjectVirtualSphereSpace(currNDCPos);

	// Problem #1: cross product order:
	rotationAxis = glm::cross(prevSpherePoint, currSpherePoint);

	auto axis4 = glm::inverse(getSceneObject()->getRotationMatrix()) * Vector4D(rotationAxis, 0);
	rotationAxis = Vector3D(axis4.x, axis4.y, axis4.z);

	float rotationAxisLength = glm::length(rotationAxis);

	// Problem #2: atan vs ctg (1/tan):
	rotationAngle = atan(rotationAxisLength / glm::dot(prevSpherePoint, currSpherePoint));
	rotationAngle = glm::degrees(rotationAngle);
}
//----------------------------------------------------------------------------------------------
void TrackBallRotationController::initScheduler()
{
	m_inersionScheduler.reset(new Scheduler());

	auto inersionRotation = [&](float deltaTime) -> void
	{
		Matrix4D currRotationMatrix;
		currRotationMatrix = glm::rotate(currRotationMatrix, m_inersionRotationAngle, m_inersionRotationAxis);
		currRotationMatrix = math::getRotationMatrixAroundObjectCenter(getSceneObject(), currRotationMatrix);

		Matrix4D newRotationMatrix = getSceneObject()->getRotationMatrix() * currRotationMatrix;
		getSceneObject()->setRotationMatrix(newRotationMatrix);

		m_inersionRotationAngle -= m_inersionRotationDelta;
	};

	auto stopCondition = [&]() -> bool
	{
		return m_inersionRotationAngle <= 0;
	};

	m_inersionScheduler->setUpdateFunc(inersionRotation);
	m_inersionScheduler->setStopConditionFunc(stopCondition);
}
//----------------------------------------------------------------------------------------------
Abyss::Vector3D TrackBallRotationController::getObjectRotationCenter() const
{
	return math::getObjectLocalRotationCenter(getSceneObject());
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE