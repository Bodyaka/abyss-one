#include "AbyssRendererMaterialExtension.h"

#include "AbyssMesh.h"
#include "AbyssTexture.h"
#include "AbyssMaterial.h"
#include "AbyssShaderProgram.h"
#include "AbyssRenderableObject.h"

ABYSS_BEGIN_NAMESPACE

namespace
{
	const std::string MATERIAL_NAME = "material";
	const std::string MATERIAL_AMBIENT_NAME = MATERIAL_NAME + ".ambient";
	const std::string MATERIAL_DIFFUSE_NAME = MATERIAL_NAME + ".diffuse";
	const std::string MATERIAL_SPECULAR_NAME = MATERIAL_NAME + ".specular";
	const std::string MATERIAL_DIFFUSE_TEXTURE_NAME = MATERIAL_NAME + ".diffuseTexture";
	const std::string MATERIAL_NORMAL_TEXTURE_NAME = MATERIAL_NAME + ".normalTexture";
	const std::string MATERIAL_SHINESS_NAME = MATERIAL_NAME + ".shiness";
	const std::string MATERIAL_DIFFUSE_INTENSITY_NAME = MATERIAL_NAME + ".diffuseIntensity";
	const std::string MATERIAL_SPECULAR_INTENSITY_NAME = MATERIAL_NAME + ".specularIntensity";
	const std::string MATERIAL_INTERACTIVIT_FLAG_NAME = MATERIAL_NAME + ".interactivityFlag";

	const size_t DIFFUSE_TEXTURE_BINDING_POINT = 0;
	const size_t NORMAL_TEXTURE_BINDING_POINT = 1;
	const std::string NORMAL_MAPPING_ENABLED_NAME = "enableNormalMapping";
}


RendererMaterialExtension::RendererMaterialExtension(ShaderProgram* program)
	: RendererExtension(program)
{

}

void RendererMaterialExtension::prepareForRendering(RenderableObject * const object)
{
	/*
	// TODO: should we setup materials here?
	if (auto material = object->getMaterial().get())
	{
		setupMaterialUniformValues(material);
	}
	*/
}

void RendererMaterialExtension::unprepareFromRendering(RenderableObject * const object)
{
}

void RendererMaterialExtension::setupMaterialUniformValues(const Material* const material)
{
	getShaderProgram()->setUniform(MATERIAL_DIFFUSE_NAME, material->getDiffuseColor());
	getShaderProgram()->setUniform(MATERIAL_SPECULAR_NAME, material->getSpecularColor());
	getShaderProgram()->setUniform(MATERIAL_SHINESS_NAME, material->getShiness());
	getShaderProgram()->setUniform(MATERIAL_DIFFUSE_INTENSITY_NAME, material->getDiffuseIntensity());
	getShaderProgram()->setUniform(MATERIAL_SPECULAR_INTENSITY_NAME, material->getSpecularIntensity());
}

void RendererMaterialExtension::beforeRendering(Mesh* mesh)
{
	if (auto material = mesh->getMaterial().get())
	{
		setupMaterialUniformValues(material);
		bindMaterialTextureUniformValues(material, getDiffuseTextureBindingPoint(), getNormalTextureBindingPoint());
	}
}

void RendererMaterialExtension::afterRendering(Mesh* mesh)
{
	if (auto material = mesh->getMaterial().get())
	{
		releaseMaterialTextureUniformValues(material);
		GLenum error = glGetError();
	}
}

void RendererMaterialExtension::bindMaterialTextureUniformValues(const Material* const material, int diffuseBindingPoint, int normalsBindingPoint)
{
	getShaderProgram()->setUniform(MATERIAL_DIFFUSE_TEXTURE_NAME, int(diffuseBindingPoint));
	getShaderProgram()->setUniform(MATERIAL_NORMAL_TEXTURE_NAME, int(normalsBindingPoint));
	getShaderProgram()->setUniform(NORMAL_MAPPING_ENABLED_NAME, material->hasTexture(NormalMapTextureType));
	getShaderProgram()->setUniform(MATERIAL_AMBIENT_NAME, material->getAmbientColor());
	getShaderProgram()->setUniform(MATERIAL_INTERACTIVIT_FLAG_NAME, (unsigned char)material->getInteractivityMask());

	if (material->hasTexture(DiffuseTextureType))
		material->getTexture(DiffuseTextureType)->bind(diffuseBindingPoint);

	if (material->hasTexture(NormalMapTextureType))
		material->getTexture(NormalMapTextureType)->bind(normalsBindingPoint);
}

void RendererMaterialExtension::releaseMaterialTextureUniformValues(const Material* const material)
{
	if (material->hasTexture(DiffuseTextureType))
		material->getTexture(DiffuseTextureType)->release();

	if (material->hasTexture(NormalMapTextureType))
		material->getTexture(NormalMapTextureType)->release();
}

void RendererMaterialExtension::setDiffuseTextureBindingPoint(size_t location)
{
	m_diffuseTextureBindingPoint = location;
}

void RendererMaterialExtension::setNormalTextureBindingPoint(size_t location)
{
	m_normalTextureBindingPoint = location;
}


ABYSS_END_NAMESPACE

