#pragma once

#include <map>
#include <vector>
#include "AbyssRendering.h"

ABYSS_BEGIN_NAMESPACE

class ShaderProgram;

class ShaderProgramManager
{
public:

	typedef std::vector<ShaderProgram*>		 TShaderProgramsVector;
	typedef std::map<String, ShaderProgram*> TShaderProgramsContainer;

public:
	// Constructor.
	ShaderProgramManager();
	// Destructor.
	~ShaderProgramManager();

	// Get container.
	inline TShaderProgramsContainer & getShaderPrograms();

	// Creates shader program.
	ShaderProgram * createShaderProgram(const String & shaderName, const String & vsFileName, const String & fsFileName);

	// Creates vector of shader programs. Names will be set automatically. TODO.
	TShaderProgramsVector createShaderProgramVector(const String & directory);
	// Creates shader program by specified xml file. TODO.
	TShaderProgramsVector createShaderProgramVectorXml(const String & shaderXmlFile);
	// Load and creates shader program by specified cfg file.
	TShaderProgramsVector loadShaderPrograms(const String & shaderCfgFile);

	// Return shader with specified name.
	ShaderProgram * getShaderProgram(const String & name);
	// Check, is there are some shader program.
	bool hasShaderProgram(const String & name);

	static ShaderProgramManager * instance();

private:

	// Get cfg value of specified line string with keyWord value. 
	// I.e. CFG has some format like: keyWord = "value"
	static String getCfgFormatValue(const String & line, const String & keyWord);

	TShaderProgramsContainer m_shaderPrograms;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
ShaderProgramManager::TShaderProgramsContainer & ShaderProgramManager::getShaderPrograms()
{	return m_shaderPrograms;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE