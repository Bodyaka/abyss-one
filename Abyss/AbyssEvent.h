#pragma once

#include "Abyss.h"
#include "AbyssEventsDef.h"

ABYSS_BEGIN_NAMESPACE

class Event
{
public:
	enum Type
	{
		MouseMoved,
		MousePressed,
		MouseReleased,
		MouseDoubleClick,
		MouseWheel,
		KeyPressed,
		KeyReleased,
	};

	// Constructs event:
	Event(Type type)
		: m_type(type){};

	// Destructor
	virtual ~Event(){};

	// Return type of event.
	Type getType() const;

private:
	Type m_type;

};

// INLINE FUNCTIONS:
//-------------------------------------------------------------------------------------------------
inline Event::Type Event::getType() const
{	return m_type; }
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE