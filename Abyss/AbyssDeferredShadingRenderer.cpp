#include "AbyssDeferredShadingRenderer.h"

#include "AbyssGBuffer.h"
#include "AbyssTexture.h"
#include "AbyssShaderProgram.h"
#include "AbyssMeshRenderable.h"
#include "AbyssSceneManager.h"

#include "AbyssRendererLightingExtension.h"
#include "AbyssRenderingResult.h"

ABYSS_BEGIN_NAMESPACE

static const std::string SHADER_NAME = "DeferredShadingRenderer";

static const std::map<GBuffer::TextureType, std::pair<std::string, int>> s_gBufferTextureUniformsMap =
{
	{ GBuffer::TextureType::POSITIONS,	std::make_pair<std::string, int>(std::string("gPositionsTexture"), 0)},
	{ GBuffer::TextureType::NORMALS,	std::make_pair<std::string, int>(std::string("gNormalsTexture"), 1) },
	{ GBuffer::TextureType::COLORS,		std::make_pair<std::string, int>(std::string("gAlbedoTexture"), 2) },
};

static const std::string SSAO_TEXTURE_UNIFORM_NAME = "ssaoTexture";
static const size_t SSAO_TEXTURE_BINDING_POINT = 3;
static const std::string ENABLE_SSAO_UNIFORM_NAME = "enableSSAO";

DeferredShadingRenderer::DeferredShadingRenderer()
	: Renderer(SHADER_NAME)
{

}

DeferredShadingRenderer* DeferredShadingRenderer::getInstance()
{
	static DeferredShadingRenderer renderer;
	return &renderer;
}

bool DeferredShadingRenderer::isSupports(const RenderableObject * const renderable) const
{
	return (dynamic_cast<const MeshRenderable* const>(renderable) != nullptr);
}

void DeferredShadingRenderer::beforeRender(RenderableObject * const object)
{
	setupMatrixUniformsValues(object->getBoundSceneObject());
}

void DeferredShadingRenderer::afterRender(RenderableObject * const object)
{
	// nothing
}

void DeferredShadingRenderer::startRendering(RenderingResultPtr input)
{
	Renderer::startRendering(input);

	auto gBuffer = input->getGBuffer();

	for (const auto& gBufferIter : s_gBufferTextureUniformsMap)
	{
		const auto& uniformName = gBufferIter.second.first;
		const auto& bindingPoint = gBufferIter.second.second;

		getShaderProgram()->setUniform(uniformName, bindingPoint);
		gBuffer->getTexture(gBufferIter.first)->bind(bindingPoint);
	}

	auto ssaoTexture = input->getTexture(constants::ABYSS_SSAO_TEXTURE);
	if (ssaoTexture)
	{
		ssaoTexture->bind(SSAO_TEXTURE_BINDING_POINT);
		getShaderProgram()->setUniform(SSAO_TEXTURE_UNIFORM_NAME, int(SSAO_TEXTURE_BINDING_POINT));
	}

	getShaderProgram()->setUniform(ENABLE_SSAO_UNIFORM_NAME, ssaoTexture != nullptr);
}

void DeferredShadingRenderer::registerExtensions()
{
	m_extensions.push_back(std::make_shared<RendererLightingExtension>(this->getShaderProgram()));
}

void DeferredShadingRenderer::finishRendering(RenderingResultPtr output)
{
	// Releasing GBuffer:
	if (auto gBuffer = output->getGBuffer())
	{
		for (const auto& gBufferIter : s_gBufferTextureUniformsMap)
		{
			gBuffer->getTexture(gBufferIter.first)->release();
		}
	}
	else 
	{
		raiseAnError("Cannot find a GBuffer!");
	}
	
	// Releasing SSAO texture:
	if (auto ssaoTexture = output->getTexture(constants::ABYSS_SSAO_TEXTURE))
		ssaoTexture->release();
	else
		raiseAnError("Cannot find SSAO texture!");

	// Also need to perform copy of GBuffer's depth buffer to the "main" buffer.
	Renderer::finishRendering(output);
}

ABYSS_END_NAMESPACE