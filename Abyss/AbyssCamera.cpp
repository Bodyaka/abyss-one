#include "AbyssCamera.h"
#include "AbyssSceneObject.h"
#include "AbyssWindow.h"

ABYSS_BEGIN_NAMESPACE


class Camera::ResizeListener : public WindowListener
{
public:
	explicit ResizeListener(Camera* camera)
		: m_camera(camera)
	{	}

	virtual void onWindowResize(size_t width, size_t height) override
	{
		auto params = m_camera->getParams();
		params.m_aspect = float(width) / height;
		m_camera->setParams(params);
	}

private:
	Camera* m_camera;
};

//----------------------------------------------------------------------------------------------
Camera::Camera(const Vector3D & eye, const Vector3D & center, const Vector3D & up, const ViewingFrustum::Params & params)
	: ViewingFrustum(params)
	, m_target(nullptr)
{
	setup(eye, center, up);
}
//----------------------------------------------------------------------------------------------
Camera::~Camera()
{

}
//----------------------------------------------------------------------------------------------
void Camera::setup(const Vector3D & eye, const Vector3D & center, const Vector3D & up)
{
	m_eyePosition = eye;
	m_centerPosition = center;
	m_upVector = up;

	updateViewMatrix();
}
//----------------------------------------------------------------------------------------------
void Camera::setEyePosition(const Vector3D & position)
{
	m_eyePosition = position;
	updateViewMatrix();
}
//----------------------------------------------------------------------------------------------
void Camera::setCenterPosition(const Vector3D & position)
{
	m_centerPosition = position;
	updateViewMatrix();
}
//--------------------------------------------------------------------------------------------------
void Camera::setUpVector(const Vector3D & upVector)
{
	m_upVector = upVector;
	updateViewMatrix();
}
//--------------------------------------------------------------------------------------------------
void Camera::yaw(float degree)
{
	setYawRoll(degree, 0.f);
}
//--------------------------------------------------------------------------------------------------
void Camera::roll(float degree)
{
	setYawRoll(0.f, degree);
}
//--------------------------------------------------------------------------------------------------
void Camera::setYawRoll(float yawOffset, float rollOffset)
{
	Vector3D distance = getEyePosition() - getCenterPosition();

	float x = distance.x;
	float y = distance.y;
	float z = distance.z;

	bool isNegative = z < 0.f;

	float xx = x*x;
	float yy = y*y; 
	float zz = z*z; 

	float radius = sqrt(xx + yy + zz);

	static const float epsilon = 0.00001f;
	if (std::abs(z) < epsilon)
		z = epsilon;
	if (std::abs(radius) < epsilon)
		radius = epsilon;

	//float newYaw = atan(x/z) + yawOffset;
	float newYaw = atan2(x, z) + yawOffset;
	float newRoll = acos(y/radius) + rollOffset;

	float sinYaw = sin(newYaw);
	float cosYaw = cos(newYaw);
	float sinRoll = sin(newRoll);
	float cosRoll = cos(newRoll);

	z = radius * sinRoll * cosYaw;
	x = radius * sinRoll * sinYaw;
	y = radius * cosRoll;

	setEyePosition(getCenterPosition() + Vector3D(x, y, z));
}
//--------------------------------------------------------------------------------------------------
void Camera::updateViewMatrix()
{
	// TODO: Why m_upVector does not do anything here?
	Vector3D up = Vector3D(0, -1, 0);
	Vector3D lookDir = glm::normalize(getCenterPosition() - getEyePosition());
	Vector3D sideDir = glm::cross(lookDir, up);

	m_upVector = glm::cross(lookDir, sideDir);

	setViewMatrix(glm::lookAt(getEyePosition(), getCenterPosition(), getUpVector()));
}
//--------------------------------------------------------------------------------------------------
void Camera::focusOnObject(SceneObject* object)
{
	const auto& objectAABB = object->getWorldAABB();

	float objectWidth = objectAABB.width();
	float objectHeight = objectAABB.height();
	float visibleHeight = objectHeight;
	float distanceToCamera = 0;
	if (objectWidth > objectHeight)
		visibleHeight = objectWidth / getAspect();

	auto halfHeight = visibleHeight / 2;
	auto halfFieldOfViewY = getFovY() / 2;
	auto contangent = 1. / tan(glm::radians(halfFieldOfViewY));
	distanceToCamera = contangent * halfHeight;

	Vector3D lookAt = objectAABB.center();
	distanceToCamera += objectAABB.depth() / 2;

	Vector3D eyePos = lookAt + Vector3D(0, 0, 1) * distanceToCamera;

	m_centerPosition = lookAt;
	m_eyePosition = eyePos;
	updateViewMatrix();
}
//--------------------------------------------------------------------------------------------------
void Camera::stickToWindow(Window* window)
{
	if (!window)
	{
		verifyStatement(window != nullptr, "Should valid window be specified!");
		return;
	}
	m_resizeListener.reset(new ResizeListener(this));
	
	window->addListener(m_resizeListener.get());
}
//--------------------------------------------------------------------------------------------------
Window* Camera::getStickedWindow() const
{
	if (!m_resizeListener)
		return nullptr;

	return m_resizeListener->getWindow();
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE