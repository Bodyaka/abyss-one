#include "AbyssApplication.h"

#include "AbyssGL.h"
#include <cassert>

#include "AbyssEventHandler.h"
#include "AbyssBoundingBox.h"
#include "AbyssSceneManager.h"
#include "AbyssShaderProgramManager.h"


ABYSS_BEGIN_NAMESPACE

Application * Application::m_sharedApplication = NULL;
//-----------------------------------------------------------------------------------------------
Application::Application(const CreateOptions & createOptions)
	: m_sceneManager(new SceneManager())
	, m_shaderProgramManager(new ShaderProgramManager())
{
	ApplicationContext::create(createOptions);

	makeCurrent();

	if (glewInit() != GLEW_OK)
	{
		assert(false);
	}
}
//-----------------------------------------------------------------------------------------------
Application::~Application()
{
	delete m_sceneManager;
	delete m_shaderProgramManager;

	for (auto handler : m_eventHandlers)
		delete handler;

}
//-----------------------------------------------------------------------------------------------
void Application::makeCurrent()
{
	m_sharedApplication = this;
}
//-----------------------------------------------------------------------------------------------
void Application::start()
{
	ApplicationContext::getSharedApplicationContext()->go();
}
//-----------------------------------------------------------------------------------------------
void Application::render()
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	getSceneManager()->render();
}
//-----------------------------------------------------------------------------------------------
void Application::setup()
{
	glEnable(GL_DEPTH_TEST);

	getSceneManager()->init();
}
//-----------------------------------------------------------------------------------------------
Application * Application::getCurrentApplication() 
{
	return m_sharedApplication;
}
//-----------------------------------------------------------------------------------------------
void Application::processEvent(const EventPtr & event)
{
	if(event == nullptr)
	{
		return;
	}

	for (auto handler : getEventHandlers())
		handler->event(event);
}
//-----------------------------------------------------------------------------------------------
void Application::addEventHandler(EventHandler * handler)
{
	if (!handler)
	{
		assert(false && "Input handler cannot be null!");
		return;
	}

	m_eventHandlers.push_back(handler);
}
//-----------------------------------------------------------------------------------------------
void Application::deleteEventHandler(EventHandler* handler)
{
	delete removeEventHandler(handler);
}
//-----------------------------------------------------------------------------------------------
EventHandler* Application::removeEventHandler(EventHandler* handler)
{
	auto iter = std::find(m_eventHandlers.begin(), m_eventHandlers.end(), handler);
	if (iter != m_eventHandlers.end())
	{
		EventHandler* handler = *iter;
		m_eventHandlers.erase(iter);
		return handler;
	}
		
	return nullptr;
}
//-----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE