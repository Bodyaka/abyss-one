#pragma once

#include "AbyssMath.h"

ABYSS_BEGIN_NAMESPACE

class Plane
{
public:
	// Creates empty plane
	Plane(){};
	Plane(const Vector4D & i_vec);

	// absolute (by modulo) distance from plane to point.
	float distance(const Vector3D & point) const;
	// signed distance from plane to given point.
	float signedDistance(const Vector3D & point) const;

	float d() const;
	Vector3D n() const;

	// Casting to vector4d...
	operator Vector4D() const {return m_plane;	};
	// operator assignment.
	void operator = (const Vector4D & plane) {m_plane = plane; };


private:
	Vector4D m_plane;
};

//--------------------------------------------------------------------------------------------------
inline Plane::Plane(const Vector4D & i_vec)
	: m_plane(i_vec)
{}
//--------------------------------------------------------------------------------------------------
inline float Plane::signedDistance(const Vector3D & point) const
{	return glm::dot(point, n()) + d();	}
//--------------------------------------------------------------------------------------------------
inline float Plane::distance(const Vector3D & point) const
{	return std::abs(signedDistance(point));	}
//--------------------------------------------------------------------------------------------------
inline Vector3D Plane::n() const
{	return math::swizzle3D(m_plane);	}
//--------------------------------------------------------------------------------------------------
inline float Plane::d() const
{	return m_plane.w;	}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE