#pragma once

#include "AbyssGlm.h"
#include "AbyssChangeableObject.h"

ABYSS_BEGIN_NAMESPACE

class TransformableObject
	: public ChangeableObject
{
public:
	// By default, AutoUpdate set to false.
	TransformableObject();
	virtual ~TransformableObject(){};

	// Return model matrix of this one fucker.
	const Matrix4D & getModelMatrix() const;
	// Return world matrix of this one fucker.
	const Matrix4D & getWorldMatrix() const;
	// Return Normal World Matrix of this one fucker.
	const Matrix3D & getNormalMatrix() const;
	// Return combined WV matrix of this one fucker:
	const Matrix4D & getWorldViewMatrix() const;
	// Return combined WVP matrix of this one fucker.
	const Matrix4D & getWorldViewProjectionalMatrix() const;
	
	// updates transform using attached scene object. If not attached, should do nothing.
	virtual void updateTransform() = 0;
	// Updates transform with specified model matrix (as parent model matrix).
	void updateTransform(const Matrix4D & projectionMatrix, const Matrix4D & viewMatrix, const Matrix4D & parentModelMatrix = cIdentityMatrix4D);

	// Set enabled updates. I.e., when will setting modelMatrix, it will update other matrices, if its attached node is not NULL.
	// Return older auto update flag.
	bool setAutoUpdateEnabled(bool flag);
	// Check, whether auto update is enabled.
	inline bool isAutoUpdateEnabled() const;

	/*	// Setting model matrix of this one transform object. Please, do not use this one fucker.
	void setModelMatrix(const Matrix4D & modelMatrix);
	// Setting world matrix of this one transform object. Please, do not use this one fucker.
	void setWorldMatrix(const Matrix4D & modelMatrix);
	// Setting view matrix of this one transform object. Please, do not use this one fucker.
	void setViewMatrix(const Matrix4D & viewMatrix);
	// Setting projectional matrix of this one transform object. Please, do not use this one fucker.
	void setProjectionMatrix(const Matrix4D & projMatrix);*/
protected:

	// Setting model matrix.
	void setModelMatrix(const Matrix4D & matrix);

	// Updates transform of this one object. I.e., when you changes model matrix, you need to change this function too.
	virtual void onUpdateTransform(){}; 
	
	Matrix3D m_normalMatrix;
	Matrix4D m_modelMatrix;
	Matrix4D m_worldMatrix;
	Matrix4D m_worldViewMatrix;
	Matrix4D m_wvpMatrix;

	// Flag for auto update.
	bool m_isAutoUpdateEnabled;
};

// INLINE
//----------------------------------------------------------------------------------------------
inline const Matrix4D & TransformableObject::getModelMatrix() const
{	return m_modelMatrix;	}
//----------------------------------------------------------------------------------------------
inline const Matrix4D & TransformableObject::getWorldMatrix() const
{	return m_worldMatrix;	}
//----------------------------------------------------------------------------------------------
inline const Matrix3D & TransformableObject::getNormalMatrix() const
{	return m_normalMatrix;	}
//----------------------------------------------------------------------------------------------
inline const Matrix4D & TransformableObject::getWorldViewMatrix() const
{	return m_worldViewMatrix;	}
//----------------------------------------------------------------------------------------------
inline const Matrix4D & TransformableObject::getWorldViewProjectionalMatrix() const
{	return m_wvpMatrix;	}
//----------------------------------------------------------------------------------------------
inline bool TransformableObject::isAutoUpdateEnabled() const
{	return m_isAutoUpdateEnabled;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE