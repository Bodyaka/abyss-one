#include "AbyssFrustum.h"

#include "AbyssAABB.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
Frustum::Frustum(const TPlanes & planes)
	: m_planes(planes)
{
}
//----------------------------------------------------------------------------------------------
Frustum::~Frustum()
{

}
//----------------------------------------------------------------------------------------------
void Frustum::setPlanes(const TPlanes & planes)
{
	m_planes = planes;
}
//----------------------------------------------------------------------------------------------
// Check, whether this one frustum contains given point.
bool Frustum::contains(const Vector3D & point) const
{
	for (int i = 0; i<PlaneMax; ++i)
	{
		float dist = getPlane(PlaneId(i)).signedDistance(point);

		if (dist < 0)
			return false;
	}

	return true;
}
//----------------------------------------------------------------------------------------------
IntersectionResult Frustum::intersect(const AABB & box) const
{
	return intersect1(box);
}
//----------------------------------------------------------------------------------------------
IntersectionResult Frustum::intersect1(const AABB & box) const
{
	bool isIntersect = false;
	ContainerV3D points = box.points();

	bool isContainsAllPoints = true;

	for (size_t i = 0; i<points.size(); ++i)
	{
		bool isContains = contains(points.at(i));

		if (!isContains)
		{
			if ((i > 0) && isContainsAllPoints)
				return IR_Intersects;

			isContainsAllPoints = false;
		}
		else
		{
			if ((i > 0) && !isContainsAllPoints)
				return IR_Intersects;
		}
	}

	return isContainsAllPoints ? IR_Inside : IR_NoIntersection;
}
//----------------------------------------------------------------------------------------------
IntersectionResult Frustum::intersect2(const AABB & box) const
{
	bool isIntersect = false;
	for (int i = 0; i<PlaneMax; ++i)
	{
		IntersectionResult res = box.intersect(getPlane(PlaneId(i)));
		if (res == IR_NoIntersection)
			return res;
		if (res == IR_Intersects)
			isIntersect = true;
	}

	return (isIntersect) ? IR_Intersects : IR_Inside;
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE