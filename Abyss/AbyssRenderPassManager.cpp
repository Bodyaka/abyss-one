#include "AbyssRenderPassManager.h"

#include "AbyssDeferredRenderer.h"
#include "AbyssPhongShadingRenderer.h"
#include "AbyssFullScreenRectRenderer.h"
#include "AbyssDeferredShadingRenderer.h"
#include "AbyssSSAORenderer.h"
#include "AbyssSSAOBlurRenderer.h"
#include "AbyssBlurRenderer.h"

#include "AbyssSceneManager.h"

#include "AbyssCompositorPass.h"
#include "AbyssRenderTarget.h"
#include "AbyssRenderingResult.h"

ABYSS_BEGIN_NAMESPACE

namespace DefaultRenderPasses
{
	static const std::set<std::string> s_allDefaultRenderPasses = 
	{
		DEFERRED_RENDERING,
		SSAO,
		SSAO_BLUR,
		DEFERRED_SHADING,
		FORWARD_RENDERING,
		PHONG_SHADING,
		DEBUG_RENDERING,
		GAUSSIAN_BLUR_RENDER_PASS,
		FINAL_PASS
	};
}

RenderPassManager::RenderPassManager(SceneManager* manager)
	: SceneManagerListener(manager)
{
	initDefaultRenderPasses();
}

RenderPassManager::~RenderPassManager()
{
}

bool RenderPassManager::addRenderPass(RenderPassWeakPtr renderPassBefore, RenderPassSharedPtr renderPass)
{
	std::string renderPassBeforeName = renderPassBefore ? renderPassBefore->getName() : "";

	return addRenderPass(renderPassBeforeName, renderPass);
}

bool RenderPassManager::addRenderPass(const std::string& renderPassBefore, RenderPassSharedPtr renderPass)
{
	if (renderPassBefore.empty())
		return addRenderPassToBegin(renderPass);

	// Find iter to insert render pass:
	auto iterToInsert = m_renderPasses.end();
	for (auto tempIter = m_renderPasses.begin(); tempIter != m_renderPasses.end(); ++tempIter)
	{
		if (tempIter->get() && tempIter->get()->getName() == renderPassBefore)
		{
			iterToInsert = tempIter;
			break;
		}
	}

	if (iterToInsert == m_renderPasses.end() && !m_renderPasses.empty())
	{
		assert(false && "RenderPassManager::addRenderPass - renderPassBefore should be registered in RenderPassManager!");
		return false;
	}

	return addRenderPass_impl(iterToInsert, renderPass);
}

bool RenderPassManager::addRenderPassToBegin(RenderPassSharedPtr renderPass)
{
	return addRenderPass_impl(m_renderPasses.begin(), renderPass);
}

bool RenderPassManager::addRenderPassToEnd(RenderPassSharedPtr renderPass)
{
	return addRenderPass_impl(m_renderPasses.end(), renderPass);
}

bool RenderPassManager::addRenderPass_impl(RenderPassInternalContainerType::const_iterator iterToInsert, RenderPassSharedPtr renderPass)
{
	if (renderPass == nullptr || hasRenderPass(renderPass->getName()) || (iterToInsert != m_renderPasses.end() && (*iterToInsert)->getName() == renderPass->getName()))
	{
		assert(false && "RenderPassManager::addRenderPass - such render pass is already registered!");
		return false;
	}

	m_renderPasses.insert(iterToInsert, renderPass);
	m_cachedDirtyRenderPasses.push_back(renderPass);

	m_isChanged = true;
	return true;
}

bool RenderPassManager::removeRenderPass(const std::string& renderPassToRemove)
{
	if (!hasRenderPass(renderPassToRemove))
		return false;

	if (DefaultRenderPasses::s_allDefaultRenderPasses.find(renderPassToRemove) != DefaultRenderPasses::s_allDefaultRenderPasses.end())
	{
		std::string exceptionStr = "Cannot removed default renderPass: " + renderPassToRemove + "!";
		throw std::exception(exceptionStr.c_str());
	}

	m_renderPasses.remove_if([&renderPassToRemove](const RenderPassSharedPtr& storedRenderPass) -> bool
							{
								return storedRenderPass->getName() == renderPassToRemove;
							});

	m_isChanged = true;
	
	return true;
}


bool RenderPassManager::hasRenderPass(const std::string& renderPassName) const
{
	return getRenderPass(renderPassName) != nullptr;
}

RenderPassWeakPtr RenderPassManager::getRenderPass(const std::string& renderPassName) const
{
	auto iter = std::find_if(m_renderPasses.begin(), m_renderPasses.end(), [&renderPassName](const RenderPassSharedPtr& storedRenderPass) -> bool
																			{
																				return renderPassName == storedRenderPass->getName();
																			});

	if (iter == m_renderPasses.end())
		return RenderPassWeakPtr();

	return *iter;
}

const RenderPassManager::RenderPassContainerType& RenderPassManager::getAllRenderPasses() const
{
	if (m_isChanged)
	{
		m_cachedRenderPassesVector.clear();
		m_cachedRenderPassesVector.reserve(m_renderPasses.size());
		for (const auto& renderPass : m_renderPasses)
			m_cachedRenderPassesVector.push_back(renderPass);

		m_isChanged = false;
	}

	return m_cachedRenderPassesVector;
}

void RenderPassManager::initDefaultRenderPasses()
{
	addRenderPassToEnd(std::make_shared<RenderPass>(DefaultRenderPasses::DEFERRED_RENDERING, DeferredRenderer::getInstance()));
	addRenderPassToEnd(std::make_shared<RenderPass>(DefaultRenderPasses::SSAO, SSAORenderer::getInstance()));
	addRenderPassToEnd(std::make_shared<RenderPass>(DefaultRenderPasses::SSAO_BLUR, SSAOBlurRenderer::getInstance()));
	addRenderPassToEnd(std::make_shared<RenderPass>(DefaultRenderPasses::DEFERRED_SHADING, DeferredShadingRenderer::getInstance()));
	addRenderPassToEnd(std::make_shared<RenderPass>(DefaultRenderPasses::PHONG_SHADING, PhongShadingRenderer::getInstance()));
	//addRenderPassToEnd(std::make_shared<RenderPass>(DefaultRenderPasses::FINAL_PASS, BlurRenderer::getInstance()));

#ifdef ABYSS_ENABLE_BLUR
	CompositorPassPtr compositorPass = std::make_shared<CompositorPass>("COMPOSITOR_PASS");
	compositorPass->emplaceRenderPass("ABYSS_HORIZONTAL_BLUR_RENDER_PASS", new BlurRenderer(constants::ABYSS_MAIN_TEXTURE, false));
	compositorPass->emplaceRenderPass("ABYSS_VERTICAL_BLUR_RENDER_PASS", new BlurRenderer(constants::ABYSS_MAIN_TEXTURE, true));
	compositorPass->setIsPostProcessingEffect(true);
	addRenderPassToEnd(compositorPass);
#endif

	addRenderPassToEnd(std::make_shared<RenderPass>(DefaultRenderPasses::FINAL_PASS, new FullScreenRectRenderer(constants::ABYSS_MAIN_TEXTURE)));

	//addRenderPassToEnd(std::make_shared<RenderPass>(DefaultRenderPasses::FINAL_PASS, FullScreenRectRenderer::getInstance()));

	/*
	// TODO: move me to some Unit testing code!
	assert(m_renderPasses.size() == 6);
	assert(getIndexOf(DefaultRenderPasses::DEFERRED_RENDERING) == 0);
	assert(getIndexOf(DefaultRenderPasses::SSAO) == 1);
	assert(getIndexOf(DefaultRenderPasses::SSAO_BLUR) == 2);
	assert(getIndexOf(DefaultRenderPasses::DEFERRED_SHADING) == 3);
	assert(getIndexOf(DefaultRenderPasses::PHONG_SHADING) == 4);
	assert(getIndexOf(DefaultRenderPasses::FINAL_PASS) == 5);
	*/
}

int RenderPassManager::getIndexOf(RenderPassWeakPtr renderPassPtr) const
{
	assert(renderPassPtr);
	return getIndexOf(renderPassPtr->getName());
}

int RenderPassManager::getIndexOf(const std::string& renderPassName) const
{
	int renderPassIndex = 0;
	for (const auto& renderPass : m_renderPasses)
	{
		if (renderPass->getName() == renderPassName)
		{
			return renderPassIndex;
		}

		++renderPassIndex;
	}

	return -1;
}

void RenderPassManager::onRenderingStarted(SceneManager* manager)
{
	auto fullScreenRect = manager->getFullScreenRectRenderable();
	for (auto renderPass : m_cachedDirtyRenderPasses)
	{
		renderPass->polish(manager);

		if (renderPass->isPostProcessingEffect())
			fullScreenRect->addRenderPass(renderPass);
	}
	
	m_cachedDirtyRenderPasses.clear();
}

void RenderPassManager::onRenderingFinished(SceneManager* manager)
{
	// Nothing
}

ABYSS_END_NAMESPACE