#include "AbyssSSAOBlurRenderer.h"

#include "AbyssTexture.h"
#include "AbyssSceneManager.h"
#include "AbyssShaderProgram.h"
#include "AbyssRenderTarget.h"
#include "AbyssWindow.h"

#include "AbyssRenderPass.h"
#include "AbyssRenderPassManager.h"

#include "AbyssSSAORenderer.h"

ABYSS_BEGIN_NAMESPACE

namespace
{
	const std::string KERNEL_SIZE_UNIFORM_NAME = "uKernelSize";
	const std::string INPUT_TEXTURE_UNIFORM_NAME = "uInputTexture";

	const size_t TEMP_TEXTURE_BINDING_POINT = 0;
	const size_t INPUT_TEXTURE_BINDING_POINT = 1;

}

SSAOBlurRenderer::SSAOBlurRenderer()
	: Renderer("SimpleBlur")
	, m_kernelSize(2)
{

}

SSAOBlurRenderer::~SSAOBlurRenderer()
{

}

SSAOBlurRenderer* SSAOBlurRenderer::getInstance()
{
	static SSAOBlurRenderer s_instance;
	return &s_instance;
}

void SSAOBlurRenderer::startRendering(RenderingResultPtr input)
{
	Renderer::startRendering(input);

	m_renderTarget->notifyStartRendering();
	m_tempTexture->bind(TEMP_TEXTURE_BINDING_POINT);
}

void SSAOBlurRenderer::finishRendering(RenderingResultPtr output)
{
	m_tempTexture->release();
	m_renderTarget->notifyFinishRendering();

	Renderer::finishRendering(output);

	auto ssaoRenderTarget = SSAORenderer::getInstance()->getRenderTarget();
	m_renderTarget->blitTo(ssaoRenderTarget);
}

void SSAOBlurRenderer::doInit(SceneManager* manager)
{
	initRenderTarget(manager);
	initFullScreenRect(manager);
}

void SSAOBlurRenderer::initRenderTarget(SceneManager* manager)
{
	auto window = manager->getWindow();
	auto windowSize = window->getSize();

	m_renderTarget.reset(new RenderTarget(manager->getWindow(), 0.f, 0.f, SSAORenderer::TEXTURE_SIZE.x, SSAORenderer::TEXTURE_SIZE.y));
	
	m_tempTexture.reset(new Texture());
	m_tempTexture->setRawData(nullptr, windowSize.x, windowSize.y, PixelFormat::PF_FLOAT, GL_RGB, GL_RED); // Actually, no matters, which input format of texture...
	m_tempTexture->setFiltering(Abyss::Texture::NearestFiltering);
	m_renderTarget->attachTexture(RenderTarget::ColorAttachment0, m_tempTexture);
	// No necessity to add depth buffer here! 

	assert(m_renderTarget->isValid());
}

void SSAOBlurRenderer::initFullScreenRect(SceneManager* manager)
{
	auto fullScreenRendeable = manager->getFullScreenRectRenderable();
	assert(fullScreenRendeable);

	auto thisRenderPass = manager->getRenderPassManager()->getRenderPass(DefaultRenderPasses::SSAO_BLUR);
	fullScreenRendeable->addRenderPass(thisRenderPass);
}

void SSAOBlurRenderer::beforeRender(RenderableObject * const object)
{
	auto boundSceneObject = object->getBoundSceneObject();

	auto ssaoTexture = boundSceneObject->getSceneManager()->getSSAOTexture();

	assert(ssaoTexture->getPixelFormat() == m_tempTexture->getPixelFormat() && "These textures must have the same pixel storage!");
	assert(ssaoTexture->isSamePhysicalSpace(m_tempTexture.get()) && "These textures must have the same physical space!");
	
	setupMatrixUniformsValues(boundSceneObject);

	// Setup uniform values:
	getShaderProgram()->setUniform(KERNEL_SIZE_UNIFORM_NAME, int(getKernelSize()));
	getShaderProgram()->setUniform(INPUT_TEXTURE_UNIFORM_NAME, int(INPUT_TEXTURE_BINDING_POINT));

	ssaoTexture->bind(INPUT_TEXTURE_BINDING_POINT);
}

void SSAOBlurRenderer::afterRender(RenderableObject * const object)
{
	auto boundSceneObject = object->getBoundSceneObject();
	auto ssaoTexture = boundSceneObject->getSceneManager()->getSSAOTexture();

	ssaoTexture->release();
}

bool SSAOBlurRenderer::isSupports(const RenderableObject * const object) const
{
	// TODO: I think it is needed to delete this method at all!
	return true;
}

void SSAOBlurRenderer::setKernelSize(size_t kernelSize)
{
	m_kernelSize = kernelSize;
}

ABYSS_END_NAMESPACE