#pragma once

#include "AbyssRenderer.h"

ABYSS_BEGIN_NAMESPACE

class SimpleRenderer
	: public Renderer
{
public:

	static SimpleRenderer * getInstance();

	SimpleRenderer();
	virtual ~SimpleRenderer();

	virtual bool isSupports(const RenderableObject * const object) const override;
private:
	virtual void beforeRender(RenderableObject * const object) override;

	virtual void afterRender(RenderableObject * const object) override;

};

ABYSS_END_NAMESPACE