#include "AbyssSimpleRenderer.h"

#include "AbyssShaderProgram.h"
#include "AbyssMaterial.h"
#include "AbyssSceneObject.h"
#include "AbyssRenderableObject.h"

namespace
{
	const std::string MATERIAL_NAME = "material";
	const std::string MATERIAL_DEFAULT_COLOT_NAME = MATERIAL_NAME + ".defaultColor";

	const std::string SHADER_NAME = "SimpleRenderer";
}

ABYSS_BEGIN_NAMESPACE

//-------------------------------------------------------------------------------------------------
SimpleRenderer::SimpleRenderer()
	: Renderer(SHADER_NAME)
{

}
//-------------------------------------------------------------------------------------------------
SimpleRenderer::~SimpleRenderer()
{

}
//-------------------------------------------------------------------------------------------------
bool SimpleRenderer::isSupports(const RenderableObject * const object) const
{
	return object != nullptr;
}
//-------------------------------------------------------------------------------------------------
void SimpleRenderer::beforeRender(RenderableObject * const object)
{
	MaterialPtr material = object->getMaterial();
	SceneObject* boundSceneObject = object->getBoundSceneObject();

	getShaderProgram()->setUniform(MATERIAL_DEFAULT_COLOT_NAME, material->getDefaultColor());
	setupMatrixUniformsValues(boundSceneObject);

}
//-------------------------------------------------------------------------------------------------
SimpleRenderer * SimpleRenderer::getInstance()
{
	static SimpleRenderer * renderer = nullptr;
	if (!renderer)
		renderer = new SimpleRenderer();

	return renderer;
}
//-------------------------------------------------------------------------------------------------
void SimpleRenderer::afterRender(RenderableObject * const object)
{
	// Nothing to do here!
}

//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE