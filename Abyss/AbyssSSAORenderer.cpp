#include "AbyssSSAORenderer.h"
#include "AbyssUtilities.h"
#include "AbyssGBuffer.h"
#include "AbyssSceneManager.h"
#include "AbyssShaderProgram.h"
#include "AbyssTexture.h"
#include "AbyssWindow.h"
#include "AbyssRenderPass.h"
#include "AbyssRenderTarget.h"
#include "AbyssRenderPassManager.h"

#include "AbyssRenderingResult.h"

#include <map>

ABYSS_BEGIN_NAMESPACE

namespace
{
	const std::string KERNEL_SIZE_UNIFORM_NAME   = "uKernelSize";
	const std::string KERNEL_RADIUS_UNIFORM_NAME = "uSampleRadius";
	const std::string NOISE_SCALE_UNIFORM_NAME   = "uNoiseScale";
	const std::string SAMPLES_ARRAY_UNIFORM_NAME = "uSamples";
	const std::string NOISE_TEXTURE_UNIFORM_NAME = "uNoiseTexture";

	const size_t SSAO_TEXTURE_BINDING_POINT = 0;

	const std::map<GBuffer::TextureType, std::pair<std::string, int>> s_gBufferTextureUniformsMap =
	{
		{ GBuffer::TextureType::POSITIONS, std::make_pair<std::string, int>(std::string("gPositionsTexture"), 1) },
		{ GBuffer::TextureType::NORMALS, std::make_pair<std::string, int>(std::string("gNormalsTexture"), 2) },
		{ GBuffer::TextureType::COLORS, std::make_pair<std::string, int>(std::string("gAlbedoTexture"), 3) },
	};

	const size_t NOISE_TEXTURE_BINDING_POINT = 4;
}

const Vector2D SSAORenderer::TEXTURE_SIZE = Vector2D(0.25f, 0.25f);

SSAORenderer::SSAORenderer()
	: Renderer("SSAORenderer")
{
	getShaderProgram()->addCustomDefine(GL_FRAGMENT_SHADER, "ABYSS_SSAO_RANGE_CHECK");
}


SSAORenderer::~SSAORenderer()
{

}


SSAORenderer* SSAORenderer::getInstance()
{
	static SSAORenderer s_renderer;
	return &s_renderer;
}

bool SSAORenderer::isSupports(const RenderableObject * const object) const
{
	// TODO: I think it is needed to delete this method at all!
	return true;
}

void SSAORenderer::doInit(SceneManager* manager)
{
	initSamplesContainer();
	initSSAONoiseTexture();

	initFullScreenRect(manager);
	initRenderTarget(manager);
}

void SSAORenderer::beforeRender(RenderableObject * const object)
{
	setupMatrixUniformsValues(object->getBoundSceneObject());

	// TODO: brrr, need to refactor this! SSAO shouldn't rely on only current window.
	Vector2D noiseScale;
	Window* appWindow = SceneManager::instance()->getWindow();
	noiseScale.x = appWindow->getCurrentSize().x;
	noiseScale.y = appWindow->getCurrentSize().y;

	noiseScale /= NOISE_TEXTURE_SIZE;

	// Setup samples container:
	getShaderProgram()->setUniform(KERNEL_SIZE_UNIFORM_NAME, int(NUMBER_OF_SAMPLES));
	getShaderProgram()->setUniform(KERNEL_RADIUS_UNIFORM_NAME, 1.0f);
	getShaderProgram()->setUniform(NOISE_SCALE_UNIFORM_NAME, noiseScale);

	// Vector-based uniform value should be set via first element.
	getShaderProgram()->setUniform(SAMPLES_ARRAY_UNIFORM_NAME + "[0]", m_samples.data(), m_samples.size());

	// Bind noise texture:
	getShaderProgram()->setUniform(NOISE_TEXTURE_UNIFORM_NAME, int(NOISE_TEXTURE_BINDING_POINT));
	m_ssaoNoiseTexture->bind(NOISE_TEXTURE_BINDING_POINT);
}

void SSAORenderer::afterRender(RenderableObject * const object)
{
	m_ssaoNoiseTexture->release();
	// Also need to perform copy of GBuffer's depth buffer to the "main" buffer.
}

void SSAORenderer::initSamplesContainer()
{
	m_samples.clear();
	m_samples.reserve(NUMBER_OF_SAMPLES);

	for (size_t i = 0; i < NUMBER_OF_SAMPLES; ++i)
	{
		auto xPos = Utilities::getRandomFloat() * 2.f - 1.f;
		auto yPos = Utilities::getRandomFloat() * 2.f - 1.f;
		auto zPos = Utilities::getRandomFloat(); // In order to generate hemisphere, not perfect sphere.

		// Some magic with positioning hemisphere samples closer to the actual fragment.
		// Currently the samples are evenly (uniformly) distributed all over the hemisphere.
		// But in our case it is needed to perform sampling closer to the fragment (WHY???)
		// TODO: please, check why this is needed at all...
		float scale = float(i) / NUMBER_OF_SAMPLES;
		// 1. Why need perform LERP here? Why not using smth like scale = scale*scale?
		// 2. Note, that scale ^ 2 makes the sampling distribution closer to the target fragment.
		scale = math::lerp(0.1f, 1.0f, scale * scale);

		Vector3D samplePos = Vector3D(xPos, yPos, zPos);
		samplePos *= scale;

		m_samples.push_back(samplePos);
	}
}

void SSAORenderer::initSSAONoiseTexture()
{
	auto noiseTextureFullSize = NOISE_TEXTURE_SIZE * NOISE_TEXTURE_SIZE;

	std::vector<Vector3D> ssaoNoise;
	ssaoNoise.reserve(noiseTextureFullSize);

	for (GLuint i = 0; i < noiseTextureFullSize; i++)
	{
		Vector3D noise(Utilities::getRandomFloat() * 2.0f - 1.0f, Utilities::getRandomFloat() * 2.0f - 1.0f, 0.0f);
		ssaoNoise.push_back(noise);
	}

	// Init actual texture:
	m_ssaoNoiseTexture.reset(new Texture());
	m_ssaoNoiseTexture->setRawData(reinterpret_cast<const void*>(ssaoNoise.data()),
								   NOISE_TEXTURE_SIZE,
								   NOISE_TEXTURE_SIZE,
								   Abyss::PF_FLOAT,
								   GL_RGB,
								   GL_RGB16F);


	// TODO: Why nearest? Better to use linear filtering!
	m_ssaoNoiseTexture->setFiltering(Abyss::Texture::NearestFiltering);
	// Repeat is needed in order to achieve sampling all over the texture.
	m_ssaoNoiseTexture->setWrapping(Abyss::Texture::RepeatWrapping);

}

void SSAORenderer::initFullScreenRect(SceneManager* manager)
{
	auto fullScreenRendeable = manager->getFullScreenRectRenderable();
	assert(fullScreenRendeable);

	auto ssaoRenderPass = manager->getRenderPassManager()->getRenderPass(DefaultRenderPasses::SSAO);
	fullScreenRendeable->addRenderPass(ssaoRenderPass);
}

void SSAORenderer::initRenderTarget(SceneManager* manager)
{
	auto window = manager->getWindow();
	auto windowSize = window->getSize();

	m_renderTarget.reset(new RenderTarget(manager->getWindow(), 0.f, 0.f, TEXTURE_SIZE.x, TEXTURE_SIZE.y));

	auto ssaoTexture = manager->getSSAOTexture();
	assert(ssaoTexture);
	ssaoTexture->setRawData(nullptr, windowSize.x, windowSize.y, PixelFormat::PF_FLOAT, GL_RGB, GL_RED); // Actually, no matters, which input format of texture...
	ssaoTexture->setFiltering(Abyss::Texture::NearestFiltering);
	m_renderTarget->attachTexture(RenderTarget::ColorAttachment0, ssaoTexture);

	//Abyss::DepthBuffer::Pointer depthBuffer(new Abyss::DepthBuffer(windowSize.x, windowSize.y, 0));
	//m_renderTarget->attachDepthBuffer(depthBuffer);

	assert(m_renderTarget->isValid());

}

void SSAORenderer::startRendering(RenderingResultPtr input)
{
	Renderer::startRendering(input);

	// Attaching output SSAO texture:
	m_renderTarget->notifyStartRendering();
	getSSAOTexture()->bind(SSAO_TEXTURE_BINDING_POINT);

	// Attaching input GBuffer:
	if (auto gBuffer = input->getGBuffer())
	{
		for (const auto& gBufferIter : s_gBufferTextureUniformsMap)
		{
			const auto& uniformName = gBufferIter.second.first;
			const auto& bindingPoint = gBufferIter.second.second;

			getShaderProgram()->setUniform(uniformName, bindingPoint);
			gBuffer->getTexture(gBufferIter.first)->bind(bindingPoint);
		}
	}
	else 
	{
		raiseAnError("Can't find an GBuffer!");
	}
}

void SSAORenderer::finishRendering(RenderingResultPtr output)
{
	auto gBuffer = output->getGBuffer();
	for (const auto& gBufferIter : s_gBufferTextureUniformsMap)
	{
		gBuffer->getTexture(gBufferIter.first)->release();
	}
	
	getSSAOTexture()->release();
	output->setTexture(constants::ABYSS_SSAO_TEXTURE, getSSAOTexture());
	m_renderTarget->notifyFinishRendering();
	
	Renderer::finishRendering(output);
}

RenderTarget* SSAORenderer::getRenderTarget() const
{
	return m_renderTarget.get();
}

void SSAORenderer::onDetach(RenderPass* renderPass)
{
	assert(renderPass);

	m_renderTarget->bind();
	m_renderTarget->clear();
	m_renderTarget->release();
}

Abyss::TexturePtr SSAORenderer::getSSAOTexture() const
{
	return m_renderTarget->getAttachedTexture(RenderTarget::ColorAttachment0);
}

ABYSS_END_NAMESPACE