#pragma once

#include "AbyssRenderableObject.h"

ABYSS_BEGIN_NAMESPACE

class Renderer;
class SceneObject;

class SimpleRenderableObject
	: public RenderableObject
{

public:

	// Creates renderable object with specified geometry object and name.
	// Renderable will using SimpleRenderer as own renderer.
	static SimpleRenderableObjectPtr create(const String & name, GeometryObjectPtr geometryObject);

	// Creates renderable object with specified name. The renderer and geometry objects are undefined!
	SimpleRenderableObject(const String & name);
	~SimpleRenderableObject();

	// SETTERS
	// Change mesh for this one rednerable object.
	void setGeometryObject(GeometryObjectPtr object);
	virtual  void setupDefaultRenderPasses() override;

	// GETTERS
	virtual GeometryObjectPtr getGeometryObject() const override;

protected:
	virtual AABB getAABB() const override;
	virtual void performRendering(RendererVisitor* visitor) override;

private: // Methods

	GeometryObjectPtr m_geometryObject;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline void SimpleRenderableObject::setGeometryObject(GeometryObjectPtr object)
{	m_geometryObject = object; }
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE