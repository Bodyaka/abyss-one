#include "AbyssMeshRenderable.h"

#include <limits>
#include "AbyssMesh.h"
#include "AbyssRenderer.h"
#include "AbyssBoundingBox.h"
#include "AbyssSceneObject.h"

ABYSS_BEGIN_NAMESPACE
//----------------------------------------------------------------------------------------------
MeshRenderablePtr MeshRenderable::create(const String & name, MeshPtr mesh)
{
	MeshRenderable * object = new MeshRenderable(name);

	object->setupDefaultRenderPasses();
	object->setMesh(mesh);
	object->setMaterial(mesh->getMaterial());

	return MeshRenderablePtr(object);
}
//----------------------------------------------------------------------------------------------
MeshRenderablePtr MeshRenderable::create(const String & name, MeshPtr mesh, MaterialPtr material)
{
	MeshRenderable * object = new MeshRenderable(name);

	object->setupDefaultRenderPasses();
	object->setMesh(mesh);
	object->setMaterial(material);

	return MeshRenderablePtr(object);
}
//---------------------------------------------------------------------------------------------
MeshRenderable::MeshRenderable(const String & name)
	: RenderableObject(name)
	, m_mesh()
{
}
//--------------------------------------------------------------------------------------------------
MeshRenderable::~MeshRenderable()
{
}
//-------------------------------------------------------------------------------------------------
GeometryObjectPtr MeshRenderable::getGeometryObject() const
{
	return std::static_pointer_cast<GeometryObject, Mesh>(getMesh());
}
//-------------------------------------------------------------------------------------------------
AABB MeshRenderable::getAABB() const
{
	if (getMesh())
		return getMesh()->getAABB();

	return AABB();
}
//-------------------------------------------------------------------------------------------------
void MeshRenderable::performRendering(RendererVisitor* visitor)
{
	if (auto mesh = getMesh())
		mesh->render(visitor);
}
//-------------------------------------------------------------------------------------------------
void MeshRenderable::setMesh(MeshPtr mesh)
{
	if (mesh == getMesh())
		return;

	m_mesh = mesh;

	MaterialPtr material;
	if (mesh)
		material = mesh->getMaterial();

	setMaterial(material);

	m_isBoundingBoxDirty = true;
}
//----------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE

