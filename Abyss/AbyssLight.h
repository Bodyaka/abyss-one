#pragma once

#include "AbyssRendering.h"
#include "AbyssMovableObject.h"

ABYSS_BEGIN_NAMESPACE

class SceneObject;
class LightsManager;

enum class LightType
{
	PointLight = 0,
	DirectionalLight,
	SpotLight
};

class Light
	: public MovableObject
{
public:
	Light(const std::string& name);
	~Light();

	void setType(LightType lightType);
	LightType getLightType() const;

	// Specifies point light attenuation.
	// http://www.ogre3d.org/tikiwiki/-Point+Light+Attenuation
	void setAttenutation(float constantFactor, float linearFactor, float quadraticFactor);
	const Vector3D& getAttenuation() const;
	float getConstantAttenutation() const;
	float getLinearAttenutation() const;
	float getQuadraticAttenuation() const;

	// Color of the Light
	void setColor(const Vector4D& color);
	const Vector4D& getColor() const;

	// Changes the direction of the light (affects only directional light).
	void setDirection(const Vector3D& direction);
	const Vector3D& getDirection() const;

	// Changes the inner and outer angles of the point light:
	// Note, that all angles are in radians!
	void setInnerCutOffAngle(float radians);
	float getInnerCutOffAngle() const;

	void setOuterCutOffAngle(float radians);
	float getOuterCutOffAngle() const;

	// Specifies whether this light is active
	void setActive(bool activeFlag);
	bool isActive() const;

	// Specifies whether light is visible. The appearance of the light is specified by the Light's renderable.
	void setVisible(bool visibility);
	bool isVisible() const;

	// Set the name, which will be specified in the LightsManager.
	const std::string& getName() const;

	virtual void updateTransform() override;

	// Specifies the renderable object, which will render the light:
	void setRenderableObject(RenderableObjectPtr object);
	RenderableObjectPtr getRenderableObject() const;

	void attachToSceneObject(SceneObject* object);
	void deattachFromSceneObject();
	SceneObject* getAttachedSceneObject() const;

private:

	friend class LightsManager;

	Light(const Light& other) = delete;
	Light& operator=(const Light& other) = delete;

	void setCreator(LightsManager* manager);

private:

	LightsManager* m_creator;

	LightType m_lightType;

	std::string m_name;
	Vector3D	m_attenutation;
	Vector4D	m_color;
	Vector3D m_direction;
	float m_innerCutOffAngle;
	float m_outerCutOffAngle;

	bool m_isActive;
	bool m_isVisible;

	RenderableObjectPtr m_renderableObject;

	SceneObject* m_attachedSceneObject;
};


// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline float Light::getConstantAttenutation() const
{	return m_attenutation.r;	}
//----------------------------------------------------------------------------------------------
inline float Light::getLinearAttenutation() const
{	return m_attenutation.g;	}
//----------------------------------------------------------------------------------------------
inline float Light::getQuadraticAttenuation() const
{	return m_attenutation.b;	}
//----------------------------------------------------------------------------------------------
inline const Vector4D& Light::getColor() const
{	return m_color;	}
//----------------------------------------------------------------------------------------------
inline bool Light::isActive() const
{	return m_isActive;	}
//----------------------------------------------------------------------------------------------
inline bool Light::isVisible() const
{	return m_isVisible;	}
//----------------------------------------------------------------------------------------------
inline const std::string& Light::getName() const
{	return m_name;	}
//----------------------------------------------------------------------------------------------
inline RenderableObjectPtr Light::getRenderableObject() const
{	return m_renderableObject;	}
//----------------------------------------------------------------------------------------------
inline Abyss::LightType Light::getLightType() const
{	return m_lightType;	}
//----------------------------------------------------------------------------------------------
inline const Vector3D& Light::getDirection() const
{	return m_direction; }
//----------------------------------------------------------------------------------------------
inline SceneObject* Light::getAttachedSceneObject() const
{	return m_attachedSceneObject;	}
//----------------------------------------------------------------------------------------------
inline const Vector3D& Light::getAttenuation() const
{	return m_attenutation;	}
//----------------------------------------------------------------------------------------------
inline float Light::getInnerCutOffAngle() const
{	return m_innerCutOffAngle;	}
//----------------------------------------------------------------------------------------------
inline float Light::getOuterCutOffAngle() const
{	return m_outerCutOffAngle;	}
//----------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE