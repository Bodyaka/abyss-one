#include "AbyssDepthBuffer.h"

ABYSS_BEGIN_NAMESPACE

//-------------------------------------------------------------------------------------------------
DepthBuffer::DepthBuffer(size_t width, size_t height, size_t depth)
	: m_width(width)
	, m_height(height)
	, m_depth(depth)
	, m_handle(-1)
{
	glGenRenderbuffers(1, &m_handle);
	setupBufferStorage();
}
//-------------------------------------------------------------------------------------------------
DepthBuffer::~DepthBuffer()
{
	glDeleteRenderbuffers(1, &m_handle);
}
//-------------------------------------------------------------------------------------------------
void DepthBuffer::setupBufferStorage()
{
	bind();
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, getWidth(), getHeight());
	release();
}
//-------------------------------------------------------------------------------------------------
void DepthBuffer::bind()
{
	glBindRenderbuffer(GL_RENDERBUFFER, getHandle());
}
//-------------------------------------------------------------------------------------------------
void DepthBuffer::release()
{
	glBindRenderbuffer(GL_RENDERBUFFER, 0); // WHAT?
}
//-------------------------------------------------------------------------------------------------
void DepthBuffer::setSize(size_t width, size_t height)
{
	if (getWidth() == width && getHeight() == height)
		return;

	m_width = width;
	m_height = height;

	setupBufferStorage();
}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE