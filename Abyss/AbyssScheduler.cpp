#include "AbyssScheduler.h"
#include "AbyssSchedulersManager.h"

ABYSS_BEGIN_NAMESPACE

//----------------------------------------------------------------------------------------------
Scheduler::Scheduler()
	: m_currTimeInMsecs(0)
	, m_targetTimeInMsecs(0)
	, m_isSingleShot(false)
	, m_isNeedDeleteOnStop(false)
	, m_isPaused(true)
	, m_stopConditionFunc(nullptr)
	, m_updateFunc(nullptr)
{
	SchedulersManager::getInstance()->registerScheduler(this);
}
//----------------------------------------------------------------------------------------------
Scheduler::~Scheduler()
{
	SchedulersManager::getInstance()->unregisterScheduler(this);
}
//----------------------------------------------------------------------------------------------
void Scheduler::update(int msecs)
{
	if (isPaused())
		return;

	assert(getUpdateFunc() && "The update function is not set to scheduler: it is in idle working state!");

	m_currTimeInMsecs += msecs;

	if (getStopConditionFunc() && getStopConditionFunc()())
	{
		stop();
		return;
	}

	if (m_currTimeInMsecs >= m_targetTimeInMsecs)
	{
		if (getUpdateFunc())
		{
			auto deltaTime = m_currTimeInMsecs - m_targetTimeInMsecs;
			getUpdateFunc()(deltaTime);
		}

		if (isSingleShot())
		{
			stop();
			return;
		}

		m_currTimeInMsecs = 0;
	}
}
//----------------------------------------------------------------------------------------------
void Scheduler::start()
{
	m_isPaused = false;
}
//----------------------------------------------------------------------------------------------
void Scheduler::pause()
{
	m_isPaused = true;
}
//----------------------------------------------------------------------------------------------
void Scheduler::stop()
{
	if (isDeleteOnStop())
	{
		delete this;
		return;
	}

	m_isPaused = true;
}
//----------------------------------------------------------------------------------------------
Scheduler* Scheduler::scheduleOnce(int msecs, const UpdateFuncType& updateFunc)
{
	Scheduler* scheduler = new Scheduler();
	scheduler->setInterval(msecs);
	scheduler->setUpdateFunc(updateFunc);
	scheduler->setSingleShot(true);
	scheduler->setDeleteOnStop(true);

	scheduler->start();

	return scheduler;
}
//----------------------------------------------------------------------------------------------
Scheduler* Scheduler::scheduleForever(int msecs, const UpdateFuncType& updateFunc)
{
	Scheduler* scheduler = new Scheduler();
	scheduler->setInterval(msecs);
	scheduler->setUpdateFunc(updateFunc);
	scheduler->setSingleShot(false);

	scheduler->start();

	return scheduler;
}
//----------------------------------------------------------------------------------------------
Scheduler* Scheduler::scheduleUntil(int msecs, const UpdateFuncType& updateFunc, const StopConditionFuncType& stopFunc)
{
	Scheduler* scheduler = new Scheduler();
	scheduler->setInterval(msecs);
	scheduler->setUpdateFunc(updateFunc);
	scheduler->setStopConditionFunc(stopFunc);
	scheduler->setDeleteOnStop(true);

	scheduler->start();

	return scheduler;
}
//----------------------------------------------------------------------------------------------
void Scheduler::setInterval(int msecs)
{
	m_targetTimeInMsecs = msecs;
	m_currTimeInMsecs = 0;
}
//----------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE