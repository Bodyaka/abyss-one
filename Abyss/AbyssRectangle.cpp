#include "AbyssRectangle.h"

ABYSS_BEGIN_NAMESPACE

Rectangle::Rectangle(const Vector2D & position, const Vector2D & size)
	: Mesh(GL_TRIANGLES)
{
	setPosition(position);
	setSize(size);
}

void Rectangle::setPosition(const Vector2D & pos)
{
	if (pos == getPosition())
		return;

	m_position = pos;
}

void Rectangle::setSize(const Vector2D & size)
{
	if (size == getSize())
		return;

	m_size = size;
}

ABYSS_END_NAMESPACE