#include "AbyssRenderer.h"

ABYSS_BEGIN_NAMESPACE

class RenderTarget;

class SSAORenderer
	: public Renderer
{
public:
	// This static variable describes the size of the SSAO texture in relative coordinate system.
	// This value will be multiplied by the actual size of window, where this renderer is rendered to.
	// By default, the size is (0.25f, 0.25f)
	static const Vector2D TEXTURE_SIZE;

public:

	static SSAORenderer* getInstance();

	virtual bool isSupports(const RenderableObject * const object) const override;

	virtual void startRendering(RenderingResultPtr input) override;
	virtual void finishRendering(RenderingResultPtr output) override;

	// TODO: maybe need to remove this method!
	virtual void onDetach(RenderPass* renderPass) override;
	
	RenderTarget* getRenderTarget() const;

protected:
	virtual void doInit(SceneManager* manager);
	virtual void beforeRender(RenderableObject * const object);
	virtual void afterRender(RenderableObject * const object);

	void initRenderTarget(SceneManager* manager);
	void initFullScreenRect(SceneManager* manager);

	// Ctor and assignment:
	SSAORenderer();
	~SSAORenderer();

private:
	
	void initSamplesContainer();
	void initSSAONoiseTexture();
	
	// Helper method to return output SSAO texture:
	TexturePtr getSSAOTexture() const;

	SSAORenderer(const SSAORenderer& other) = delete;
	SSAORenderer& operator=(const SSAORenderer& other) = delete;


	std::vector<Vector3D> m_samples;
	TexturePtr m_ssaoNoiseTexture;
	
	std::unique_ptr<RenderTarget> m_renderTarget;

	const size_t NUMBER_OF_SAMPLES = 64;
	const size_t NOISE_TEXTURE_SIZE = 4;
};

ABYSS_END_NAMESPACE