#include "AbyssVertexBufferObject.h"

#include <map>

ABYSS_BEGIN_NAMESPACE

#define VBO_BIND_ASSERT assert(false && "Buffer object should be binded to use this method!");
//----------------------------------------------------------------------------------------------
VertexBufferObject::VertexBufferObject(GLenum type)
	: m_type(type)
{
	glGenBuffers(1, &m_id);
}
//----------------------------------------------------------------------------------------------
VertexBufferObject::~VertexBufferObject()
{
	glDeleteBuffers(1, &m_id);
}
//----------------------------------------------------------------------------------------------
VertexBufferObjectPtr VertexBufferObject::createBufferObject(GLenum type)
{
	return VertexBufferObjectPtr(new VertexBufferObject(type));
}
//----------------------------------------------------------------------------------------------
VertexBufferObjectPtr VertexBufferObject::createBufferObject(GLenum type, size_t byteCount, void * data, GLenum drawMode)
{
	VertexBufferObjectPtr buffer = createBufferObject(type);
	buffer->bufferData(byteCount, data, drawMode);
	return buffer;
}
//----------------------------------------------------------------------------------------------
void VertexBufferObject::bind() const
{
	glBindBuffer(m_type, m_id);
}
//----------------------------------------------------------------------------------------------
void VertexBufferObject::release() const
{
	glBindBuffer(m_type, 0);
}
//----------------------------------------------------------------------------------------------
bool VertexBufferObject::bufferData(size_t byteCount, void * data, GLenum usage)
{
	if (!data || !byteCount)
		return false;

	// Bind this one buffer
	bind();

	glBufferData(m_type, byteCount, data, usage);

	// Release it:
	release();

	return true;
}
//----------------------------------------------------------------------------------------------
size_t VertexBufferObject::getSize() const
{
	if (!isBinded())
	{
		VBO_BIND_ASSERT
		return size_t();
	}

	GLint value;
	glGetBufferParameteriv(m_type, GL_BUFFER_SIZE, &value);
	return size_t(value);
}
//----------------------------------------------------------------------------------------------
GLenum VertexBufferObject::getUsage() const
{
	if (!isBinded())
	{
		VBO_BIND_ASSERT

		return GLenum(0);
	}

	GLint value;
	glGetBufferParameteriv(m_type, GL_BUFFER_USAGE, &value);
	return value;
}
//----------------------------------------------------------------------------------------------
bool VertexBufferObject::isBinded() const
{
	GLint value;
	GLenum params = (m_type == GL_ARRAY_BUFFER) ? GL_ARRAY_BUFFER_BINDING : GL_ELEMENT_ARRAY_BUFFER_BINDING;
	glGetIntegerv(params, &value);

	return size_t(value) == m_id;
}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
