#include "AbyssMath.h"
#include "AbyssSceneObject.h"

ABYSS_BEGIN_NAMESPACE
ABYSS_MATH_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------------------
Vector3D swizzleProj(const Vector4D & in_vec)
{
	Vector4D temp = in_vec / in_vec.w;

	return Vector3D(temp.x, temp.y, temp.z);
}
//--------------------------------------------------------------------------------------------------
Vector3D swizzle3D(const Vector4D & in_vec)
{
	return Vector3D(in_vec.x, in_vec.y, in_vec.z);
}
//--------------------------------------------------------------------------------------------------
Abyss::Matrix4D getRotationMatrixAroundObjectCenter(SceneObject* object, Matrix4D inRotationMatrix)
{
	auto objectLocalPos = getObjectLocalRotationCenter(object);
	Matrix4D resultMatrix;

	// The order is reversed. I.e. last transformation will occur first.
	resultMatrix = glm::translate(resultMatrix, objectLocalPos);
	resultMatrix = resultMatrix * inRotationMatrix;
	resultMatrix = glm::translate(resultMatrix, -objectLocalPos);

	return resultMatrix;
}
//--------------------------------------------------------------------------------------------------
Abyss::Vector3D getObjectLocalRotationCenter(SceneObject* object)
{
	// The transformation order is next: Scale -> Rotation -> Translation.
	// So, before rotation, we have to include scale in order to properly compute object's local center.
	return object->getLocalAABB().center() * object->getScale();
}
//--------------------------------------------------------------------------------------------------

ABYSS_MATH_END_NAMESPACE
ABYSS_END_NAMESPACE
