#pragma once

#include <list>
#include <vector>

#include "AbyssEvent.h"
#include "AbyssApplicationContext.h"

ABYSS_BEGIN_NAMESPACE

// FORWARD DECLARATIONS
class Window;
class MeshManager;
class SceneManager;
class ShaderProgramManager;
class CameraController;
class EventHandler;

class Application
{
public:
	// Typedefs:
	typedef ApplicationContext::CreateOptions CreateOptions; 

	// Friends:
	friend class Window;
	friend class ApplicationContext;
public:

	Application(const CreateOptions & createOptions);
	~Application();

	// Make this one application current:
	void makeCurrent();

	// Start application!!!1
	void start();

	// Render function.
	void render();
	

	// Process input event.
	void processEvent(const EventPtr & event);

	void addEventHandler(EventHandler * handler);
	void deleteEventHandler(EventHandler* handler);
	EventHandler* removeEventHandler(EventHandler* handler);

	const std::vector<EventHandler*>& getEventHandlers() const;

	// Return scene manager object:
	SceneManager * getSceneManager() const;
	// Return shaders' manager:
	ShaderProgramManager * getShaderProgramManager() const;

	// Return shared application:
	static Application * getCurrentApplication();


	// This method will be called before start of main loop.
	// You can setup neccessary data here.
	virtual void setup();

private:

	Application(const Application & );
	void operator = (const Application &);

private:

	std::vector<EventHandler*> m_eventHandlers;

	// Mesh Manager.
	MeshManager * m_meshManager;
	// Scene manager.
	SceneManager * m_sceneManager;
	// Shader Manager.
	ShaderProgramManager * m_shaderProgramManager;

	static Application * m_sharedApplication;
};

// INLINE METHODS
//-----------------------------------------------------------------------------------------------
inline const std::vector<EventHandler*>& Application::getEventHandlers() const
{	return m_eventHandlers;	}
//-----------------------------------------------------------------------------------------------
inline SceneManager * Application::getSceneManager() const
{	return m_sceneManager;	}
//-----------------------------------------------------------------------------------------------
inline ShaderProgramManager * Application::getShaderProgramManager() const
{   return m_shaderProgramManager;	}
//-----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE
