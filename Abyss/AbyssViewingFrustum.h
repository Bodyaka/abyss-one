#pragma once

#include "AbyssGlm.h"
#include "AbyssPlane.h"
#include "AbyssFrustum.h"
#include "AbyssChangeableObject.h"

ABYSS_BEGIN_NAMESPACE

class ViewingFrustum
	: public ChangeableObject
{
public:
	struct Params
	{
		float m_fovy,
			  m_aspect,
			  m_near,
			  m_far;
	};
public:
	ViewingFrustum(const Params & params);
	~ViewingFrustum();

	// Setup Frustum with given params
	void setParams(const Params & params);
	const Params& getParams() const;

	// project from 2d device coordinates into 3d world coordinates.
	Vector3D unProject(const Vector2D & point);

	// ACCESS
	// horizontal field of view.
	float getFovX() const;
	// vertical field of view.
	float getFovY() const;
	// focal length. Distance from eye point to projection plane.
	float getFocalLength() const;

	// return aspect (width / height)
	float getAspect() const;

	// Return distance from eye to near projection plane
	float getNearDistance() const;
	// Return distance from eye to far projection plane
	float getFarDistance() const;

	// Normal planes. All planes are set in clipspace coordinate system.
	Plane getNearPlane()   const;
	Plane getFarPlane()    const;
	Plane getLeftPlane()   const;
	Plane getRightPlane()  const;
	Plane getBottomPlane() const;
	Plane getTopPlane()    const;

	// Return view matrix of frustum
	const Matrix4D & getViewMatrix() const;
	// Return projection matrix of frustum.
	const Matrix4D & getProjectionMatrix() const;
	// Return PorjectionViewMatrix:
	const Matrix4D & getProjectionViewMatrix() const;

	Frustum getClipSpaceFrustum() const;
	const Frustum & getWorldSpaceFrustum() const;

protected:

	void setViewMatrix(const Matrix4D & viewMatrix);

private:

	void updateProjectionViewMatrix();

private: // members

	Params m_params;

	// Additional cashed parameters:
	float m_fovx,
		  m_focalLength;

	Matrix4D m_viewMatrix;
	Matrix4D m_projectionMatrix;
	Matrix4D m_projecionViewMatrix;

	Frustum m_worldFrustum;
};

// INLINE METHODS
//--------------------------------------------------------------------------------------------------
inline const Matrix4D & ViewingFrustum::getViewMatrix() const
{	return m_viewMatrix;	}
//--------------------------------------------------------------------------------------------------
inline const Matrix4D & ViewingFrustum::getProjectionMatrix() const
{	return m_projectionMatrix;	}
//--------------------------------------------------------------------------------------------------
inline const Matrix4D & ViewingFrustum::getProjectionViewMatrix() const
{	return m_projecionViewMatrix;	}
//--------------------------------------------------------------------------------------------------
inline float ViewingFrustum::getAspect() const
{	return getParams().m_aspect;	}
//--------------------------------------------------------------------------------------------------
inline float ViewingFrustum::getFovX() const
{	return m_fovx;	}
//--------------------------------------------------------------------------------------------------
inline float ViewingFrustum::getFovY() const
{	return getParams().m_fovy;	}
//--------------------------------------------------------------------------------------------------
inline float ViewingFrustum::getFocalLength() const
{	return m_focalLength;	}
//--------------------------------------------------------------------------------------------------
inline float ViewingFrustum::getNearDistance() const
{	return m_params.m_near;	}
//--------------------------------------------------------------------------------------------------
inline float ViewingFrustum::getFarDistance() const
{	return getParams().m_far;	}
//--------------------------------------------------------------------------------------------------
inline const Frustum & ViewingFrustum::getWorldSpaceFrustum() const
{	return m_worldFrustum;	}
//--------------------------------------------------------------------------------------------------
inline const ViewingFrustum::Params& ViewingFrustum::getParams() const
{	return m_params;	}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE