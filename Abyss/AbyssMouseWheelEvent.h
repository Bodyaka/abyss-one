#pragma once

#pragma once

#include "AbyssMouseEvent.h"

ABYSS_BEGIN_NAMESPACE

class MouseWheelEvent
	: public MouseEvent
{
public:

	// FABRICS
	static MouseWheelEventPtr createMouseWheelEvent(const Vector2D & position, int delta);

	int getDelta() const;

protected:

	// Constructor
	MouseWheelEvent(const Vector2D & position, int delta);

	int m_delta;
};

//-------------------------------------------------------------------------------------------------
inline int MouseWheelEvent::getDelta() const
{	return m_delta;	}
//-------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE