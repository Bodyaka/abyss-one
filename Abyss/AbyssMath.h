#pragma once

#include "AbyssGlm.h"

#define ABYSS_MATH_BEGIN_NAMESPACE namespace math {
#define ABYSS_MATH_END_NAMESPACE }

ABYSS_BEGIN_NAMESPACE

class SceneObject;

ABYSS_MATH_BEGIN_NAMESPACE

Vector3D swizzle3D(const Vector4D & in_vec);
Vector3D swizzleProj(const Vector4D & in_vec);

Matrix4D getRotationMatrixAroundObjectCenter(SceneObject* object, Matrix4D inRotationMatrix);
Vector3D getObjectLocalRotationCenter(SceneObject* object);

template <class Type>
Type lerp(const Type& a, const Type& b, float f)
{
	return a + f * (b - a);
}


ABYSS_MATH_END_NAMESPACE
ABYSS_END_NAMESPACE