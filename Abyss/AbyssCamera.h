#pragma once

#include "AbyssGlm.h"
#include "AbyssViewingFrustum.h"

ABYSS_BEGIN_NAMESPACE
	
class SceneObject;
class Window;

class Camera
	: public ViewingFrustum
{
public:
	Camera(const Vector3D & eye, const Vector3D & center, const Vector3D & up, const ViewingFrustum::Params & params);
	~Camera();

	void move(const Vector3D & offset);
	void setup(const Vector3D & eye, const Vector3D & center, const Vector3D & up);

	void setEyePosition(const Vector3D & position);
	void setCenterPosition(const Vector3D & position);
	void setUpVector(const Vector3D & upVector);

	void yaw(float degree);
	void roll(float degree);
	
	// Giving yaw and roll offset we updates current position of camera's eye:
	void setYawRoll(float yawOffset, float rollOffset);

	// Set target scene object, which camera will chase
	void setTarget(SceneObject * object);
	// Return target scene object, which camera is chasing
	SceneObject * getTarget() const;

	const Vector3D & getEyePosition() const;
	const Vector3D & getCenterPosition() const;
	const Vector3D & getUpVector() const;

	void focusOnObject(SceneObject* object);
	void stickToWindow(Window* window);
	Window* getStickedWindow() const;

protected:

	void updateViewMatrix();

	// Main camera entities:
	Vector3D m_eyePosition,
			 m_centerPosition,
			 m_upVector;

private:
	SceneObject * m_target;

	class ResizeListener;
	std::unique_ptr<ResizeListener> m_resizeListener;
};

// INLINE METHODS
//----------------------------------------------------------------------------------------------
inline const Vector3D & Camera::getEyePosition() const
{	return m_eyePosition;	}
//----------------------------------------------------------------------------------------------
inline const Vector3D & Camera::getCenterPosition() const
{	return m_centerPosition;	}
//----------------------------------------------------------------------------------------------
inline const Vector3D & Camera::getUpVector() const
{	return m_upVector;	}
//----------------------------------------------------------------------------------------------
inline SceneObject * Camera::getTarget() const
{	return m_target;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE