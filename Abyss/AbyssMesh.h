#pragma once

#include "AbyssGlm.h"
#include "AbyssDeus.h"
#include "AbyssGeometryObject.h"

ABYSS_BEGIN_NAMESPACE

class AABB;

// TODO need optimization with HashMap.
// Mesh - just mesh, it cannot has it's own model matrix. It has only geometry.
class Mesh
	: public Deus<Mesh>
	, public GeometryObject
{
	typedef Deus<Mesh> BaseMesh;

public:

	// Specify data for vertices:
	struct VertexData
	{
	public:// Members
		ContainerV3D  positions;
		ContainerV2D  texels;
		ContainerV3D  normals;
		ContainerV3D  tangents;
		ContainerV3D  bitangents;

		// Clear all vertex data containers:
		void clear();
	};

public:
	// Creates mesh. Note, that name will be generated automatically.
	Mesh(GLenum drawMode, Mesh * parent = NULL);
	// Creating mesh with specified attributes (properties). Parent will set to NULL.
	Mesh(const String & name, GLenum drawMode, Mesh * parent = NULL);
	~Mesh();

	// Render mesh without bounding box.
	virtual void render(RendererVisitor* visitor);

	// SUBMESHING.
	// Creates sub mesh with specified name and drawMode.
	Mesh * createSubMesh(const String & name, GLenum drawMode);

	// Creates sub mesh with specified name and drawMode and specified data.
	Mesh * createSubMesh(const String & name, GLenum drawMode, const VertexData & data);

	// Return mesh of specified name.
	Mesh * getSubMesh(const String & name);

	// DATA PROVIDING.
	// Set specified data with cleaning the old one.
	void setVertexData(const VertexData & data);
	// Overloaded function with raw arrays.
	void setVertexData(size_t numOfVertices, float * verticesPositions, float * verticesTexels, float * verticesNormals, float * verticesTangents, float * verticesBitangents);

	// Set indices of mesh
	void setIndices(const ContainerUint & indices);

	void setMaterial(MaterialPtr material);
	MaterialPtr getMaterial() const;

	// ACCESS
	// Return draw mode of this one mesh (I.e. LINES, TRIANGLES, ETC.)
	inline GLenum getDrawMode() const;
	//Return name of this one mesh.
	inline const String & getName() const;
		
	// Return number of vertex per primitive in mesh.
	// If primitive is point - will return 1, line - 2, triangle - 3, etc.
	size_t getVerticesSizePerPrimitive() const;

	// INQUIRY
	
	virtual const AABB& getAABB() const override;

	// Some generators:
	static MeshPtr createFullScreenRect();
	static MeshPtr createRect(const std::string& meshName, const Vector3D& topLeft, const Vector3D& bottomLeft, const Vector3D& bottomRight, const Vector3D& topRight);


protected:
	// Generating specified texture coordinates depends on AABB. So, you need to generate AABB first.
	void generateTextureCoords(const ContainerV3D & vertices, ContainerV2D & texels);

protected: // Mesh:

	// name of mesh.
	String m_name;

private:
	// Generates some mesh name.
	static String generateMeshName();

private:
	MaterialPtr m_material;

	static int m_numberOfGeneratedMeshes;

	// TODO: dirty flag should be accessible higher
	void markBboxDirty();
	mutable bool m_isBoundingBoxDirty = true;

	mutable AABB m_unitedAABB;
};

// INLINE
//----------------------------------------------------------------------------------------------
GLenum Mesh::getDrawMode() const
{	return m_drawMode;	}
//----------------------------------------------------------------------------------------------
const String & Mesh::getName() const
{	return m_name;	}
//----------------------------------------------------------------------------------------------
inline void Mesh::setMaterial(MaterialPtr material)
{	m_material = material;	}
//----------------------------------------------------------------------------------------------
inline MaterialPtr Mesh::getMaterial() const
{	return m_material;	}
//----------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE


