#pragma once

#include "Abyss.h"
#include "AbyssSingleton.h"
#include <string>

ABYSS_BEGIN_NAMESPACE

class FileSystem
	: public Singleton<FileSystem>
{
public:


	FileSystem();
	virtual ~FileSystem();

	static char getDelimeter();

	std::string getDirectory(const std::string& filePath) const;
	std::string getFileName(const std::string& filePath) const;

	// Add new path entry to the filePath.
	// It is like filePath += getDelimeter() + pathEntry;
	void appendPathEntry(std::string& filePath, const std::string& pathEntry) const;

	bool writeToFile(const std::string& filePath, const std::string& contents, bool overwrite = true);

	

};

ABYSS_END_NAMESPACE