#include "AbyssRenderableObject.h"

#include <limits>
#include "AbyssMesh.h"
#include "AbyssMaterial.h"
#include "AbyssRenderer.h"
#include "AbyssBoundingBox.h"
#include "AbyssSceneObject.h"
#include "AbyssBoundingBox.h"
#include "AbyssRenderQueue.h"

#include "AbyssSimpleRenderableObject.h"
#include "AbyssPhongShadingRenderer.h"

#include "AbyssDeferredRenderer.h"
#include "AbyssSceneManager.h"
#include "AbyssRenderPassManager.h"
#include "AbyssRenderPass.h"

ABYSS_BEGIN_NAMESPACE
//----------------------------------------------------------------------------------------------
static const Vector4D BlackPickColor = Vector4D(0.f, 0.f, 0.f, 1.0f);
RenderableObject::PickSet RenderableObject::m_sReservedPickColors = PickSet(); // TODO: there should be initializer list

namespace
{
	void setupDefaultRenderPassess(RenderableObject* renderable)
	{
		auto renderPassPriority = DefaultRenderPasses::DEFERRED_RENDERING; // DefaultRenderPasses::PhongShadingPriority
		renderable->addRenderPass(SceneManager::instance()->getRenderPassManager()->getRenderPass(renderPassPriority));
	}
}

RenderableObject::RenderPassesSetupFunc RenderableObject::m_defaultRenderPassesInitializer = std::bind(&setupDefaultRenderPassess, std::placeholders::_1);

//----------------------------------------------------------------------------------------------
RenderableObject::RenderableObject(const String & name)
	: m_name(name)
	, m_renderer(nullptr)
	, m_isVisible(true)
	, m_boundSceneObject(nullptr)
	, m_isLightingEnabled(true)
	, m_isBoundingBoxEnabled(false)
	, m_isBoundingBoxDirty(true)
	, m_material(new Material())
{
	// 	Initialize pick renderers: when initializer list will be, this code should be removed.
	// TODO.
	if (m_sReservedPickColors.empty())
		m_sReservedPickColors.push_back(BlackPickColor);

	m_pickColor = generateNewPickColor();

	m_sReservedPickColors.push_back(m_pickColor);
}
//--------------------------------------------------------------------------------------------------
RenderableObject::~RenderableObject()
{
	auto iter = std::find(m_sReservedPickColors.begin(),
					      m_sReservedPickColors.end(),
						  m_pickColor);

	if (iter != m_sReservedPickColors.end())
		m_sReservedPickColors.erase(iter);
}
//-------------------------------------------------------------------------------------------------
Vector4D RenderableObject::generateNewPickColor()
{
	static const int cMaxByte = std::numeric_limits<unsigned char>::max();
	
	Vector4D newColor;

	// Computing new pick color.
	// TODO: rand should be removed with std::randomer:
	while (true)
	{
		int iR = rand() % cMaxByte;
		int iG = rand() % cMaxByte;
		int iB = rand() % cMaxByte;

		float r = float(iR) / (cMaxByte - 1);
		float g = float(iG) / (cMaxByte - 1);
		float b = float(iB) / (cMaxByte - 1);

		newColor = Vector4D(r, g, b, 1.f);

		auto iter = std::find(m_sReservedPickColors.begin(),
							  m_sReservedPickColors.end(),
							  newColor);

		// Our reserved pick colors has no such color:
		if (iter == m_sReservedPickColors.end())
			break;
	}

	return newColor;
}
//----------------------------------------------------------------------------------------------
void RenderableObject::setLightingEnabled(bool isEnabled)
{
	m_isLightingEnabled = isEnabled;
}
//----------------------------------------------------------------------------------------------
void RenderableObject::addRenderPass(RenderPassWeakPtr pass)
{
	assert(pass);
	m_renderPasses.insert(pass);
}
//----------------------------------------------------------------------------------------------
void RenderableObject::removeRenderPass(RenderPassWeakPtr pass)
{
	auto iter = m_renderPasses.find(pass);
	if (iter != m_renderPasses.end())
		m_renderPasses.erase(iter);
}
//----------------------------------------------------------------------------------------------
bool RenderableObject::hasRenderPass(RenderPassWeakPtr pass) const
{
	return m_renderPasses.find(pass) != m_renderPasses.end();
}
//----------------------------------------------------------------------------------------------
void RenderableObject::removeAllRenderPasses()
{
	m_renderPasses.clear();
}
//----------------------------------------------------------------------------------------------
void RenderableObject::setupDefaultRenderPasses()
{
	getDefaultRenderPassesInitializer()(this);
}
//----------------------------------------------------------------------------------------------
void RenderableObject::setDefaultRenderPassesInitializer(RenderPassesSetupFunc setupFunc)
{
	m_defaultRenderPassesInitializer = setupFunc;
}
//----------------------------------------------------------------------------------------------
RenderableObject::RenderPassesSetupFunc RenderableObject::getDefaultRenderPassesInitializer()
{
	return m_defaultRenderPassesInitializer;
}
//----------------------------------------------------------------------------------------------
void RenderableObject::setBoundingBoxEnabled(bool enable)
{
	if (enable == isBoundingBoxEnabled())
		return;

	m_isBoundingBoxEnabled = enable;

	if (isBoundingBoxEnabled())
		m_isBoundingBoxDirty = true;
}
//----------------------------------------------------------------------------------------------
void RenderableObject::bindTo(SceneObject * object)
{
	if (getBoundSceneObject() == object)
		return;

	m_boundSceneObject = object;

	if (m_bboxRenderable)
		m_bboxRenderable->bindTo(object);
}
//----------------------------------------------------------------------------------------------
void RenderableObject::prepareForRendering(RenderQueueVisitor* visitor)
{
	assert(!m_renderPasses.empty());

	// First, render object itself:
	for (auto pass : m_renderPasses)
	{
		assert(pass);
		visitor->visit(pass, this);
	}

	// Then, render debug object.
	// TODO: need revisit this shit again :)
	if (isBoundingBoxEnabled())
	{
		constructBoundingBox();

		if (m_bboxRenderable)
			m_bboxRenderable->prepareForRendering(visitor);
	}
}

void RenderableObject::constructBoundingBox()
{
	if (!m_isBoundingBoxDirty)
		return;

	AABB aabb = getAABB();

	if (!m_bboxRenderable)
	{
		m_bboxRenderable.reset(new SimpleRenderableObject(getName() + "_BoundingBox"));
		m_bboxRenderable->setGeometryObject(GeometryObjectPtr(new BoundingBox()));
		m_bboxRenderable->setupDefaultRenderPasses();
		m_bboxRenderable->bindTo(getBoundSceneObject());
	}

	// TODO: need better abstraction:
	if (auto bbox = dynamic_cast<BoundingBox*>(m_bboxRenderable->getGeometryObject().get()))
	{
		bbox->setAABB(aabb);
		m_isBoundingBoxDirty = false;
	}
	else
		ABYSS_LOG("RenderableObject::render: invalid BoundingBox object!")
}

//----------------------------------------------------------------------------------------------


ABYSS_END_NAMESPACE