#pragma once

#include "Abyss.h"
#include <memory>

ABYSS_BEGIN_NAMESPACE

template <class PointerType>
class WeakPtr final
{
public:
	WeakPtr() = default;
	WeakPtr(const std::weak_ptr<PointerType>& weakPtr);
	WeakPtr(const std::shared_ptr<PointerType>& sharedPtr);

	operator PointerType*() const;
	PointerType* operator->() const;
	
	PointerType* get() const;

private:
	std::weak_ptr<PointerType> m_weakPtr;
};

// TEMPLATE METHODS IMPLEMENTATION
template <class PointerType>
WeakPtr<PointerType>::WeakPtr(const std::weak_ptr<PointerType>& weakPtr)
	: m_weakPtr(weakPtr)
{	}

template <class PointerType>
WeakPtr<PointerType>::WeakPtr(const std::shared_ptr<PointerType>& sharedPtr)
	: m_weakPtr(sharedPtr)
{	}

template <class PointerType>
PointerType* WeakPtr<PointerType>::operator->() const
{	return get();	}

template <class PointerType>
PointerType* WeakPtr<PointerType>::get() const
{	return m_weakPtr.lock().get();	}

template <class PointerType>
WeakPtr<PointerType>::operator PointerType*() const
{	return get();	}



ABYSS_END_NAMESPACE