#include "AbyssDefaultCameraController.h"

#include "AbyssCameraMoveEventAction.h"
#include "AbyssCameraFreeFlyEventAction.h"

ABYSS_BEGIN_NAMESPACE

//--------------------------------------------------------------------------------------------------
DefaultCameraController::DefaultCameraController(CameraPtr camera)
	: m_camera(camera)
	, m_keyEventAction(new CameraMoveEventAction(camera))
	, m_mouseEventAction(new CameraFreeFlyEventAction(camera))
{

}
//--------------------------------------------------------------------------------------------------
DefaultCameraController::~DefaultCameraController()
{
	delete m_keyEventAction;
	delete m_mouseEventAction;
}
//--------------------------------------------------------------------------------------------------
void DefaultCameraController::mouseMoveEvent(const MouseEventPtr & event)
{
	m_mouseEventAction->perform(event);
}
//--------------------------------------------------------------------------------------------------
void DefaultCameraController::keyPressEvent(const Abyss::KeyEventPtr & event)
{
	m_keyEventAction->perform(event);
}
//--------------------------------------------------------------------------------------------------
void DefaultCameraController::resetKeyEventAction(KeyEventAction * eventAction)
{
	if (m_keyEventAction == eventAction)
		return;

	delete m_keyEventAction;
	m_keyEventAction = eventAction;
}
//--------------------------------------------------------------------------------------------------
void DefaultCameraController::resetMouseEventAction(MouseEventAction * eventAction)
{
	if (m_mouseEventAction == eventAction)
		return;

	delete m_mouseEventAction;
	m_mouseEventAction = eventAction;
}
//--------------------------------------------------------------------------------------------------

ABYSS_END_NAMESPACE